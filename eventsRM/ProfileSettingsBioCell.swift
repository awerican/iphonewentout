//
//  ProfileSettingsBioCell.swift
//  WentOut
//
//  Created by Fure on 22.09.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

class ProfileSettingsBioCell: UICollectionViewCell {
    
    var currentSize: CGSize {
        get {
            frame.size.width = UIScreen.width
            self.layoutIfNeeded()
            return CGSize.init(width: UIScreen.width, height: bottomLine.frame.maxY)
        }
    }
    
    @IBOutlet weak var bottomLine: UIView!
    
    @IBOutlet weak var bioLabel: UILabel!
    
    func configCell(bio: String) {
        if bio.count == 0 {
            bioLabel.text = " "
        } else {
            bioLabel.text = bio
        }
    }
}









