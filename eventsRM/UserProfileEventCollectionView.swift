//
//  UserProfileEventCollectionView.swift
//  WentOut
//
//  Created by Fure on 27.07.2018.
//  Copyright © 2018 uzuner. All rights reserved.
// 

import UIKit

protocol UserProfileEventCollectionViewDelegate: class {
    func userAvatarWasTapped(indexPath: IndexPath)
    func usernameWasTapped(indexPath: IndexPath)
    func eventAvatarWasTapped(indexPath: IndexPath)
}

class UserProfileEventCollectionView: UICollectionViewCell {

    @IBOutlet var basisView: UIView!
    @IBOutlet var userAvatar: UIImageView!
    @IBOutlet var eventAvatar: UIImageView!
//    @IBOutlet var buttonLeft: UIButton!
//    @IBOutlet var buttonRight: UIButton!
    @IBOutlet var userNickname: UILabel!
    @IBOutlet var eventTitle: UILabel!
    @IBOutlet var eventTime: UILabel!
    @IBOutlet var location: UILabel!
    @IBOutlet var guests: UILabel!
    @IBOutlet weak var groundView: UIView!
    
    weak var delegate: UserProfileEventCollectionViewDelegate?
    
    lazy var userAvatarGesture: UITapGestureRecognizer = {
        return UITapGestureRecognizer(target: self, action: #selector(userAvatarWasTapped(_:)))
    }()
    
    lazy var eventAvatarGesture: UITapGestureRecognizer = {
        return UITapGestureRecognizer(target: self, action: #selector(eventAvatarWasTapped(_:)))
    }()
    
    lazy var usernameGesture: UITapGestureRecognizer = {
        return UITapGestureRecognizer(target: self, action: #selector(usernameWasTapped(_:)))
    }()
    
    var currentSize: CGSize {
        get {
            self.frame.size.width = UIScreen.width
            self.layoutIfNeeded()
            
            return self.systemLayoutSizeFitting(CGSize.init(width: UIScreen.main.bounds.width, height: 1000), withHorizontalFittingPriority: UILayoutPriority.init(rawValue: 1000.0), verticalFittingPriority: .defaultLow)
        }
    }
    
    func config(username: String, eventTitle: String, time: Date, location: EventLocation, guests: Int, eventImage: String, userImage: String) {
        self.userNickname.text = getUsernameText(username)
        self.eventTitle.text = eventTitle
        self.eventTime.text = time.timeDifferenceOnMap
        self.location.text = location.city + ", " + location.country
        self.guests.text = getGuestText(guests)
        Webservice.loadImage(url: eventImage) { (_image) in
            self.eventAvatar.image = _image
        }
        Webservice.loadImage(url: userImage) { (_image) in
            self.userAvatar.image = _image
        }
        userAvatar.isUserInteractionEnabled = true
        userNickname.isUserInteractionEnabled = true
        eventAvatar.isUserInteractionEnabled = true
        userAvatar.addGestureRecognizer(userAvatarGesture)
        userNickname.addGestureRecognizer(usernameGesture)
        eventAvatar.addGestureRecognizer(eventAvatarGesture)
    }
    
    func config(username: String, eventTitle: String, time: Date, location: EventLocation, guests: Int) {
        self.userNickname.text = getUsernameText(username)
        self.eventTitle.text = eventTitle
        self.eventTime.text = time.timeDifferenceOnMap
        self.location.text = location.city + ", " + location.country
        self.guests.text = getGuestText(guests)
    }
    
    func configImages(eventAvatar: UIImage) {
        self.eventAvatar.image = eventAvatar
    }
    
    func configImages(userAvatar: UIImage) {
        self.userAvatar.image = userAvatar
    }
    
    func getGuestText(_ guestNumber: Int) -> String {
        if guestNumber > 1 {
            return guestNumber.description + " guests"
        } else {
            return guestNumber.description + " guest"
        }
    }
    func getUsernameText(_ username: String) -> String {
        return "@"+username
    }
    
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        self.layer.shadowColor = UIColor.lightGray.cgColor
//        self.layer.shadowOffset = CGSize.zero
//        self.layer.shadowRadius = 8.0
//        self.layer.shadowOpacity = 0.3
//        self.layer.masksToBounds = false
//        let bounds = CGRect.init(x: self.basisView.frame.origin.x + 0, y: self.basisView.frame.origin.y + 2, width: self.basisView.frame.width, height: self.basisView.frame.height)
//        self.layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: 15).cgPath
//    }
    
    override func draw(_ rect: CGRect) {
        basisView.makeOval(cornerRadius: 15)
        userAvatar.makeCircle()
        eventAvatar.makeOval(cornerRadius: eventAvatar.frame.height / 5)
    }
}

extension UserProfileEventCollectionView {
    @objc func userAvatarWasTapped(_ gesture: UIGestureRecognizer) {
        let cv = self.superview as! UICollectionView
        let indexPath = cv.indexPath(for: self)!
        self.delegate?.userAvatarWasTapped(indexPath: indexPath)
    }
    @objc func usernameWasTapped(_ gesture: UIGestureRecognizer) {
        let cv = self.superview as! UICollectionView
        let indexPath = cv.indexPath(for: self)!
        self.delegate?.usernameWasTapped(indexPath: indexPath)
    }
    @objc func eventAvatarWasTapped(_ gesture: UIGestureRecognizer) {
        let cv = self.superview as! UICollectionView
        let indexPath = cv.indexPath(for: self)!
        self.delegate?.eventAvatarWasTapped(indexPath: indexPath)
    }
}
