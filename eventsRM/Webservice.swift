//
//  WebService.swift
//  eventsRM
//
//  Created by Fure on 14.11.17.
//  Copyright © 2017 uzuner. All rights reserved.
//

import Foundation
import Alamofire
import UIKit
import MapKit

typealias JSONDictionary = [String: AnyObject]

final class Webservice {
    private var currentTasks: [URLRequest: URLSessionDataTask] = [:]
    
    private var cancellingAllTasks: Bool = false
    
    private var requestBeginTime: [URLRequest: DispatchTime] = [:]
    
    private static var imageCache = NSCache<NSURL, UIImage>()
    
    func cancelAllTasks() {
        print("currentTasks number =", currentTasks.count)
        // undefined outcome if dict count is changed
        for task in self.currentTasks {
            task.value.cancel()
        }
        self.currentTasks.removeAll()
    }
    
    func getCurrentTasksNumber() -> Int {
        return self.currentTasks.count
    }
    
    func load<A: Decodable, T: Encodable>(auth: AuthHeader, resource: Resource<A, T>, completion: ((A?, Error?)->())? = nil) {
        let request = self.makeRequest(resource: resource, authHeaders: auth)
        self.makeTask(with: request) { (_data, _error) in
            guard let data = _data else {
                completion?(nil, _error)
                return
            }
            completion?(resource.parse(data), nil)
        }
    }
    
    func load<A: Decodable, T: Encodable>(resource: Resource<A, T>, completion: ((A?, Error?)->())? = nil) {
        let request = self.makeRequest(resource: resource)
        self.makeTask(with: request) { (_data, _error) in
            guard let data = _data else {
                completion?(nil, _error)
                return
            }
            completion?(resource.parse(data), nil)
//            self.printResponseJSON(data: data)
        }
    }
    
    private func printResponseJSON(data: Data) {
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: [])
            if let dict = json as? [String: Any] {
                print(dict)
            } else {
                print("RESPONSE GOT NO DATA")
            }
        } catch {
            print("RESPONSE WAS NOT ABLE TO BE PARSED")
        }
    }
    
    private func makeTask(with request: URLRequest, completion: ((Data?, Error?)->())? = nil) {
        self.requestBeginTime[request] = DispatchTime.now()
        self.taskDidStart(url: request.url, reqHash: request.hashValue)
        let task = URLSession.shared.dataTask(with: request) { (_data, _response, _error) in
            DispatchQueue.main.async {
                self.removeTask(for: request)
                self.taskDidFinish(url: request.url, reqHash: request.hashValue, reqStartTime: self.requestBeginTime[request])
                completion?(_data, _error)
            }
        }
        self.addTask(task: task, for: request)
        task.resume()
    }
    
    private func makeRequest<A: Decodable, T: Encodable>(resource: Resource<A, T>, httpMethod: HTTPMethod? = .post, authHeaders: [String: String]? = nil) -> URLRequest {
        var request = URLRequest.init(url: resource.url)
        request.httpMethod = httpMethod!.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        if let authHeaders = authHeaders {
            for header in authHeaders {
                request.setValue(header.value, forHTTPHeaderField: header.key)
            }
        }
        
        print(" req headers: =", request.allHTTPHeaderFields)
        
        if let params = resource.params {
            if let jsonData = try? JSONEncoder().encode(params) {
                request.httpBody = jsonData
            }
        }
        
        return request
    }

    init() {}
}

extension Webservice {
    func taskDidStart(url: URL?, reqHash: Int) {
        taskDidStartPrint(url: url?.absoluteString, reqHash: reqHash.description)
    }
    
    func taskDidFinish(url: URL?, reqHash: Int, reqStartTime: DispatchTime?) {
        if let startTime = reqStartTime {
            let elapsedNanoSec = DispatchTime.now().uptimeNanoseconds - startTime.uptimeNanoseconds
            let elapsedMiliSec = elapsedNanoSec / (1000 * 1000)
            let elapsedSec = elapsedNanoSec / (1000 * 1000 * 1000)
            taskDidFinishPrint(url: url?.absoluteString, reqHash: reqHash.description, elapsedSec: elapsedSec.description, elapsedMiliSec: elapsedMiliSec.description)
        } else {
            taskDidFinishPrint(url: nil, reqHash: reqHash.description, elapsedSec: nil, elapsedMiliSec: nil)
        }
    }
}

extension Webservice {
    private func removeTask(for key: URLRequest) {
        removeTaskPrint(url: key.url?.absoluteString)
        self.currentTasks.removeValue(forKey: key)
    }
    
    private func addTask(task: URLSessionDataTask, for key: URLRequest) {
        addTaskPrint(url: key.url?.absoluteString)
        self.currentTasks[key] = task
    }
}

extension Webservice {

    static func loadImage(url: String, completion: @escaping (UIImage?)->()) {
        guard let url = URL.init(string: url) else {
            completion(nil)
            return
        }
        
        if let image = self.imageCache.object(forKey: url as NSURL) {
//            print("taking from imageCache")
//            print("54326", url)
            completion(image)
            return
        }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            DispatchQueue.main.async {
                guard let data = data else {
                    completion(nil)
                        return
                    }
                switch url.pathExtension {
                    case "gif":
                        let image = UIImage.gifImageWithData(data: data as NSData)
                        if image != nil {
                            self.imageCache.setObject(image!, forKey: url as NSURL)
                        }
                        completion(image)
                    default:
                        let image = UIImage.init(data: data)
                        if image != nil {
                            self.imageCache.setObject(image!, forKey: url as NSURL)
                        }
                        completion(image)
                }
            }
        }.resume()
    }
    
    // load an image only if it is not in cache
//    static func loadImage(url: String, completion: @escaping (UIImage?)->()) {
//        guard let url = URL.init(string: url) else {
//            completion(nil)
//            return
//        }
//
//        if let image = self.imageCache.object(forKey: url as NSURL) {
//            print("taking from imageCache")
//            print("54326", url)
//            completion(image)
//            return
//        }
//
//        getImageFromLocalStorage(imageURL: encodeURLToNotURL(urlString: url.absoluteString)) { (_image) in
//            if let image = _image {
//                self.imageCache.setObject(image, forKey: url as NSURL)
//                completion(image)
//            } else {
//                URLSession.shared.dataTask(with: url) { (data, response, error) in
//                    DispatchQueue.main.async {
//                        guard let data = data else {
//                            completion(nil)
//                            return
//                        }
//                        switch url.pathExtension {
//                        case "gif":
//                            let image = UIImage.gifImageWithData(data: data as NSData)
//                            if image != nil {
//                                saveImageToLocalStorage(image: image!, imageURL: encodeURLToNotURL(urlString: url.absoluteString), completion: { (finish) in
//                                    print(finish)
//                                })
//                                self.imageCache.setObject(image!, forKey: url as NSURL)
//                            }
//                            completion(image)
//                        default:
//                            let image = UIImage.init(data: data)
//                            if image != nil {
//                                saveImageToLocalStorage(image: image!, imageURL: encodeURLToNotURL(urlString: url.absoluteString), completion: { (finish) in
//                                    print(finish)
//                                })
//                                self.imageCache.setObject(image!, forKey: url as NSURL)
//                            }
//                            completion(image)
//                        }
//                    }
//                }.resume()
//            }
//        }
//    }
}

struct UploadMemoryFileRes {
    var filename: String
    var mimeType: String
}

extension Webservice {
    
    static func uploadImageProcess(completion: ((Int)->())? = nil) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            completion?(30)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                completion?(50)
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    completion?(99)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        completion?(100)
                    }
                }
            }
        }
    }
    
    static func uploadMemory(payload: Uploadable, auth: AuthHeader = [String: String](), uploadProcess: ((_ process: Double?, _ res: UploadMemoryFileRes?)->())?) {
        
        let url = URL.uploadMemory
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(payload.data, withName: "media", fileName: "media", mimeType: payload.mimeType)
        }, to: url, method: .post, headers: auth) { (result) in
            switch result {
            case .success(request: let _upload, streamingFromDisk: _, streamFileURL: _):
                print("fractionCompleted =", _upload.uploadProgress.fractionCompleted)
                _upload.uploadProgress(closure: { (_progress) in
                    print("completed on: ", _progress.fractionCompleted)
                })
                _upload.responseJSON(completionHandler: { (_response) in
                    if let json = _response.result.value as? [String: Any] {
                        print("imageJSON =", json)
                        if let object = json["data"] as? [String: String] {
                            if let filename = object["filename"], let mimeType = object["mimetype"] {
                                uploadProcess?(nil, UploadMemoryFileRes(filename: filename, mimeType: mimeType))
                            }
                        }
                    }
                })
            case .failure(let _error):
                print(_error)
            }
        }
    }
    
    static func uploadChatImages(media: [(data: Data, mimetype: Mimetype)], params: [String: String] = [String: String](), uploadProcess: ((_ process: Double?, _ filenames: [(index: Int, filename: String)]?, _ error: String?)->())?) {
        
        let url = URL.uploadFile
        let parameters = params

        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (index, media) in media.enumerated() {
                let filename = "name" + index.description
                let mimeType = media.mimetype.rawValue
                print(mimeType)
                multipartFormData.append(media.data, withName: "media", fileName: filename, mimeType: mimeType)
            }
            // should contain some passwords and shit
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: .utf8)!, withName: key)
            }
        }, to: url) { (result) in
            switch result {
            case .success(request: let _upload, streamingFromDisk: _, streamFileURL: _):
                
                print("fractionCompleted =", _upload.uploadProgress.fractionCompleted)
                _upload.uploadProgress(closure: { (_progress) in
                    print("completed on: ", _progress.fractionCompleted)
                    if _progress.fractionCompleted == 1.0 {
                        print("images was uploaded")
                        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0, execute: {
                            print("progress is finished? =", _upload.progress.isFinished)
                            if !_upload.progress.isFinished {
                                _upload.cancel()
                            }
                        })
                    }
                })
                _upload.responseJSON(completionHandler: { (_response) in
                    print("request time duration =", _response.timeline.totalDuration)
                    if let error = _response.error {
                        print("FUCK ERROR", error.localizedDescription)
                        uploadProcess?(nil, nil, error.localizedDescription)
                    }
                    if let json = _response.result.value as? [String: Any] {
                        if let filenames = json["data"] as? [[String: Any]] {
                            var medias: [(index: Int, filename: String)] = []
                            for filename in filenames {
                                let index = filename["index"] as! Int
                                let name = filename["filename"] as! String
                                let media = (index: index, filename: name)
                                medias.append(media)
                            }
                            uploadProcess?(nil, medias, nil)
                        }
                    } else {
                        print("I GOT ERROR")
                    }
                })
            case .failure(let _error):
                print(_error.localizedDescription)
            }
        }
    }
    
    static func uploadAvatar(avatar: Uploadable, auth: AuthHeader = [String: String](), uploadProcess: ((_ process: Double?, _ url: String?)->())?) {
        let avatarData = avatar.data
        let url = URL.uploadEventAvatar
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(avatarData, withName: "media", fileName: "media", mimeType: avatar.mimeType)
        }, to: url, method: .post, headers: auth) { (result) in
            switch result {
            case .success(request: let _upload, streamingFromDisk: _, streamFileURL: _):
                print("fractionCompleted =", _upload.uploadProgress.fractionCompleted)
                _upload.uploadProgress(closure: { (_progress) in
                    print("completed on: ", _progress.fractionCompleted)
                })
                _upload.responseJSON(completionHandler: { (_response) in
                    if let json = _response.result.value as? [String: Any] {
                        print("imageJSON =", json)
                        if let filename = json["data"] as? String {
                            uploadProcess?(nil, filename)
                        }
                    }
                })
            case .failure(let _error):
                print(_error)
            }
        }
    }
    
    static func uploadImage(img: UIImage, params: [String: String] = [String: String](), uploadProcess: ((_ process: Double?, _ url: String?)->())?) {
        let imageData = UIImageJPEGRepresentation(img, 0.75)!
        let url = URL.uploadFile
        let parameters = params
        
        let bcf = ByteCountFormatter()
        bcf.allowedUnits = [.useMB, .useKB] // optional: restricts the units to MB only
        bcf.countStyle = .file
        let string = bcf.string(fromByteCount: Int64(imageData.count))
        print("image size: \(string)")
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(imageData, withName: "image", fileName: "image", mimeType: "image/jpeg")
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: .utf8)!, withName: key)
            }
        }, to: url) { (result) in
            switch result {
            case .success(request: let _upload, streamingFromDisk: _, streamFileURL: _):
                print("fractionCompleted =", _upload.uploadProgress.fractionCompleted)
                _upload.uploadProgress(closure: { (_progress) in
                    print("completed on: ", _progress.fractionCompleted)
                })
                _upload.responseJSON(completionHandler: { (_response) in
                    if let json = _response.result.value as? [String: Any] {
                        if let filename = json["data"] as? String {
                            uploadProcess?(nil, filename)
                        }
                    }
                })
            case .failure(let _error):
                print(_error)
            }
        }
    }
}

extension Webservice {
    var terminalTag: String {
        get {
            return "WB: "
        }
    }
    
    private func removeTaskPrint(url: String?) {
        print("\n")
        print(terminalTag, "***********")
        print(terminalTag, "* Remove task with URL =", url ?? "nil")
        print(terminalTag, "***********")
        print("\n")
    }
    
    private func addTaskPrint(url: String?) {
        print("\n")
        print(terminalTag, "***********")
        print(terminalTag, "* Add task with URL =", url ?? "nil")
        print(terminalTag, "***********")
        print("\n")
    }
    
    private func taskDidStartPrint(url: String?, reqHash: String) {
        print("\n")
        print(terminalTag, "***********")
        print(terminalTag, "* REQ STARTED.")
        print(terminalTag, "* REQ-URL =", url ?? "* nil")
        print(terminalTag, "* REQ-HASH =", reqHash)
        print(terminalTag, "***********")
        print("\n")
    }
    
    private func taskDidFinishPrint(url: String?, reqHash: String, elapsedSec: String?, elapsedMiliSec: String?) {
        print("\n")
        print(terminalTag, "***********")
        print(terminalTag, "* REQ ENDED.")
        print(terminalTag, "* REQ-URL =", url ?? "* nil")
        print(terminalTag, "* REQ-HASH =", reqHash)
        print(terminalTag, "* REQ-ELTIME: sec =", elapsedSec ?? "nil", ", miliSec =", elapsedMiliSec ?? "nil")
        print(terminalTag, "***********")
        print("\n")
    }
}















