//
//  ChatLeftMsgMoreImageTextGrouppedCell.swift
//  WentOut
//
//  Created by Fure on 15.10.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import UIKit

class ChatLeftMsgMoreImageTextGrouppedCell: ChatMessageBaseCell {
    
    @IBOutlet var avatarOutlet: UIImageView!
    @IBOutlet var messageOutlet: UILabel!
    @IBOutlet var mediaCollectionOutlet: [UIImageView]!
    @IBOutlet var timeOutlet: UILabel!
    @IBOutlet var messageBackgroundOutlet: UIView!
    
    @IBOutlet var imageHiddenQuantityBlurViewOutlet: UIVisualEffectView!
    @IBOutlet var imageHiddenQuantityOutlet: UILabel!
    
    @IBOutlet var timeLeadingConstrOutlet: NSLayoutConstraint!
    @IBOutlet var timeTopConstrOutlet: NSLayoutConstraint!
    
    override var avatar: UIImageView! {
        return self.avatarOutlet
    }
    override var message: UILabel? {
        return self.messageOutlet
    }
    override var mediaCollection: [UIImageView]? {
        return self.mediaCollectionOutlet
    }
    override var time: UILabel! {
        return self.timeOutlet
    }
    override var messageBackground: UIView! {
        return self.messageBackgroundOutlet
    }
    override var imageHiddenQuantityBlurView: UIVisualEffectView? {
        return self.imageHiddenQuantityBlurViewOutlet
    }
    override var imageHiddenQuantity: UILabel? {
        return self.imageHiddenQuantityOutlet
    }
    override var timeLeadingConstr: NSLayoutConstraint? {
        return self.timeLeadingConstrOutlet
    }
    override var timeTopConstr: NSLayoutConstraint? {
        return self.timeTopConstrOutlet
    }
}
