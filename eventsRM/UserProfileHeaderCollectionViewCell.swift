//
//  UserProfileHeaderCollectionViewCell.swift
//  WentOut
//
//  Created by Fure on 25.07.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import UIKit

enum UserProfileHeaderState {
    case owner, otherUser(isFollowed: Bool)
}

protocol UserProfileHeaderCollectionViewDelegate: class {
    func followersWasTapped()
    func followingsWasTapped()
    func eventsWasTapped()
}

class UserProfileHeaderCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var basisView: UIView!
    @IBOutlet weak var userAvatar: UIImageView!
    @IBOutlet weak var followersStackView: UIStackView!
    @IBOutlet weak var followersTitle: UILabel!
    @IBOutlet weak var followers: UILabel!
    @IBOutlet weak var followingStackView: UIStackView!
    @IBOutlet weak var followingLabel: UILabel!
    @IBOutlet weak var following: UILabel!
    @IBOutlet weak var events: UILabel!
    @IBOutlet weak var guests: UILabel!
    @IBOutlet weak var userDescription: UILabel!
//    @IBOutlet weak var realnameLabel: UILabel!
    
    @IBOutlet weak var eventsTitleLabel: UILabel!
    
    let followButtonTag: Int = 57432
    
    var state: UserProfileHeaderState = .owner
    
    weak var delegate: UserProfileHeaderCollectionViewDelegate?
    
    var currentSize: CGSize {
        get {
            self.frame.size.width = UIScreen.main.bounds.width
            self.layoutIfNeeded()
            let size = CGSize.init(width: UIScreen.main.bounds.width, height: self.basisView.frame.maxY)
            print("height shit = ", size.height)
            return size
        }
    }
    
    override func draw(_ rect: CGRect) {
        setupViews()
    }

    func configCell(username: String, followers: Int, following: Int, events: Int, guests: Int, userDescription: String, userAvatar: String, state: UserProfileHeaderState) {
        self.followers.text = followers.description
        self.following.text = following.description
        self.events.text = events.description
        self.guests.text = guests.description
        self.userDescription.text = userDescription
        self.userAvatar.moa.url = userAvatar
        
        let followersGesture = UITapGestureRecognizer.init(target: self, action: #selector(followersWasTapped))
        self.followersStackView.isUserInteractionEnabled = true
        self.followersStackView.addGestureRecognizer(followersGesture)
        
        let followingsGesture = UITapGestureRecognizer.init(target: self, action: #selector(followingsWasTapped))
        self.followingStackView.isUserInteractionEnabled = true
        self.followingStackView.addGestureRecognizer(followingsGesture)
        
        self.events.isUserInteractionEnabled = true
        let eventsGesture = UITapGestureRecognizer.init(target: self, action: #selector(eventsWasTapped))
        self.events.addGestureRecognizer(eventsGesture)
        
        self.eventsTitleLabel.isUserInteractionEnabled = true
        let eventsTitleGesture = UITapGestureRecognizer.init(target: self, action: #selector(eventsWasTapped))
        self.eventsTitleLabel.addGestureRecognizer(eventsTitleGesture)
    }

    func bundleConfigCell(username: String, followers: Int, following: Int, events: Int, guests: Int, userDescription: String) {
        self.followers.text = followers.description
        self.following.text = following.description
        self.events.text = events.description
        self.guests.text = guests.description
        self.userDescription.text = userDescription
    }
    
    func configUserAvatar(_ image: UIImage) {
        self.userAvatar.image = image
    }
    
    private func setupViews() {
        self.userAvatar.makeCircle()
    }
}

extension UserProfileHeaderCollectionViewCell {
    @objc func followersWasTapped() {
        delegate?.followersWasTapped()
    }
    
    @objc func followingsWasTapped() {
        delegate?.followingsWasTapped()
    }
    
    @objc func eventsWasTapped() {
        delegate?.eventsWasTapped()
    }
}












