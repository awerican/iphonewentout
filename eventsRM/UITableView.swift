//
//  UITableView.swift
//  WentOut
//
//  Created by Fure on 27.10.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    
    var lastIndexPath: IndexPath {
        get {
            let lastSectionIndex = max(self.numberOfSections - 1, 0)
            let lastRowIndex = max(self.numberOfRows(inSection: lastSectionIndex) - 1, 0)
            return IndexPath.init(row: lastRowIndex, section: lastSectionIndex)
        }
    }
}
