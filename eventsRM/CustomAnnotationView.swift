//
//  CustomAnnotationView.swift
//  eventsRM
//
//  Created by Fure on 25.07.17.
//  Copyright © 2017 uzuner. All rights reserved.
//

import UIKit
import Mapbox
import Foundation

protocol AnnotationSelectable {
    func willSelect(annotationView: CustomAnnotationView)
}

class CustomAnnotationView: MGLAnnotationView {
    
    static let annotationsSize: CGSize = CGSize.init(width: 55.0, height: 55.0)
    
    var imageView: UIImageView = {
        let view = UIImageView()
        view.clipsToBounds = true
        view.contentMode = UIViewContentMode.scaleAspectFill
        view.layer.borderColor = UIColor.red.cgColor
        view.layer.cornerRadius = annotationsSize.width / 4.5
        view.backgroundColor = .clear
        return view
    }()
    
//    var image: UIImage?
    
    var delegate: AnnotationSelectable?
    
    var touchWasMoved: Bool = false
    
    static let imageBorderWidth = CGFloat(2.0)
    static let imageViewSize = CGSize.init(width: annotationsSize.width - 2 * (imageBorderWidth), height: annotationsSize.height - 2 * (imageBorderWidth))
    
    init(reuseIdentifier: String?, imageURL: String, delegate: AnnotationSelectable) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        Webservice.loadImage(url: imageURL) { (_image) in
            if let image = _image {
                self.imageView.image = image
            }
        }
        
        self.delegate = delegate
        self.backgroundColor = UIColor.white
        self.layer.cornerRadius = CustomAnnotationView.annotationsSize.width / 4.5
        self.clipsToBounds = true
        
        self.addSubview(imageView)
        
        imageView.frame.origin = CGPoint.init(x: CustomAnnotationView.imageBorderWidth, y: CustomAnnotationView.imageBorderWidth)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
//            if !self.touchWasMoved {
//                self.delegate?.willSelect(annotationView: self)
//            }
//            self.touchWasMoved = false
//        }
        self.delegate?.willSelect(annotationView: self)
        
        UIView.animate(withDuration: 0.1, delay: 0.0, options: .curveEaseIn, animations: {
            [weak self] in
            self?.frame.size = CGSize.init(width: CustomAnnotationView.annotationsSize.width - (CustomAnnotationView.imageBorderWidth * 2), height: CustomAnnotationView.annotationsSize.height - (CustomAnnotationView.imageBorderWidth * 2))
            self?.frame.origin.x += CustomAnnotationView.imageBorderWidth
            self?.frame.origin.y += CustomAnnotationView.imageBorderWidth
            self?.imageView.frame.size = CGSize.init(width: CustomAnnotationView.imageViewSize.width - (CustomAnnotationView.imageBorderWidth * 2), height: CustomAnnotationView.imageViewSize.height - (CustomAnnotationView.imageBorderWidth * 2))
        }) { (finish) in
            UIView.animate(withDuration: 0.4, delay: 0.0, options: .curveEaseIn, animations: {
                [weak self] in
                self?.frame.size = CustomAnnotationView.annotationsSize
                self?.frame.origin.x -= CustomAnnotationView.imageBorderWidth
                self?.frame.origin.y -= CustomAnnotationView.imageBorderWidth
                self?.imageView.frame.size = CustomAnnotationView.imageViewSize
            })
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("#333 touches ENDED")
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("#333 touches MOVED")
        self.touchWasMoved = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var appearInProcess = false
    var disappearInProcess = false
    
    func animateToAppear() {
        if !appearInProcess && self.isHidden {
            print("ПОКАЗЫВАЮ")
            self.appearInProcess = true
            self.isHidden = false
            UIView.animate(withDuration: 0.35, animations: {
                [weak self] in
                self?.frame.size = CustomAnnotationView.annotationsSize
                self?.frame.origin = CGPoint.init(x: self!.frame.minX - (CustomAnnotationView.annotationsSize.width / 2), y: self!.frame.minY - (CustomAnnotationView.annotationsSize.width / 2))
                self?.imageView.frame.size = CustomAnnotationView.imageViewSize
                self?.imageView.frame.origin = CGPoint.init(x: CustomAnnotationView.imageBorderWidth, y: CustomAnnotationView.imageBorderWidth)
            }) { (finish) in
                if finish {
                    self.appearInProcess = false
                }
            }
        }
    }
    
    func animateToDisappear() {
        if !disappearInProcess && !self.isHidden {
            print("ПРЯЧУ")
            self.disappearInProcess = true
            UIView.animate(withDuration: 0.35, animations: {
                [weak self] in
                self?.frame.origin = CGPoint.init(x: self!.frame.midX, y: self!.frame.midY)
                self?.frame.size = CGSize.zero
                self?.imageView.frame.size = CGSize.zero
                self?.imageView.frame.origin = CGPoint.zero
            }) { (finish) in
                if finish {
                    self.disappearInProcess = false
                    self.isHidden = true
                }
            }
        }
    }
}
