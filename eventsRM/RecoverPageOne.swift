//
//  RecoverPageOne.swift
//  WentOut
//
//  Created by Fure on 29.07.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import UIKit

class RecoverPageOne: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var getTokenButton: UIButton!
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    @IBAction func sendAction(_ sender: UIButton) {
        guard let email = emailTextField.text else {
            return
        }
        
        if !EmailChecker.isValidEmail(email) {
            return
        }
        
        let param = RecoverOneParameters.init(email: email)
        
        webservice.load(resource: ResList.recoverOne(param)) { (_result, _error) in
            if let data = _result?.data {
                switch data.status {
                case .succeed:
                    self.segueToNext(email: email)
                case .failed:
                    print("email is not exist or some other reason")
                }
            } else {
                print("ERROR: could not init")
            }
            if let error = _error {
                print("REQ ERROR: ", error)
            }
        }
    }
}

extension RecoverPageOne {
    private func setupView() {
        getTokenButton.makeOval(cornerRadius: getTokenButton.frame.height / 5)
        navigationItem.title = "Recovery"
    }
    
    private func segueToNext(email: String) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "RecoverPageTwo") as! RecoverPageTwo
        controller.email = email
        self.navigationController?.pushViewController(controller, animated: true)
    }
}













