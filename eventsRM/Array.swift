//
//  Array.swift
//  WentOut
//
//  Created by Fure on 09.06.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation

//extension Array where Element: ClassObject {
//    func someFunc() -> String {
//        return "bitch"
//    }
//}

//extension Array where Element == StructObject {
//    func someFunc() -> String {
//        return "bitch"
//    }
//}

extension Array where Element == FeedbackEntity {
    func getIDs() -> [String] {
        var ids: [String] = []
        for e in self {
            ids.append(e.id)
        }
        return ids
    }
}






