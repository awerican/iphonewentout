//
//  ReportSecViewControllerCell.swift
//  WentOut
//
//  Created by Fure on 21.10.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

protocol ReportSecViewControllerCellDelegate: class {
    func reportAction(sender: UIButton)
}

class ReportSecViewControllerCell: UICollectionViewCell {

    static var currentSize: CGSize {
        get {
            return CGSize.init(width: UIScreen.width, height: UIScreen.height)
        }
    }
    
    weak var delegate: ReportSecViewControllerCellDelegate?
    
    @IBOutlet weak var reasonLabel: UILabel!
    
    @IBAction func reportAction(_ sender: UIButton) {
        delegate?.reportAction(sender: sender)
    }
    
    func configCell(reportReason: String) {
        reasonLabel.text = "Report as " + reportReason.lowercased() + "?"
    }
}













