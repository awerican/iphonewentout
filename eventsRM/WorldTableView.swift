//
//  WorldTableView.swift
//  eventsRM
//
//  Created by Fure on 20.07.17.
//  Copyright © 2017 uzuner. All rights reserved.
//

import UIKit

//enum WorldTableViewState: Equatable {
//    case topEvents
//    case pickedEvent(HeaderTableViewCellSecondModel)
//    static func ==(lhs: WorldTableViewState, rhs: WorldTableViewState) -> Bool {
//        switch (lhs, rhs) {
//        case (.topEvents, .topEvents):
//            return true
//        default:
//            return false
//        }
//    }
//}

enum WorldTableviewStateNoData {
    case zeroData, error
}

enum WorldTableViewState: Equatable {
//    case isLoading
//    case noData(state: WorldTableviewStateNoData)
    case topEvents
    case pickedEvent(eventID: String)
    static func ==(lhs: WorldTableViewState, rhs: WorldTableViewState) -> Bool {
        switch (lhs, rhs) {
        case (.topEvents, .topEvents):
            return true
//        case (.isLoading, .isLoading):
//            return true
//        case (.noData(let lhsState), .noData(let rhsState)):
//            if lhsState == rhsState {
//                return true
//            } else {
//                return false
//            }
        case (.pickedEvent(let lhsEvent), .pickedEvent(let rhsEvent)):
            if lhsEvent == rhsEvent {
                return true
            } else {
                return false
            }
        default:
            return false
        }
    }
}

protocol WorldTableViewAnimatable {
    func willOpenTableView(in state: WorldTableViewState)
    func didOpenTableView(in state: WorldTableViewState)
    func willCloseTableView(in state: WorldTableViewState)
    func didCloseTableView(in state: WorldTableViewState)
    func longPressGestureHandler(_ gesture: UILongPressGestureRecognizer)
}

@objc protocol HeaderTableViewCellProtocol {
    @objc optional func viewGestureRecognizer(gesture: UILongPressGestureRecognizer)
}

class WorldTableView: UITableView, HeaderTableViewCellProtocol {

    static let headerHeight: CGFloat = 45
    
    private var _isOpen: Bool = false
    private var screenHeight = UIScreen.main.bounds.height
    private var originYValue: CGFloat!
    
    var state: WorldTableViewState = .topEvents
    var delegateAnimation: WorldTableViewAnimatable?
    
    var isOpen: Bool {
        get {
            return _isOpen
        }
        set {
            self._isOpen = newValue
        }
    }

    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        commonInit()
    }
    
    private func commonInit() {
        originYValue = frame.origin.y
        setupView()
    }
    
    private func setupView() {
        alwaysBounceVertical = true
        bounces = true
        backgroundColor = UIColor.clear
        separatorStyle = .none
        showsVerticalScrollIndicator = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    func animateToCloseTableView() {
        if isOpen == true {
            self.isScrollEnabled = false
            self.isOpen = false
            self.delegateAnimation?.willCloseTableView(in: self.state)
            UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseIn, animations: {
                self.frame.origin.y = UIScreen.main.bounds.height - 35 - WorldTableView.headerHeight
            }, completion: { (finish) in
                if finish == true {
                    self.isScrollEnabled = true
                    self.delegateAnimation?.didCloseTableView(in: self.state)
                }
            })
        }
    }
    
    func animateToOpenTableView() {
        if isOpen == false {
            self.isScrollEnabled = false
            self.isOpen = true
            self.delegateAnimation?.willOpenTableView(in: self.state)
            UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseIn, animations: {
                self.frame.origin.y = 20 + 20
            }, completion: { (finish) in
                if finish == true {
                    self.isScrollEnabled = true
                    self.delegateAnimation?.didOpenTableView(in: self.state)
                }
            })
        }
    }
}

extension WorldTableView {
    func viewGestureRecognizer(gesture: UILongPressGestureRecognizer) {
        if gesture.state == .began {
            if self.isOpen == false {
                animateToOpenTableView()
            } else {
                animateToCloseTableView()
            }
        }
    }
}

//extension WorldTableView {
//    func changeState(to state: WorldTableViewState) {
//        if self.state != state {
//            switch state {
//            case .topEvents:
//                self.state = .topEvents
//                self.reloadData()
//            case .pickedEvent(let eventID):
//                self.state = .pickedEvent(eventID: eventID)
//                self.reloadData()
//            }
//        }
//    }
//    func startLoading() {
//        if let headerView = self.headerView(forSection: 0) as? HeaderTableViewCell {
//            headerView.startLoading()
//        }
//    }
//    func stopLoading() {
//        if let headerView = self.headerView(forSection: 0) as? HeaderTableViewCell {
//            headerView.stopLoading()
//        }
//    }
//}
