//
//  CustomBarButtonItem.swift
//  WentOut
//
//  Created by Fure on 06.04.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import UIKit

extension UIBarButtonItem {

    static func getUserFollowBarButton(target: Any?, selector: Selector, for event: UIControlEvents = .touchUpInside) -> UIBarButtonItem {
        let size = CGSize(width: 27.0, height: 27.0)
        
        let view = UIView.init(frame: CGRect.init(origin: CGPoint.zero, size: size))
        
        let button = UIButton.init(type: .system)
        button.backgroundColor = .clear
        button.frame = view.frame
        button.setImage(#imageLiteral(resourceName: "followButton"), for: .normal)
        button.tintColor = .black
        button.autoresizesSubviews = true
        button.autoresizingMask = [.flexibleWidth , .flexibleHeight]
        button.addGestureRecognizer(UITapGestureRecognizer.init(target: target, action: selector))
        
        view.addSubview(button)
        
        let item = UIBarButtonItem.init(customView: view)
        
        return item
    }
    
    static func getUserUnfollowBarButton(target: Any?, selector: Selector, for event: UIControlEvents = .touchUpInside) -> UIBarButtonItem {
        let size = CGSize(width: 27.0, height: 27.0)
        
        let view = UIView.init(frame: CGRect.init(origin: CGPoint.zero, size: size))
        
        let button = UIButton.init(type: .system)
        button.backgroundColor = .clear
        button.frame = view.frame
        button.setImage(#imageLiteral(resourceName: "unfollowButton"), for: .normal)
        button.tintColor = .black
        button.autoresizesSubviews = true
        button.autoresizingMask = [.flexibleWidth , .flexibleHeight]
        button.addGestureRecognizer(UITapGestureRecognizer.init(target: target, action: selector))
        
        view.addSubview(button)
        
        let item = UIBarButtonItem.init(customView: view)
        
        return item
    }
    
    static func getSettingsBarButton(target: Any?, selector: Selector, for event: UIControlEvents = .touchUpInside) -> UIBarButtonItem {
        let size = CGSize(width: 30.0, height: 30.0)
        
        let view = UIView.init(frame: CGRect.init(origin: CGPoint.zero, size: size))
        
        let button = UIButton.init(type: .system)
        button.backgroundColor = .clear
        button.frame = view.frame
        button.setImage(#imageLiteral(resourceName: "buttonSettings"), for: .normal)
        button.tintColor = .black
        button.autoresizesSubviews = true
        button.autoresizingMask = [.flexibleWidth , .flexibleHeight]
        button.addTarget(target, action: selector, for: event)
        
        view.addSubview(button)
        
        return UIBarButtonItem.init(customView: view)
    }
    
    static func getChatBarButton(target: Any?, selector: Selector, for event: UIControlEvents = .touchUpInside) -> UIBarButtonItem {
        let size = CGSize(width: 30.0, height: 30.0)
        
        let view = UIView.init(frame: CGRect.init(origin: CGPoint.zero, size: size))
        
        let button = UIButton.init(type: .system)
        button.backgroundColor = .clear
        button.frame = view.frame
        button.setImage(#imageLiteral(resourceName: "chatButton"), for: .normal)
        button.tintColor = .black
        button.autoresizesSubviews = true
        button.autoresizingMask = [.flexibleWidth , .flexibleHeight]
        button.addTarget(target, action: selector, for: event)
        
        view.addSubview(button)
        
        return UIBarButtonItem.init(customView: view)
    }
}
