//
//  ActivityPageMemoryListCollectionViewCell.swift
//  WentOut
//
//  Created by Fure on 22.05.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

protocol ActivityPageMemoryListDelegate: class {
    func addMemory()
    func openStory(eventID: String, index: Int)
}

class ActivityPageMemoryListCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    
    weak var delegate: Downloader<[StoryEntity], UserIDParameter>?
    
    private var _superviewHeight: CGFloat?
    
    @IBOutlet weak var loadingView: UIView!
    
    weak var addMemoryDelegate: ActivityPageMemoryListDelegate?
    
    var superviewHeight: CGFloat {
        get {
            return self._superviewHeight ?? 0.0
        }
    }
    
    // otherwise activityIndicator will stop
    override func prepareForReuse() {}
    
    func configCell(superviewHeight: CGFloat) {
        self.backgroundColor = UIColor.init(hex: "FAFAFA")
        setupCollectionView()
        self._superviewHeight = superviewHeight
        if let del = self.delegate {
            if let _ = del.data {
                self.loadingView.alpha = 0
                self.collectionView.reloadSections([1])
            }
        }
    }
}

extension ActivityPageMemoryListCollectionViewCell {
    fileprivate func setupCollectionView() {
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        for reuseID in self.reuseIds {
            self.collectionView!.register(UINib.init(nibName: reuseID, bundle: nil), forCellWithReuseIdentifier: reuseID)
        }
    }
}

extension ActivityPageMemoryListCollectionViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    
    var reuseIds: [String] {
        get {
            return ["ActivityPageMemoryListAddNewCollectionViewCell", "ActivityPageMemoryListUnitCollectionViewCell"]
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0 && indexPath.section == 0 {
            self.addMemoryDelegate?.addMemory()
        }
        if indexPath.section == 1 {
            let eventID = self.delegate!.data![indexPath.row].eventID
            self.addMemoryDelegate?.openStory(eventID: eventID, index: indexPath.row)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let del = self.delegate {
            guard let memories = del.data else {
                return
            }
            // на этой строчке вылетало
            if indexPath.row == memories.count - 1 {
                del.getNext(section: 1) { (paths) in
                    self.collectionView.insertItems(at: paths)
                }
            }
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else if section == 1 {
            return self.delegate?.data?.count ?? 0
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIds[0], for: indexPath) as! ActivityPageMemoryListAddNewCollectionViewCell
            cell.configCell()
            return cell
        } else if indexPath.section == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIds[1], for: indexPath) as! ActivityPageMemoryListUnitCollectionViewCell
            if let del = self.delegate {
                let model = del.data![indexPath.row]
                cell.configCell(eventTitle: model.eventTitle, memory: model.memories.data[0])
            }
            return cell
        } else {
            return UICollectionViewCell()
        }
    }
}

extension ActivityPageMemoryListCollectionViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: (self.superviewHeight - 12 - 12) / 5 * 3.8, height: self.superviewHeight - 12 - 12)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if section == 0 {
            return UIEdgeInsetsMake(0, 7, 0, 0)
        } else if section == 1 {
            return UIEdgeInsetsMake(0, 0, 0, 14)
        } else {
            return UIEdgeInsets.zero
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
}













