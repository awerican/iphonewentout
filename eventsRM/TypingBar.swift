//
//  TypingBar.swift
//  WentOut
//
//  Created by Fure on 03.10.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import Foundation
import UIKit

@objc protocol TypingBarDelegate: class {
    @objc optional func textViewHeightChangeDifference(heightDiff: CGFloat)
    @objc optional func sendButtonWasPressed(text: String?)
    @objc optional func attachImage()
}

class TypingBar: UIView, Nibable {
    
    enum SendButtonState {
        case active, unactive
    }
    
    var sendButtonCurrentState: SendButtonState = .unactive
    
    var bundleName: String = "TypingBar"
    
    @IBOutlet var contentView: UIView!
    
    @IBOutlet var textView: UITextView!
    @IBOutlet var textViewBackground: UIView!
    @IBOutlet var textViewHeightConstr: NSLayoutConstraint!
    @IBOutlet var leftBttnBottomConstr: NSLayoutConstraint!
    @IBOutlet var textViewBottomConstr: NSLayoutConstraint!
    @IBOutlet var leftBttnHeightConstr: NSLayoutConstraint!
    
    weak var delegate: TypingBarDelegate?
    
    var keyboardIsShown: Bool = false
    
    @IBOutlet weak var sendButtonBackground: UIView!
    
    let activeSendButtonColor = UIColor.init(hex: "7699f8")
    let unactiveSendButtonColor = UIColor.white
    
    let activeSendButtonTextColor = UIColor.white
    let unactiveSendButtonTextColor = UIColor.black
    
    var attachmentImages: [UIImage] = []
    
    var currentTextViewHeight: CGFloat? {
        didSet {
            if let newValue = self.currentTextViewHeight, let previousValue = oldValue {
                if newValue != previousValue {
                    delegate?.textViewHeightChangeDifference?(heightDiff: newValue - previousValue)
                }
            }
        }
    }
    
    @IBOutlet weak var attachmentQuantity: UILabel!
    
    @IBOutlet weak var sendButton: UIButton!
    
    @IBAction func sendButtonAction(_ sender: UIButton) {
        if self.textView.text != "" || self.attachmentImages.count > 0 {
            self.delegate?.sendButtonWasPressed?(text: self.textView.text)
        }
        self.textView.text = ""
        self.switchSendButton(to: .unactive)
        
        // LOL
        let newSize = textView.sizeThatFits(CGSize(width: textView.frame.width, height: CGFloat.greatestFiniteMagnitude))
        print(newSize.height)
        self.textViewHeightConstr.constant = newSize.height
        self.currentTextViewHeight = newSize.height
        self.layoutIfNeeded()
        textView.setContentOffset(CGPoint.zero, animated: false)
    }
    
    @IBOutlet weak var attachImageButton: UIButton!
    
    @IBAction func attachImageAction(_ sender: Any) {
        self.delegate?.attachImage?()
    }
    
    func receiveImage(image: UIImage, callback: ((Bool) -> ())?) {
        self.attachmentImages.append(image)
        self.attachmentQuantity.text = self.attachmentImages.count.description
        if self.attachmentImages.count > 0 {
            self.switchSendButton(to: .active)
        }
        callback?(true)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.setupViewFromNib()
        self.textView.delegate = self
    }
    
    override func draw(_ rect: CGRect) {
        setupTextView()
        self.textViewBackground.makeOval(cornerRadius: 15.0)
        self.sendButtonBackground.makeCircle()
        setupSendButton(with: .unactive)
        self.attachmentQuantity.text = nil
    }
}

extension TypingBar {
    private func switchSendButton(to state: SendButtonState) {
        if state == self.sendButtonCurrentState {
            return
        } else {
            UIView.animate(withDuration: 0.4) {
                [weak self] in
                self?.setupSendButton(with: state)
            }
        }
    }
    
    private func setupSendButton(with state: SendButtonState) {
        switch state {
        case .active:
            self.sendButtonBackground.backgroundColor = activeSendButtonColor
            self.sendButton.setTitleColor(activeSendButtonTextColor, for: .normal)
            self.sendButton.titleLabel?.font =  UIFont.systemFont(ofSize: 12, weight: .bold)
            self.sendButtonCurrentState = .active
        case .unactive:
            self.sendButtonBackground.backgroundColor = unactiveSendButtonColor
            self.sendButton.titleLabel?.font =  UIFont.systemFont(ofSize: 12, weight: .medium)
            self.sendButton.setTitleColor(unactiveSendButtonTextColor, for: .normal)
            self.sendButtonCurrentState = .unactive
        }
    }
    
    private func setupTextView() {
        let newSize = textView.sizeThatFits(CGSize(width: textView.frame.width, height: CGFloat.greatestFiniteMagnitude))
        self.textViewHeightConstr.constant = newSize.height
        self.layoutIfNeeded()
        
        let diff = self.textViewHeightConstr.constant - self.leftBttnHeightConstr.constant
        let halfDiff = diff / 2
        
        if self.textViewHeightConstr.constant >= self.leftBttnHeightConstr.constant {
            self.leftBttnBottomConstr.constant = self.textViewBottomConstr.constant + halfDiff
        } else {
            self.leftBttnBottomConstr.constant = self.textViewBottomConstr.constant - halfDiff
        }
    }
    func resignKeyboard() {
        self.textView.resignFirstResponder()
    }
}

extension TypingBar: UITextViewDelegate {

    func textViewDidBeginEditing(_ textView: UITextView) {
        self.keyboardIsShown = true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.keyboardIsShown = false
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if textView.numberOfLines() <= 7 {
            let newSize = textView.sizeThatFits(CGSize(width: textView.frame.width, height: CGFloat.greatestFiniteMagnitude))
            print(newSize.height)
            self.textViewHeightConstr.constant = newSize.height
            self.currentTextViewHeight = newSize.height
            self.layoutIfNeeded()
            textView.setContentOffset(CGPoint.zero, animated: false)
        } else {
            textView.setContentOffset(CGPoint.init(x: 0, y: textView.contentSize.height - textView.frame.size.height), animated: false)
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        if newText.count == 0 && self.attachmentImages.count < 1 {
            self.switchSendButton(to: .unactive)
        } else {
            self.switchSendButton(to: .active)
        }
        return true
    }
}
