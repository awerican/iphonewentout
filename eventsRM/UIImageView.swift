//
//  UIImageView.swift
//  WentOut
//
//  Created by Fure on 30.08.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    func setColor(_ color: UIColor) {
        self.image = self.image?.withRenderingMode(.alwaysTemplate)
        self.tintColor = color
    }
}
