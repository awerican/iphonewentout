//
//  CustomPointAnnotation.swift
//  eventsRM
//
//  Created by Fure on 06.07.17.
//  Copyright © 2017 uzuner. All rights reserved.
//

import UIKit
import Mapbox
import UIKit
import Mapbox
import CoreLocation
import Geodesy

class CustomPointAnnotation: MGLPointAnnotation {
    var eventID: String
    init(eventID: String) {
        self.eventID = eventID
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
