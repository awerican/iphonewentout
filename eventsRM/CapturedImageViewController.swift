//
//  CapturedImageViewController.swift
//  WentOut
//
//  Created by Fure on 20.05.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import UIKit
import AVKit
import CropViewController

class MemorySendEntity: Uploadable {
    var data: Data
    var mimeType: String
    
    init(videoURL: URL) {
        let asset = AVAsset(url: videoURL)
        let imgGenerator = AVAssetImageGenerator(asset: asset)
        imgGenerator.appliesPreferredTrackTransform = true
        
        self.mimeType = "video/mp4"
        self.data = try! Data.init(contentsOf: videoURL)
    }
    
    init(image: UIImage, compressionQuality: CGFloat = 0.75) {
        self.mimeType = "image/jpeg"
        self.data = UIImageJPEGRepresentation(image, compressionQuality)!
    }
}

class PostMemoryViewController: UIViewController {
    
    var media: PickedLibraryMedia?
    
    var eventID: String!

    let memoryImageRation = CGSize.init(width: 1.1, height: 2)
    
    @IBOutlet weak var eventImage: UIImageView!
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBAction func doneButton(_ sender: UIBarButtonItem) {
        sender.isEnabled = false
//        uploadImage()
        print("upload image")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let media = self.media {
            self.imageView.image = media.thumbnail
        }
        self.imageView.clipsToBounds = true
        self.imageView.makeOval(cornerRadius: imageView.frame.width / 12)
        self.eventImage.makeOval(cornerRadius: self.eventImage.frame.height / 7)
    }
}

extension PostMemoryViewController {
    private func uploadImage() {
        //        let memory = MemorySendEntity(image: self.media!.thumbnail)
        Webservice.uploadMemory(payload: self.media!) { (_proccess, _res) in
            if let res = _res {
                let param = InsertMemoryParameters(userID: LocalAuth.userID!, eventID: self.eventID, filename: res.filename, mimetype: res.mimeType)
                print(param)
                webservice.load(resource: ResList.insertMemory(param), completion: { (_res, _error) in
                    if let res = _res {
                        if let data = res.data {
                            switch data.status {
                            case .succeed:
                                self.dismiss(animated: true, completion: {
                                    (self.navigationController?.viewControllers[0] as! AddMemoryViewController).addMemoryDelegate?.closeVC()
                                    guard let tb = ((self.parent as! UINavigationController).presentingViewController as? UINavigationController)?.topViewController as? UITabBarController else {
                                        return
                                    }
                                    guard let nb = tb.selectedViewController as? UINavigationController else {
                                        return
                                    }
                                    guard let eventPage = nb.topViewController as? EventProfileViewController else {
                                        return
                                    }
                                    eventPage.updateVC()
                                })
                            case .failed:
                                self.dismiss(animated: true, completion: nil)
                            }
                        }
                    }
                })
                print("res = ", res)
            }
        }
    }
}
