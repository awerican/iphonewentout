//
//  SearchControllerEventCell.swift
//  eventsRM
//
//  Created by Fure on 02.11.17.
//  Copyright © 2017 uzuner. All rights reserved.
//

import UIKit

class SearchControllerEventCell: UICollectionViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    private struct Constants {
        static let reuseNameNibName = "SearchControllerEventCellSep"
    }
    
    var model: [String] = ["first", "second", "third"] {
        didSet {
            self.collectionView.reloadData()
        }
    }
    
    var correctSize: CGSize {
        get {
//            let cell = Bundle.main.loadNibNamed(Constants.reuseNameNibName, owner: self, options: nil)?.first as! SearchControllerEventCellSep
            return SearchControllerEventCellSep.currentSize
//            return CGSize.init(width: UIScreen.main.bounds.width, height: cell.currentSize.height)
        }
    }
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset.left = (UIScreen.main.bounds.width - SearchControllerEventCellSep.cellWidth) / 2
        layout.sectionInset.right = (UIScreen.main.bounds.width - SearchControllerEventCellSep.cellWidth) / 2
        var collectionView = UICollectionView.init(frame: CGRect.init(x: 0.0, y: 0.0, width: self.frame.width, height: self.frame.height), collectionViewLayout: layout)
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.backgroundColor = UIColor.white
        return collectionView
    }()
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        setupCollectionView()
    }
    
    private func setupCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib.init(nibName: Constants.reuseNameNibName, bundle: nil), forCellWithReuseIdentifier: Constants.reuseNameNibName)
        addSubview(collectionView)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return model.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.reuseNameNibName, for: indexPath) as! SearchControllerEventCellSep
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let cell = Bundle.main.loadNibNamed(Constants.reuseNameNibName, owner: self, options: nil)?.first as! SearchControllerEventCellSep
        return SearchControllerEventCellSep.currentSize
    }
}
