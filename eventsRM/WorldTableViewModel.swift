//
//  WorldTableViewModel.swift
//  WentOut
//
//  Created by Fure on 22.08.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import Foundation
import UIKit

enum DataDownloadState<T> {
    case notStarted
    case isLoading
    case success(result: T)
    case failed(Error?)
}

enum WorldTableViewCellState<T> {
    case isLoading
    case fail
    case normal(model: T)
}

protocol WorldTableViewModelDelegate: class {
    func tableViewBeginUpdates()
    func tableViewShouldReloadData()
    func tableViewShouldReloadRows(at indexPath: [IndexPath])
    func tableViewShouldInsertRows(at indexPath: [IndexPath])
    func tableViewShouldDeleteRows(at indexPath: [IndexPath])
    func tableViewEndUpdates()
}

class WorldTableViewModel {
    var model: [EventPanel] = []
    var imageCache: NSCache<NSURL, UIImage>
    var downloadState: DataDownloadState<[EventPanel]> = .notStarted
    var tableViewState: WorldTableViewState
    
    weak var delegate: WorldTableViewModelDelegate?
    
    init(state: WorldTableViewState, imageCache: NSCache<NSURL, UIImage>, delegate: WorldTableViewModelDelegate) {
        self.tableViewState = state
        self.delegate = delegate
        self.imageCache = imageCache
//        downloadData()
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        switch downloadState {
        case .notStarted:
            return 0
        case .isLoading, .failed:
            if section == 0 {
                return 1
            }
            return 0
        case .success(result: _):
            switch tableViewState {
            case .topEvents:
                if section == 0 {
                    return model.count
                }
                return 0
            case .pickedEvent(eventID: _):
                if section == 0 {
                    return 1
                } else {
                    if self.model.count > 1 {
                        return model.count - 1
                    }
                    return 0
                }
            }
        }
    }
    
    func numberOfSections() -> Int {
        switch self.tableViewState {
        case .topEvents:
            return 1
        case .pickedEvent(eventID: _):
            return 2
        }
    }
    
    func cellForRow(at indexPath: IndexPath) -> WorldTableViewCellState<TableViewCellModel> {
        switch self.downloadState {
        case .notStarted:
            return .isLoading
        case .isLoading:
            return .isLoading
        case .success(result: _):
            return .normal(model: modelCellForRow(at: indexPath))
        case .failed(_):
            return WorldTableViewCellState.fail
        }
    }
    
    func downloadData() {
        switch self.tableViewState {
        case .topEvents:
            downloadTopEvents()
        case .pickedEvent(eventID: let eventID):
            downloadPickedEvent(eventID)
        }
    }
    
    private func downloadTopEvents() {
        self.downloadState = .isLoading
        
        self.delegate?.tableViewShouldReloadData()
        
        webservice.load(auth: LocalAuth.getHeader()!, resource: ResList.getTopEvents()) { (_result, _error) in
            guard let data = _result?.data else {
                self.downloadState = .failed(_error)
                self.delegate?.tableViewBeginUpdates()
                self.delegate?.tableViewShouldDeleteRows(at: [IndexPath.init(row: 0, section: 0)])
                self.delegate?.tableViewShouldInsertRows(at: [IndexPath.init(row: 0, section: 0)])
                self.delegate?.tableViewEndUpdates()
                return
            }
            
            self.model = data
            
            print(data.count)
            
            guard data.count > 0 else {
                self.downloadState = .failed(nil)
                self.delegate?.tableViewBeginUpdates()
                self.delegate?.tableViewShouldDeleteRows(at: [IndexPath.init(row: 0, section: 0)])
                self.delegate?.tableViewShouldInsertRows(at: [IndexPath.init(row: 0, section: 0)])
                self.delegate?.tableViewEndUpdates()
                return
            }
            
            self.downloadState = .success(result: data)
            
            var indexPaths: [IndexPath] = []
            for i in 0...data.count - 1 {
                indexPaths.append(IndexPath.init(row: i, section: 0))
            }
            
            self.delegate?.tableViewBeginUpdates()
            self.delegate?.tableViewShouldDeleteRows(at: [IndexPath.init(row: 0, section: 0)])
            self.delegate?.tableViewShouldInsertRows(at: indexPaths)
            self.delegate?.tableViewEndUpdates()
        }
    }
    
    private func downloadPickedEvent(_ eventID: String) {
        self.downloadState = .isLoading
        
        self.delegate?.tableViewShouldReloadData()
        
        webservice.load(resource: ResList.getEventByID(EventByIDParameters(eventID: eventID))) { (_result, _error) in
            guard let data = _result?.data else {
                    self.downloadState = .failed(_error)
                    self.delegate?.tableViewBeginUpdates()
                    self.delegate?.tableViewShouldDeleteRows(at: [IndexPath.init(row: 0, section: 0)])
                    self.delegate?.tableViewShouldInsertRows(at: [IndexPath.init(row: 0, section: 0)])
                    self.delegate?.tableViewEndUpdates()
                return
            }
            
            self.model = [data]
            self.downloadState = .success(result: [data])
            
            self.delegate?.tableViewBeginUpdates()
            self.delegate?.tableViewShouldDeleteRows(at: [IndexPath.init(row: 0, section: 0)])
            self.delegate?.tableViewShouldInsertRows(at: [IndexPath.init(row: 0, section: 0)])
            self.delegate?.tableViewEndUpdates()
            
            webservice.load(resource: ResList.getNearestEventsOnMap(EventByIDParameters.init(eventID: eventID)), completion: { (_res, _err) in
                guard let data = _res?.data else {
                    if let err = _err {
                        print("ERROR WITH DOWNLOADING NEAREST EVENTS", err)
                    }
                    return
                }
                
                self.model.append(contentsOf: data)
                
                guard data.count > 0 else {
                    return
                }
                
                var indexPaths: [IndexPath] = []
                for i in 0...data.count - 1 {
                    indexPaths.append(IndexPath.init(row: i, section: 1))
                }
                
                self.delegate?.tableViewShouldInsertRows(at: indexPaths)
            })
        }
    }
}

extension WorldTableViewModel {
    func modelCellForRow(at indexPath: IndexPath) -> TableViewCellModel {
        switch self.tableViewState {
        case .topEvents:
            let username = getUsernameText(model[indexPath.row].userNickname)
            let eventTitle = model[indexPath.row].title
            let eventTime = model[indexPath.row].time.timeDifferenceOnMap ?? ""
            let location = model[indexPath.row].location
            let userAvatar = model[indexPath.row].userPhoto
            let eventAvatar = model[indexPath.row].eventPhoto
            let guests = getGuestText(model[indexPath.row].guests)
            
            return TableViewCellModel.init(imageCache: self.imageCache, userAvatarURL: userAvatar, username: username, eventTitle: eventTitle, time: eventTime, eventAvatarURL: eventAvatar, location: location.city, guests: guests)
        case .pickedEvent(eventID: _):
            if indexPath.section == 0 {
                let username = getUsernameText(model[indexPath.row].userNickname)
                let eventTitle = model[indexPath.row].title
                let eventTime = model[indexPath.row].time.timeDifferenceOnMap ?? ""
                let location = model[indexPath.row].location
                let userAvatar = model[indexPath.row].userPhoto
                let eventAvatar = model[indexPath.row].eventPhoto
                let guests = getGuestText(model[indexPath.row].guests)
                return TableViewCellModel.init(imageCache: self.imageCache, userAvatarURL: userAvatar, username: username, eventTitle: eventTitle, time: eventTime, eventAvatarURL: eventAvatar, location: location.city, guests: guests)
            } else {
                let username = getUsernameText(model[indexPath.row + 1].userNickname)
                let eventTitle = model[indexPath.row + 1].title
                let eventTime = model[indexPath.row + 1].time.timeDifferenceOnMap ?? ""
                let location = model[indexPath.row + 1].location
                let userAvatar = model[indexPath.row + 1].userPhoto
                let eventAvatar = model[indexPath.row + 1].eventPhoto
                let guests = getGuestText(model[indexPath.row + 1].guests)
                return TableViewCellModel.init(imageCache: self.imageCache, userAvatarURL: userAvatar, username: username, eventTitle: eventTitle, time: eventTime, eventAvatarURL: eventAvatar, location: location.city, guests: guests)
            }
        }
    }
    
    private func getUsernameText(_ username: String) -> String {
        return "@" + username
    }
    
    private func getGuestText(_ guestNumber: Int) -> String {
        if guestNumber > 1 {
            return guestNumber.description + " went"
        } else {
            return guestNumber.description + " went"
        }
    }
}
