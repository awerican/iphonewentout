//
//  ChatLeftMsgTwoImageCell.swift
//  WentOut
//
//  Created by Fure on 14.10.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import UIKit

class ChatLeftMsgTwoImageCell: ChatMessageBaseCell {
    
    @IBOutlet var avatarOutlet: UIImageView!
    @IBOutlet var mediaCollectionOutlet: [UIImageView]!
    @IBOutlet var usernameOutlet: UILabel!
    @IBOutlet var timeOutlet: UILabel!
    @IBOutlet var messageBackgroundOutlet: UIView!
    @IBOutlet var timeBackgroundOutlet: UIView!
    
    override var avatar: UIImageView? {
        return self.avatarOutlet
    }
    override var mediaCollection: [UIImageView]? {
        return self.mediaCollectionOutlet
    }
    override var username: UILabel! {
        return self.usernameOutlet
    }
    override var time: UILabel! {
        return self.timeOutlet
    }
    override var messageBackground: UIView! {
        return self.messageBackgroundOutlet
    }
    override var timeBackground: UIView! {
        return self.timeBackgroundOutlet
    }
    
//    override func draw(_ rect: CGRect) {
//        self.avatar.makeCircle()
//        self.messageBackground.makeOval(cornerRadius: 15.0)
//        self.mediaOne.makeOval(cornerRadius: 10.0)
//        self.mediaTwo.makeOval(cornerRadius: 10.0)
//        self.timeBackground.makeOval(cornerRadius: self.timeBackground.frame.height / 4)
//    }
}

//protocol ChatLeftMsgTwoImageCellDelegate { }
//
//protocol ChatLeftMsgTwoImageCellInteractor: MessageInteractor { }
//
//extension ChatLeftMsgTwoImageCell: ChatLeftMsgTwoImageCellInteractor { }
//
//extension ChatLeftMsgTwoImageCell: ConfigurableCell {
//    typealias CellDelegate = ChatLeftMsgTwoImageCellDelegate
//    typealias CellData = MessageWrapper
//    typealias CellInteractor = ChatLeftMsgTwoImageCellInteractor
//
//    func configure(with data: CellData, delegate: CellDelegate?) {
//        self.avatar.image = nil
//
//        self.username.text = data.message.senderUsername
//
//        Webservice.loadImage(url: data.message.senderAvatarURL) { (_image) in
//            if let image = _image {
//                self.avatar.image = image
//            }
//        }
//
//        Webservice.loadImage(url: data.message.attch[0].url) { (_image) in
//            if let image = _image {
//                self.mediaOne.image = image
//            }
//        }
//
//        Webservice.loadImage(url: data.message.attch[1].url) { (_image) in
//            if let image = _image {
//                self.mediaTwo.image = image
//            }
//        }
//
//        let dateFormatter: DateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "HH:mm"
//        let timeString = dateFormatter.string(from: data.message.time)
//        self.time.text = timeString
//
//        if data.avatarIsHidden {
//            self.avatar.isHidden = true
//        } else {
//            self.avatar.isHidden = false
//        }
//
//        self.layoutIfNeeded()
//    }
//}
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
