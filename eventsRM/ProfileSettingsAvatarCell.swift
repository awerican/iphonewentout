//
//  ProfileSettingsAvatarCell.swift
//  WentOut
//
//  Created by Fure on 19.09.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

protocol ProfileSettingsAvatarDelegate: class {
    func changePhoto()
}

class ProfileSettingsAvatarCell: UICollectionViewCell {

    static var currentSize: CGSize {
        get {
//            return CGSize.init(width: UIScreen.width, height: 4 + 140 + 4 + 30 + 4)
            return CGSize.init(width: UIScreen.width, height: 146)
        }
    }
    
    weak var delegate: ProfileSettingsAvatarDelegate?
    
    @IBOutlet weak var avatar: UIImageView!
    
    @IBAction func changeAction(_ sender: UIButton) {
        delegate?.changePhoto()
    }
    
    override func draw(_ rect: CGRect) {
        avatar.makeCircle()
    }
    
    func configCell(avatar_url: String, avatar_image: UIImage?) {
        avatar.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(avatarWasTapped))
        avatar.addGestureRecognizer(tapGesture)
        
        if let image = avatar_image {
            self.avatar.image = image
        } else {
            Webservice.loadImage(url: avatar_url) { (image) in
                self.avatar.image = image
            }
        }
    }
}

extension ProfileSettingsAvatarCell {
    @objc private func avatarWasTapped() {
        delegate?.changePhoto()
    }
}



















