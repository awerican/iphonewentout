//
//  FeedbackCellCat1.swift
//  WentOut
//
//  Created by Fure on 06.07.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

class FeedbackCellCat1: UICollectionViewCell {

    @IBOutlet weak var userAvatar: UIImageView!
    @IBOutlet weak var feedbackBody: UILabel!
    
    @IBOutlet weak var basisView: UIView!
    
    var delegate: FeedbackCellCatDelegate?
    
    var currentSize: CGSize {
        get {
            self.frame.size.width = UIScreen.width
            self.layoutIfNeeded()
            return CGSize.init(width: UIScreen.main.bounds.width, height: self.basisView.frame.size.height)
        }
    }
    
    func configCell(model: FeedbackEntity) {
        
        self.userAvatar.image = nil
        Webservice.loadImage(url: model.creatorPhoto) { (_image) in
            self.userAvatar.image = _image
        }
        
        self.feedbackBody.attributedText = self.makeBody(username: model.creatorUsername, time: Date.goodTime(inSec: model.time) ?? "")
        
        self.userAvatar.makeCircle()
        
        let leftImageTapGesture = UITapGestureRecognizer.init(target: self, action: #selector(leftImageGestureAction))
        self.userAvatar.isUserInteractionEnabled = true
        self.userAvatar.addGestureRecognizer(leftImageTapGesture)
    }
}

extension FeedbackCellCat1 {
    
    @objc func leftImageGestureAction() {
        if let ip = (self.superview as? UICollectionView)?.indexPath(for: self) {
            self.delegate?.leftImageWasTapped(at: ip)
        }
    }
    
    private func makeBody(username: String, time: String) -> NSMutableAttributedString {
        let helveticaBold = [
            NSAttributedStringKey.foregroundColor: UIColor.black,
            NSAttributedStringKey.font: UIFont.init(name: "HelveticaNeue-Bold", size: 13.5)!]
        let helveticaLight = [
            NSAttributedStringKey.foregroundColor: UIColor.black,
            NSAttributedStringKey.font: UIFont.init(name: "HelveticaNeue-Light", size: 13.5)!]
        let helveticaHour = [
            NSAttributedStringKey.foregroundColor: UIColor.gray,
            NSAttributedStringKey.font: UIFont.init(name: "HelveticaNeue-Light", size: 12.0)!]
        
        let textPartOne = NSMutableAttributedString(string: username, attributes: helveticaBold)
        let textPartTwo = NSMutableAttributedString(string: " started following you.", attributes: helveticaLight)
        let textPartThree = NSMutableAttributedString(string: " " + time, attributes: helveticaHour)
        
        let textCombination = NSMutableAttributedString()
        textCombination.append(textPartOne)
        textCombination.append(textPartTwo)
        textCombination.append(textPartThree)
        
        return textCombination
    }
}














