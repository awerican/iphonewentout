//
//  ReportViewControllerTItleCell.swift
//  WentOut
//
//  Created by Fure on 19.10.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

class ReportViewControllerTitleCell: UICollectionViewCell {

    static var currentSize: CGSize {
        get {
            return CGSize.init(width: UIScreen.width, height: 45.0)
        }
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    
    func configCell(title: String) {
        titleLabel.text = title
    }
}
