//
//  Double.swift
//  WentOut
//
//  Created by Fure on 28.07.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import Foundation

extension Double {
    var clean: String {
        get {
            return String(format: "%g", self)
        }
    }
    func round(to places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return Darwin.round(self * divisor) / divisor
    }
}
