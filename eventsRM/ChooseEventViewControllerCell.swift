//
//  ChooseEventViewControllerCell.swift
//  WentOut
//
//  Created by Fure on 20.06.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

class ChooseEventViewControllerCell: UITableViewCell {
    
    @IBOutlet weak var eventName: UILabel!
    
    func configCell(title: String) {
        self.eventName.text = title
    }
}
