//
//  UsernameValidator.swift
//  WentOut
//
//  Created by Fure on 29.08.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import Foundation

enum UsernameValidate: String {
    case correct                = "Validated."
    case usernameDoNotChanged   = "The username is the same."
    case lengthIsZero           = "Length cannot be zero."
    case lengthIsTooBig         = "Length cannot be more than 30 letters."
    case firstDot               = "First letter cannot be the dot."
    case firstUnderscope        = "First letter cannot be the underscope."
    case lastDot                = "Last letter cannot be the dot."
    case lastUnderscope         = "Last letter cannot be the underscope."
    case repeatingDots          = "Dots cannot follow each other."
    case repeatingUndescopes    = "Underscope cannot follow each other."
}

class UsernameChecker {
    private let validCharacters: [Character] = ["a", "b", "c", "d", "e",
                                                "f", "g", "h", "i", "j",
                                                "k", "l", "m", "n", "o",
                                                "p", "q", "r", "s", "t",
                                                "u", "v", "w", "x", "y",
                                                "z",
                                                "0", "1", "2", "3", "4",
                                                "5", "6", "7", "8", "9",
                                                ".", "_"]
    
    private let deletingSybmol = ""
    private let dot = Character.init(".")
    private let underscore = Character.init("_")
    private let usernameMaxLength = 30
    
    private let oldUsername: String?
    
    init(oldUsername: String?) {
        self.oldUsername = oldUsername
    }
    
    func firstValidation(_ oldText: String, range: NSRange, replacementString string: String) -> Bool {
        if string == deletingSybmol {
            return true
        }
        
        if let txt = oldText as NSString? {
            let newtext = txt.replacingCharacters(in: range, with: string)
            
            if repeatsInTheRow(repeatingElement: String(self.dot), in: newtext) {
                return false
            }
            
            if repeatsInTheRow(repeatingElement: String(self.underscore), in: newtext) {
                return false
            }
            
            if newtext.count > usernameMaxLength {
                return false
            }
        }
        
        var counter: Int = 0
        for character in validCharacters {
            if string.lowercased().contains(character) {
                counter += 1
            }
        }
        if counter < 1 {
            return false
        }
        
        if let lastCharacter = oldText.last {
            if let firstCharacter = string.first {
                if lastCharacter == firstCharacter && (lastCharacter == dot || lastCharacter == underscore) {
                    return false
                }
            }
        }
        return true
    }
    
    func secondValidation(_ username: String) -> UsernameValidate {
        if username.count == 0 {
            return .lengthIsZero
        }
        
        if username.count > usernameMaxLength {
            return .lengthIsTooBig
        }
        
        if username.first == self.dot {
            return .firstDot
        }
        
        if username.first == self.underscore {
            return .firstUnderscope
        }
        
        if username.last == self.dot {
            return .lastDot
        }
        
        if username.last == self.underscore {
            return .lastUnderscope
        }
        
        if repeatsInTheRow(repeatingElement: String(self.dot), in: username) {
            return .repeatingDots
        }
        
        if repeatsInTheRow(repeatingElement: String(self.underscore), in: username) {
            return .repeatingUndescopes
        }
        
        if username == self.oldUsername {
            return .usernameDoNotChanged
        }
        
        return .correct
    }
    
    private func repeatsInTheRow(repeatingElement: String, in string: String) -> Bool {
        for dot in string.indexes(of: repeatingElement) {
            if string.indexes(of: repeatingElement).contains(dot + 1) {
                return true
            }
        }
        return false
    }
}
