//
//  CustomSegue.swift
//  eventsRM
//
//  Created by Fure on 08.11.16.
//  Copyright © 2016 uzuner. All rights reserved.
//

import UIKit
import QuartzCore

class CustomSegueEvent: UIStoryboardSegue {
    
    override func perform() {
        let src = self.source
        let dst = self.destination
        
        src.view.superview?.insertSubview(dst.view, aboveSubview: src.view)
        dst.view.transform = CGAffineTransform(translationX: src.view.frame.size.width, y: 0)
        
        UIView.animate(withDuration: 0.35,
                       delay: 0.0,
                       options: UIViewAnimationOptions.curveEaseIn,
                       animations: {
                        dst.view.transform = CGAffineTransform(translationX: 0, y: 0)
                        src.view.transform = CGAffineTransform(translationX: -src.view.frame.size.width, y: 0)
        },
            completion: {finished in src.addChildViewController(dst)})
    }
}
