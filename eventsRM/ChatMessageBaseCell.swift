//
//  ChatMessageBaseCell.swift
//  WentOut
//
//  Created by Fure on 15.10.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import Foundation
import UIKit

enum MessageSendLoadingAction {
    case show, hide
}

protocol MessageInteractor {
    var avatar: UIImageView? { get }
    func hideAvatar()
    func sendingLoadingIndicator(_ action: MessageSendLoadingAction)
    func sendingMsgError(_ action: MessageSendLoadingAction)
    func errorIndicatorIsVisible(_ visible: Bool)
    func loadingIndicatorIsVisible(_ visible: Bool)
}

extension MessageInteractor {
    func hideAvatar() {
        self.avatar?.isHidden = true
    }
}

protocol ChatMessageBaseDelegate { }

protocol ChatMessageBaseInteractor: MessageInteractor { }

class ChatMessageBaseCell: UITableViewCell {

    private(set) var time: UILabel!
    private(set) var messageBackground: UIView!
    
    private(set) var avatar: UIImageView?
    private(set) var username: UILabel?
    private(set) var message: UILabel?
    private(set) var mediaCollection: [UIImageView]?
    private(set) var timeBackground: UIView?
    private(set) var imageHiddenQuantity: UILabel?
    private(set) var imageHiddenQuantityBlurView: UIVisualEffectView?
    
    private(set) var timeLeadingConstr: NSLayoutConstraint?
    private(set) var timeTopConstr: NSLayoutConstraint?
    
    private var messageLabelLineNumber: Int?
    
    var messageEntity: MessageEntity?
    
    var indicatorStillNeeded: Bool = false
    
    var indicator = UIActivityIndicatorView()
    
    var errorIndicator = UIButton.init(type: .infoLight)
    
    override func draw(_ rect: CGRect) {
        self.avatar?.makeCircle()
        self.messageBackground.makeOval(cornerRadius: 15.0)
        
        setupIndicator()
        setupErrorIndicator()
        
        if let mediaCollection = self.mediaCollection {
            for media in mediaCollection {
                media.makeOval(cornerRadius: 10.0)
            }
        }
        self.imageHiddenQuantityBlurView?.makeOval(cornerRadius: 10.0)
        
        self.timeBackground?.makeOval(cornerRadius: self.timeBackground!.frame.height / 4)
        
        if let message = self.messageEntity {
            if message.senderID == LocalAuth.userID! {
                self.messageBackground.backgroundColor = UIColor.init(hex: "cbdeff")
            } else {
                self.messageBackground.backgroundColor = UIColor.init(hex: "F1F2F2")
            }
        }
    }
    
    private func setupIndicator() {
        indicator.autoresizingModeOn()
        indicator.activityIndicatorViewStyle = .gray
        indicator.isHidden = true
        self.addSubview(indicator)
        
        indicator.trailingAnchor.constraint(equalTo: self.messageBackground.leadingAnchor, constant: 0.0).isActive = true
        indicator.bottomAnchor.constraint(equalTo: self.messageBackground.bottomAnchor, constant: -5.0).isActive = true
        indicator.widthAnchor.constraint(equalToConstant: 35.0).isActive = true
        indicator.heightAnchor.constraint(equalToConstant: 35.0).isActive = true
    }
    
    private func setupErrorIndicator() {
        errorIndicator.autoresizingModeOn()
        errorIndicator.tintColor = .red
        errorIndicator.isHidden = true
        
        errorIndicator.addTarget(self, action: #selector(errorButtonAction(_:)), for: .touchUpInside)
        
        self.addSubview(errorIndicator)
        
        errorIndicator.trailingAnchor.constraint(equalTo: self.messageBackground.leadingAnchor, constant: 0.0).isActive = true
        errorIndicator.bottomAnchor.constraint(equalTo: self.messageBackground.bottomAnchor, constant: -5.0).isActive = true
        errorIndicator.widthAnchor.constraint(equalToConstant: 35.0).isActive = true
        errorIndicator.heightAnchor.constraint(equalToConstant: 35.0).isActive = true
    }
    
    @objc private func errorButtonAction(_ button: UIButton) {
        let tb = self.superview as! UITableView
        let indexPath = tb.indexPath(for: self)!
        
        tb.delegate?.tableView!(tb, performAction: #selector(errorButtonSelector), forRowAt: indexPath, withSender: self.messageEntity)
        print("error button was pressed at", indexPath)
    }
    
    @objc func errorButtonSelector() {
        print("perform action in action")
    }
}

extension ChatMessageBaseCell: ChatMessageBaseInteractor {
    
    func loadingIndicatorIsVisible(_ visible: Bool) {
        self.indicator.isHidden = !visible
        self.layoutIfNeeded()
    }
    
    func errorIndicatorIsVisible(_ visible: Bool) {
        self.errorIndicator.isHidden = !visible
        self.layoutIfNeeded()
    }
    
    func sendingMsgError(_ action: MessageSendLoadingAction) {
        switch action {
        case .show:
            self.errorIndicator.isHidden = false
            print("YOYOYO-show error")
        case .hide:
            self.errorIndicator.isHidden = true
            print("YOYOYO-hide error")
        }
    }
    
    func sendingLoadingIndicator(_ action: MessageSendLoadingAction) {
        switch action {
        case .show:
            self.indicatorStillNeeded = true
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                [weak self] in
                if self!.indicatorStillNeeded {
                    self?.indicator.isHidden = false
                    self?.indicator.startAnimating()
                }
            }
        case .hide:
            self.indicator.isHidden = true
            self.indicator.stopAnimating()
            self.indicatorStillNeeded = false
        }
    }
}

extension ChatMessageBaseCell: ConfigurableCell {
    
    typealias CellDelegate = ChatMessageBaseDelegate
    typealias CellData = MessageWrapper
    typealias CellInteractor = ChatMessageBaseInteractor
    
    func configure(with data: MessageWrapper, delegate: ChatMessageBaseDelegate?) {
        self.messageEntity = data.message
        
        self.avatar?.image = nil
        
        if let mediaCol = self.mediaCollection {
            for media in mediaCol {
                media.image = nil
            }
        }
        
        self.username?.text = data.message.senderUsername
        self.message?.text = data.message.text
        self.imageHiddenQuantity?.text = "+" + (data.message.attch.count - 3).description
        
        if data.loadingIndicatorIsHidden {
            self.indicator.isHidden = true
            self.indicator.stopAnimating()
        } else {
            self.indicator.isHidden = false
            self.indicator.startAnimating()
        }
        
        errorIndicator.isHidden = data.errorIndicatorIsHidden
        
        if let avatar = self.avatar {
            Webservice.loadImage(url: data.message.senderAvatarURL) { (_image) in
                if let image = _image {
                    avatar.image = image
                }
            }
        }
        
        if let mediaCollection = self.mediaCollection {
            var forLoopTill: Int = 0
            if mediaCollection.count < 5 {
                forLoopTill = mediaCollection.count - 1
            } else {
                forLoopTill = 4
            }
            for i in 0 ... forLoopTill {
                if let image = data.message.attch[i].image {
                    self.mediaCollection![i].image = image
                } else {
                    Webservice.loadImage(url: data.message.attch[i].url) { [weak self] (_image) in
                        if let image = _image {
                            self!.mediaCollection![i].image = image
                        }
                    }
                }
            }
        }
        
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        
        if let time = data.message.time {
            let timeString = dateFormatter.string(from: time)
            self.time.text = timeString
        } else {
            let timeString = dateFormatter.string(from: Date())
            self.time.text = timeString
        }

        if data.avatarIsHidden {
            self.avatar?.isHidden = true
        } else {
            self.avatar?.isHidden = false
        }
        
        if let _ = self.message {
            self.timeLeadingConstr!.constant = -self.time.frame.width
            self.timeTopConstr!.constant = 0.0
        }
    }
}
