//
//  SearchUserViewController.swift
//  WentOut
//
//  Created by Fure on 11.07.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import UIKit

class SearchUserViewController: UIViewController {
    
    let usersDownloader = Downloader<[UserMini], SearchPageTextParameters>.init(resource: URL.Search.findUser, param: SearchPageTextParameters.init(text: ""))
    
    weak var scrollViewDelegate: SearchPageScrollViewDelegate?
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height), collectionViewLayout: layout)
        return collectionView
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let searchPageVC = self.parent as? SearchPageViewController {
            if let searchText = searchPageVC.searchBar.text {
                if searchText != self.usersDownloader.getParam().text {
                    searchTextDidChange(text: searchText)
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        setupCollectionView()
    }
}

extension SearchUserViewController: SearchableVC {
    func searchTextDidChange(text: String) {
        usersDownloader.changeParam(newParam: SearchPageTextParameters.init(text: text))
        usersDownloader.data = nil
        collectionView.reloadSections([0])
        if text.count != 0 {
            usersDownloader.getFirst {
                self.collectionView.reloadSections([0])
            }
        }
    }
}

extension SearchUserViewController {
    
    private func setupView() {
        view.backgroundColor = UIColor.init(hex: "FAFAFA")
        view.addSubview(collectionView)
    }
    
    private func setupCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.keyboardDismissMode = .onDrag
        collectionView.backgroundColor = UIColor.white
        collectionView.contentInset.bottom = UITabBar.height + 3.0
        collectionView.contentInset.top = SearchPageViewController.searchTypeViewHeight + 10.0
        for reuseID in reuseIDs {
            collectionView.register(UINib.init(nibName: reuseID, bundle: nil), forCellWithReuseIdentifier: reuseID)
        }
    }
}

extension SearchUserViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if let data = usersDownloader.data {
            if data.count > 0 {
                self.scrollViewDelegate?.controllerDidScroll(scrollView)
            }
        }
    }
}

extension SearchUserViewController: UICollectionViewDataSource {
    var reuseIDs: [String] {
        get {
            return ["SearchControllerUserCell"]
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
        controller.profileOwnerID = self.usersDownloader.data?[indexPath.row].id
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let users = usersDownloader.data else {
            return
        }
        if indexPath.row == users.count - 1 {
            usersDownloader.getNext(section: 0) { (paths) in
                collectionView.insertItems(at: paths)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.usersDownloader.data?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIDs[0], for: indexPath) as! SearchControllerUserCell
        let model = usersDownloader.data![indexPath.row]
        if indexPath.row == usersDownloader.data!.count - 1 {
            cell.configCell(model: model, underlined: false)
        } else {
            cell.configCell(model: model, underlined: true)
        }
        return cell
    }
}

extension SearchUserViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return SearchControllerUserCell.currentSize
    }
}



























