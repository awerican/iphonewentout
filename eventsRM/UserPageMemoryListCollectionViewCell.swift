//
//  UserPageMemoryListCollectionViewCell.swift
//  WentOut
//
//  Created by Fure on 03.07.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

class UserPageMemoryListCollectionViewCell: UICollectionViewCell {

    weak var delegate: Downloader<[StoryEntity], UserProfileEventParameters>?
    
    private var _superviewHeight: CGFloat?
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var loadingView: UIView!
    
    weak var userProfileDelegate: UserProfileMemoryDelegate?
    
    var currentHeight: CGFloat {
        get {
            return self._superviewHeight ?? 0.0
        }
    }
    
    // otherwise activityIndicator will stop
    override func prepareForReuse() {}
    
    func configCell(superviewHeight: CGFloat) {
        self.backgroundColor = UIColor.init(hex: "FAFAFA")
        setupCollectionView()
        self._superviewHeight = superviewHeight
        if let del = self.delegate {
            if let _ = del.data {
                self.loadingView.alpha = 0
                self.collectionView.reloadSections([0])
            }
        }
    }
}

extension UserPageMemoryListCollectionViewCell {
    fileprivate func setupCollectionView() {
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        for reuseID in self.reuseIds {
            self.collectionView!.register(UINib.init(nibName: reuseID, bundle: nil), forCellWithReuseIdentifier: reuseID)
        }
    }
}

extension UserPageMemoryListCollectionViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    
    var reuseIds: [String] {
        get {
            return ["ActivityPageMemoryListUnitCollectionViewCell"]
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let eventID = self.delegate!.data![indexPath.row].eventID
        self.userProfileDelegate?.openStory(eventID: eventID, index: indexPath.row)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let del = self.delegate {
            guard let memories = del.data else {
                return
            }
            if indexPath.row == memories.count - 1 {
                del.getNext(section: 0) { (paths) in
                    self.collectionView?.insertItems(at: paths)
                }
            }
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return self.delegate?.data?.count ?? 0
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIds[0], for: indexPath) as! ActivityPageMemoryListUnitCollectionViewCell
        if let del = self.delegate {
            let model = del.data![indexPath.row]
            cell.configCell(eventTitle: model.eventTitle, memory: model.memories.data[0])
        }
        return cell
    }
}

extension UserPageMemoryListCollectionViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize.init(width: (self.currentHeight - 12 - 12) / 5 * 3.8, height: self.currentHeight - 12 - 12)
        return CGSize.init(width: (self.currentHeight - 12 - 12) / 5 * 3.5, height: self.currentHeight - 12 - 12)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 7, 0, 7)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
}








