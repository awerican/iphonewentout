//
//  CEViewController.swift
//  WentOut
//
//  Created by Fure on 10.07.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import UIKit
import CropViewController

protocol PublishEventDelegate: class {
    func avatarWasPressed()
    func eventTitle(new eventTitle: String?)
    func switchEventType(to eventType: EventType)
    func addLocation(location: EventLocation?)
    func add(eventDesc: String)
    func add(hashtags: [String])
}

protocol CEDeleteCellProtocol: class {
    func deleteCell(at indexPath: IndexPath, cell: UICollectionViewCell)
}

enum CEMode {
    case create
    case edit
}

class CEViewController: UIViewController {
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        let cv = UICollectionView(frame: UIScreen.main.bounds, collectionViewLayout: layout)
        return cv
    }()
    
    var eventHeader: EventProfileHeader?
    
    var pageMode: CEMode = .create {
        didSet {
            if pageMode == CEMode.edit {
                publishButton.title = "Save"
            }
            print("mode has changed")
        }
    }
    
    @IBOutlet weak var publishButton: UIBarButtonItem!
    
    var coreDataManager = CoreDataManager()
    
    lazy var manager: InsertEventManager = {
        if let data = coreDataManager.getData() {
            return data
        } else {
            return InsertEventManager()
        }
    }()
    
    func _insertEvent(param: InsertEventParameters) -> Downloader<SucceedFailedEntity, InsertEventParameters> {
        return Downloader<SucceedFailedEntity, InsertEventParameters>.init(resource: URL.Event.insertEvent, param: param)
    }
    
    @IBOutlet weak var pubishItem: UIBarButtonItem!
    
    var imagePicker = UIImagePickerController()
    
    func showCancelAlert() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let action1 = UIAlertAction(title: "Save", style: .default) { (action: UIAlertAction) in
            print("saving draft")
            do {
                if self.pageMode == .create {
                    try self.coreDataManager.saveEvent(event: self.manager)
                }
            } catch {
                print("error appeared")
            }
            self.dismiss(animated: true, completion: nil)
        }
        
        let action2 = UIAlertAction(title: "Delete", style: .cancel) { (action) in
            self.coreDataManager.deleteEvent()
            self.dismiss(animated: true, completion: nil)
        }
        
        alertController.addAction(action1)
        alertController.addAction(action2)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func cancelButtonAction(_ sender: UIBarButtonItem) {
        if pageMode == .edit {
            self.dismiss(animated: true, completion: nil)
            return
        }
        if self.manager.isModified {
            showCancelAlert()
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func navigationItemResignFirstResponder() {
        self.navigationItem.titleView = nil
        self.navigationItem.title = "Create Event"
        pubishItem.isEnabled = true
    }
    
    func showErrorView(error: String) {
        let errorView = getErrorNavigationView(message: error)
        navigationController?.view.addSubview(errorView)
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            errorView.removeFromSuperview()
        }
    }
    
    func getErrorNavigationView(message: String) -> UIView {
        let frame = navigationController!.navigationBar.frame
        let view = UILabel.init(frame: frame)
        view.backgroundColor = UIColor.orica
        view.text = message
        view.textColor = .black
        view.textAlignment = .center
        return view
    }
    
    @IBAction func publishAction(_ sender: UIBarButtonItem) {
        let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        activityIndicator.color = .gray
        self.navigationItem.titleView = activityIndicator
        activityIndicator.startAnimating()
        sender.isEnabled = false
        
        if pageMode == .edit {
            if manager.isModified == false {
                self.dismiss(animated: true, completion: nil)
                return
            }
            guard let eventID = eventHeader?.eventID else {
                // imposible case scenario, cause value has to be inited before this VC is presented
                self.dismiss(animated: true, completion: nil)
                return
            }
            manager.saveChanges(eventID: eventID, userID: LocalAuth.userID!) { (_error) in
                if let error = _error {
                    switch error {
                    case .noLocation:
                        self.showErrorView(error: "Enter location")
                    case .noPhoto:
                        self.showErrorView(error: "Choose a photo")
                    case .noTime:
                        self.showErrorView(error: "Set time")
                    case .noTitle:
                        self.showErrorView(error: "Enter a title")
                    }
                    self.navigationItemResignFirstResponder()
                    return
                }
                self.dismiss(animated: true, completion: nil)
            }
        } else {
            manager.getParams(userID: LocalAuth.userID!) { (_param, _error) in
                guard let param = _param else {
                    if let error = _error {
                        switch error {
                        case .noLocation:
                            self.showErrorView(error: "Enter location")
                        case .noPhoto:
                            self.showErrorView(error: "Choose a photo")
                        case .noTime:
                            self.showErrorView(error: "Set time")
                        case .noTitle:
                            self.showErrorView(error: "Enter a title")
                        }
                        self.navigationItemResignFirstResponder()
                    }
                    return
                }
                let insertEvent = self._insertEvent(param: param)
                insertEvent.getFirst(reload: {
                    if let data = insertEvent.data {
                        switch data.status {
                        case .failed:
                            self.presentStatusVC()
                        case .succeed:
                            self.coreDataManager.deleteEvent()
                            self.dismiss(animated: true, completion: nil)
                        }
                    } else {
                        self.presentStatusVC()
                    }
                })
            }
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        print("didReceiveMemoryWarning")
        print("delete all saving from DB")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupVC(basedOn: pageMode)
        setupNavigationController()
        setupCollectionView(collectionView: collectionView)
        setupImagePicker(imagePicker: imagePicker)
    }
}

extension CEViewController: CEStatusVCDelegate {
    func retry() {
        navigationItemResignFirstResponder()
    }
}

extension CEViewController {
    private func setupVC(basedOn pageMode: CEMode) {
        guard pageMode == .edit else {
            return
        }
        
        let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        activityIndicator.color = .gray
        self.navigationItem.titleView = activityIndicator
        activityIndicator.startAnimating()
        
        let coverView = UIView.init(frame: collectionView.frame)
        coverView.backgroundColor = UIColor.init(hex: "F9F9F9")
        collectionView.addSubview(coverView)
        
        let eventHeader = self.eventHeader!
        let hashtagsDownloader = Downloader<[EventHashtag], SelectEventHashtagsParameters>.init(resource: URL.Event.selectHashtags, param: SelectEventHashtagsParameters.init(eventID: eventHeader.eventID))
        
        Webservice.loadImage(url: eventHeader.avatar) { (_avatar) in
            guard let avatar = _avatar else {
                return
            }
            hashtagsDownloader.getFirst {
                if let hashtags = hashtagsDownloader.data {
                    self.manager = InsertEventManager.init(eventTitle: eventHeader.title, eventAvatar: avatar, location: eventHeader.location!, description: eventHeader.description, timeStart: eventHeader.timeStart, eventType: eventHeader.eventType, hashtags: EventHashtag.convert(hashtags: hashtags))
                    self.collectionView.reloadData()
                    coverView.removeFromSuperview()
                    self.navigationItemResignFirstResponder()
                }
            }
        }
    }
    
    private func presentStatusVC() {
        print("presentStatus")
        let storyboard = UIStoryboard(name: "Second", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "CEStatusViewController") as! CEStatusViewController
        controller.delegate = self
        self.present(controller, animated: false, completion: nil)
    }
    
    private func setupImagePicker(imagePicker: UIImagePickerController) {
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        imagePicker.mediaTypes = ["public.image"]
    }
}

extension CEViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            dismiss(animated: true) {
                self.presentCropViewController(image: pickedImage)
            }
        } else {
            print("something went wrong")
        }
    }
}

//extension CEViewController: AddMemoryViewControllerDelegate {
//    func addMemoryViewController(image: UIImage) {
//        self.manager.eventAvatar = image
//        self.collectionView.reloadItems(at: [IndexPath.init(row: 0, section: 0)])
//        try? self.coreDataManager.saveEvent(event: self.manager)
//    }
//}

extension CEViewController: CropViewControllerDelegate {
    private func presentCropViewController(image: UIImage) {
        let cropViewController = CropViewController(image: image)
        cropViewController.customAspectRatio = CGSize.init(width: 1.0, height: 1.0)
        cropViewController.aspectRatioLockEnabled = true
        cropViewController.aspectRatioPickerButtonHidden = true
        cropViewController.resetButtonHidden = true
        cropViewController.delegate = self
        present(cropViewController, animated: true, completion: nil)
    }
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        dismiss(animated: true) {
            self.manager.eventAvatar = image
            self.collectionView.reloadItems(at: [IndexPath.init(row: 0, section: 0)])
        }
    }
    
    func cropViewController(_ cropViewController: CropViewController, didFinishCancelled cancelled: Bool) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension CEViewController: PublishEventDelegate, AddMemoryViewControllerDelegate {
    func addMemoryViewController(image: UIImage) {
        self.manager.eventAvatar = image
        self.collectionView.reloadItems(at: [IndexPath.init(row: 0, section: 0)])
        if pageMode == .create {
            try? self.coreDataManager.saveEvent(event: self.manager)
        }
    }
    
    func eventTitle(new eventTitle: String?) {
        if let title = eventTitle {
            if title.count > 0 {
                manager.eventTitle = title
                if pageMode == .create {
                    try? self.coreDataManager.saveEvent(event: self.manager)
                }
                return
            }
        }
        manager.eventTitle = nil
        if pageMode == .create {
            try? self.coreDataManager.saveEvent(event: self.manager)
        }
    }
    
    func addLocation(location: EventLocation?) {
        manager.location = location
        collectionView.reloadItems(at: [IndexPath.init(row: 3, section: 0)])
        if pageMode == .create {
            try? self.coreDataManager.saveEvent(event: self.manager)
        }
    }
    func avatarWasPressed() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "NavAddMemoryViewController") as! UINavigationController
        if let vc = controller.viewControllers.first as? AddMemoryViewController {
//            vc.event = event
            vc.memoryImageRation = CGSize.init(width: 1.0, height: 1.0)
            vc.delegate = self
            self.present(controller, animated: true, completion: nil)
        }
//        present(imagePicker, animated: true, completion: nil)
    }
    func switchEventType(to eventType: EventType) {
        manager.eventType = eventType
        if pageMode == .create {
            try? self.coreDataManager.saveEvent(event: self.manager)
        }
    }
    func add(eventDesc: String) {
        if eventDesc.count > 0 {
            manager.description = eventDesc
            collectionView.reloadItems(at: [IndexPath.init(row: 6, section: 0)])
            if pageMode == .create {
                try? self.coreDataManager.saveEvent(event: self.manager)
            }
        }
    }
    
    func add(hashtags: [String]) {
        print("hashtags =", hashtags)
        manager.hashtags = hashtags
        collectionView.reloadItems(at: [IndexPath.init(row: 5, section: 0)])
        if pageMode == .create {
            try? self.coreDataManager.saveEvent(event: self.manager)
        }
    }
}

extension CEViewController: CEDeleteCellProtocol {
    func deleteCell(at indexPath: IndexPath, cell: UICollectionViewCell) {
        if indexPath.row == 3 {
            manager.location = nil
            collectionView.reloadItems(at: [indexPath])
            if pageMode == .create {
                try? self.coreDataManager.saveEvent(event: self.manager)
            }
        }
        if indexPath.row == 4 {
            manager.timeStart = nil
            collectionView.reloadItems(at: [indexPath])
            if pageMode == .create {
                try? self.coreDataManager.saveEvent(event: self.manager)
            }
        }
        if indexPath.row == 6 {
            manager.description = nil
            collectionView.reloadItems(at: [indexPath])
            if pageMode == .create {
                try? self.coreDataManager.saveEvent(event: self.manager)
            }
        }
    }
}

extension CEViewController {
    
    var reuseIDs: [String] {
        get {
            return ["CEViewControllerEventAvatarCell", "CEViewControllerEventTitleCell", "CEViewControllerEventTypeCell", "CEViewControllerAddLocationCell", "CEViewControllerAddTimeCell", "CEViewControllerAddHashtagCell", "CEViewControllerAddDescrCell", "CEViewControllerSetLocationCell", "CEViewControllerSetDescrCell", "CEViewControllerSetTimeCell", "CEViewControllerSetHashtagCell"]
        }
    }
    
    private func setupNavigationController() {
        navigationItem.title = "Create Event"
    }
    
    private func setupCollectionView(collectionView: UICollectionView) {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = .white
        collectionView.contentInset.bottom = 150
        collectionView.keyboardDismissMode = .onDrag
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        for reuseID in reuseIDs {
            collectionView.register(UINib.init(nibName: reuseID, bundle: nil), forCellWithReuseIdentifier: reuseID)
        }
        self.view.addSubview(collectionView)
    }
}

extension CEViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 7
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 3 {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "AddLocationViewController") as! AddLocationViewController
            self.navigationController?.pushViewController(controller, animated: true)
        }
        if indexPath.row == 4 {
            let alert = UIAlertController.init(title: "Select date", message: "The time event is going to start", preferredStyle: .actionSheet)
            
            let threeYears = 3 * TimeInterval.oneYearPeriod
            
            let in3Year = Date.init(timeIntervalSince1970: Date().timeIntervalSince1970 + threeYears)
        
            var timeStart: Int?
            
            alert.addDatePicker(mode: .dateAndTime, date: Date(), minimumDate: Date(), maximumDate: in3Year) { date in
                timeStart = Int(date.timeIntervalSince1970)
            }
            
            let okAction = UIAlertAction.init(title: "OK", style: .cancel) { (action) in
                self.manager.timeStart = timeStart
                self.collectionView.reloadItems(at: [IndexPath.init(row: 4, section: 0)])
                if self.pageMode == .create {
                    try? self.coreDataManager.saveEvent(event: self.manager)
                }
            }
            
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
        if indexPath.row == 5 {
            print("add hashtag")
            let storyboard = UIStoryboard(name: "Second", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "CEHashtagViewController") as! CEHashtagViewController
            controller.data = manager.hashtags
            self.navigationController?.pushViewController(controller, animated: true)
        }
        if indexPath.row == 6 {
            let storyboard = UIStoryboard(name: "Second", bundle: nil)
//            let controller = storyboard.instantiateViewController(withIdentifier: "CEDescViewController") as! CEDescViewController
            let controller = storyboard.instantiateViewController(withIdentifier: "CEDescriptionViewController") as! CEDescriptionViewController
            controller.defaultText = manager.description
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIDs[0], for: indexPath) as! CEViewControllerEventAvatarCell
            cell.configCell(image: manager.eventAvatar)
            cell.delegate = self
            return cell
        }
        if indexPath.row == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIDs[1], for: indexPath) as! CEViewControllerEventTitleCell
            cell.configCell(title: manager.eventTitle)
            cell.delegate = self
            return cell
        }
        if indexPath.row == 2 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIDs[2], for: indexPath) as! CEViewControllerEventTypeCell
            cell.configCell(eventType: manager.eventType)
            cell.delegate = self
            if pageMode == .edit {
                cell.switcher.isEnabled = false
            }
            return cell
        }
        if indexPath.row == 3 {
            if let location = manager.location {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIDs[7], for: indexPath) as! CEViewControllerSetLocationCell
                cell.configCell(location: location)
                cell.delegate = self
                return cell
            } else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIDs[3], for: indexPath) as! CEViewControllerAddLocationCell
                return cell
            }
        }
        if indexPath.row == 4 {
            if let timeStart = manager.timeStart {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIDs[9], for: indexPath) as! CEViewControllerSetTimeCell
                cell.delegate = self
                cell.config(date: Date.init(timeIntervalSince1970: Double(timeStart)))
                return cell
            }
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIDs[4], for: indexPath) as! CEViewControllerAddTimeCell
            return cell
        }
        if indexPath.row == 5 {
            if manager.hashtags.count > 0 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIDs[10], for: indexPath) as! CEViewControllerSetHashtagCell
                cell.configCell(hashtags: manager.hashtags)
                return cell
            } else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIDs[5], for: indexPath) as! CEViewControllerAddHashtagCell
                return cell
            }
            
        }
        if indexPath.row == 6 {
            if let descr = manager.description {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIDs[8], for: indexPath) as! CEViewControllerSetDescrCell
                cell.configCell(desc: descr)
                cell.delegate = self
                return cell
            } else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIDs[6], for: indexPath) as! CEViewControllerAddDescrCell
                return cell
            }
        }
        return UICollectionViewCell()
    }
}

extension CEViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.row == 0 {
            return CEViewControllerEventAvatarCell.currentSize
        }
        if indexPath.row == 1 {
            return CEViewControllerEventTitleCell.currentSize
        }
        if indexPath.row == 2 {
            return CEViewControllerEventTypeCell.currentSize
        }
        if indexPath.row == 3 {
            if let _ = manager.location {
                return CEViewControllerSetLocationCell.currentSize
            } else {
                return CEViewControllerAddLocationCell.currentSize
            }
        }
        if indexPath.row == 4 {
            if let _ = manager.timeStart {
                return CEViewControllerSetTimeCell.currentSize
            }
            return CEViewControllerAddTimeCell.currentSize
        }
        if  indexPath.row == 5 {
            if manager.hashtags.count > 0 {
                let bundle = Bundle.main.loadNibNamed(reuseIDs[10], owner: self, options: nil)![0] as! CEViewControllerSetHashtagCell
                bundle.configCell(hashtags: manager.hashtags)
                return bundle.currentSize
            } else {
                return CEViewControllerAddHashtagCell.currentSize
            }
        }
        if indexPath.row == 6 {
            if let desc = manager.description {
                let bundle = Bundle.main.loadNibNamed(reuseIDs[8], owner: self, options: nil)![0] as! CEViewControllerSetDescrCell
                bundle.configCell(desc: desc)
                return bundle.currentSize
            } else {
                return CEViewControllerAddDescrCell.currentSize
            }
        }
        return CGSize.zero
    }
}





