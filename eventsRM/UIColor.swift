//
//  UIColor.swift
//  eventsRM
//
//  Created by Fure on 24.07.17.
//  Copyright © 2017 uzuner. All rights reserved.
//

import UIKit

extension UIColor {
    open class var orica: UIColor {
        get {
            return UIColor(red: 0.95, green: 0.82, blue: 0.90, alpha: 1.0)
        }
    }
    open class var oricaGrey: UIColor {
        get {
            return UIColor(red: 0.95, green: 0.94, blue: 0.96, alpha: 1.0)
        }
    }
    open class var pink: UIColor {
        get {
            return UIColor.init(hex: "ff5470")
        }
    }
    var toHexString: String {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        
        self.getRed(&r, green: &g, blue: &b, alpha: &a)
        
        return String(
            format: "%02X%02X%02X",
            Int(r * 0xff),
            Int(g * 0xff),
            Int(b * 0xff)
        )
    }
    convenience init(hex: String) {
        let scanner = Scanner(string: hex)
        scanner.scanLocation = 0
        
        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: 1
        )
    }
}
