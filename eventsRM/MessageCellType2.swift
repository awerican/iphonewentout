//
//  MessageCellType2.swift
//  WentOut
//
//  Created by Fure on 05.09.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

protocol MessageCellType2Delegate {
    func avatarWasTapped(at indexPath: IndexPath)
    func usernameWasTapped(at indexPath: IndexPath)
}

class MessageCellType2: UITableViewCell {

    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var messageText: UILabel!
    @IBOutlet weak var messageTime: UILabel!
    @IBOutlet weak var messageBackground: UIView!
    
    var delegate: MessageCellType2Delegate?
    
    func confiCell(msg: MessageEntity) {
        messageText.text = msg.text ?? ""
        username.text = msg.senderUsername
        
        avatar.makeCircle()
        messageBackground.makeOval(cornerRadius: 15.0)
        
        avatar.image = nil
        
        Webservice.loadImage(url: msg.senderAvatarURL) { (_image) in
            if let image = _image {
                self.avatar.image = image
            }
        }
        
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        
        if let time = msg.time {
            let timeString = dateFormatter.string(from: time)
            messageTime.text = timeString
        } else {
            let timeString = dateFormatter.string(from: Date())
            messageTime.text = timeString
        }
        
        let tapGesture1 = UITapGestureRecognizer.init(target: self, action: #selector(avatarTapGesture))
        let tapGesture2 = UITapGestureRecognizer.init(target: self, action: #selector(usernameTapGesture))
        
        avatar.isUserInteractionEnabled = true
        avatar.addGestureRecognizer(tapGesture1)
        
        username.isUserInteractionEnabled = true
        username.addGestureRecognizer(tapGesture2)
    }
    
    @objc private func avatarTapGesture() {
        if let tableView = superview as? UITableView {
            if let indexPath = tableView.indexPath(for: self) {
                delegate?.avatarWasTapped(at: indexPath)
            }
        }
    }
    
    @objc private func usernameTapGesture() {
        if let tableView = superview as? UITableView {
            if let indexPath = tableView.indexPath(for: self) {
                delegate?.usernameWasTapped(at: indexPath)
            }
        }
    }
}


















