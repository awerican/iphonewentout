//
//  CEHashtagViewController.swift
//  WentOut
//
//  Created by Fure on 17.09.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import UIKit

//class Hashtag {
//    var name: String?
//    init() {
//        name = nil
//    }
//}

class CEHashtagViewController: UITableViewController {
    
    var data: [String?] = []
    
    @IBAction func cancelButton(_ sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupTableView()
        addItemIfNeeded()
    }
}

extension CEHashtagViewController {
    private func addItemIfNeeded() {
        let doneButton = UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(doneAction))
        
        if let items = navigationItem.rightBarButtonItems {
            if items.count != 1 {
                navigationItem.rightBarButtonItems = [doneButton]
                print("## add item")
            }
        } else {
            navigationItem.rightBarButtonItems = [doneButton]
            print("## add item")
        }
    }
//    private func removeItemIfNeeded() {
//        if let items = navigationItem.rightBarButtonItems {
//            if items.count != 0 {
//                navigationItem.rightBarButtonItems = []
//                print("## delete item")
//            }
//        }
//    }
    
    @objc private func doneAction() {
        var hashtags: [String] = []
        for e in data {
            if let hashtag = e {
                hashtags.append(hashtag)
            }
        }
        if let rootVC = self.navigationController?.viewControllers.first as? PublishEventDelegate {
            rootVC.add(hashtags: hashtags)
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    private func setupView() {
        view.backgroundColor = UIColor(hex: "FAFAFA")
    }
    
    private func setupTableView() {
        tableView.contentInset.top = 27.0
        tableView.separatorStyle = .none
        tableView.setEditing(true, animated: false)
        for id in reuseIDs {
            tableView.register(UINib.init(nibName: id, bundle: nil), forCellReuseIdentifier: id)
        }
    }
}

extension CEHashtagViewController {
    var reuseIDs: [String] {
        return ["CEHashtagTableViewCell", "CEAddHashtagTableViewCell"]
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return data.count
        }
        if section == 1 {
            return 1
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            data.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.section == 0 {
            return true
        }
        if indexPath.section == 1 {
            return false
        }
        return false
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseIDs[0]) as! CEHashtagTableViewCell
            cell.configCell(hashtag: data[indexPath.row])
            cell.delegate = self
            return cell
        }
        if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseIDs[1]) as! CEAddHashtagTableViewCell
            cell.delegate = self
            return cell
        }
        return UITableViewCell()
    }
}

extension CEHashtagViewController: CEAddHashtagTableViewDelegate, CEHashtagTableViewDelegate {
    func addHashtag() {
        if data.count > 7 {
            return
        }
        data.append(nil)
        let newIndexPath = IndexPath.init(row: data.count - 1, section: 0)
        tableView.insertRows(at: [newIndexPath], with: .automatic)
        if let cell = tableView.cellForRow(at: newIndexPath) as? CEHashtagTableViewCell {
            cell.hashtagTextField.becomeFirstResponder()
        }
    }
    func hashtagWasChanged(to hashtag: String?, at indexPath: IndexPath) {
        data[indexPath.row] = hashtag
    }
}






















