//
//  CEViewControllerAddHashtagCell.swift
//  WentOut
//
//  Created by Fure on 10.07.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

class CEViewControllerAddHashtagCell: UICollectionViewCell {

    static var currentSize: CGSize {
        get {
            return CGSize.init(width: UIScreen.width, height: 49.0)
        }
    }
}
