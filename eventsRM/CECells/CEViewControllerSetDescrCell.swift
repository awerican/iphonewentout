//
//  CEViewControllerSetDescrCell.swift
//  WentOut
//
//  Created by Fure on 26.07.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

class CEViewControllerSetDescrCell: UICollectionViewCell {

    var currentSize: CGSize {
        get {
            return CGSize.init(width: UIScreen.width, height: descLabel.frame.maxY + 7)
        }
    }
    
    @IBOutlet weak var descLabel: UILabel!
    
    weak var delegate: CEDeleteCellProtocol?
    
    @IBAction func closeButtonAction(_ sender: UIButton) {
        guard let cv = superview as? UICollectionView else {
            return
        }
        guard let indexPath = cv.indexPath(for: self) else {
            return
        }
        self.delegate?.deleteCell(at: indexPath, cell: self)
    }
    
    func configCell(desc: String) {
        self.descLabel.text = desc
        self.layoutIfNeeded()
    }
}
