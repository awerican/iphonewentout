//
//  CEViewControllerHeaderCell.swift
//  WentOut
//
//  Created by Fure on 10.07.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

class CEViewControllerEventTitleCell: UICollectionViewCell {
    
    @IBOutlet weak var eventTitle: UITextField!
    
    weak var delegate: PublishEventDelegate?
    
    static var currentSize: CGSize {
        get {
            return CGSize.init(width: UIScreen.width, height: 57)
        }
    }
}

extension CEViewControllerEventTitleCell {
    func configCell(title: String?) {
        eventTitle.text = title
        eventTitle.delegate = self
    }
}

extension CEViewControllerEventTitleCell: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text as NSString? {
            let txtAfterUpdate = text.replacingCharacters(in: range, with: string)
            delegate?.eventTitle(new: txtAfterUpdate)
            return true
        }
        delegate?.eventTitle(new: nil)
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}





