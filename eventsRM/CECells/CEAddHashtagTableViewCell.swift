//
//  CEAddHashtagTableViewCell.swift
//  WentOut
//
//  Created by Fure on 17.09.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

protocol CEAddHashtagTableViewDelegate: class {
    func addHashtag()
}

class CEAddHashtagTableViewCell: UITableViewCell {
    
    weak var delegate: CEAddHashtagTableViewDelegate?
    
    @IBAction func addAction(_ sender: UIButton) {
        delegate?.addHashtag()
    }
}
