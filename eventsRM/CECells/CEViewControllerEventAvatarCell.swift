//
//  CEViewControllerEventAvatarCell.swift
//  WentOut
//
//  Created by Fure on 18.07.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

class CEViewControllerEventAvatarCell: UICollectionViewCell {

    @IBOutlet weak var eventAvatar: UIImageView!
    @IBOutlet weak var plusLabel: UILabel!
    
    weak var delegate: PublishEventDelegate?
    
    static var currentSize: CGSize {
        get {
            return CGSize.init(width: UIScreen.width, height: 171)
        }
    }
}

extension CEViewControllerEventAvatarCell {
    func configCell(image: UIImage?) {
        if let img = image {
            eventAvatar.image = img
            plusLabel.alpha = 0.0
        } else {
            eventAvatar.image = #imageLiteral(resourceName: "CEDefaultAvatar")
            plusLabel.alpha = 1.0
        }
        eventAvatar.makeOval(cornerRadius: eventAvatar.frame.height / 9)
        plusLabel.isUserInteractionEnabled = false
        eventAvatar.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(eventAvatarWasPressed))
        eventAvatar.addGestureRecognizer(tapGesture)
    }
    
    @objc private func eventAvatarWasPressed() {
        self.delegate?.avatarWasPressed()
    }
}

















