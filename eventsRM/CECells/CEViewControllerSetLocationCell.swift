//
//  CEViewControllerSetLocationCell.swift
//  WentOut
//
//  Created by Fure on 24.07.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

class CEViewControllerSetLocationCell: UICollectionViewCell {

    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var shortAddressLabel: UILabel!
    
    weak var delegate: CEDeleteCellProtocol?
    
    static var currentSize: CGSize {
        get {
            return CGSize.init(width: UIScreen.width, height: 68.0)
        }
    }
    
    @IBAction func removeCell(_ sender: UIButton) {
        guard let cv = superview as? UICollectionView else {
            return
        }
        guard let indexPath = cv.indexPath(for: self) else {
            return
        }
        self.delegate?.deleteCell(at: indexPath, cell: self)
    }
    
    func configCell(location: EventLocation) {
        addressLabel.text = location.location
        shortAddressLabel.text = getShortAddress(location)
    }
}

extension CEViewControllerSetLocationCell {
    private func getShortAddress(_ location: EventLocation) -> String {
        return location.country + ", " + location.city
    }
}










