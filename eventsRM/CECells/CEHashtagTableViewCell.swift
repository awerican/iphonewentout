//
//  CEHashtagTableViewCell.swift
//  WentOut
//
//  Created by Fure on 17.09.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

protocol CEHashtagTableViewDelegate: class {
    func hashtagWasChanged(to hashtag: String?, at indexPath: IndexPath)
}

class CEHashtagTableViewCell: UITableViewCell {

    @IBOutlet weak var hashtagTextField: UITextField!
    
    var delegate: CEHashtagTableViewDelegate?
    
    func configCell(hashtag: String?) {
        hashtagTextField.text = hashtag
        hashtagTextField.delegate = self
    }
}

extension CEHashtagTableViewCell: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text as NSString? {
            let newText = text.replacingCharacters(in: range, with: string)
            if newText.count > 0 {
                if newText.count > 40 {
                    return false
                }
                if newText.isHashtag {
                    if let tv = superview as? UITableView {
                        if let indexPath = tv.indexPath(for: self) {
                            delegate?.hashtagWasChanged(to: newText, at: indexPath)
                        }
                    }
                    return true
                }
                return false
            }
            if let tv = superview as? UITableView {
                if let indexPath = tv.indexPath(for: self) {
                    delegate?.hashtagWasChanged(to: nil, at: indexPath)
                }
            }
            return true
        }
        return true
    }
}
