//
//  CEViewControllerSetHashtagCell.swift
//  WentOut
//
//  Created by Fure on 18.09.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

class CEViewControllerSetHashtagCell: UICollectionViewCell {

    var currentSize: CGSize {
        get {
            return CGSize.init(width: UIScreen.width, height: hashtagsLabel.frame.maxY + 16)
        }
    }
    
    @IBOutlet weak var hashtagsLabel: UILabel!
    
    func configCell(hashtags: [String]) {
        var newString = ""
        for e in hashtags {
            newString.append("#" + e + " ")
        }
        hashtagsLabel.text = newString
        layoutIfNeeded()
    }
}


















