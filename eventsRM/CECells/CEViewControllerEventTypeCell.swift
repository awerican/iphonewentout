//
//  CEViewControllerEventTypeCell.swift
//  WentOut
//
//  Created by Fure on 10.07.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

class CEViewControllerEventTypeCell: UICollectionViewCell {

    @IBOutlet weak var switcher: UISwitch!
    
    weak var delegate: PublishEventDelegate?
    
    static var currentSize: CGSize {
        get {
            return CGSize.init(width: UIScreen.width, height: 49.0)
        }
    }
    
    @IBAction func switcherAction(_ sender: UISwitch) {
        if sender.isOn {
            delegate?.switchEventType(to: .priv)
        } else {
            delegate?.switchEventType(to: .pub)
        }
    }
    
    func configCell(eventType: EventType) {
        switch eventType {
        case .priv:
            switcher.setOn(true, animated: false)
        case .pub:
            switcher.setOn(false, animated: false)
        }
    }
}























