//
//  CEViewControllerSetTimeCell.swift
//  WentOut
//
//  Created by Fure on 29.07.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

class CEViewControllerSetTimeCell: UICollectionViewCell {

    static var currentSize: CGSize {
        get {
            return CGSize.init(width: UIScreen.width, height: 67.0)
        }
    }
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var bottomTimeLabel: UILabel!
    
    weak var delegate: CEDeleteCellProtocol?
    
    @IBAction func removeCell(_ sender: UIButton) {
        guard let cv = superview as? UICollectionView else {
            return
        }
        guard let indexPath = cv.indexPath(for: self) else {
            return
        }
        self.delegate?.deleteCell(at: indexPath, cell: self)
    }
    
    func config(date: Date) {
        timeLabel.text = date.CETime1
        bottomTimeLabel.text = date.CETime2
    }
}










