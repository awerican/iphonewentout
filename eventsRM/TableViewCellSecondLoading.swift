//
//  TableViewCellSecondLoading.swift
//  WentOut
//
//  Created by Fure on 15.07.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import UIKit

class TableViewCellSecondLoading: UITableViewCell {

    @IBOutlet var basisView: UIView!
    @IBOutlet var buttonLeft: UIButton!
    
    var currentSize: CGSize {
        get {
            return CGSize.init(width: UIScreen.main.bounds.width, height: buttonLeft.frame.maxY + 10 + 15)
        }
    }
    
    override func draw(_ rect: CGRect) {
        self.selectionStyle = .none
    }
}
