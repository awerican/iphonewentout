//
//  ChatLeftMsgOwnFourImageGrouppedCell.swift
//  WentOut
//
//  Created by Fure on 16.10.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import UIKit

class ChatLeftMsgOwnFourImageGrouppedCell: ChatMessageBaseCell {

    @IBOutlet var mediaCollectionOutlet: [UIImageView]!
    @IBOutlet var timeOutlet: UILabel!
    @IBOutlet var messageBackgroundOutlet: UIView!
    @IBOutlet var timeBackgroundOutlet: UIView!
    
    override var time: UILabel! {
        return self.timeOutlet
    }
    override var messageBackground: UIView! {
        return self.messageBackgroundOutlet
    }
    override var mediaCollection: [UIImageView]? {
        return self.mediaCollectionOutlet
    }
    override var timeBackground: UIView? {
        return self.timeBackgroundOutlet
    }
}
