//
//  SearchEventViewController.swift
//  WentOut
//
//  Created by Fure on 11.07.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import UIKit

class SearchEventViewController: UIViewController {
    
    let eventsDownloader = Downloader<[EventPanel], SearchPageTextParameters>.init(resource: URL.Search.findEvent, param: SearchPageTextParameters.init(text: ""))
    
    weak var scrollViewDelegate: SearchPageScrollViewDelegate?
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height), collectionViewLayout: layout)
        return collectionView
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let searchPageVC = self.parent as? SearchPageViewController {
            if let searchText = searchPageVC.searchBar.text {
                if searchText != self.eventsDownloader.getParam().text {
                    searchTextDidChange(text: searchText)
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupCollectionView()
    }
}

extension SearchEventViewController: SearchableVC {
    func searchTextDidChange(text: String) {
        eventsDownloader.changeParam(newParam: SearchPageTextParameters.init(text: text))
        eventsDownloader.data = nil
        collectionView.reloadSections([0])
        if text.count != 0 {
            eventsDownloader.getFirst {
                self.collectionView.reloadSections([0])
            }
        }
    }
}

extension SearchEventViewController {
    
    private func setupView() {
        view.backgroundColor = UIColor.init(hex: "FAFAFA")
        view.addSubview(collectionView)
    }
    
    private func setupCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.keyboardDismissMode = .onDrag
        collectionView.backgroundColor = UIColor.white
        collectionView.contentInset.bottom = UITabBar.height + 3.0
        collectionView.contentInset.top = SearchPageViewController.searchTypeViewHeight + 10.0
        for reuseID in reuseIDs {
            collectionView.register(UINib.init(nibName: reuseID, bundle: nil), forCellWithReuseIdentifier: reuseID)
        }
    }
}

extension SearchEventViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if let data = eventsDownloader.data {
            if data.count > 0 {
                self.scrollViewDelegate?.controllerDidScroll(scrollView)
            }
        }
    }
}

extension SearchEventViewController: UICollectionViewDataSource, SearchControllerEventCellSepDelegate {
    var reuseIDs: [String] {
        get {
            return ["SearchControllerEventCellSep"]
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.eventsDownloader.data?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "EventProfileViewController") as! EventProfileViewController
        controller.eventID = self.eventsDownloader.data?[indexPath.row].eventId
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func avatarWasPressed(at indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
        controller.profileOwnerID = self.eventsDownloader.data?[indexPath.row].userId
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func usernameWasPressed(at indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
        controller.profileOwnerID = self.eventsDownloader.data?[indexPath.row].userId
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let events = eventsDownloader.data else {
            return
        }
        if indexPath.row == events.count - 1 {
            eventsDownloader.getNext(section: 0) { (paths) in
                collectionView.insertItems(at: paths)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIDs[0], for: indexPath) as! SearchControllerEventCellSep
        cell.configCell(model: eventsDownloader.data![indexPath.row])
        cell.delegate = self
        return cell
    }
}

extension SearchEventViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return SearchControllerEventCellSep.currentSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 0, left: SearchControllerEventCellSep.leadingConstant, bottom: 0, right: SearchControllerEventCellSep.leadingConstant)
    }
}

























