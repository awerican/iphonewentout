//
//  SignUpPageOne.swift
//  WentOut
//
//  Created by Fure on 08.05.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import UIKit

class SignUpPageOne: UIViewController {
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var passwordRepeatTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    
    @IBOutlet weak var nextButton: UIButton!
    
    //    var userID: String?
    
    //    var password: String?
    
    func openNextController(userID: String, password: String) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "SignUpPageTwo") as! SignUpPageTwo
        controller.userID = userID
        controller.password = password
        navigationController?.pushViewController(controller, animated: true)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    @IBAction func sendAction(_ sender: UIButton) {
        if !usernameIsValid(username: usernameTextField.text) {
            self.showMessage(text: "Username is invalid", type: .error)
            return
        }
        if !passwordIsValid() {
            self.showMessage(text: "Password is invalid", type: .error)
            return
        }
        if !nameIsValid() {
            self.showMessage(text: "Name is invalid", type: .error)
            return
        }
        
        let username = usernameTextField.text!
        let pass = passwordTextField.text!
        let name = nameTextField.text!
        
        let param = RegistrationFirstParameters.init(nickname: username, password: pass.rsaEncrypt(), name: name)
        
        webservice.load(resource: ResList.regFirst(param)) { (_result, _error) in
            if let data = _result?.data {
                if let userID = data.userID {
                    self.openNextController(userID: userID, password: pass)
                    //                    self.userID = userID
                }
            } else {
                self.showMessage(text: "Something went wrong!", type: .error)
                print("could not init so this userID already existed")
            }
            if let error = _error {
                self.showMessage(text: "Error", type: .error)
                print("req error =", error)
            }
        }
    }
    
    private func setupViews() {
        usernameTextField.delegate = self
        passwordTextField.delegate = self
        passwordRepeatTextField.delegate = self
        nameTextField.delegate = self
        setupMainView()
        
        nextButton.makeOval(cornerRadius: nextButton.frame.height / 5)
        
        navigationItem.title = "Sign Up"
    }
    
    private func setupMainView() {
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(tapViewAction))
        self.view.addGestureRecognizer(tapGesture)
        self.view.isUserInteractionEnabled = true
    }
    
    @objc func tapViewAction() {
        usernameTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
        passwordRepeatTextField.resignFirstResponder()
        nameTextField.resignFirstResponder()
    }
}

extension SignUpPageOne: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension SignUpPageOne {
    func usernameIsValid(username: String?) -> Bool {
        guard let username = username else {
            return false
        }
        
        let validator = UsernameChecker.init(oldUsername: nil).secondValidation(username)
        
        switch validator {
        case .correct:
            return true
        default:
            return false
        }
        
//        guard let lastLetter = username.last else {
//            return false
//        }
//
//        // if whitespace was automatically placed by iOS at the end of a line
//        if lastLetter == " " {
//            username = username.dropLast().description
//        }
//
//        let digits = CharacterSet.decimalDigits
//        let letters = CharacterSet.letters
//
//        var _amount = 0
//
//        for c in username {
//            if let unicode = UnicodeScalar.init(String.init(c)) {
//                if digits.contains(unicode) || letters.contains(unicode) || c == "." {
//                    if username.first != "." && username.last != "." {
//                        _amount += 1
//                    }
//                }
//            }
//        }
//
//        if _amount == username.count {
//            return true
//        }
//
//        return false
    }
    
    private func passwordIsValid() -> Bool {
        if let passOne = passwordTextField.text, let passTwo = passwordRepeatTextField.text {
            if passOne.count > 5 {
                if passOne == passTwo {
                    return true
                }
            }
        }
        return false
    }
    
    private func nameIsValid() -> Bool {
        if let name = nameTextField.text {
            if name.count > 0 {
                return true
            }
        }
        return false
    }
}




















