//
//  MessageEntity.swift
//  WentOut
//
//  Created by Fure on 10.10.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import Foundation
import UIKit

struct MessageAttachment {
    var mimetype: String!
    var url: String!
    var duration: String?
    
    var image: UIImage?
    
    init(mimetype: String, url: String, duration: String? = nil) {
        self.mimetype = mimetype
        self.url = url
        self.duration = duration
    }
    
    mutating func finit(mimetype: String, url: String, duration: String?) {
        self.mimetype = mimetype
        self.url = url
        self.duration = duration
    }
    
    init(image: UIImage, mimetype: String) {
        self.image = image
        self.mimetype = mimetype
    }
    
    mutating func setImage(image: UIImage) {
        self.image = image
    }
    
    func encrypt(aesKey: AesKey) -> MessageAttachment {
        let type = AesWrapper.encrypt(self.mimetype, aesKey: aesKey)
        let url = AesWrapper.encrypt(self.url, aesKey: aesKey)
        var dur: String?
        if let _duration = self.duration {
            dur = AesWrapper.encrypt(_duration, aesKey: aesKey)
        }
        return MessageAttachment.init(mimetype: type, url: url, duration: dur)
    }
    
    func decrypt(aesKey: AesKey) -> MessageAttachment {
        let type = self.mimetype
//        let type = AesWrapper.decrypt(self.mimetype, aesKey: aesKey)
        let url = AesWrapper.decrypt(self.url, aesKey: aesKey)
        var dur: String?
        if let _duration = self.duration {
            dur = AesWrapper.decrypt(_duration, aesKey: aesKey)
        }
        return MessageAttachment.init(mimetype: type!, url: url, duration: dur)
    }
}

extension MessageAttachment: Encodable {
    private enum CodingKeys: String, CodingKey {
        case mimetype = "mimetype"
        case url = "url"
        case duration = "duration"
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(mimetype, forKey: .mimetype)
        try container.encode(url, forKey: .url)
        try container.encode(duration, forKey: .duration)
    }
}

extension MessageAttachment: Decodable {
    private enum EventApiResponseCodingKeys: String, CodingKey {
        case mimetype = "mimetype"
        case url = "url"
        case duration = "duration"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: EventApiResponseCodingKeys.self)
        self.mimetype = try container.decode(String.self, forKey: .mimetype)
        self.url = try container.decode(String.self, forKey: .url)
        if self.mimetype == "mov" || self.mimetype == "avi" || self.mimetype == "mp4" {
            self.duration = try container.decode(String.self, forKey: .duration)
        }
    }
}

struct MessageSendEntity {
    
    var senderID: String
    var msg: String?
    var attch: [MessageAttachment]
    
    var messageEntity: MessageEntity {
        get {
            return MessageEntity.init(id: "-1", senderID: self.senderID, senderUsername: "-1", senderAvatarURL: "-1", time: Date(), msg: self.msg, attch: self.attch)
        }
    }
    
    init(senderID: String, msg: String? = nil, attch: [MessageAttachment]) {
        self.senderID = senderID
        self.msg = msg
        self.attch = attch
    }
    
    mutating func insertAttchs(_ newattch: [MessageAttachment]) {
        self.attch = newattch
    }
    
    func encrypt(aesKey: AesKey) -> MessageSendEntity {
        var attchs = [MessageAttachment]()
        for attch in self.attch {
            let new = MessageAttachment.init(mimetype: attch.mimetype, url: attch.url, duration: attch.duration).encrypt(aesKey: aesKey)
            print(new)
            attchs.append(new)
        }
        let senderID = AesWrapper.encrypt(self.senderID, aesKey: aesKey)
        if let msg = self.msg {
            let encMSG = AesWrapper.encrypt(msg, aesKey: aesKey)
            return MessageSendEntity.init(senderID: senderID, msg: encMSG, attch: attchs)
        } else {
            return MessageSendEntity.init(senderID: senderID, attch: attchs)
        }
    }
}

extension MessageSendEntity: Encodable {
    private enum CodingKeys: String, CodingKey {
        case senderID = "sender_id"
        case msg = "text"
        case attch = "attch"
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(senderID, forKey: .senderID)
        try container.encode(msg, forKey: .msg)
        try container.encode(attch, forKey: .attch)
    }
}

struct MessageResponse {
    var status: Bool
    var msg: MessageEntity?
}

extension MessageResponse: Decodable {
    private enum EventApiResponseCodingKeys: String, CodingKey {
        case status = "status"
        case msg = "msg"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: EventApiResponseCodingKeys.self)
        self.status = try container.decode(Bool.self, forKey: .status)
        if self.status {
            self.msg = try container.decode(MessageEntity.self, forKey: .msg)
        }
    }
}

protocol MessageProtocol {
    var senderID: String { get }
    var msg: String? { get }
    var attch: [MessageAttachment] { get }
    mutating func finalize(msgID: String, time: Date)
    mutating func correctUsername()
}

struct MessageEntity: Equatable {
    var id: String!
    var senderID: String
    var senderUsername: String
    var senderAvatarURL: String!
    var time: Date!
    var text: String?
    var attch: [MessageAttachment]
    
    func decrypt(key: AesKey) -> MessageEntity {
        let id = self.id
        let senderID = AesWrapper.decrypt(self.senderID, aesKey: key)
        let senderUsername = AesWrapper.decrypt(self.senderUsername, aesKey: key)
        let senderAvatar = AesWrapper.decrypt(self.senderAvatarURL, aesKey: key)
        var msg: String? = nil
        if let text = self.text {
            msg = AesWrapper.decrypt(text, aesKey: key)
        }
        var attchs: [MessageAttachment] = []
        for att in self.attch {
            attchs.append(att.decrypt(aesKey: key))
        }
        return MessageEntity.init(id: id!, senderID: senderID, senderUsername: senderUsername, senderAvatarURL: senderAvatar, time: self.time, msg: msg, attch: attchs)
    }
    
    mutating func finalize(msgID: String, time: Date) {
        self.id = msgID
        self.time = time
    }
    
    init(messageSendEntity: MessageSendEntity, date: Date!) {
        let _msg = messageSendEntity.msg
        let _attch = messageSendEntity.attch
        let _senderID = messageSendEntity.senderID
        self.senderID = _senderID
        self.text = _msg
        self.attch = _attch
        self.time = date
        
        self.senderUsername = "_username"
//        self.senderUsername = AppUser.username
    }
    
    init(id: String, senderID: String, senderUsername: String, senderAvatarURL: String, time: Date, msg: String?, attch: [MessageAttachment]) {
        self.id = id
        self.senderID = senderID
        self.senderUsername = senderUsername
        self.senderAvatarURL = senderAvatarURL
        self.time = time
        self.text = msg
        self.attch = attch
    }
    
    static func ==(lhs: MessageEntity, rhs: MessageEntity) -> Bool {
        return lhs.id == rhs.id
    }
}

extension MessageEntity: Decodable, Encodable {
    private enum EventApiResponseCodingKeys: String, CodingKey {
        case id = "id"
        case senderID = "senderID"
        case senderUsername = "senderUsername"
        case senderAvatarURL = "senderAvatar"
        case time = "time"
        case text = "text"
        case attch = "attch"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: EventApiResponseCodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id)
        self.senderID = try container.decode(String.self, forKey: .senderID)
        self.senderUsername = try container.decode(String.self, forKey: .senderUsername)
        self.senderAvatarURL = try container.decode(String.self, forKey: .senderAvatarURL)
        let _time = try container.decode(Double.self, forKey: .time)
        self.time = Date.init(timeIntervalSince1970: _time / 1000)
        self.text = try? container.decode(String.self, forKey: .text)
        self.attch = try container.decode([MessageAttachment].self, forKey: .attch)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: EventApiResponseCodingKeys.self)
        try container.encode(self.id, forKey: .id)
        try container.encode(self.senderID, forKey: .senderID)
        try container.encode(self.senderUsername, forKey: .senderUsername)
        try container.encode(self.senderAvatarURL, forKey: .senderAvatarURL)
        try container.encode(self.time, forKey: .time)
        try container.encode(self.text, forKey: .text)
        try container.encode(self.attch, forKey: .attch)
    }
}

struct MessageWrapper {
    var message: MessageEntity
    var avatarIsHidden: Bool = false
    var errorIndicatorIsHidden: Bool = true
    var loadingIndicatorIsHidden: Bool = true
    var isGroupped: Bool = false
    
    private var appUserID = LocalAuth.userID!
    
    var indexPath: IndexPath!
    
    var row: Row!

    init(message: MessageEntity, indexPath: IndexPath) {
        self.message = message
        self.indexPath = indexPath
        self.row = self.getTableRow()
    }
    
    init(message: MessageEntity, indexPath: IndexPath, isGroupped: Bool) {
        self.message = message
        self.indexPath = indexPath
        self.isGroupped = isGroupped
        self.row = self.getTableRow()
    }
    
    init(message: MessageEntity, isGroupped: Bool) {
        self.message = message
        self.message.senderUsername = "@" + self.message.senderUsername
        self.isGroupped = isGroupped
    }
    
    init(message: MessageEntity, avatarIsHidden: Bool = false, errorIndicatorIsHidden: Bool = true, loadingIndicatorIsHidden: Bool = true) {
        self.message = message
        self.message.senderUsername = "@" + self.message.senderUsername
        self.avatarIsHidden = avatarIsHidden
        self.errorIndicatorIsHidden = errorIndicatorIsHidden
        self.loadingIndicatorIsHidden = loadingIndicatorIsHidden
    }
}

extension MessageWrapper {
    func getTableRow() -> Row {
        var tw: Row!
        if self.message.senderID == self.appUserID {
            if self.message.attch.count == 0 {
                tw = TableRow<ChatLeftMsgOwnGrouppedCell>.init(item: self)
            } else if self.message.attch.count == 1 {
                if self.message.text == nil {
                    tw = TableRow<ChatLeftMsgOwnOneImageGrouppedCell>.init(item: self)
                } else {
                    tw = TableRow<ChatLeftMsgOwnOneImageTextGrouppedCell>.init(item: self)
                }
            } else if self.message.attch.count == 2 {
                if self.message.text == nil {
                    tw = TableRow<ChatLeftMsgOwnTwoImageGrouppedCell>.init(item: self)
                } else {
                    tw = TableRow<ChatLeftMsgOwnTwoImageTextGrouppedCell>.init(item: self)
                }
            } else if self.message.attch.count == 3 {
                if self.message.text == nil {
                    tw = TableRow<ChatLeftMsgOwnThreeImageGrouppedCell>.init(item: self)
                } else {
                    tw = TableRow<ChatLeftMsgOwnThreeImageTextGrouppedCell>.init(item: self)
                }
            } else if self.message.attch.count == 4 {
                if self.message.text == nil {
                    tw = TableRow<ChatLeftMsgOwnFourImageGrouppedCell>.init(item: self)
                } else {
                    tw = TableRow<ChatLeftMsgOwnFourImageTextGrouppedCell>.init(item: self)
                }
            } else {
                if self.message.text == nil {
                    tw = TableRow<ChatLeftMsgOwnMoreImageGrouppedCell>.init(item: self)
                } else {
                    tw = TableRow<ChatLeftMsgOwnMoreImageTextGrouppedCell>.init(item: self)
                }
            }
        } else {
            if !self.isGroupped {
                if self.message.attch.count == 0 {
                    tw = TableRow<ChatLeftMsgCell>.init(item: self)
                } else if self.message.attch.count == 1 {
                    if self.message.text == nil {
                        tw = TableRow<ChatLeftMsgOnlyOneImageCell>.init(item: self)
                    } else {
                        tw = TableRow<ChatLeftMsgOnlyOneImageTextCell>.init(item: self)
                    }
                } else if self.message.attch.count == 2 {
                    if self.message.text == nil {
                        tw = TableRow<ChatLeftMsgTwoImageCell>.init(item: self)
                    } else {
                        tw = TableRow<ChatLeftMsgTwoImageTextCell>.init(item: self)
                    }
                } else if self.message.attch.count == 3 {
                    if self.message.text == nil {
                        tw = TableRow<ChatLeftMsgThreeImageCell>.init(item: self)
                    } else {
                        tw = TableRow<ChatLeftMsgThreeImageTextCell>.init(item: self)
                    }
                } else if self.message.attch.count == 4 {
                    if self.message.text == nil {
                        tw = TableRow<ChatLeftMsgFourImageCell>.init(item: self)
                    } else {
                        tw = TableRow<ChatLeftMsgFourImageTextCell>.init(item: self)
                    }
                } else {
                    if self.message.text == nil {
                        tw = TableRow<ChatLeftMsgMoreImageCell>.init(item: self)
                    } else {
                        tw = TableRow<ChatLeftMsgMoreImageTextCell>.init(item: self)
                    }
                }
            } else {
                if self.message.attch.count == 0 {
                    tw = TableRow<ChatLeftMsgGrouppedCell>.init(item: self)
                } else if self.message.attch.count == 1 {
                    if self.message.text == nil {
                        tw = TableRow<ChatLeftMsgOnlyOneImageGrouppedCell>.init(item: self)
                    } else {
                        tw = TableRow<ChatLeftMsgOneImageTextGrouppedCell>.init(item: self)
                    }
                } else if self.message.attch.count == 2 {
                    if self.message.text == nil {
                        tw = TableRow<ChatLeftMsgTwoImageGrouppedCell>.init(item: self)
                    } else {
                        tw = TableRow<ChatLeftMsgTwoImageTextGrouppedCell>.init(item: self)
                    }
                } else if self.message.attch.count == 3 {
                    if self.message.text == nil {
                        tw = TableRow<ChatLeftMsgThreeImageGrouppedCell>.init(item: self)
                    } else {
                        tw = TableRow<ChatLeftMsgThreeImageTextGrouppedCell>.init(item: self)
                    }
                } else if self.message.attch.count == 4 {
                    if self.message.text == nil {
                        tw = TableRow<ChatLeftMsgFourImageGrouppedCell>.init(item: self)
                    } else {
                        tw = TableRow<ChatLeftMsgFourImageTextGrouppedCell>.init(item: self)
                    }
                } else {
                    if self.message.text == nil {
                        tw = TableRow<ChatLeftMsgMoreImageGrouppedCell>.init(item: self)
                    } else {
                        tw = TableRow<ChatLeftMsgMoreImageTextGrouppedCell>.init(item: self)
                    }
                }
            }
        }
        return tw
    }
}




