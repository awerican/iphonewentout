//
//  WelcomeViewController.swift
//  WentOut
//
//  Created by Fure on 26.10.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import UIKit
import SafariServices

class WelcomeViewContoller: UIViewController {
    
//    @IBOutlet weak var signupButton: UIButton!
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    @IBAction func termsOfUserAction(_ sender: UIButton) {
        showSafariVC(for: URL.Agreement.termsOfUser)
    }
    
    @IBAction func privacyPolicyAction(_ sender: UIButton) {
        showSafariVC(for: URL.Agreement.privacyPolicy)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension WelcomeViewContoller: SFSafariViewControllerDelegate {
    private func showSafariVC(for url: URL) {
        let safariVC = SFSafariViewController(url: url)
        safariVC.delegate = self
        present(safariVC, animated: true, completion: nil)
    }
}

















