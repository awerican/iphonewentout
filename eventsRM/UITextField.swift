//
//  UITextField.swift
//  WentOut
//
//  Created by Fure on 30.01.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    func setPlaceholderColor(color: UIColor) {
        if let placeholder = self.placeholder {
            self.attributedPlaceholder = NSAttributedString.init(string: placeholder, attributes: [NSAttributedStringKey.foregroundColor: color])
        }
    }
}
