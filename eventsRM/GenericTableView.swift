//
//  GenericsTableView.swift
//  WentOut
//
//  Created by Fure on 08.09.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import Foundation
import UIKit

protocol Reusable {}

protocol GenericTableView {
    typealias reuseID = String
    static var reuseIDs: [reuseID] { get }
    func getReuseID(by indexPath: IndexPath) -> reuseID
}

extension Reusable where Self: UITableViewCell  {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}

extension UITableViewCell: Reusable {}

//extension UITableView {
//    func register<T: UITableViewCell>(_ :T.Type) {
//
//        let reuseID = T.reuseIdentifier
//        print(reuseID)
//
//        register(UINib.init(nibName: reuseID, bundle: nil), forCellReuseIdentifier: reuseID)
//    }
//
////    func dequeueReusableCell<T: UITableViewCell>(forIndexPath indexPath: IndexPath) -> T {
////        print(T.reuseIdentifier)
////
////        guard let cell = dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as? T else {
////            fatalError("Could not deque cell")
////        }
////        return cell
////    }
//}

//class GTableView: UITableView, GenericTableView {
//    func getReuseID(by indexPath: IndexPath) -> GenericTableView.reuseID {
//        return ""
//    }
//
//    static var reuseIDs: [String] {
//        return []
//    }
//}
//
//extension GTableView {
//    func dequeueReusableCell<T: UITableViewCell>(forIndexPath indexPath: IndexPath) -> T {
//        print(T.reuseIdentifier)
//
//        guard let cell = dequeueReusableCell(withIdentifier: getReuseID(by: indexPath), for: IndexPath) as? T else {
//            fatalError()
//        }
//        return cell
//
////        guard let cell = dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as? T else {
////            fatalError("Could not deque cell")
////        }
////        return cell
//    }
//}

//////////////////////////////////////////////
























