//
//  SearchControllerParametersCellSep.swift
//  eventsRM
//
//  Created by Fure on 02.11.17.
//  Copyright © 2017 uzuner. All rights reserved.
//

import UIKit

class SearchControllerParametersCellSep: UICollectionViewCell {

    @IBOutlet var labelBackView: UIView!
    @IBOutlet var labelHashtag: UILabel!
    
    var currentCellSize: CGSize {
        get {
            return CGSize.init(width: labelBackView.frame.width + 5, height: labelBackView.frame.height + 5)
        }
    }
    
    func configCell(text: String) {
        self.labelHashtag.text = text
        self.layoutIfNeeded()
        self.labelBackView.makeOval(cornerRadius: self.labelBackView.frame.height / 2)
        
    }
}
