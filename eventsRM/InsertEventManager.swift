//
//  InsertEventManager.swift
//  WentOut
//
//  Created by Fure on 24.07.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import UIKit

enum InsertEventManagerError {
    case noTitle, noPhoto, noLocation, noTime
}

enum EventComponents {
    case avatar, title, type, location, time, hashtags, description
}

class InsertEventManager {
    var eventTitle: String?
    var eventAvatar: UIImage?
    var location: EventLocation?
    var description: String?
    var timeStart: Int?
    var eventType: EventType = .pub
    var hashtags: [String] = []
    
    var defaultValue: InsertEventManager?
    
    var isModified: Bool {
        get {
            if eventTitle != nil || eventAvatar != nil || location != nil ||
                description != nil || timeStart != nil || eventType != .pub || hashtags.count != 0 {
                return true
            }
            return false
        }
    }
    
    func _changeTitle(param: ChangeEventTitleParam) -> Downloader<SucceedFailedEntity, ChangeEventTitleParam> {
        return Downloader<SucceedFailedEntity, ChangeEventTitleParam>.init(resource: URL.Event.changeTitle, param: param)
    }
    
    func _changeTimestart(param: ChangeEventTimestartParam) -> Downloader<SucceedFailedEntity, ChangeEventTimestartParam> {
        return Downloader<SucceedFailedEntity, ChangeEventTimestartParam>.init(resource: URL.Event.changeTimestart, param: param)
    }
    
    func _changeDescription(param: ChangeEventDescrParam) -> Downloader<SucceedFailedEntity, ChangeEventDescrParam> {
        return Downloader<SucceedFailedEntity, ChangeEventDescrParam>.init(resource: URL.Event.changeDescription, param: param)
    }
    
    func _changeLocation(param: ChangeEventLocationParam) -> Downloader<SucceedFailedEntity, ChangeEventLocationParam> {
        return Downloader<SucceedFailedEntity, ChangeEventLocationParam>.init(resource: URL.Event.changeLocation, param: param)
    }
    
    func _changeHashtags(param: ChangeEventHashtagsParam) -> Downloader<SucceedFailedEntity, ChangeEventHashtagsParam> {
        return Downloader<SucceedFailedEntity, ChangeEventHashtagsParam>.init(resource: URL.Event.changeHashtags, param: param)
    }
    
    func _changeAvatar(param: ChangeEventAvatarParam) -> Downloader<SucceedFailedEntity, ChangeEventAvatarParam> {
        return Downloader<SucceedFailedEntity, ChangeEventAvatarParam>.init(resource: URL.Event.changeAvatar, param: param)
    }
    
    func saveChanges(eventID: String, userID: String, callback: @escaping (InsertEventManagerError?)->()) {
        guard let newTitle = self.eventTitle else {
            callback(.noTitle)
            return
        }
        
        guard let newPhoto = self.eventAvatar else {
            callback(.noPhoto)
            return
        }
        
        guard let newLocation = self.location else {
            callback(.noLocation)
            return
        }
        
        guard let newTimeStart = self.timeStart else {
            callback(.noTime)
            return
        }
        
        guard let defaultValue = defaultValue else {
            return
        }
        
        var changedComponents: [EventComponents] = []
        
        if newTitle != defaultValue.eventTitle {
            changedComponents.append(.title)
        }
        if newPhoto != defaultValue.eventAvatar {
            print("before changedComponents.append(.avatar)")
            changedComponents.append(.avatar)
        }
        if newLocation != defaultValue.location {
            changedComponents.append(.location)
        }
        if newTimeStart != defaultValue.timeStart {
            changedComponents.append(.time)
        }
        if self.eventType != defaultValue.eventType {
            changedComponents.append(.type)
        }
        if self.description != defaultValue.description {
            changedComponents.append(.description)
        }
        if self.hashtags != defaultValue.hashtags {
            changedComponents.append(.hashtags)
        }
        
        if changedComponents.count == 0 {
            callback(nil)
            return
        }
        
        var finishedComponents: Int = 0 {
            didSet {
                if finishedComponents == changedComponents.count {
                    callback(nil)
                }
            }
        }
        
        if changedComponents.contains(.title) {
            let param = ChangeEventTitleParam.init(userID: userID, eventID: eventID, title: self.eventTitle!)
            let req = _changeTitle(param: param)
            req.getFirst {
                finishedComponents += 1
            }
        }
        
        if changedComponents.contains(.time) {
            let param = ChangeEventTimestartParam.init(userID: userID, eventID: eventID, timeStart: self.timeStart!)
            print("timeStart =", self.timeStart!)
            let req = _changeTimestart(param: param)
            req.getFirst {
                finishedComponents += 1
            }
        }
        
        if changedComponents.contains(.description) {
            let param = ChangeEventDescrParam.init(userID: userID, eventID: eventID, description: self.description ?? "")
            let req = _changeDescription(param: param)
            req.getFirst {
                finishedComponents += 1
            }
        }
        
        if changedComponents.contains(.location) {
            let param = ChangeEventLocationParam.init(userID: userID, eventID: eventID, location: self.location!)
            let req = _changeLocation(param: param)
            req.getFirst {
                finishedComponents += 1
            }
        }
        
        if changedComponents.contains(.hashtags) {
            let param = ChangeEventHashtagsParam.init(userID: userID, eventID: eventID, hashtagName: self.hashtags)
            let req = _changeHashtags(param: param)
            req.getFirst {
                finishedComponents += 1
            }
        }
        
        if changedComponents.contains(.avatar) {
            let photoForUpload = UploadableMedia.init(image: self.eventAvatar!, compressionQuality: 0.5)
            
            Webservice.uploadAvatar(avatar: photoForUpload) { (process, _url) in
                guard let url = _url else {
                    finishedComponents += 1
                    return
                }
                
                let param = ChangeEventAvatarParam.init(userID: userID, eventID: eventID, photo: url)
                let req = self._changeAvatar(param: param)
                req.getFirst {
                    finishedComponents += 1
                }
            }
        }
    }
    
    func getParams(userID: String, callback: @escaping (InsertEventParameters?, InsertEventManagerError?)->()) {
        guard let title = self.eventTitle else {
            callback(nil, .noTitle)
            return
        }
        guard let photo = self.eventAvatar else {
            callback(nil, .noPhoto)
            return
        }
        guard let location = self.location else {
            callback(nil, .noLocation)
            return
        }
        
        guard let timeStart = self.timeStart else {
            callback(nil, .noTime)
            return
        }
        
        let photoForUpload = UploadableMedia.init(image: photo, compressionQuality: 0.5)
        
        Webservice.uploadAvatar(avatar: photoForUpload) { (process, _url) in
            guard let url = _url else {
                return
            }
//            print("add description =", description)
            print("add hashtags =", self.hashtags)

            let param = InsertEventParameters.init(userID: userID, title: title, photo: url, location: location, timeStart: timeStart, type: self.eventType, description: self.description ?? "", hashtagName: self.hashtags)
            callback(param, nil)
        }
    }
    
    init() { }
    
    init(eventTitle: String, eventAvatar: UIImage, location: EventLocation, description: String?, timeStart: Int, eventType: EventType, hashtags: [String]) {
        self.eventTitle = eventTitle
        self.eventAvatar = eventAvatar
        self.location = location
        self.description = description
        self.timeStart = timeStart
        self.eventType = eventType
        self.hashtags = hashtags
        
        self.defaultValue = InsertEventManager()
        self.defaultValue!.eventTitle = eventTitle
        self.defaultValue!.eventAvatar = eventAvatar
        self.defaultValue!.location = location
        self.defaultValue!.description = description
        self.defaultValue!.timeStart = timeStart
        self.defaultValue!.eventType = eventType
        self.defaultValue!.hashtags = hashtags
    }
}

























