//
//  UserProfileMediaBottomView.swift
//  WentOut
//
//  Created by Fure on 31.07.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import UIKit
import moa

class UserProfileMediaBottomView: UIView, Nibable {

    var bundleName = "UserProfileMediaBottomView"
    
    @IBOutlet var contentView: UIView!
    @IBOutlet var backgroundBlurView: UIVisualEffectView!
    @IBOutlet var eventImage: UIImageView!
    @IBOutlet var eventName: UILabel!
    @IBOutlet var guestLabel: UILabel!
    @IBOutlet var guestsQuantityBackground: UIView!
    @IBOutlet var eventDate: UILabel!
    @IBOutlet var mediaCreator: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupViewFromNib()
        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupViewFromNib()
        self.setupViews()
    }
    
    override func draw(_ rect: CGRect) {
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 8.0
        self.layer.shadowOpacity = 0.3
        self.layer.masksToBounds = false
        let bounds = CGRect.init(x: self.backgroundBlurView.frame.origin.x + 0, y: self.backgroundBlurView.frame.origin.y + 2, width: self.backgroundBlurView.frame.width, height: self.backgroundBlurView.frame.height)
        self.layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: 15).cgPath
        
        self.guestsQuantityBackground.makeOval(cornerRadius: self.guestsQuantityBackground.frame.height / 2)
    }
    
    lazy var guestQuantityTapGesture = UITapGestureRecognizer.init(target: self, action: #selector(guestQuantityTapGesture(_:)))
    
    @objc func guestQuantityTapGesture(_ gesture: UITapGestureRecognizer) {
        print("open guests list")
    }
    
    func configCell(eventImage: String, eventTitle: String, mediaCreator: String, friendsQuantity: Int, guestsQuantity: Int, date: Date) {
        self.eventImage.moa.url = eventImage
        self.eventName.text = eventTitle
        self.eventDate.text = date.timeDifferenceOnMap //change to date.timeDifference // PS user needs to see "2d ago" not "STARTED"
        self.mediaCreator.text = setMediaCreator(mediaCreator)
        self.guestLabel.text = setGuestButtonTitle(guest: guestsQuantity, friends: friendsQuantity)
        self.guestLabel.isUserInteractionEnabled = false
        self.guestsQuantityBackground.isUserInteractionEnabled = true
        self.guestsQuantityBackground.addGestureRecognizer(guestQuantityTapGesture)
    }
    
    func animateToAppear(constraint: NSLayoutConstraint, superview: UIView) {
        if #available(iOS 11.0, *) {
            constraint.constant = -superview.safeAreaInsets.bottom - UITabBar.height - 8.0
        } else {
            constraint.constant = -UITabBar.height - 8.0
        }
        UIView.animate(withDuration: 0.25) {
            superview.layoutIfNeeded()
        }
    }

    func animateToDisappear(constraint: NSLayoutConstraint, superview: UIView) {
        constraint.constant = self.backgroundBlurView.frame.height
        UIView.animate(withDuration: 0.25) {
            superview.layoutIfNeeded()
        }
    }
    
    private func setMediaCreator(_ nickname: String) -> String {
        return "@" + nickname
    }
    
    private func setGuestButtonTitle(guest: Int, friends: Int) -> String {
        if friends == 0 {
            if guest == 0 {
                return guest.description + " " + "guest"
            } else {
                return guest.description + " " + "guests"
            }
        } else {
            return friends.description + " " + "friends"
        }
    }
    
    private func setupViews() {
        self.backgroundColor = .clear
        self.contentView.isUserInteractionEnabled = false
        self.backgroundBlurView.makeOval(cornerRadius: self.backgroundBlurView.frame.height / 5)
        self.eventImage.makeOval(cornerRadius: self.eventImage.frame.height / 5)
    }
    
//    private func loadViewFromNib() {
//        self.contentView = UINib.init(nibName: self.bundleName, bundle: nil).instantiate(withOwner: self, options: nil).first as! UIView
//        self.contentView.frame = self.frame
//        self.addSubview(self.contentView)
//        self.backgroundColor = .clear
//    }
}
