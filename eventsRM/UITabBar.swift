//
//  UITabBar.swift
//  eventsRM
//
//  Created by Fure on 08.09.17.
//  Copyright © 2017 uzuner. All rights reserved.
//

import Foundation
import UIKit

extension UITabBar {
    
    static let height: CGFloat = 35.0
    
    func customSetup() {
        var tabFrame = self.frame
        tabFrame.size.height = UITabBar.height
        tabFrame.origin.y = UIScreen.main.bounds.height - UITabBar.height
        self.frame = tabFrame
        if let items = self.items {
            for item in items {
                item.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0)
            }
        }
        self.unselectedItemTintColor = UIColor.gray
        self.tintColor = UIColor.black
        self.shadowImage = UIImage()
        self.barTintColor = UIColor.clear
        self.backgroundImage = UIImage.init()
        self.isTranslucent = true
        let blurView = UIView.blurView(bounds: bounds, effect: .regular)
        blurView.tag = 1000
        self.insertSubview(blurView, at: 0)
    }
    
    func hideWithAnimation(completion: ((_ finish: Bool) -> ())?) {
        var frame = self.frame
        frame.origin.y = UIScreen.main.bounds.height + frame.size.height
        UIView.animate(withDuration: 0.25, animations: {
            [weak self] in
            self?.frame = frame
        }) { (finishAnim) in
            if finishAnim == true {
                completion?(true)
            }
        }
    }

    func showWithAnimation(completion: ((_ finish: Bool) -> ())?) {
        var frame = self.frame
        frame.origin.y = UIScreen.main.bounds.height - frame.size.height
        if self.frame.origin.y != frame.origin.y {
            UIView.animate(withDuration: 0.25, animations: {
                [weak self] in
                self?.frame = frame
            }) { (finishAnim) in
                if finishAnim == true {
                    completion?(true)
                }
            }
        }
    }
}
