//
//  URL.swift
//  WentOut
//
//  Created by Fure on 27.06.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import Foundation

extension URL {
    private static let homeIP = "192.168.0.29"
    
    private static let localhost = "localhost"
    private static let chatPort = 4100
    private static let mapPort = 4000
    
    static let remoteIP = "134.209.23.113"
    private static let remotePort = 80
    
    private static let globalIP = URL.remoteIP
    private static let globalPORT = URL.remotePort.description
    
    private static let host = "http://\(URL.globalIP):\(URL.globalPORT)"

    static let socketMapServerUrl = URL(string: "http://\(URL.globalIP):\(URL.mapPort)")!
    
//    static let socketChat = URL(string: "http://\(URL.globalIP):\(URL.chatPort)")!
    static let socketChat = URL(string: "http://134.209.23.113/")!

    
    static let uploadFile = URL.makeURL(path: "/request/upload")
    static let uploadEventAvatar = URL.makeURL(path: "/request/uploadAvatar")
    static let uploadMemory = URL.makeURL(path: "/request/uploadMemory")
    
    struct Reg {
        static let login = URL.makeURL(path: "/request/check/auth")
        static let first = URL.makeURL(path: "/request/insert/createUser")
        static let sec   = URL.makeURL(path: "/request/insert/updateEmailFirst")
        static let three = URL.makeURL(path: "/request/insert/updateEmailSecond")
    }
    
    struct Recover {
        static let one = URL.makeURL(path: "/request/update/forgotPassFirst")
        static let two = URL.makeURL(path: "/request/update/forgotPassSecond")
    }
    
//    struct Auth {
//        static let verify = URL.makeURL(path: "/request/check/verifyAuth")
//    }
    
    struct Map {
        static let eventMiniInfo  = URL.makeURL(path: "/request/select/worldEventShort")
        static let eventTopPanels = URL.makeURL(path: "/request/select/worldTop")
        static let eventMedium    = URL.makeURL(path: "/request/select/eventMedium")
        static let nearestEvents  = URL.makeURL(path: "/request/select/nearestEvents")
    }
    
    struct App {
        static let checkVersion = URL.makeURL(path: "/request/select/appVersion")
        static let pushToken = URL.makeURL(path: "/request/insert/pushToken")
        static let badgeToZero = URL.makeURL(path: "/request/update/badge")
    }
    
    struct Agreement {
        static let termsOfUser = URL.makeURL(path: "/termservice")
        static let privacyPolicy = URL.makeURL(path: "/privacy")
    }
    
    struct Event {
        static let insertEvent  = URL.makeURL(path: "/request/insert/createEvent")
        static let followingsList = URL.makeURL(path: "/request/select/userFollowersList")
        static let header = URL.makeURL(path: "/request/select/eventHeader")
        static let memories = URL.makeURL(path: "/request/select/eventMemories")
        static let go           = URL.makeURL(path: "/request/insert/createGo")
        static let insertMemory = URL.makeURL(path: "/request/insert/memory")
        static let eventNameByID = URL.makeURL(path: "/request/select/getEventName")
        static let invite = URL.makeURL(path: "/request/insert/invite")
        static let friendsList = URL.makeURL(path: "/request/select/listOfFriendsOnEvent")
        static let guestList = URL.makeURL(path: "/request/select/listOfGuests")
        static let changeTitle = URL.makeURL(path: "/request/update/title")
        static let changeTimestart = URL.makeURL(path: "/request/update/timeStart")
        static let changeDescription = URL.makeURL(path: "/request/update/description")
        static let changeLocation = URL.makeURL(path: "/request/update/location")
        static let changeAvatar = URL.makeURL(path: "/request/update/changeEventAvatar")
        static let changeHashtags = URL.makeURL(path: "/request/update/hashtag")
        static let selectHashtags = URL.makeURL(path: "/request/select/hashtags")
    }
    
    struct Story {
        static let open = URL.makeURL(path: "/request/select/story")
    }
    
    struct News {
        static let memory = URL.makeURL(path: "/request/select/news")
        static let memoryMini = URL.makeURL(path: "/request/select/newsMemories")
        static let feedback = URL.makeURL(path: "/request/select/feedback")
        static let personalFeedback = URL.makeURL(path: "/request/select/newFollowFeedback")
        static let userEventList = URL.makeURL(path: "/request/select/userEventList")
    }
    
    struct Search {
        static let events = URL.makeURL(path: "/request/search/getCategories")
        static let users = URL.makeURL(path: "/request/search/topUsers")
        static let findEvent = URL.makeURL(path: "/request/search/findEvent")
        static let findUser = URL.makeURL(path: "/request/search/findUser")
    }
    
    struct Block {
        static let memory = URL.makeURL(path: "/request/insert/blockMemory")
        static let user = URL.makeURL(path: "/request/insert/blockUser")
        static let event = URL.makeURL(path: "/request/insert/blockEvent")
    }

    struct UserProfile {
        static let username = URL.makeURL(path: "/request/select/userNickname")
        static let header = URL.makeURL(path: "/request/select/userHeader")
        static let events = URL.makeURL(path: "/request/select/eventListProfile")
        static let memory =  URL.makeURL(path: "/request/select/memoriesProfile")
        static let follow = URL.makeURL(path: "/request/insert/createFollow")
        static let followings = URL.makeURL(path: "/request/select/profileFollowings")
        static let followers = URL.makeURL(path: "/request/select/profileFollowers")
        
        struct Setting {
            static let editProfile =        URL.makeURL(path: "/request/select/editProfileSetting")
            static let updateDescription =  URL.makeURL(path: "/request/update/changeDescription")
            static let updateUsername =     URL.makeURL(path: "/request/update/changeNickname")
            static let updateEmail =        URL.makeURL(path: "/request/update/updateEmailFirst")
            static let validateEmail =      URL.makeURL(path: "/request/update/updateEmailSecond")
            static let changeUserAvatar =   URL.makeURL(path: "/request/update/changeUserAvatar")
            static let changePassword =     URL.makeURL(path: "/request/update/changeUserPassword")
            static let blockedList =        URL.makeURL(path: "/request/select/showBlockedUserList")
        }
    }
    
    private static func makeURL(path: String) -> URL {
        return URL.init(string: host + path)!
    }
}






































