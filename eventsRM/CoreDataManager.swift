//
//  CoreDataManager.swift
//  WentOut
//
//  Created by Fure on 28.11.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class CoreDataManager {
    
    let entityName = "CreateEventEntity"
    
    func saveEvent(event: InsertEventManager) throws {
        
        deleteAllData(entity: entityName)
        
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
        let entity = NSEntityDescription.entity(forEntityName: entityName, in: context)
        let newEntity = NSManagedObject(entity: entity!, insertInto: context)
        
        if let avatar = event.eventAvatar {
            if let data = UIImageJPEGRepresentation(avatar, 1.0) {
                newEntity.setValue(data, forKey: "avatar")
            }
        }
        
        if let location = event.location {
            newEntity.setValue(location.city, forKey: "city")
            newEntity.setValue(location.country, forKey: "country")
            newEntity.setValue(location.location, forKey: "location")
            newEntity.setValue(location.latitude, forKey: "latitude")
            newEntity.setValue(location.longitude, forKey: "longitude")
        }
        
        if let title = event.eventTitle {
            newEntity.setValue(title, forKey: "eventTitle")
        }
        
        newEntity.setValue(event.eventType.rawValue, forKey: "eventType")
        
        if let timeStart = event.timeStart {
            newEntity.setValue(timeStart, forKey: "timeStart")
        }
        
        if event.hashtags.count > 0 {
            var hashtags = String()
            for (index, hashtag) in event.hashtags.enumerated() {
                hashtags += hashtag
                if index != event.hashtags.count - 1 {
                    hashtags += " "
                }
            }
            newEntity.setValue(hashtags, forKey: "hashtags")
        }
        
        if let descr = event.description {
            newEntity.setValue(descr, forKey: "descr")
        }

        try context.save()
    }
    
    func getData() -> InsertEventManager? {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "CreateEventEntity")
        request.returnsObjectsAsFaults = false
        
        do {
            let result = try context.fetch(request)
            
            let insertEventManager = InsertEventManager()
            
            guard let data = (result as! [NSManagedObject]).first else {
                return nil
            }
            
            if let avatarData = data.value(forKey: "avatar") as? Data {
                insertEventManager.eventAvatar = UIImage.init(data: avatarData)
            }
            
            if let city = data.value(forKey: "city") as? String, let country = data.value(forKey: "country") as? String, let location = data.value(forKey: "location") as? String, let longitude = data.value(forKey: "longitude") as? Double, let latitude = data.value(forKey: "latitude") as? Double {
                let location = EventLocation.init(address: location, country: country, city: city, longitude: longitude, latitude: latitude)
                insertEventManager.location = location
            }
            
            if let title = data.value(forKey: "eventTitle") as? String {
                insertEventManager.eventTitle = title
            }
            
            if let hashtagsString = data.value(forKey: "hashtags") as? String {
                let hashtags : [String] = hashtagsString.components(separatedBy: " ")
                insertEventManager.hashtags = hashtags
                print("hashtagzz")
            }
            
            if let type = data.value(forKey: "eventType") as? String {
                insertEventManager.eventType = EventType.init(type: type)
            }
            
            if let time = data.value(forKey: "timeStart") as? Int {
                insertEventManager.timeStart = time
            }
            
            if let descr = data.value(forKey: "descr") as? String {
                insertEventManager.description = descr
            }
                
            return insertEventManager
        } catch {
            return nil
        }
    }
    
    func deleteEvent() {
        deleteAllData(entity: entityName)
    }
    
    private func deleteAllData(entity: String) {
        
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        fetchRequest.returnsObjectsAsFaults = false
        
        do {
            let results = try context.fetch(fetchRequest)
            for managedObject in results
            {
                let managedObjectData:NSManagedObject = managedObject as! NSManagedObject
                context.delete(managedObjectData)
                print("object was deleted")
            }
            try? context.save()
        } catch let error as NSError {
            print("Delete all data in \(entity) error : \(error) \(error.userInfo)")
        }
    }
}















