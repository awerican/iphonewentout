//
//  TableViewCellSecond.swift
//  eventsRM
//
//  Created by Fure on 28.10.17.
//  Copyright © 2017 uzuner. All rights reserved.
//

import UIKit

struct TableViewCellModel {
    private var imageCache: NSCache<NSURL, UIImage>
    
    var userAvatarURL: String
    var username: String
    var eventTitle: String
    var time: String
    var eventAvatarURL: String
    var location: String
    var guests: String
    
    init(imageCache: NSCache<NSURL, UIImage>, userAvatarURL: String, username: String, eventTitle: String, time: String, eventAvatarURL: String, location: String, guests: String) {
        self.imageCache = imageCache
        
        self.userAvatarURL = userAvatarURL
        self.username = username
        self.eventTitle = eventTitle
        self.time = time
        self.eventAvatarURL = eventAvatarURL
        self.location = location
        self.guests = guests
    }
    
    func getUserAvatar(completion: @escaping (UIImage?)->()) {
        Webservice.loadImage(url: self.userAvatarURL, completion: completion)
//        Webservice.loadImageIfNeeded(imageCache: self.imageCache, url: self.userAvatarURL, completion: completion)
    }
    
    func getEventAvatar(completion: @escaping (UIImage?)->()) {
        Webservice.loadImage(url: self.eventAvatarURL, completion: completion)
//        Webservice.loadImageIfNeeded(imageCache: self.imageCache, url: self.eventAvatarURL, completion: completion)
    }
    
    private func downloadIfNeeded(imageCache: NSCache<NSURL, UIImage>, url: URL, completion: @escaping (UIImage?)->()) {
        if let image = imageCache.object(forKey: url as NSURL) {
            completion(image)
        } else {
            
            Webservice.loadImage(url: url.absoluteString) { (_image) in
                if let image = _image {
                    self.imageCache.setObject(image, forKey: url as NSURL)
                    completion(image)
                } else {
                    completion(nil)
                }
            }
        }
    }
}

protocol TableViewCellSecondDelegate: class {
    func avatarWasTapped(at indexPath: IndexPath)
    func usernameWasTapped(at indexPath: IndexPath)
}

class TableViewCellSecond: UITableViewCell {

    @IBOutlet var basisView: UIView!
    @IBOutlet var userAvatar: UIImageView!
    @IBOutlet var eventAvatar: UIImageView!
//    @IBOutlet var buttonLeft: UIButton!
    @IBOutlet var buttonRight: UIButton!
    @IBOutlet var userNickname: UILabel!
    @IBOutlet var eventTitle: UILabel!
    @IBOutlet var eventTime: UILabel!
    @IBOutlet var location: UILabel!
    @IBOutlet var guests: UILabel!
    
    var delegate: TableViewCellSecondDelegate?
    
    let leadingConstant: CGFloat = 55.0 / 2
    
    var userImage: UIImage?
    var eventImage: UIImage?
    
    var currentSize: CGSize {
        get {
            return CGSize.init(width: UIScreen.width - 2 * leadingConstant, height: 223)
//            self.frame.size.width = UIScreen.main.bounds.width
//            self.layoutIfNeeded()
//            return CGSize.init(width: UIScreen.main.bounds.width, height: self.basisView.frame.maxY + 10)
        }
    }
    
    func configNib(username: String?, eventTitle: String?, time: String?, location: String?, guests: String?) {
        self.selectionStyle = .none
        self.userNickname.text = username
        self.eventTitle.text = eventTitle
        self.eventTime.text = time
        self.location.text = location
        self.guests.text = guests
    }
    
    func configNib() {
        self.selectionStyle = .none
    }
    
    func config(model: TableViewCellModel) {
        self.selectionStyle = .none
        self.userNickname.text = model.username
        self.eventTitle.text = model.eventTitle
        self.eventTime.text = model.time
        self.location.text = model.location
        self.guests.text = model.guests
        model.getUserAvatar { (image) in
            self.userAvatar.image = image
        }
        model.getEventAvatar { (image) in
            self.eventAvatar.image = image
        }
        
        userAvatar.isUserInteractionEnabled = true
        let avatarGesture = UITapGestureRecognizer.init(target: self, action: #selector(avatarWasTapped))
        userAvatar.addGestureRecognizer(avatarGesture)
        
        userNickname.isUserInteractionEnabled = true
        let usernameGesture = UITapGestureRecognizer.init(target: self, action: #selector(usernameWasTapped))
        userNickname.addGestureRecognizer(usernameGesture)
        
    }
    
    @objc private func avatarWasTapped() {
        if let cv = superview as? UITableView {
            if let ip = cv.indexPath(for: self) {
                delegate?.avatarWasTapped(at: ip)
            }
        }
    }
    
    @objc private func usernameWasTapped() {
        if let cv = superview as? UITableView {
            if let ip = cv.indexPath(for: self) {
                delegate?.usernameWasTapped(at: ip)
            }
        }
    }

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        contentView.layer.shadowColor = UIColor.lightGray.cgColor
        contentView.layer.shadowOffset = CGSize.zero
        contentView.layer.shadowRadius = 3.0
        contentView.layer.shadowOpacity = 0.3
        contentView.layer.masksToBounds = false
        let bounds = CGRect.init(x: basisView.frame.origin.x + 0, y: basisView.frame.origin.y + 2, width: basisView.frame.width, height: basisView.frame.height)
        contentView.layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: 15).cgPath
        
        basisView.makeOval(cornerRadius: 15)
        userAvatar.makeCircle()
        eventAvatar.makeOval(cornerRadius: eventAvatar.frame.height / 5)
//        buttonLeft.makeOval(cornerRadius: buttonLeft.frame.height / 4)
        buttonRight.makeOval(cornerRadius: buttonRight.frame.height / 4)
    }
}
