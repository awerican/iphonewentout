//
//  CEStatusViewController.swift
//  WentOut
//
//  Created by Fure on 26.07.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import UIKit

protocol CEStatusVCDelegate: class {
    func retry()
}

class CEStatusViewController: UIViewController {
    
    @IBOutlet weak var blurView: UIVisualEffectView!
    
    weak var delegate: CEStatusVCDelegate?
    
    @IBAction func buttonAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        delegate?.retry()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupViews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
}

private extension CEStatusViewController {
    private func setupViews() {
        blurView.makeOval(cornerRadius: 20.0)
        blurView.contentView.makeOval(cornerRadius: 6.0)
        blurView.contentView.layer.addBorder(edge: [.all], color: UIColor.gray, thickness: 1.0)
    }
}





















