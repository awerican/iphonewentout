//
//  BarController2.swift
//  eventsRM
//
//  Created by Fure on 29.11.16.
//  Copyright © 2016 uzuner. All rights reserved.
//
import UIKit
import MapKit
import CoreLocation
import Mapbox

typealias AnnotationHash = Int
typealias EventId = String

class WorldViewModel {
    var networkViewIsVisible: Box<Bool?> = Box(nil)
}

class WorldViewController: UIViewController, UINavigationControllerDelegate {
    var mapView: MGLMapView!
    var eventAmountView: EventAmountView!
    var defaultStateGesture: UITapGestureRecognizer!
    var bottomView: WorldBottomView!
    var worldTableVC: WorldTableViewController?
    
    struct Constants {
        struct Segues {
            static let toEventPage = "eventSegue"
        }
    }
    
    var userLocation: CLLocationCoordinate2D? {
        didSet {
            if let location = userLocation {
                mapView.setCenter(location, zoomLevel: 12, animated: true)
            }
        }
    }
    
    var locationManager: CLLocationManager!
    
    var annotationDidSelected: Bool = false
    var eventAndAnnotation: [EventId: MapEvent] = [:]
    var regionNeverChanged: Bool = true
    var events: Set<MapEvent> = []
    
    let imageCache = NSCache<NSURL, UIImage>()
    
    var annotationTouchesBegan = false
    
    var selectedEvent: MapEventMini?
    
    var currentZoom: Int {
        get {
            return Int(self.mapView.zoomLevel.rounded(.down))
        }
    }
    
    lazy var statusBar: UIVisualEffectView = {
        let view = UIVisualEffectView()
        view.translatesAutoresizingMaskIntoConstraints = false
        let effect = UIBlurEffect.init(style: .regular)
        view.effect = effect
        return view
    }()
    
    lazy var networkView: InternetErrorView = {
        let view = InternetErrorView.init(frame: CGRect.zero)
        view.autoresizingModeOn()
        return view
    }()
    
    lazy var widthNetworkViewConstraint: NSLayoutConstraint = {
        return NSLayoutConstraint.init(item: self.networkView, attribute: .width, relatedBy: .equal, toItem: self.view, attribute: .width, multiplier: 1.0, constant: 0.0)
    }()
    
    lazy var centerXNetworkViewConstraint: NSLayoutConstraint = {
        return NSLayoutConstraint.init(item: self.networkView, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1.0, constant: 0.0)
    }()
    
    lazy var heightNetworkViewConstraint: NSLayoutConstraint = {
        return NSLayoutConstraint.init(item: self.networkView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1.0, constant: InternetErrorView.height)
    }()
    
    lazy var topNetworkViewConsraint: NSLayoutConstraint = {
        return NSLayoutConstraint.init(item: self.networkView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1.0, constant: 0.0)
    }()
    
    var viewModel = WorldViewModel()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.askForEventsByRegion(self.mapView)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.view.frame.size.height = UIScreen.main.bounds.height
//        self.navigationController?.navigationBar.isHidden = true
        self.navigationController!.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        performUI(dependsOn: self.status)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupNetWorking()
        setupConstraints()
        retrieveUserLocation()
    }
    
    private func setupConstraints() {
        self.view.addSubview(self.networkView)
        NSLayoutConstraint.activate([self.widthNetworkViewConstraint, self.heightNetworkViewConstraint, self.topNetworkViewConsraint, self.centerXNetworkViewConstraint])
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        SocketMapManager.shared.closeConnection()
        self.events = []
        self.regionNeverChanged = true
        if let annotations = self.mapView.annotations {
            self.mapView.removeAnnotations(annotations)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let id = segue.identifier else {
            return
        }
        switch id {
        case Constants.Segues.toEventPage:
            if let destVC = segue.destination as? EventProfileViewController {
                if let eventID = sender as? String {
                    print("eventID =", eventID)
                    destVC.eventID = eventID
                }
            }
        default:
            break
        }
    }
}

extension WorldViewController: CLLocationManagerDelegate {
    private func retrieveUserLocation() {
        if (CLLocationManager.locationServicesEnabled()) {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last! as CLLocation
        
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        
        userLocation = center
        
        print("user location =", userLocation ?? "error")
        
        locationManager.stopUpdatingLocation()
    }
}

extension WorldViewController: InternetObserverDelegate {
    func reachabilityStatusHasChanged(to status: Network.Status) {
        performUI(dependsOn: status)
        self.askForEventsByRegion(self.mapView)
    }
    
    func performUI(dependsOn status: Network.Status?) {
        guard let status = status else { return }
        switch status {
        case .unreachable:
            if self.topNetworkViewConsraint.constant != self.heightNetworkViewConstraint.constant + self.topLayoutGuide.length {
                self.topNetworkViewConsraint.constant = self.heightNetworkViewConstraint.constant + self.topLayoutGuide.length
                UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseIn, animations: {
                    [weak self] in
                    self?.view.layoutIfNeeded()
                })
            }
        default:
            if self.topNetworkViewConsraint.constant != 0.0 {
                self.topNetworkViewConsraint.constant = 0.0
                UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseIn, animations: {
                    [weak self] in
                    self?.view.layoutIfNeeded()
                })
            }
        }
    }
}

extension WorldViewController: UIGestureRecognizerDelegate {
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        switch gestureRecognizer {
        case self.defaultStateGesture:
            if self.annotationTouchesBegan == true {
                self.annotationTouchesBegan = false
            } else {
                self.selectedEvent = nil
            }
        default:
            break
        }
        return false
    }
}

//NETWORKING
extension WorldViewController {
    func setupNetWorking() {
        SocketMapManager.shared.delegate = self
    }}

// VISUALS
extension WorldViewController {
    func setupViews() {
        self.view.backgroundColor = .black
        initSetupMapView()
        initSetupEventAmountView()
        initSetupBottomView()
        initSetupGestures()
        setupStatusBarView()
        setupInfoButton()
    }
    
    private func setupInfoButton() {
        let button = UIButton.init(type: .infoLight)
        
        button.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(button)
        
        button.topAnchor.constraint(equalTo: self.topLayoutGuide.bottomAnchor, constant: 10.0).isActive = true
        button.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 10.0).isActive = true
        
        button.addTarget(self, action: #selector(infoButtonAction), for: .touchUpInside)
    }
    
    private func initSetupMapView() {
        mapView = MGLMapView(frame: UIScreen.main.bounds)
        mapView.allowsRotating = false
        mapView.allowsTilting = false
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.delegate = self
        view.insertSubview(self.mapView, at: 0)
    }
    
    private func initSetupEventAmountView() {
        self.eventAmountView = EventAmountView.init(frame: CGRect.zero)
        self.eventAmountView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(eventAmountView)
        
        self.eventAmountView.widthAnchor.constraint(equalToConstant: self.eventAmountView.visualEffectView.frame.width).isActive = true
        self.eventAmountView.heightAnchor.constraint(equalToConstant: self.eventAmountView.visualEffectView.frame.height).isActive = true
        self.eventAmountView.topAnchor.constraint(equalTo: self.topLayoutGuide.bottomAnchor, constant: 10.0).isActive = true
        self.eventAmountView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -10.0).isActive = true
    }
    
    private func initSetupBottomView() {
        let getLongPressGesture = { () -> UILongPressGestureRecognizer in
            let gesture = UILongPressGestureRecognizer(target: self, action: #selector(self.longPressGestureHandler(_:)))
            gesture.minimumPressDuration = 0.0
            return gesture
        }
        self.bottomView = WorldBottomView()
        self.bottomView.translatesAutoresizingMaskIntoConstraints = false
        self.bottomView.addGestureRecognizer(getLongPressGesture())
        self.view.addSubview(self.bottomView)
        
        self.bottomView.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 1.0, constant: 0.0)
            .isActive = true
        self.bottomView.heightAnchor.constraint(equalToConstant: 45.0)
            .isActive = true
        self.bottomView.bottomAnchor.constraint(equalTo: self.bottomLayoutGuide.topAnchor, constant: -UITabBar.height)
            .isActive = true
        self.bottomView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0.0)
            .isActive = true
    }

    private func initSetupGestures() {
        self.defaultStateGesture = UITapGestureRecognizer.init()
        self.defaultStateGesture.delegate = self
        self.mapView.addGestureRecognizer(self.defaultStateGesture)
    }
    
    private func setupStatusBarView() {
        self.view.addSubview(self.statusBar)
        self.statusBar.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 1.0).isActive = true
        self.statusBar.heightAnchor.constraint(equalTo: self.topLayoutGuide.heightAnchor, constant: 0.0).isActive = true
        self.statusBar.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        self.statusBar.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
    }
}

extension WorldViewController {
    @objc private func infoButtonAction() {
        let alert = UIAlertController.init(title: nil, message: "You can see events taking place within 24 hours", preferredStyle: .actionSheet)
        
        let okAction = UIAlertAction.init(title: "Got It", style: .cancel) { (action) in
            print("close info alert menu")
        }
        
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
}








