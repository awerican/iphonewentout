//
//  EmailValidation.swift
//  WentOut
//
//  Created by Fure on 30.08.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import Foundation

enum EmailValidate: String {
    case correct            = "Validated."
    case emailDoNotChanged  = "The email is the same."
    case isNotAnEmail       = "This string is not an email"
}

class EmailChecker {
    private let email: String?
    
    init(oldEmail: String?) {
        self.email = oldEmail
    }
    
    func validate(email: String) -> EmailValidate {
        if email == self.email {
            return EmailValidate.emailDoNotChanged
        } else if EmailChecker.isValidEmail(email) {
            return EmailValidate.correct
        } else {
            return EmailValidate.isNotAnEmail
        }
    }

    static func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
}
