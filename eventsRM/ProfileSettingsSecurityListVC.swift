//
//  ProfileSettingsSecurityListVC.swift
//  WentOut
//
//  Created by Fure on 26.09.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

class ProfileSettingsSecurityListVC: UICollectionViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupCollectionView()
        setupNavBar()
    }
}

extension ProfileSettingsSecurityListVC {
    private func setupNavBar() {
        navigationItem.title = "Security"
    }
    
    private func setupView() {
        view.backgroundColor = UIColor.init(hex: "FAFAFA")
    }
    
    private func setupCollectionView() {
        collectionView?.backgroundColor = .clear
        for id in reuseIDs {
            collectionView?.register(UINib.init(nibName: id, bundle: nil), forCellWithReuseIdentifier: id)
        }
    }
}

extension ProfileSettingsSecurityListVC: UICollectionViewDelegateFlowLayout {
    
    var reuseIDs: [String] {
        get {
            return ["ProfileSettingsArrowCell"]
        }
    }

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        let controller = ProfileSettingsChangePassVC.init(collectionViewLayout: layout)
        self.navigationController?.pushViewController(controller, animated: true)
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIDs[0], for: indexPath) as! ProfileSettingsArrowCell
        cell.configCell(title: "Change a Password")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return ProfileSettingsArrowCell.currentSize
    }
}





















