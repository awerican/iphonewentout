//
//  UserProfileEvent3CollectionView.swift
//  WentOut
//
//  Created by Fure on 07.11.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

class UserProfileEvent3CollectionView: UICollectionViewCell {

    @IBOutlet weak var eventTitle: UILabel!
    
    @IBOutlet weak var eventTime: UILabel!
    @IBOutlet weak var eventImage: UIImageView!
    
    @IBOutlet weak var eventLocation: UILabel!
//    @IBOutlet weak var openButtonBackview: UIView!
    @IBOutlet weak var eventGuests: UILabel!
    
    var defaultColor: UIColor = UIColor.white
    
//    @IBOutlet weak var openButton: UIButton!
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.contentView.backgroundColor = UIColor(hex: "E4E4E4")
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
        self.contentView.backgroundColor = defaultColor
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        self.contentView.backgroundColor = defaultColor
    }
    
    static var searchCurrentSize: CGSize {
        get {
            let width = UIScreen.width - 17 - 17
            return CGSize.init(width: width, height: width / 3)
        }
    }
    
    static var currentSize: CGSize {
        get {
//            return CGSize.init(width: UIScreen.width, height: 90.0)
            return CGSize.init(width: UIScreen.width, height: 8 + UIScreen.width / 3)
        }
    }
    
    override func draw(_ rect: CGRect) {
        eventImage.makeOval(cornerRadius: eventImage.frame.height / 6)
    }
    
    func configCell(model: EventPanel) {
        print("model.guests.count =", model.guests)
        eventTitle.text = model.title
        eventTime.text = model.time.eventPanel1
        eventLocation.text = model.location.location
        if model.guests == 0 {
            let str = NSLocalizedString("guest", comment: "")
            eventGuests.text = model.guests.description + " " + str
        } else if model.guests == 1 {
            let str = NSLocalizedString("guest", comment: "")
            eventGuests.text = model.guests.description + " " + str
        } else if model.guests < 5 {
            let str = NSLocalizedString("guest2to4", comment: "")
            eventGuests.text = model.guests.description + " " + str
        } else {
            let str = NSLocalizedString("guest5more", comment: "")
            eventGuests.text = model.guests.description + " " + str
        }
        Webservice.loadImage(url: model.eventPhoto) { (_image) in
            if let image = _image {
                self.eventImage.image = image
            }
        }
    }
}
