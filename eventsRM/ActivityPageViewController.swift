//
//  ActivityPageViewController.swift
//  WentOut
//
//  Created by Fure on 06.07.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

protocol DefaultStateVCDelegate: class {
    func scrollToTop()
}

class ActivityPageViewController: UIPageViewController, UIPageViewControllerDelegate {

    lazy var viewControllerList: [UIViewController] = {
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        let vc1 = sb.instantiateViewController(withIdentifier: "ActivityViewController")
//        let vc2 = sb.instantiateViewController(withIdentifier: "MyFeedbackViewController")
//        return [vc1, vc2]
        return [vc1]
    }()
    
//    @IBAction func openFeedback(_ sender: UIBarButtonItem) {
//        self.setViewControllers([viewControllerList[1]], direction: .forward, animated: true, completion: nil)
//    }
    
    func _setBadgeToZero(param: UserIDParameter) -> Downloader<SucceedFailedEntity, UserIDParameter> {
        return Downloader<SucceedFailedEntity, UserIDParameter>.init(resource: URL.App.badgeToZero, param: param)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.view.frame.size.height = UIScreen.main.bounds.height
        self.navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupVC()
        setBadgeToZero()
    }
}

extension ActivityPageViewController {
    private func setupVC() {
        self.delegate = self
        self.dataSource = self
        let firstViewController = viewControllerList[0]
        self.setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
        self.view.backgroundColor = UIColor.white
        //        for view in self.view.subviews {
        //            if let scrollView = view as? UIScrollView {
        //                scrollView.delegate = self
        //            }
        //        }
    }
    
    private func setBadgeToZero() {
        let req = _setBadgeToZero(param: UserIDParameter.init(userID: LocalAuth.userID!))
        req.getFirst {
            if let data = req.data {
                switch data.status {
                case .succeed:
                    UIApplication.shared.applicationIconBadgeNumber = 0
                    print("SUCCESS: badge was set to zero")
                case .failed:
                    print("ERROR: badge was set to zero")
                }
            }
        }
    }
}

extension ActivityPageViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let vcIndex = viewControllerList.index(of: viewController) else {
            return nil
        }
        let previousIndex = vcIndex + 1
        guard previousIndex >= 0 else {
            return nil
        }
        guard viewControllerList.count > previousIndex else {
            return nil
        }
        return viewControllerList[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let vcIndex = viewControllerList.index(of: viewController) else {
            return nil
        }
        let nextIndex = vcIndex - 1
        guard nextIndex >= 0 else {
            return nil
        }
        guard viewControllerList.count != nextIndex else {
            return nil
        }
        guard viewControllerList.count > nextIndex else {
            return nil
        }
        return viewControllerList[nextIndex]
    }
}


