//
//  MyFeedbackViewController.swift
//  WentOut
//
//  Created by Fure on 06.07.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import UIKit

class MyFeedbackViewController: UIViewController {

    var collectionView: UICollectionView!
    
    let feedbackDownloader = Downloader<[FeedbackEntity], SelectNews>.init(resource: URL.News.personalFeedback, param: SelectNews.init(userID: LocalAuth.userID!))
    
    let heightForItem: CGFloat = 60.0
    
    lazy var activityView: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView.init(frame: CGRect.init(x: 0, y: self.navigationController?.navigationBar.frame.maxY ?? 100.0, width: UIScreen.width, height: heightForItem))
        indicator.activityIndicatorViewStyle = .gray
        return indicator
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupNetworking()
    }
}

extension MyFeedbackViewController {

    private func removeActivityView() {
        activityView.removeFromSuperview()
    }
    
    private func setupViews() {
        setupCollectionView()
    }
    
    private func setupNetworking() {
        downloadFeedback()
    }
    
    private func downloadFeedback() {
        feedbackDownloader.getFirst {
            self.collectionView?.reloadSections([0])
        }
    }
    
    private func setupCollectionView() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        collectionView = UICollectionView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.width, height: UIScreen.height), collectionViewLayout: layout)
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.backgroundColor = UIColor.white
        collectionView.contentInset.bottom = UITabBar.height + 6.0
        
        for reuseID in self.reuseIds {
            self.collectionView!.register(UINib.init(nibName: reuseID, bundle: nil), forCellWithReuseIdentifier: reuseID)
        }
        
        self.view.addSubview(collectionView)
    }
}

extension MyFeedbackViewController: DefaultStateVCDelegate {
    func scrollToTop() {
        if collectionView.numberOfItems(inSection: 0) > 0 {
            collectionView.scrollToItem(at: IndexPath.init(row: 0, section: 0), at: .top, animated: true)
        }
    }
}

extension MyFeedbackViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    var reuseIds: [String] {
        get {
            return ["FeedbackCellCat1", "CollectionViewErrorImageCell"]
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let _ = self.feedbackDownloader.data else {
            return 0
        }
        if feedbackDownloader.firstDownloadIsFinished {
            if let count = self.feedbackDownloader.data?.count {
                if count == 0 {
                    return 1
                } else {
                    return count
                }
            } else {
                return 1
            }
        } else {
            return self.feedbackDownloader.data?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let data = self.feedbackDownloader.data {
            if data.count == 0 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIds[1], for: indexPath) as! CollectionViewErrorImageCell
                cell.configCell(image: #imageLiteral(resourceName: "errorNoData"))
                return cell
            } else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIds[0], for: indexPath) as! FeedbackCellCat1
                cell.configCell(model: feedbackDownloader.data![indexPath.row])
                cell.delegate = self
                return cell
            }
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIds[1], for: indexPath) as! CollectionViewErrorImageCell
            cell.configCell(image: #imageLiteral(resourceName: "errorNoData"))
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let feedback = self.feedbackDownloader.data else {
            return
        }
        // вылетало тут
        if indexPath.row == feedback.count - 1 {
            self.feedbackDownloader.getNext(section: 0) { (paths) in
                self.collectionView?.insertItems(at: paths)
            }
        }
    }
}

extension MyFeedbackViewController: FeedbackCellCatDelegate {
    func leftImageWasTapped(at indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
        controller.profileOwnerID = self.feedbackDownloader.data?[indexPath.row].creatorID
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func rightImageWasTapped(at indexPath: IndexPath, category: FeedbackCategory) {}
}

extension MyFeedbackViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let feedbackSize = CGSize.init(width: UIScreen.width, height: heightForItem)
        guard let data = self.feedbackDownloader.data else {
            if feedbackDownloader.firstDownloadIsFinished {
                return CollectionViewErrorImageCell.currentSize
            }
            return CGSize.zero
        }
        if data.count == 0 {
            return CollectionViewErrorImageCell.currentSize
        } else {
            return feedbackSize
        }
    }
}












