//
//  SearchControllerEventWrapperCell.swift
//  WentOut
//
//  Created by Fure on 11.07.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

enum SearchControllerPageType {
    case user, event
}

protocol SearchControllerEventWrapperCellDelegate: class {
    func didSelectCell(at indexPath: IndexPath, pageType: SearchControllerPageType)
}

protocol SearchControllerEventCellSepDelegate: class {
    func avatarWasPressed(at indexPath: IndexPath)
    func usernameWasPressed(at indexPath: IndexPath)
}

class SearchControllerEventWrapperCell: UICollectionViewCell {
    
    @IBOutlet weak var categoryName: UILabel!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    weak var delegate: SearchControllerEventWrapperCellDelegate?
    
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    
    static var currentSize: CGSize {
        get {
//            return CGSize.init(width: UIScreen.width, height: 269)
//            let cellWidth = UIScreen.width - 17 - 17
//            let cellHeight = 8 + cellWidth / 3
            
            let height = UserProfileEvent3CollectionView.searchCurrentSize.height
            let bottomSpace = CGFloat(15.0)
            return CGSize.init(width: UIScreen.width, height: 6 + 35 + 6 + height + bottomSpace)
//            return CGSize.init(width: UIScreen.width, height: 6 + 35 + 6 + 130)
        }
    }
    
    var model: EventCategoryEntity? {
        didSet {
            categoryName.text = model?.name
            setupCollectionView()
            collectionViewHeight.constant = UserProfileEvent3CollectionView.searchCurrentSize.height
        }
    }
}

extension SearchControllerEventWrapperCell {
    private func setupCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.showsHorizontalScrollIndicator = false
        for reuseID in reuseIDs {
            collectionView.register(UINib.init(nibName: reuseID, bundle: nil), forCellWithReuseIdentifier: reuseID)
        }
    }
}

extension SearchControllerEventWrapperCell: UICollectionViewDataSource {
    
    var reuseIDs: [String] {
        get {
            return ["UserProfileEvent3CollectionView"]
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let model = self.model {
            return model.events.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIDs[0], for: indexPath) as! UserProfileEvent3CollectionView
        cell.configCell(model: model!.events[indexPath.row])
        cell.defaultColor = UIColor.init(hex: "FAFAFA")
//        cell.delegate = self
        return cell
    }
}

extension SearchControllerEventWrapperCell: UICollectionViewDelegate, SearchControllerEventCellSepDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = superview as? UICollectionView {
            if let _indexPath = cell.indexPath(for: self) {
                let ip = IndexPath.init(row: indexPath.row, section: _indexPath.row)
                self.delegate?.didSelectCell(at: ip, pageType: .event)
            }
        }
    }
    func avatarWasPressed(at indexPath: IndexPath) {
        if let cell = superview as? UICollectionView {
            if let _indexPath = cell.indexPath(for: self) {
                let ip = IndexPath.init(row: indexPath.row, section: _indexPath.row)
                self.delegate?.didSelectCell(at: ip, pageType: .user)
            }
        }
    }
    func usernameWasPressed(at indexPath: IndexPath) {
        if let cell = superview as? UICollectionView {
            if let _indexPath = cell.indexPath(for: self) {
                let ip = IndexPath.init(row: indexPath.row, section: _indexPath.row)
                self.delegate?.didSelectCell(at: ip, pageType: .user)
            }
        }
    }
}

extension SearchControllerEventWrapperCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize.init(width: UIScreen.width - 17 - 17, height: 130)
        return UserProfileEvent3CollectionView.searchCurrentSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//        return UIEdgeInsets.init(top: 0, left: 17.0, bottom: 0, right: 17.0)
        return UIEdgeInsets.init(top: 0, left: 14.0, bottom: 0, right: 14.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
}









