//
//  ReportSecViewController.swift
//  WentOut
//
//  Created by Fure on 21.10.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import UIKit

class ReportSecViewController: UICollectionViewController {
    
    var reportReason: String!
    var downloader: Downloadable!
    
//    lazy var _report: Downloader<SucceedFailedEntity, BlockMemoryParameters> = {
//        return Downloader<SucceedFailedEntity, BlockMemoryParameters>.init(resource: URL.Block.memory, param: BlockMemoryParameters.init(userID: LocalAuth.userID!, memoryID: ""))
//    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCV()
        setupNC()
    }
}

extension ReportSecViewController {
    private func setupNC() {
        navigationItem.title = "Report"
    }
    
    private func setupCV() {
        collectionView?.backgroundColor = .white
        for id in reuseIDs {
            collectionView?.register(UINib.init(nibName: id, bundle: nil), forCellWithReuseIdentifier: id)
        }
    }
}

extension ReportSecViewController: ReportSecViewControllerCellDelegate {
    func reportAction(sender: UIButton) {
//        sender.isUserInteractionEnabled = false
//        showLoadingIndicator()
//        _report.getFirst {
//            if let data = self._report.data {
//                self.removeLoadingIndicator()
//                switch data.status {
//                case .succeed:
//                    print("success")
//                    self.navigationController?.dismiss(animated: true, completion: nil)
//                case .failed:
//                    sender.isUserInteractionEnabled = true
//                    self.showMessage(text: "Error!", type: .error)
//                }
//            }
//        }
        
        sender.isUserInteractionEnabled = false
        showLoadingIndicator()
        
        downloader.getFirst {
            let data = self.downloader.getData()! as SucceedFailedEntity
            switch data.status {
            case .succeed:
                if self.downloader.url == URL.Block.event {
                    EventProfileViewController.eventWasBanned = true
                }
                self.navigationController?.dismiss(animated: true, completion: nil)
            case .failed:
                sender.isUserInteractionEnabled = true
                self.showMessage(text: "Error!", type: .error)
            }
        }
        
//        _report.getFirst {
//            if let data = self._report.data {
//                self.removeLoadingIndicator()
//                switch data.status {
//                case .succeed:
//                    print("success")
//                    self.navigationController?.dismiss(animated: true, completion: nil)
//                case .failed:
//                    sender.isUserInteractionEnabled = true
//                    self.showMessage(text: "Error!", type: .error)
//                }
//            }
//        }
    }
}

extension ReportSecViewController: UICollectionViewDelegateFlowLayout {
    var reuseIDs: [String] {
        get {
            return ["ReportSecViewControllerCell"]
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIDs[0], for: indexPath) as! ReportSecViewControllerCell
        cell.configCell(reportReason: reportReason)
        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return ReportSecViewControllerCell.currentSize
    }
}














