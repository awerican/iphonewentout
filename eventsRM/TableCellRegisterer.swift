//
//  TableCellRegisterer.swift
//  WentOut
//
//  Created by Fure on 10.09.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import UIKit

class TableCellRegisterer {
    
    private var registeredIds = Set<String>()
    private weak var tableView: UITableView?
    
    init(tableView: UITableView?) {
        self.tableView = tableView
    }
    
    func register(cellType: AnyClass, forCellReuseIdentifier reuseIdentifier: String) {
        
        if registeredIds.contains(reuseIdentifier) {
            return
        }
        
        // check if cell is already registered, probably cell has been registered by storyboard
        if tableView?.dequeueReusableCell(withIdentifier: reuseIdentifier) != nil {
            
            registeredIds.insert(reuseIdentifier)
            return
        }
        
        let bundle = Bundle(for: cellType)
        
        // we hope that cell's xib file has name that equals to cell's class name
        // in that case we could register nib
        if let _ = bundle.path(forResource: reuseIdentifier, ofType: "nib") {
            tableView?.register(UINib(nibName: reuseIdentifier, bundle: bundle), forCellReuseIdentifier: reuseIdentifier)
            // otherwise, register cell class
        } else {
            tableView?.register(cellType, forCellReuseIdentifier: reuseIdentifier)
        }
        
        registeredIds.insert(reuseIdentifier)
    }
}























