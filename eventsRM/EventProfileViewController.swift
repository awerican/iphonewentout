//
//  EventProfileViewController.swift
//  WentOut
//
//  Created by Fure on 06.04.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import UIKit

class EventProfileViewController: UICollectionViewController {
    
    var eventID: String!
    
    static var eventWasBanned: Bool = false
    
//    var imagePicker = UIImagePickerController()
    
    lazy var headerBundle: EventProfileHeader2CollectionViewCell = {
        return Bundle.main.loadNibNamed(reuseIds[0], owner: self, options: nil)?[0] as! EventProfileHeader2CollectionViewCell
    }()
    
    lazy var locationBundle: EventProfileLocationCollectionViewCell = {
        return Bundle.main.loadNibNamed(reuseIds[3], owner: self, options: nil)?[0] as! EventProfileLocationCollectionViewCell
    }()
    
    lazy var headerDownloader: Downloader<EventProfileHeader, EventPageParameters> = {
        return Downloader<EventProfileHeader, EventPageParameters>.init(resource: URL.Event.header, param: EventPageParameters(eventID: eventID, userID: LocalAuth.userID!))
    }()
    
    lazy var memoryDownloader: Downloader<[StoryMemory], EventPageParameters> = {
        return Downloader<[StoryMemory], EventPageParameters>.init(resource: URL.Event.memories, param: EventPageParameters(eventID: eventID, userID: LocalAuth.userID!))
    }()
    
    let imageViewTag: Int = 445682
    
    lazy var noDataImageView: UIImageView? = {
        if let maxY = navigationController?.navigationBar.frame.maxY {
            let imageView = UIImageView.init(frame: CGRect.init(x: 0, y: maxY, width: UIScreen.width, height: UIScreen.width))
            imageView.tag = imageViewTag
            imageView.image = #imageLiteral(resourceName: "errorNoData")
            return imageView
        }
        return nil
    }()

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if EventProfileViewController.eventWasBanned == true {
            EventProfileViewController.eventWasBanned = false
            self.navigationController?.popViewController(animated: true)
            return
        }
        setupNetworking()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.view.frame.size.height = UIScreen.main.bounds.height
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
//    func updateVC() {
//        setupNetworking()
//    }
}

extension EventProfileViewController {
    private func setupViews() {
//        setupNavigationBar()
        setupCollectionView()
//        setupImagePicker(imagePicker: self.imagePicker)
//        setupViewsDependsOnState()
    }
    
    private func removeItemsIfNeeded() {
        navigationItem.rightBarButtonItems = []
    }
    
    private func addNavItemsIsNeeded() {
        
        var items: [UIBarButtonItem] = []
        
        let chat = UIBarButtonItem.getChatBarButton(target: self, selector: #selector(openChat))
        
        let settings = UIBarButtonItem.getSettingsBarButton(target: self, selector: #selector(openEventSettings))
        
        items.append(settings)
        items.append(chat)
        
//        if let creatorID = headerDownloader.data?.creatorID {
//            if creatorID != LocalAuth.userID! {
//                let settings = UIBarButtonItem.getSettingsBarButton(target: self, selector: #selector(openEventSettings))
//                items.insert(settings, at: 0)
//            }
//        }
        
//        if headerDownloader.data!.creatorID != LocalAuth.userID! {
//            let settings = UIBarButtonItem.getSettingsBarButton(target: self, selector: #selector(openEventSettings))
//            items.insert(settings, at: 0)
//        }
        
        navigationItem.rightBarButtonItems = items
    }
    
    @objc private func openEventSettings() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let action0 = UIAlertAction(title: "Edit Info", style: .default) { (action: UIAlertAction) in
            self.editEvent()
        }
        
        let action1 = UIAlertAction(title: "Report", style: .default) { (action: UIAlertAction) in
            self.reportEvent()
        }
        
        let action2 = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            print("cancel")
        }
        
        if let creatorID = headerDownloader.data?.creatorID {
            if creatorID == LocalAuth.userID! {
                alertController.addAction(action0)
            } else {
                alertController.addAction(action1)
            }
        }
        
        alertController.addAction(action2)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func editEvent() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "CreateEventNavigationVC") as! UINavigationController
        let ceVC = controller.viewControllers.first as! CEViewController
        ceVC.pageMode = .edit
        
        guard let eventHeader = headerDownloader.data else {
            return
        }
        
        ceVC.eventHeader = eventHeader
        
        present(controller, animated: true, completion: nil)
        
//        guard let data = headerDownloader.data else {
//            return
//        }
//
//        Webservice.loadImage(url: data.avatar) { (_avatar) in
//            guard let avatar = _avatar else {
//                return
//            }
//
//            var description: String?
//
//            if data.description != "" {
//                description = data.description
//            }
//
//            let eventID = self.headerDownloader.data!.eventID
//
//            ceVC.eventID = eventID
//            let hashtagsDownloader = Downloader<[EventHashtag], SelectEventHashtagsParameters>.init(resource: URL.Event.selectHashtags, param: SelectEventHashtagsParameters.init(eventID: eventID))
//            hashtagsDownloader.getFirst {
//                if let hashtags = hashtagsDownloader.data {
//                    ceVC.manager = InsertEventManager.init(eventTitle: data.title, eventAvatar: avatar, location: data.location!, description: description, timeStart: data.timeStart, eventType: data.eventType, hashtags: EventHashtag.convert(hashtags: hashtags))
//                    self.present(controller, animated: true, completion: nil)
//                }
//            }
//        }
    }
    
    private func reportEvent() {
        let storyboard = UIStoryboard.init(name: "Second", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ReportViewControllerNav") as! UINavigationController
        let rootVC = controller.viewControllers.first as! ReportViewController
        let downloader = Downloader<SucceedFailedEntity, BlockEventParameters>.init(resource: URL.Block.event, param: BlockEventParameters.init(userID: LocalAuth.userID!, eventID: headerDownloader.data!.eventID))
        rootVC.downloader = downloader
        self.navigationController?.present(controller, animated: true, completion: nil)
    }
    
    private func setupCollectionView() {
        self.collectionView!.contentInset.bottom = UITabBar.height + 10.0
        for reuseID in self.reuseIds {
            self.collectionView!.register(UINib.init(nibName: reuseID, bundle: nil), forCellWithReuseIdentifier: reuseID)
        }
    }
    
    private func setupNetworking() {
        downloadHeader()
        downloadMemory()
    }
    
    private func downloadHeader() {
        if headerDownloader.data == nil {
            let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            activityIndicator.color = .gray
            self.navigationItem.titleView = activityIndicator
            activityIndicator.startAnimating()
        }
        
        headerDownloader.getFirst {
            print("122 infodownlaoded event")
            
            if self.headerDownloader.data == nil {
                if self.view.viewWithTag(self.imageViewTag) == nil {
                    if let iv = self.noDataImageView {
                        self.view.addSubview(iv)
                    }
                }
            }
            
            self.navigationItem.title = self.getEventType()
            self.navigationItem.titleView = nil
            self.collectionView?.reloadSections([0, 1])
            if self.headerDownloader.data?.eventType == .priv && self.headerDownloader.data?.statusGo == false {
                return
            } else {
                self.addNavItemsIsNeeded()
            }
        }
    }
    
    private func getEventType() -> String {
        guard let type = headerDownloader.data?.eventType else {
            return ""
        }
        if type == .priv {
            return "🚷 Private"
        } else {
            return "Public"
        }
    }
    
//    private func setEventType(_ eventType: EventType?) -> String? {
//        guard let type = eventType else {
//            return nil
//        }
//        if type == .priv {
//            return "Private"
//        } else {
//            return "Public"
//        }
//    }
    
    private func downloadMemory() {
        memoryDownloader.getFirst {
            self.collectionView?.reloadSections([1])
        }
    }
}

extension EventProfileViewController {
    var reuseIds: [String] {
        get {
            return ["EventProfileHeader2CollectionViewCell", "EventProfileActionsCollectionViewCell", "EventProfileMemoriesCollectionViewCell", "EventProfileLocationCollectionViewCell",
                "MemoryPreviewCollectionViewCell",
                "CollectionViewErrorImageCell"]
        }
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 3
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 0:
            if let _ = self.headerDownloader.data {
                return 2
            }
            return 0
        case 1:
            guard let _  = self.headerDownloader.data else {
                return 0
            }
            if memoryDownloader.firstDownloadIsFinished {
                if let count = self.memoryDownloader.data?.count {
                    if count == 0 {
                        return 1
                    } else {
                        return count
                    }
                } else {
                    return 1
                }
            } else {
                return self.memoryDownloader.data?.count ?? 0
            }
        default:
            return 0
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let data = memoryDownloader.data else {
            return
        }
        guard data.count > 0 else {
            return
        }
        if indexPath.section == 1 {
            let storyboard = UIStoryboard(name: "Second", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "StoryNavVC") as! UINavigationController
            let storyVC = controller.viewControllers[0] as! StoryViewController
            storyVC.storyDownloader = memoryDownloader.clone()
            let model = headerDownloader.data!
            storyVC.eventID = model.eventID
            storyVC.totalStories = model.memoryCount
            storyVC._eventTitle = model.title
            storyVC._eventAvatar = model.avatar
            storyVC.currentStoryIndex = indexPath.row
            present(controller, animated: true, completion: nil)
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIds[0], for: indexPath) as! EventProfileHeader2CollectionViewCell
                let _m = self.headerDownloader.data!
                cell.delegate = self
                cell.config(model: _m)
                return cell
            } else if indexPath.row == 1 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIds[1], for: indexPath) as! EventProfileActionsCollectionViewCell
//                cell.configCell(statusGo: self.headerDownloader.data!.statusGo)
                cell.configCell(model: self.headerDownloader.data!)
                cell.delegate = self
                return cell
            }
                return UICollectionViewCell()
        } else if indexPath.section == 1 {
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIds[4], for: indexPath) as! MemoryPreviewCollectionViewCell
//            let model = memoryDownloader.data![indexPath.row]
//            let creatorID = headerDownloader.data!.creatorID
//            cell.configCell(memory: model, hostID: creatorID)
//            return cell
            
            if let data = self.memoryDownloader.data {
                if data.count == 0 {
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIds[5], for: indexPath) as! CollectionViewErrorImageCell
                    if headerDownloader.data?.eventType == .priv && headerDownloader.data?.statusGo == false {
                        cell.configCell(image: #imageLiteral(resourceName: "errorPrivateEvent"))
                    } else {
                        cell.configCell(image: #imageLiteral(resourceName: "errorNoMemories"))
                    }
                    return cell
                } else {
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIds[4], for: indexPath) as! MemoryPreviewCollectionViewCell
                    let model = memoryDownloader.data![indexPath.row]
                    let creatorID = headerDownloader.data!.creatorID
                    cell.configCell(memory: model, hostID: creatorID)
                    return cell
                }
            } else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIds[5], for: indexPath) as! CollectionViewErrorImageCell
                if headerDownloader.data?.eventType == .priv && headerDownloader.data?.statusGo == false {
                    cell.configCell(image: #imageLiteral(resourceName: "errorPrivateEvent"))
                } else {
                    // NEEDED BE TESTED
                    cell.configCell(image: #imageLiteral(resourceName: "errorNoMemories"))
//                    cell.configCell(image: #imageLiteral(resourceName: "errorNoData"))
                }
                
                return cell
            }
        } else {
            return UICollectionViewCell()
        }
    }
}

extension EventProfileViewController {
    func makeGoRequest(status: EventGoStatus, callback: @escaping (Bool)->()) {
        
        let param = EventGoParameters(status: status, eventType: headerDownloader.data!.eventType, userID: LocalAuth.userID!, eventID: headerDownloader.data!.eventID)
        
        print(param)

        webservice.load(resource: ResList.goEvent(param)) { (_result, _error) in
            if let result = _result {
                if let status = result.data?.status {
                    switch status {
                    case .succeed:
                        callback(true)
                    case .failed:
                        callback(false)
                    }
                } else {
                    callback(false)
                }
            } else {
                callback(false)
            }
        }
    }
}

extension EventProfileViewController: EventProfileActionsDelegate {
    
    func leftAction(status: EventProfileActionsLeftAction, callback: @escaping (Bool) -> ()) {
        switch status {
        case .go:
            self.makeGoRequest(status: .go) { (success) in
                if (success) {
                    self.headerDownloader.getFirst {
                        self.collectionView?.reloadSections([0])
                    }
                    self.memoryDownloader.getFirst {
                        self.collectionView?.reloadSections([1])
                    }
                    self.addNavItemsIsNeeded()
                    callback(true)
                }
            }
        case .leave:
            let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            let action1 = UIAlertAction(title: "Leave", style: .default) { (action: UIAlertAction) in
                self.makeGoRequest(status: .leave) { (success) in
                    if (success) {
                        self.headerDownloader.getFirst {
                            self.collectionView?.reloadSections([0])
                        }
                        self.memoryDownloader.getFirst {
                            self.collectionView?.reloadSections([1])
                        }
                        self.removeItemsIfNeeded()
                        callback(true)
                    }
                }
            }
            
            let action2 = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                callback(false)
            }
            
            alertController.addAction(action1)
            alertController.addAction(action2)
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func invite() {
        // EventInviteFollowingsNavVC
        let storyboard = UIStoryboard(name: "Second", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "EventInviteFollowingsNavVC") as! UINavigationController
        (controller.viewControllers.first as! EventInviteFollowingsVC).eventID = eventID
        self.present(controller, animated: true, completion: nil)
    }
    
    func addMemory() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "NavAddMemoryViewController") as! UINavigationController
        //        (controller.viewControllers.first as! AddMemoryViewController).eventID = self.headerModel!.eventID
        
        // это очень плохо
        let header = self.headerDownloader.data!
        let _event = EventPanel.init(eventID: header.eventID, eventPhoto: header.avatar, title: header.title, time: Date.init(timeIntervalSince1970: TimeInterval(header.timeStart)), location: header.location!, guests: header.guests, userID: header.creatorID, username: header.creatorUsername, userPhoto: "")
        
        (controller.viewControllers.first as! AddMemoryViewController).event = _event
        self.present(controller, animated: true, completion: nil)
    }
}

extension EventProfileViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if section == 0 {
            return 0.0
        } else {
            return 0.0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            guard let memories = self.memoryDownloader.data else {
                return
            }
            if indexPath.row == memories.count - 1 {
                self.memoryDownloader.getNext(section: 1) { (paths) in
                    self.collectionView?.insertItems(at: paths)
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        switch section {
        case 0:
            return UIEdgeInsets.init(top: 0, left: 0, bottom: 10, right: 0)
        case 1:
            return UIEdgeInsets.init(top: 0, left: 11, bottom: 0, right: 11)
        default:
            return UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let cell = Bundle.main.loadNibNamed(reuseIds[0], owner: self, options: nil)?[0] as! EventProfileHeader2CollectionViewCell
                cell.config(model: headerDownloader.data!)
                return cell.currentSize
//                let cell = headerBundle
//                cell.config(model: headerDownloader.data!)
//                return cell.currentSize
            }
            if indexPath.row == 1 {
                return EventProfileActionsCollectionViewCell.currentSize
            }
            else {
                return CGSize.zero
            }
        } else if indexPath.section == 1 {
            let memorySize = CGSize.init(width: UIScreen.width / 3 - 8, height: UIScreen.width / 3 * 1.6)
            guard let data = self.memoryDownloader.data else {
                if memoryDownloader.firstDownloadIsFinished {
                    return CollectionViewErrorImageCell.currentSize
                }
                return CGSize.zero
            }
            if data.count == 0 {
                return CollectionViewErrorImageCell.currentSize
            } else {
                return memorySize
            }
//            return CGSize.init(width: UIScreen.width / 3 - 8, height: UIScreen.width / 3 * 1.6)
        } else {
            return CGSize.zero
        }
    }
}

extension EventProfileViewController: EventProfileHeaderDelegate {
    func memoryWasTapped() {
        // NEEDED BE TESTED
        print("memory was tapped")
        if let memoryCount = collectionView?.numberOfItems(inSection: 1) {
            if memoryCount > 0 {
                let topMemoryIP = IndexPath.init(row: 0, section: 1)
                collectionView?.scrollToItem(at: topMemoryIP, at: .top, animated: true)
            }
        }
    }
    
    func placeWasTapped() {
        let storyboard = UIStoryboard(name: "Second", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "EventProfileLocationNavVC") as! UINavigationController
        let locationVC = controller.viewControllers.first as! EventProfileLocationViewController
        let location = headerDownloader.data!.location
        locationVC.location = location
        locationVC.eventTitle = headerDownloader.data!.title
        present(controller, animated: true, completion: nil)
    }
    
    func friendsWasTapped() {
        let friends = headerDownloader.data!.friends
        if friends.count < 1 {
            return
        }
        if friends.count == 1 {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
            controller.profileOwnerID = friends[0].id
            navigationController?.pushViewController(controller, animated: true)
        }
        if friends.count > 1 {
            let storyboard = UIStoryboard.init(name: "Second", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "UserListViewController") as! UserListViewController
            controller.modalPresentationStyle = .overCurrentContext
            
            let eventID = headerDownloader.data!.eventID
            
            let downloader = Downloader<[UserMini], EventPageParameters>.init(resource: URL.Event.friendsList, param: EventPageParameters.init(eventID: eventID, userID: LocalAuth.userID!))
            
            controller.setupVC(downloadable: downloader, title: "Friends")

            navigationController!.pushViewController(controller, animated: true)
        }
    }
    
    func guestsWasTapped() {
        let storyboard = UIStoryboard.init(name: "Second", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "UserListViewController") as! UserListViewController
        controller.modalPresentationStyle = .overCurrentContext
        
        let eventID = headerDownloader.data!.eventID
        
        let downloader = Downloader<[UserMini], EventPageParameters>.init(resource: URL.Event.guestList, param: EventPageParameters.init(eventID: eventID, userID: LocalAuth.userID!))
        
        controller.setupVC(downloadable: downloader, title: "Guests")
        
        navigationController!.pushViewController(controller, animated: true)
    }
    
    func hostWasTapped() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
        controller.profileOwnerID = self.headerDownloader.data!.creatorID
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

//extension EventProfileViewController: EventProfileMemoriesDelegate {

extension EventProfileViewController {
//    @objc private func openEventSettings() {
//        print("open Event Settings, bitch")
//    }
    
    @objc private func openChat() {
        guard let eventTitle = headerDownloader.data?.title else {
            return
        }
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "MessageViewController") as! MessageViewController
        controller.eventID = eventID
        controller.eventTitle = eventTitle
        if let baseVC = self.navigationController?
            .tabBarController?
            .navigationController {
            baseVC.pushViewController(controller, animated: true)
        } else if let baseVC = self.navigationController {
            baseVC.pushViewController(controller, animated: true)
        }
        print("open Chat")
    }
}

















