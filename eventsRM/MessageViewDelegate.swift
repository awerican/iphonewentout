//
//  ErrorViewManager.swift
//  WentOut
//
//  Created by Fure on 07.10.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import UIKit

protocol MessageViewDelegate {
    var message: UILabel { get }
}

enum MessageType {
    case error
    case success
}

extension UIViewController: MessageViewDelegate {
    
    var message: UILabel {
        get {
            let frame = self.navigationController?.navigationBar.frame ?? CGRect.init(x: 0, y: 0, width: UIScreen.width, height: 40.0)
            let label = UILabel.init(frame: frame)
            label.backgroundColor = UIColor.orica
            return label
        }
    }
    
    func showLoadingIndicator() {
        let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        activityIndicator.color = .gray
        navigationItem.titleView = activityIndicator
        activityIndicator.startAnimating()
    }
    
    func removeLoadingIndicator() {
        navigationItem.titleView = nil
    }
    
    func showMessage(text: String, type: MessageType) {
        
        var view: UIView!
        
        if type == .error {
            let msg = message
            msg.text = text
            msg.textAlignment = .center
            msg.font = UIFont.systemFont(ofSize: 17.0, weight: .medium)
            msg.numberOfLines = 0
            view = msg
        } else if type == .success {
            // nothign else yet
            view = UIView()
        }
        
        if let _parent = parent {
            _parent.view.addSubview(view)
        } else {
            view.addSubview(view)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            view.removeFromSuperview()
        }
    }
}



























