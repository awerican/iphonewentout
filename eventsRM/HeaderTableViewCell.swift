//
//  HeaderTableViewCell.swift
//  eventsRM
//
//  Created by Fure on 28.10.17.
//  Copyright © 2017 uzuner. All rights reserved.
//

import UIKit

struct HeaderTableViewCellSecondModel: Equatable {
    var eventId: String
    var eventName: String
    var eventTime: Date
    static func ==(lhs: HeaderTableViewCellSecondModel, rhs: HeaderTableViewCellSecondModel) -> Bool {
        if lhs.eventId == rhs.eventId {
            return true
        } else {
            return false
        }
    }
}

enum HeaderTableViewCellSecondStates: Equatable {
    case topEvents
    case eventInfo
    case eventNear
//    case chosenEvent
//    static func ==(lhs: HeaderTableViewCellSecondStates, rhs: HeaderTableViewCellSecondStates) -> Bool {
//        switch (lhs, rhs) {
//        case (.topEvents, .topEvents):
//            return true
//        case (.eventNear, .eventNear):
//            return true
////        case (.chosenEvent, .chosenEvent):
////            return true
//        case (.eventInfo(let fModel), .eventInfo(let sModel)):
//            if fModel == sModel {
//                return true
//            } else {
//                return false
//            }
//        default:
//            return false
//        }
//    }
}

class HeaderTableViewCell: UITableViewHeaderFooterView {
//    let defaultTrailingConstraintConstant: CGFloat = 12.0

    @IBOutlet var labelInfo: UILabel!
    @IBOutlet private var basisView: UIView!
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        basisView.makeRound(roudingCorners: [.topLeft, .topRight], cornerRadius: CGSize.init(width: 12, height: 12))
        basisView.layer.addBorder(edge: .bottom, color: UIColor(red: 0.91, green: 0.91, blue: 0.91, alpha: 1.0), thickness: 1.0)
    }
    
    func configView(_ state: HeaderTableViewCellSecondStates) {
        switch state {
        case .topEvents:
            self.labelInfo.text = "Top Events"
            self.labelInfo.textAlignment = .center
        case .eventNear:
            self.labelInfo.text = "Nearest events"
            self.labelInfo.textAlignment = .left
        case .eventInfo:
            self.labelInfo.text = "Chosen event"
            self.labelInfo.textAlignment = .left
        }
    }
    
    func configView(_ state: HeaderTableViewCellSecondStates, gesture: UITapGestureRecognizer) {
        switch state {
        case .topEvents:
            self.labelInfo.text = "Top Events"
            self.labelInfo.textAlignment = .center
            self.addGestureRecognizer(gesture)
        case .eventNear:
            self.labelInfo.text = "Nearest events"
            self.labelInfo.textAlignment = .left
        case .eventInfo:
            self.labelInfo.text = "Chosen event"
            self.labelInfo.textAlignment = .left
            self.addGestureRecognizer(gesture)
        }
    }
}
