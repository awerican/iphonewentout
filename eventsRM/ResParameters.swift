//
//  Parameters.swift
//  WentOut
//
//  Created by Fure on 29.08.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import Foundation
import UIKit

protocol PEventID: Encodable, Decodable {
    var eventID: String { get set }
}

protocol PUserID: Encodable, Decodable {
    var userID: String { get set }
}

protocol PUsername: Encodable {
    var userNickname: String { get set }
}

protocol PVisitorID: Encodable {
    var visiterID: String { get set }
}

protocol PProfileOwnerID: Encodable {
    var profileOwnerID: String { get set }
}

protocol PTimeLimit: Encodable {
    var timeLimit: Int { get set }
}

protocol PEmail: Encodable {
    var email: String { get set }
}

protocol PSkip: Encodable {
    var skip: Int { get set }
}

protocol PDescription: Encodable {
    var description: String { get set }
}

protocol PToken: Encodable {
    var token: Int { get set }
}

protocol PPassword: Encodable {
    var password: String { get set }
}

protocol PImage: Encodable, Decodable {
    var imageBase64String: String { get set }
}

protocol POldPsw: Encodable {
    var oldPassword: String { get set }
}

protocol PNewPsw: Encodable {
    var newPassword: String { get set }
}

protocol PTitle: Encodable {
    var title: String { get set }
}

protocol PPhoto: Encodable {
    var photo: String { get set }
}

protocol PLocation: Encodable {
    var location: String { get set }
}

protocol PLatitude: Encodable {
    var latitude: Double { get set }
}

protocol PLongitude: Encodable {
    var longitude: Double { get set }
}

protocol PTimeStart: Encodable {
    var timeStart: Int { get set }
}

protocol PEventType: Encodable {
    var type: String { get set }
}

protocol PUserProfile: PVisitorID, PProfileOwnerID, PTimeLimit, PSkip { }

struct EmptyParameters: Encodable, Decodable { }

struct InsertEventParameters: Encodable, Decodable {
    var userID: String
    var title: String
    var photo: String
    var location: EventLocation
    var timeStart: Int
    var type: EventType
    var description: String = ""
    var hashtagName: [String] = []
}

struct SelectEventHashtagsParameters: Encodable, Decodable, PEventID {
    var eventID: String
}

struct RegistrationFirstParameters: Encodable {
    var nickname: String
    var password: String
    var name: String
}

struct RegistrationSecParameters: Encodable {
    var userID: String
    var email: String
}

//protocol UserIDParamProtocol: PUserID { }

struct RecoverOneParameters: Encodable {
    var email: String
}

struct RecoverTwoParameters: Encodable {
    var email: String
    var token: String
    var newPassword: String
}

struct RegistrationThreeParameters: Encodable {
    var userID: String
    var token: Int
}

struct LoginParameters: PEmail, PPassword {
    var email: String
    var password: String
}

struct EventByIDParameters: PEventID {
    var eventID: String
}

struct EventPageParameters: PEventID, PUserID, Decodable {
    var eventID: String
    var userID: String
}

struct UpdateUsernameParameters: PUserID, PUsername {
    var userID: String
    var userNickname: String
}

struct UpdateEmailParameters: PUserID, PEmail {
    var userID: String
    var email: String
}

struct UpdateEmailTokenParameters: PUserID, PToken {
    var userID: String
    var token: Int
}

struct UpdateDescParameters: PUserID, PDescription {
    var userID: String
    var description: String
}

struct ChangeUserAvatarParameters: PUserID, Encodable, Decodable {
    var userID: String
    var photo: String
}

struct AppVersionParam: Encodable, Decodable {
    var version: Double
}

struct ChangeEventTitleParam: Encodable, Decodable, PEventID, PUserID {
    var userID: String
    var eventID: String
    var title: String
}

struct ChangeEventTimestartParam: Encodable, Decodable, PEventID, PUserID {
    var userID: String
    var eventID: String
    var timeStart: Int
}

struct ChangeEventDescrParam: Encodable, Decodable, PEventID, PUserID {
    var userID: String
    var eventID: String
    var description: String
}

struct ChangeEventLocationParam: Encodable, Decodable, PEventID, PUserID {
    var userID: String
    var eventID: String
    var location: EventLocation
}

struct ChangeEventHashtagsParam: Encodable, Decodable, PEventID, PUserID {
    var userID: String
    var eventID: String
    var hashtagName: [String]
}

struct ChangeEventAvatarParam: Encodable, Decodable, PEventID, PUserID {
    var userID: String
    var eventID: String
    var photo: String
}

struct UserProfileParameters: PUserProfile {
    var visiterID: String
    var profileOwnerID: String
    var timeLimit: Int
    var skip: Int
}

enum UserFollowStatus: String, Encodable, Decodable {
    case follow = "follow"
    case unfollow = "unfollow"
}

struct FollowUserParameters: Encodable {
    var status: UserFollowStatus
    var targetID: String
    var newFollowerID: String
}

struct SearchPageTextParameters: Encodable, Decodable {
    var text: String
}

struct PushTokenParam: Encodable, Decodable {
    var userID: String
    var pushToken: String
}

struct UserProfileHeaderParameters: PVisitorID, PProfileOwnerID, Decodable {
    var visiterID: String
    var profileOwnerID: String
}

struct BlockMemoryParameters: Decodable, Encodable {
    var userID: String
    var memoryID: String
}

struct BlockEventParameters: Decodable, Encodable {
    var userID: String
    var eventID: String
}

struct BlockUserParameters: Decodable, Encodable {
    var userID: String
    var profileOwnerID: String
}

struct UserProfileEventParameters: Decodable, Encodable {
    var visiterID: String
    var profileOwnerID: String
//    var timeLimit: Int
//    var skip: Int
}

enum EventGoStatus: String, Decodable, Encodable {
    case go = "go"
    case leave = "ungo"
}

//struct SelectNewsNext: Encodable, Decodable {
//    var timeLimit: String
//    var skip: String
//}

struct PEventNameByID: Encodable, Decodable {
    var eventID: String
    
    init(eventID: String) {
        self.eventID = eventID
    }
}

struct SelectNews: Encodable, Decodable {
    var userID: String
    
    init(userID: String) {
        self.userID = userID
    }
}

//struct SelectNews: Encodable, Decodable {
//    var userID: String
//    var next: SelectNewsNext?
//
//    init(userID: String, next: SelectNewsNext? = nil) {
//        self.userID = userID
//        self.next = next
//    }
//}

struct InsertMemoryParameters: Encodable, Decodable {
    var userID: String
    var eventID: String
    var filename: String
    var mimetype: String
}

struct EventInviteParameters: Encodable, Decodable {
    var userID: String
    var eventGuests: [String]
    var eventID: String
}

struct UserListParameters: Encodable, Decodable {
    var userID: String
    var profileOwnerID: String
}

struct EventGoParameters: Encodable, Decodable {
    var status: EventGoStatus
    var eventType: EventType
    var userID: String
    var eventID: String
}

struct UserProfileMediaParameters: PUserProfile {
    var visiterID: String
    var profileOwnerID: String
    var timeLimit: Int
    var skip: Int
}

struct UserIDParameter: PUserID {
    var userID: String
}

struct UserSettingsChangePsw: PUserID, PNewPsw, POldPsw {
    var userID: String
    var oldPassword: String
    var newPassword: String
}



























