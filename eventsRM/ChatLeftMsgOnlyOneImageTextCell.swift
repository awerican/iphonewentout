//
//  ChatLeftMsgOnlyOneImageTextCell.swift
//  WentOut
//
//  Created by Fure on 14.10.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import UIKit

class ChatLeftMsgOnlyOneImageTextCell: ChatMessageBaseCell {
    
    @IBOutlet var avatarOutlet: UIImageView?
    @IBOutlet var messageOutlet: UILabel?
    @IBOutlet var mediaCollectionOutlet: [UIImageView]!
    @IBOutlet var usernameOutlet: UILabel!
    @IBOutlet var timeOutlet: UILabel!
    @IBOutlet var messageBackgroundOutlet: UIView!
    
    @IBOutlet var timeLeadingConstrOutlet: NSLayoutConstraint!
    @IBOutlet var timeTopConstrOutlet: NSLayoutConstraint!
    
    override var avatar: UIImageView? {
        return self.avatarOutlet
    }
    override var message: UILabel! {
        return self.messageOutlet
    }
    override var mediaCollection: [UIImageView]! {
        return self.mediaCollectionOutlet
    }
    override var username: UILabel! {
        return self.usernameOutlet
    }
    override var time: UILabel! {
        return self.timeOutlet
    }
    override var messageBackground: UIView! {
        return self.messageBackgroundOutlet
    }
    override var timeLeadingConstr: NSLayoutConstraint! {
        return self.timeLeadingConstrOutlet
    }
    override var timeTopConstr: NSLayoutConstraint! {
        return self.timeTopConstrOutlet
    }
    
    
//    override func draw(_ rect: CGRect) {
//        self.avatar.makeCircle()
//        self.messageBackground.makeOval(cornerRadius: 15.0)
//        self.media.makeOval(cornerRadius: 10.0)
//    }
}

//protocol ChatLeftMsgOnlyOneImageTextCellDelegate { }
//
//protocol ChatLeftMsgOnlyOneImageTextCellInteractor: MessageInteractor { }
//
//extension ChatLeftMsgOnlyOneImageTextCell: ChatLeftMsgOnlyOneImageTextCellInteractor { }
//
//extension ChatLeftMsgOnlyOneImageTextCell: ConfigurableCell {
//    typealias CellDelegate = ChatLeftMsgOnlyOneImageTextCellDelegate
//    typealias CellData = MessageWrapper
//    typealias CellInteractor = ChatLeftMsgOnlyOneImageTextCellInteractor
//
//    func configure(with data: MessageWrapper, delegate: CellDelegate?) {
//        self.avatar.image = nil
//        self.username.text = data.message.senderUsername
//        self.message.text = data.message.msg
//
//        Webservice.loadImage(url: data.message.senderAvatarURL) { (_image) in
//            if let image = _image {
//                self.avatar.image = image
//            }
//        }
//
//        Webservice.loadImage(url: data.message.attch.first!.url) { (_image) in
//            if let image = _image {
//                self.media.image = image
//            }
//        }
//
//        let dateFormatter: DateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "HH:mm"
//        let timeString = dateFormatter.string(from: data.message.time)
//        self.time.text = timeString
//
//        if data.avatarIsHidden {
//            self.avatar.isHidden = true
//        } else {
//            self.avatar.isHidden = false
//        }
//
//        self.layoutIfNeeded()
//
//        if self.message.calculateMaxLines() > 1 {
//            self.messageTimeLeadingConstr.constant = -self.time.frame.width
//            self.messageTimeTopConstr.constant = 0.0 // 3.0
//        } else {
//            self.messageTimeTopConstr.constant = -15.0
//            self.messageTimeLeadingConstr.constant = 11.0
//        }
//    }
//}
//
//
//
//
//
//
//
//
//
//
//
