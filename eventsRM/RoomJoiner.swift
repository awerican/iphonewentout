//
//  RoomJoiner.swift
//  WentOut
//
//  Created by Fure on 22.01.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation

struct RoomJoiner: Encodable {
    var userID: String
    var eventID: String
    func encrypt(aesKey: AesKey) -> RoomJoiner {
        let userID = AesWrapper.encrypt(self.userID, aesKey: aesKey)
        let eventID = AesWrapper.encrypt(self.eventID, aesKey: aesKey)
        return RoomJoiner(userID: userID, eventID: eventID)
    }
}

struct RoomJoinerRes {
    var status: Bool
    var reason: String?
}

extension RoomJoinerRes: Decodable {
    private enum CodingKeys: String, CodingKey {
        case status = "status"
        case reason = "reason"
    }
    
    init(from decoder: Decoder) throws {
        do {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            self.status = try container.decode(Bool.self, forKey: .status)
            if let reason = try? container.decode(String.self, forKey: .reason) {
                self.reason = reason
            } else {
                self.reason = nil
            }
        } catch {
            throw error
        }
    }
}
