//
//  Date.swift
//  WentOut
//
//  Created by Fure on 30.01.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import Foundation

extension Date {
    var formatted: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEE MMM dd   HH : mm"
        return formatter.string(from: self)
    }
    
    var CETime1: String {
        let formatter1 = DateFormatter()
        formatter1.dateFormat = "EEEE"
        let str1 = formatter1.string(from: self)
        
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "hh:mm a"
        let str2 = formatter2.string(from: self)
        
        return str1 + " at " + str2
    }
    
    var eventPageTime: String {
        let formatter1 = DateFormatter()
        formatter1.dateFormat = "HH:mm"
        
        let formatterX = DateFormatter()
        formatterX.dateFormat = "EEE"
        
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "d"
        
        let formatter3 = DateFormatter()
        formatter3.dateFormat = "MMMM yyyy"
        
        let time = formatter1.string(from: self)
        let monday = formatterX.string(from: self)
        let date = formatter2.string(from: self) + "th"
        let other = formatter3.string(from: self)
        
        return time + " " + monday + " - " + date + " " + other
    }
    
    var eventPanel1: String {
        let formatter = DateFormatter()
        let formatter2 = DateFormatter()
//        formatter.dateFormat = "d MMMM HH:mm"
        formatter.dateFormat = "d MMM yyyy"
        formatter2.dateFormat = "HH:mm"
        
        let str1 = formatter.string(from: self)
        let str2 = formatter2.string(from: self)
        return str1 + " at " + str2
    }
    
    var eventHour: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        
        let str1 = formatter.string(from: self)
        return str1
    }
    
    var CETime2: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM dd, yyyy"
        return formatter.string(from: self)
    }
    
    // ДОБАВИТЬ ПОДДЕРЖКУ ГОДА
    static func goodTime(inSec timeSince1970: Int) -> String? {
        return __goodTime(inSec: timeSince1970)
    }
    
    var timeDifferenceOnMap: String? {
        return Date.__goodTime(inSec: Int(self.timeIntervalSince1970))
    }
    
    static func differenceInSeconds(beginDate: Date, endDate: Date) -> Int? {
        
        let boardingTime = beginDate
        let now = endDate
        let difference = Calendar.current.dateComponents([.hour, .minute, .second], from: now, to: boardingTime)
        
        guard let seconds = difference.second, let minutes = difference.minute, let hours = difference.hour else {
            return nil
        }
        
        return seconds + minutes * 60 + hours * 60 * 60
    }
    
    private static func __goodTime(inSec timeSince1970: Int) -> String? {
        let time = Date.init(timeIntervalSince1970: Double(timeSince1970))
        let now = Date.init()
        let timeDifference = Calendar.current.dateComponents([.hour, .minute], from: now, to: time)
        
        guard let hour = timeDifference.hour else {
            return nil
        }
        
        guard let min = timeDifference.minute else {
            return nil
        }
        
        if hour >= 0 {
            if hour < 1 {
                if min <= 0 {
                    return (-min).description + "m ago"
                } else {
                    return "in " + min.description + "m"
                }
            } else if hour >= 1 && hour < 25 {
                return "in " + hour.description + "h"
            } else {
                return "in " + (Double(hour) / 24.0).rounded(.towardZero).clean + "d"
            }
        } else {
            if hour > -25 && hour <= -1 {
                return (-hour).description + "h ago"
            } else {
                return (Double(-hour) / 24.0).rounded(.towardZero).clean + "d ago"
            }
        }
    }
}
