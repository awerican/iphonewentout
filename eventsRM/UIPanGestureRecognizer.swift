//
//  UIPanGestureRecognizer.swift
//  WentOut
//
//  Created by Fure on 14.07.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import UIKit
import Foundation

public enum Direction: Int {
    case up
    case down
    case left
    case right
    
    public var isX: Bool { return self == .left || self == .right }
    public var isY: Bool { return !isX }
}

public extension UIPanGestureRecognizer {
    
    public var direction: Direction? {
        let velocity = self.velocity(in: view)
        let vertical = fabs(velocity.y) > fabs(velocity.x)
        switch (vertical, velocity.x, velocity.y) {
        case (true, _, let y) where y < 0: return .up
        case (true, _, let y) where y > 0: return .down
        case (false, let x, _) where x > 0: return .right
        case (false, let x, _) where x < 0: return .left
        default: return nil
        }
    }
}
