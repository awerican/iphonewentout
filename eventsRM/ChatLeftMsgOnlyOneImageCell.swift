//
//  ChatLeftMsgOnlyOneImageCell.swift
//  WentOut
//
//  Created by Fure on 09.10.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import Foundation
import UIKit

class ChatLeftMsgOnlyOneImageCell: ChatMessageBaseCell {
    
    @IBOutlet var avatarOutlet: UIImageView?
    @IBOutlet var mediaCollectionOutlet: [UIImageView]?
    @IBOutlet var usernameOutlet: UILabel!
    @IBOutlet var timeOutlet: UILabel!
    @IBOutlet var messageBackgroundOutlet: UIView!
    @IBOutlet var timeBackgroundOutlet: UIView!
    
    override var avatar: UIImageView? {
        return self.avatarOutlet
    }
    override var mediaCollection: [UIImageView]! {
        return self.mediaCollectionOutlet
    }
    override var username: UILabel! {
        return self.usernameOutlet
    }
    override var time: UILabel! {
        return self.timeOutlet
    }
    override var messageBackground: UIView! {
        return self.messageBackgroundOutlet
    }
    override var timeBackground: UIView! {
        return self.timeBackgroundOutlet
    }
    
//    override func draw(_ rect: CGRect) {
//        self.avatar.makeCircle()
//        self.messageBackground.makeOval(cornerRadius: 15.0)
//        self.media.makeOval(cornerRadius: 10.0)
//        self.timeBackground.makeOval(cornerRadius: self.timeBackground.frame.height / 4)
//    }
}

//protocol ChatLeftMsgOnlyOneImageDelegate {}
//
//protocol ChatLeftMsgOnlyOneImageInteractor: MessageInteractor {}
//
//extension ChatLeftMsgOnlyOneImageCell: ChatLeftMsgOnlyOneImageInteractor { }
//
//extension ChatLeftMsgOnlyOneImageCell: ConfigurableCell {
//    func configure(with data: ChatLeftMsgOnlyOneImageCell.CellData, delegate: ChatLeftMsgOnlyOneImageCell.CellDelegate?) {
//        self.avatar.image = nil
//
//        self.username.text = data.message.senderUsername
//
//        Webservice.loadImage(url: data.message.senderAvatarURL) { (_image) in
//            if let image = _image {
//                self.avatar.image = image
//            }
//        }
//
//        Webservice.loadImage(url: data.message.attch.first!.url) { (_image) in
//            if let image = _image {
//                self.media.image = image
//            }
//        }
//
//        let dateFormatter: DateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "HH:mm"
//        let timeString = dateFormatter.string(from: data.message.time)
//        self.time.text = timeString
//
//        if data.avatarIsHidden {
//            self.avatar.isHidden = true
//        } else {
//            self.avatar.isHidden = false
//        }
//
//        self.layoutIfNeeded()
//    }
//}

