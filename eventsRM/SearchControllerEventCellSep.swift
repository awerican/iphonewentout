//
//  SearchControllerEventCell.swift
//  eventsRM
//
//  Created by Fure on 02.11.17.
//  Copyright © 2017 uzuner. All rights reserved.
//

import UIKit

class SearchControllerEventCellSep: UICollectionViewCell {

    @IBOutlet var basisView: UIView!

    @IBOutlet var userAvatar: UIImageView!
    @IBOutlet var eventAvatar: UIImageView!
    @IBOutlet weak var eventTitle: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var peopleQuantity: UILabel!
    @IBOutlet weak var eventTime: UILabel!
    
//    @IBOutlet var buttonLeft: UIButton!
    @IBOutlet var buttonRight: UIButton!
    
    static let cellWidth = CGFloat(UIScreen.main.bounds.width - 40)
    
    static let leadingConstant: CGFloat = 55.0 / 2
    
    weak var delegate: SearchControllerEventCellSepDelegate?
    
    static var currentSize: CGSize {
        get {
            return CGSize.init(width: UIScreen.width - 2 * leadingConstant , height: 223)
        }
    }
    
    func configCell(model: EventPanel) {
        Webservice.loadImage(url: model.userPhoto) { (_image) in
            self.userAvatar.image = _image
        }
        Webservice.loadImage(url: model.eventPhoto) { (_image) in
            self.eventAvatar.image = _image
        }
        userName.text = model.userNickname
        eventTitle.text = model.title
        address.text = model.location.location
        peopleQuantity.text = model.guests.description + " people"
        eventTime.text = model.time.timeDifferenceOnMap ?? ""
        
        let avatarTapGesture = UITapGestureRecognizer.init(target: self, action: #selector(userAvatarWasTapped))
        userAvatar.isUserInteractionEnabled = true
        userAvatar.addGestureRecognizer(avatarTapGesture)
        
        let userTapGesture = UITapGestureRecognizer.init(target: self, action: #selector(usernameWasTapped))
        userName.isUserInteractionEnabled = true
        userName.addGestureRecognizer(userTapGesture)
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 3.0
        self.layer.shadowOpacity = 0.3
        self.layer.masksToBounds = false
        let bounds = CGRect.init(x: self.basisView.frame.origin.x + 0, y: self.basisView.frame.origin.y + 2, width: self.basisView.frame.width, height: self.basisView.frame.height)
        self.layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: 15).cgPath
        
        basisView.makeOval(cornerRadius: 15)
        userAvatar.makeCircle()
        eventAvatar.makeOval(cornerRadius: eventAvatar.frame.height / 5)
//        buttonLeft.makeOval(cornerRadius: buttonLeft.frame.height / 4)
        buttonRight.makeOval(cornerRadius: buttonRight.frame.height / 4)
    }
    
    @objc private func userAvatarWasTapped() {
        if let collectionView = superview as? UICollectionView {
            if let ip = collectionView.indexPath(for: self) {
                delegate?.avatarWasPressed(at: ip)
            }
        }
    }
    
    @objc private func usernameWasTapped() {
        if let collectionView = superview as? UICollectionView {
            if let ip = collectionView.indexPath(for: self) {
                delegate?.usernameWasPressed(at: ip)
            }
        }
    }
}





















