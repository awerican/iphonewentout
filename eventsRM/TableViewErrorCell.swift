//
//  TableViewErrorCell.swift
//  WentOut
//
//  Created by Fure on 25.08.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import UIKit

class TableViewErrorCell: UITableViewCell {
    
    @IBOutlet var heightConstraints: NSLayoutConstraint!
    
    func config(_ height: CGFloat) {
        self.heightConstraints.constant = height
    }
}
