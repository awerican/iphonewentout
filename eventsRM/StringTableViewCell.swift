//
//  StringTableViewCell.swift
//  WentOut
//
//  Created by Fure on 11.09.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import Foundation
import UIKit

struct StringTableViewModel {
    var title: String
    var body: String
}

@objc protocol StringTableViewCellDelegate: class {
    func buttonWasPressed(_ button: UIButton)
}

@objc protocol StringTableViewCellInteractor {
    @objc func changeBackgroundColor()
}

class StringTableViewCell: UITableViewCell, ConfigurableCell {
    typealias CellData = StringTableViewModel
    typealias CellDelegate = StringTableViewCellDelegate
    typealias CellInteractor = StringTableViewCellInteractor
    
    private(set) weak var delegate: CellDelegate?
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var bodyLabel: UILabel!
    
    func configure(with model: StringTableViewModel, delegate: StringTableViewCellDelegate? = nil) {
        self.titleLabel.text = model.title
        self.bodyLabel.text = model.body
        self.delegate = delegate
    }
}

extension StringTableViewCell: StringTableViewCellInteractor {
    func changeBackgroundColor() {
        self.titleLabel.backgroundColor = .red
        self.bodyLabel.backgroundColor = .red
    }
}


























