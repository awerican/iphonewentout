//
//  SearchTypeManager.swift
//  WentOut
//
//  Created by Fure on 11.07.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import UIKit

class SearchTypeManager {
    
    var basisView: UIView
    var frameY: CGFloat
    var height: CGFloat
    
    weak var delegate: SearchTypeViewDelegate?
    
    lazy var searchTypeView: SearchTypeView = {
        let view = SearchTypeView.init(frame: CGRect.init(x: 0, y: frameY, width: UIScreen.width, height: height))
        view.delegate = self
        return view
    }()
    
    func setToDefault() {
        searchTypeView.setToDefault()
    }
    
    func switchStyle(to index: Int) {
        if index == 0 {
            searchTypeView.eventButtonAction()
        }
        if index == 1 {
            searchTypeView.userButtonAction()
        }
    }
    
    func addView() {
        basisView.addSubview(searchTypeView)
    }
    
    func removeView() {
        searchTypeView.removeFromSuperview()
    }
    
    init(basisView: UIView, y: CGFloat, height: CGFloat) {
        self.basisView = basisView
        self.frameY = y
        self.height = height
    }
}

extension SearchTypeManager: SearchTypeViewDelegate {
    func eventButtonWasPressed() {
        self.delegate?.eventButtonWasPressed()
    }
    
    func userButtonWasPressed() {
        self.delegate?.userButtonWasPressed()
    }
}


















