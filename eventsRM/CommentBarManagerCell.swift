//
//  CommentBarManagerCell.swift
//  WentOut
//
//  Created by Fure on 11.02.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

class CommentBarManagerCell: UICollectionViewCell {

    @IBOutlet private weak var image: UIImageView!
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var playImage: UIImageView!
    
    func configCell(type: CommentMediaType, image: UIImage) {
        self.image.image = image
        
        playImage.setColor(UIColor.white)
        self.image.makeOval(cornerRadius: 15.0)
        button.makeCircle()
        
        if type == .image {
            playImage.isHidden = true
        } else {
            playImage.isHidden = false
        }
    }
}
