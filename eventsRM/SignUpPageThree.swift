//
//  SignUpPageThree.swift
//  WentOut
//
//  Created by Fure on 08.05.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import UIKit

class SignUpPageThree: UIViewController {
    
    var userID: String!
    var password: String!
    var email: String!
    
    let mainAppSegue = "mainAppSegue"
    
    @IBOutlet weak var tokenTextField: UITextField!
    
    @IBOutlet weak var finishButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    func login(param: LoginParameters) {
        webservice.load(resource: ResList.login(param)) { (_result, _error) in
            if let error = _error {
                print("REQ ERROR =", error)
                return
            }
            
            if let result = _result?.data {
                switch result.status {
                case .success:
                    if let key = result.aesKey, let token = result.token, let userID = result.userID {
                        LocalAuth.saveLoginInfo(aesKey: key, token: token, userID: userID)
                        // open an app
                        self.performSegue(withIdentifier: self.mainAppSegue, sender: nil)
                    } else {
                        print("something went wrong")
                    }
                case .incorrectEmail:
                    self.navigationController?.popToRootViewController(animated: true)
                    print("email incorrect")
                case .incorrectPass:
                    self.navigationController?.popToRootViewController(animated: true)
                    print("pass incorrect")
                }
            } else {
                self.navigationController?.popToRootViewController(animated: true)
                print("ERROR")
            }
        }
    }
    
    @IBAction func sendAction(_ sender: UIButton) {
        guard let _token = tokenTextField.text, let token = Int(_token) else {
            return
        }
        
        let param = RegistrationThreeParameters.init(userID: userID, token: token)
        
        webservice.load(resource: ResList.regThree(param)) { (_result, _error) in
            if let data = _result?.data {
                switch data.status {
                case .success:
                    let param = LoginParameters.init(email: self.email, password: self.password.rsaEncrypt())
                    self.login(param: param)
                case .failed:
                    //                    self.navigationController?.popToRootViewController(animated: true)
                    self.showMessage(text: "Wrong token", type: .error)
                case .isExisted:
                    self.showMessage(text: "Email is existed, try another one", type: .error)
                    self.navigationController?.popViewController(animated: true)
                }
            } else {
                print("ERROR: could not init")
            }
            if let error = _error {
                print("REQ ERROR: ", error)
            }
        }
    }
}

extension SignUpPageThree {
    private func setupViews() {
        setupMainView()
        
        finishButton.makeOval(cornerRadius: finishButton.frame.height / 5)
        
        navigationItem.title = "Sign Up"
    }
    
    private func setupMainView() {
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(tapViewAction))
        self.view.addGestureRecognizer(tapGesture)
        self.view.isUserInteractionEnabled = true
    }
    
    @objc func tapViewAction() {
        tokenTextField.resignFirstResponder()
    }
}























