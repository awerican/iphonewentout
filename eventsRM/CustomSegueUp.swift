//
//  CustomSegue.swift
//  eventsRM
//
//  Created by Fure on 08.11.16.
//  Copyright © 2016 uzuner. All rights reserved.
//

import UIKit
import QuartzCore

class CustomSegueUp: UIStoryboardSegue {
    
    override func perform() {
        
        let src = self.source
        let dst = self.destination
        src.view.superview?.insertSubview(dst.view, aboveSubview: src.view)
        src.present(dst, animated: false, completion: nil)
        
    }
    
}
class CustomSegueDown: UIStoryboardSegue {
    
    override func perform() {
        
        let src = self.source
        let dst = self.destination
        
        src.view.superview?.insertSubview(dst.view, aboveSubview: src.view)
        
        self.source.dismiss(animated: false, completion: nil)
    }
}
