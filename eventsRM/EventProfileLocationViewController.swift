//
//  EventProfileLocationViewController.swift
//  WentOut
//
//  Created by Fure on 20.08.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import UIKit
import Mapbox

class EventProfileLocationViewController: UIViewController {
    
    var location: EventLocation!
    
    var eventTitle: String!
    
    @IBOutlet weak var addressLabel: UILabel!
    
    @IBAction func copyAction(_ sender: UIButton) {
        UIPasteboard.general.string = location.location
        sender.setTitle("copied!", for: .normal)
    }
    
    //    @IBAction func copyAction(_ sender: Any) {
//        UIPasteboard.general.string = location.location
//    }
    
    lazy var mapView: MGLMapView = {
        let mapView = MGLMapView.init(frame: UIScreen.main.bounds)
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.isPitchEnabled = false
        mapView.isRotateEnabled = false
        return mapView
    }()
    
    @IBAction func closeAction(_ sender: Any) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
}

private extension EventProfileLocationViewController {
    private func setupViews() {
        setupMapView()
        setupAddressLabel()
    }
    
    private func setupAddressLabel() {
        addressLabel.text = location.location
    }
    
    private func setupMapView() {
        
        let coord = CLLocationCoordinate2D.init(latitude: location.latitude, longitude: location.longitude)
        
        mapView.setCenter(coord, zoomLevel: 12, animated: false)
        view.insertSubview(mapView, at: 0)
        
        mapView.delegate = self
    
        let event = MGLPointAnnotation()
        event.coordinate = coord
        event.title = eventTitle
        
        mapView.addAnnotation(event)
    }
}

extension EventProfileLocationViewController: MGLMapViewDelegate {
    func mapView(_ mapView: MGLMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {
        return nil
    }
    
//    func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
//        return true
//    }
}












