//
//  StoryCollectionViewController.swift
//  WentOut
//
//  Created by Fure on 08.04.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import UIKit

class StoryViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var memoryImageView: UIImageView!
    @IBOutlet weak var eventImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var eventTitleLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var progressStackView: UIStackView!
    @IBOutlet weak var optionButton: UIImageView!
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    var downloadedImages: [String: UIImage] = [:]
    
    var oneTimeExecution = false
    
    var currentStoryIndex = 0
    
    var totalStories: Int!
    
//    var stories: [StoryMemory]!
    
    var storyIsDownloading: Bool = false
    
    var storyDownloader: Downloader<[StoryMemory], EventPageParameters>!
    
//    UserProfileEventParameters
    
    var eventID: String!
    
    var _eventTitle: String!
    var _eventAvatar: String!
    
    lazy var swipeDown: UISwipeGestureRecognizer = {
        let gesture = UISwipeGestureRecognizer.init(target: self, action: #selector(swipeDownAction))
        gesture.direction = .down
        return gesture
    }()
    
    @objc private func swipeDownAction() {
        _closePage()
    }
    
    @IBAction func closePageAction(_ sender: UIButton) {
        _closePage()
    }
    
    private func _closePage() {
        if let storyPageVC = parent as? StoryPageViewController {
            storyPageVC.dismiss(animated: true) {
                let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
                statusBar.isHidden = false
            }
        } else {
            self.navigationController?.dismiss(animated: true, completion: {
                let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
                statusBar.isHidden = false
            })
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        view.frame = UIScreen.main.bounds
        navigationController?.setNavigationBarHidden(true, animated: false)
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.isHidden = true
        
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            let safeTop = window!.safeAreaInsets.top
            print("111 safeTop =", safeTop)
            if safeTop >= 20.0 {
                topConstraint.constant = safeTop
            } else {
                topConstraint.constant = 20.0
            }
        } else {
            topConstraint.constant = 20.0
        }
        
        print("111 top constraints =", topConstraint.constant)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupNetwork()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if oneTimeExecution {
            return
        }
        
        if let storyPageVC = self.parent as? StoryPageViewController {
            let index = storyPageVC.viewControllerList.index(of: self)!
            
            let model = (storyPageVC.storyDownloader.getData()! as [StoryEntity])[index]
            
            let param = EventPageParameters.init(eventID: model.eventID, userID: LocalAuth.userID!)
            
            print("chigga param = ", param)
            
            storyDownloader = Downloader<[StoryMemory], EventPageParameters>.init(resource: URL.News.memoryMini, param: param)
            if let nextHref = model.memories.nextHref {
                storyDownloader.nextHref = URL(string: nextHref)
            }
            storyDownloader.data = model.memories.data
            
            eventID = model.eventID
            totalStories = model.totalMemories
            
//            setupViewController(eventTitle: model.eventTitle, eventAvatar: model.eventAvatar, currentStoryIndex: 0)

            setupViewController(eventTitle: model.eventTitle, eventAvatar: model.eventAvatar)
            
            oneTimeExecution = true
        } else {
            setupViewController(eventTitle: _eventTitle, eventAvatar: _eventAvatar)
            
            
//            let currentPV = self.progressStackView.arrangedSubviews[index] as! UIProgressView
//            currentPV.setProgress(1.0, animated: false)
            
            oneTimeExecution = true
        }
        
//        let storyPageVC = self.parent as! StoryPageViewController
//        let index = storyPageVC.viewControllerList.index(of: self)!
//
//        let model = (storyPageVC.storyDownloader.getData()! as [StoryEntity])[index]
//
////        let model = storyPageVC.storyDownloader.data![index]
//
//        let param = EventPageParameters.init(eventID: model.eventID, userID: LocalAuth.userID!)
//
//        storyDownloader = Downloader<[StoryMemory], EventPageParameters>.init(resource: URL.News.memoryMini, param: param)
//        if let nextHref = model.memories.nextHref {
//            storyDownloader.nextHref = URL(string: nextHref)
//        }
//        storyDownloader.data = model.memories.data
//
//        eventID = model.eventID
//        totalStories = model.totalMemories
//
//        setupViewController(eventTitle: model.eventTitle, eventAvatar: model.eventAvatar, currentStoryIndex: 0)
//
//        oneTimeExecution = true
    }
}

extension StoryViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return memoryImageView
    }
    
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        
    }
    
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        scrollView.setZoomScale(1.0, animated: true)
    }
}
    
extension StoryViewController {

    private func setupViewController(eventTitle: String, eventAvatar: String) {
        
//        let story = stories[0]
        let story = storyDownloader.data![0]
        
        usernameLabel.text = getMediaCreator(story.username)
        timeLabel.text = Date.goodTime(inSec: story.time)
        
        eventTitleLabel.text = eventTitle
        
        eventImageView.image = nil
        memoryImageView.image = nil
        
        Webservice.loadImage(url: eventAvatar) { (_image) in
            self.eventImageView.image = _image
        }
        
        for i in 0...totalStories-1 {
            let progressView = getProgressView()
            if i < currentStoryIndex {
                progressView.setProgress(1.0, animated: false)
            }
            progressStackView.addArrangedSubview(progressView)
        }

        __doTheStory(index: currentStoryIndex)
    }
}

extension StoryViewController {
    
    private func showReportVC() {
        let storyboard = UIStoryboard.init(name: "Second", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ReportViewControllerNav") as! UINavigationController
        let rootVC = controller.viewControllers.first as! ReportViewController
        let downloader = Downloader<SucceedFailedEntity, BlockMemoryParameters>.init(resource: URL.Block.memory, param: BlockMemoryParameters.init(userID: LocalAuth.userID!, memoryID: storyDownloader.data![currentStoryIndex].id))
        rootVC.downloader = downloader
        self.navigationController?.present(controller, animated: true, completion: nil)
    }
    
    private func downloadStories(to index: Int, callback: (()->())?) {
        
        if storyIsDownloading == true {
            callback?()
            return
        }
        
        storyIsDownloading = true
        
        func __download(_index: Int, _callback: (()->())?) {
            if storyDownloader.nextHref == nil {
                _callback?()
                return
            }
            storyDownloader.getNext(section: 0) { (indexPaths) in
                if self.storyDownloader.data!.count > _index {
                    _callback?()
                } else {
                    __download(_index: _index, _callback: nil)
                }
            }
        }
        
        __download(_index: index) {
            self.storyIsDownloading = false
            callback?()
        }
    }
    
    private func downloadImage(url: String, nextURL: String?, callback: @escaping (UIImage?) -> ()) {
        
        func __(_ url: String, callback: ((UIImage?) -> ())? = nil) {
            if let image = downloadedImages[url] {
                callback?(image)
            }
            Webservice.loadImage(url: url) { (_image) in
                if let image = _image {
                    self.downloadedImages[url] = image
                }
                callback?(_image)
            }
        }

        __(url, callback: callback)
        
        if let _nextURL = nextURL {
            __(_nextURL)
        }
    }
    
    private func __doTheStory(index: Int) {
        view.bringSubview(toFront: activityIndicator)
        memoryImageView.image = nil
        usernameLabel.text = " "
        timeLabel.text = " "
        
        
        if index >= self.currentStoryIndex {
            let currentPV = self.progressStackView.arrangedSubviews[index] as! UIProgressView
            currentPV.setProgress(1.0, animated: false)
        }
        
        if index < self.currentStoryIndex {
            let currentPV = self.progressStackView.arrangedSubviews[self.currentStoryIndex] as! UIProgressView
            currentPV.setProgress(0.0, animated: false)
        }
        
        self.currentStoryIndex = index

        func __setupViewIfNeeded(index: Int) {
            let stories = storyDownloader.data!
            
            var nextURL: String?
            if index < stories.count - 1 {
                nextURL = stories[index + 1].media
            }
            
            usernameLabel.text = getMediaCreator(stories[index].username)
            timeLabel.text = Date.goodTime(inSec: stories[index].time)
            
            downloadImage(url: stories[index].media, nextURL: nextURL) { (_image) in
                self.view.sendSubview(toBack: self.activityIndicator)
                if self.currentStoryIndex == index {
                    self.memoryImageView.image = _image
                }
            }
        }
        
        if index < storyDownloader.data!.count {
            print("chigga came from index <")
            __setupViewIfNeeded(index: index)
        } else {
            downloadStories(to: index) {
                if index < self.storyDownloader.data!.count {
                    __setupViewIfNeeded(index: index)
                }
            }
        }
    }
    
    private func getProgressView() -> UIProgressView {
        let pv = UIProgressView()
        pv.progressTintColor = UIColor.white
        return pv
    }
    
    private func setupView() {
        
        optionButton.image = UIImage.init(named: "buttonSettings")?.setColor(.white)
        optionButton.isUserInteractionEnabled = true
        let optionGesture = UITapGestureRecognizer.init(target: self, action: #selector(optionButtonWasTapped(gesture:)))
        optionButton.addGestureRecognizer(optionGesture)
        
        eventImageView.isUserInteractionEnabled = true
        
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 3.0
        scrollView.bounces = false
        scrollView.bouncesZoom = false
        
        eventImageView.makeOval(cornerRadius: eventImageView.frame.height / 5)
        
        memoryImageView.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(memoryImageWasTapped(gesture:)))
        memoryImageView.addGestureRecognizer(tapGesture)
        
        memoryImageView.addGestureRecognizer(swipeDown)
        
        eventImageView.isUserInteractionEnabled = true
        let eventAvatarTapGesture = UITapGestureRecognizer.init(target: self, action: #selector(eventAvatarWasTapped(gesture:)))
        eventImageView.addGestureRecognizer(eventAvatarTapGesture)
        
        eventTitleLabel.isUserInteractionEnabled = true
        let eventTitleTapGesture = UITapGestureRecognizer.init(target: self, action: #selector(eventAvatarWasTapped(gesture:)))
        eventTitleLabel.addGestureRecognizer(eventTitleTapGesture)
        
        usernameLabel.isUserInteractionEnabled = true
        let usernameTapGesture = UITapGestureRecognizer.init(target: self, action: #selector(usernameWasTapped(gesture:)))
        usernameLabel.addGestureRecognizer(usernameTapGesture)
    }
    
    @objc private func optionButtonWasTapped(gesture: UITapGestureRecognizer) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let action1 = UIAlertAction(title: "Report", style: .default) { (action: UIAlertAction) in
            self.showReportVC()
        }
        
        let action2 = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            print("cancel")
        }
        
        alertController.addAction(action1)
        alertController.addAction(action2)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc private func usernameWasTapped(gesture: UITapGestureRecognizer) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
//        controller.profileOwnerID = stories[currentStoryIndex].userID
        controller.profileOwnerID = storyDownloader.data![currentStoryIndex].userID
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @objc private func eventAvatarWasTapped(gesture: UITapGestureRecognizer) {
        if let _ = self.parent as? StoryPageViewController {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "EventProfileViewController") as! EventProfileViewController
            controller.eventID = self.eventID
            self.navigationController?.pushViewController(controller, animated: true)
        } else {
            _closePage()
        }
    }
    
    @objc private func memoryImageWasTapped(gesture: UITapGestureRecognizer) {
        guard let view = gesture.view else {
            return
        }
        
//        let stories = storyDownloader.data!
        
        let location = gesture.location(in: view)
        
        if location.x < view.frame.width / 3 {
            
            let index = currentStoryIndex - 1
            
            if index < 0 {
                openPrevEvent()
            } else {
                __doTheStory(index: index)
            }
            
        } else {
            
            let index = currentStoryIndex + 1
            
            if index > totalStories - 1 {
                openNextEvent()
            } else {
                __doTheStory(index: index)
            }
        }
    }
    
    private func openNextEvent() {
        if let storyPageVC = self.parent as? StoryPageViewController {
            storyPageVC.nextEvent(vc: self)
        } else {
            _closePage()
        }
    }
    
    private func openPrevEvent() {
        if let storyPageVC = self.parent as? StoryPageViewController {
            storyPageVC.prevEvent(vc: self)
        } else {
            _closePage()
        }
    }
    
    private func setupNetwork() {}
    
    private func getMediaCreator(_ username: String) -> String {
        return "@" + username
    }
}









