//
//  SearchPageSeperatorCollectionViewCell.swift
//  WentOut
//
//  Created by Fure on 21.07.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

class SearchPageSeperatorCollectionViewCell: UICollectionViewCell {

    static var currentSize: CGSize {
        get {
            return CGSize.init(width: UIScreen.main.bounds.width, height: 20.0)
        }
    }
}
