//
//  IOGod.swift
//  WentOut
//
//  Created by Fure on 27.12.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import Foundation
import SocketIO

class AnyDecoder<T> where T: Decodable {
    static func decode(entity: Any) -> T?  {
        guard let parsedData = (entity as? String)?.data(using: .utf8, allowLossyConversion: false) else {
            return nil
        }
        guard let entity = try? JSONDecoder().decode(T.self, from: parsedData) else {
            return nil
        }
        return entity
    }
}

class IO {
    private init() {}
}

class IOOnner<ResEntity: Decodable>: IO {
    static func on(socket: SocketIOClient, eventName: String, response: @escaping (ResEntity?)->()) {
        socket.on(eventName) { (data, ack) in
            let entity = AnyDecoder<ResEntity>.decode(entity: data[0])
            response(entity)
        }
    }
}

class IOEmitter<ReqEntity: Encodable, ResEntity: Decodable>: IO {
    static func emit(socket: SocketIOClient, eventName: String, reqEntity: ReqEntity, response: @escaping (ResEntity?)->()) {
        let json = try! JSONEncoder().encode(reqEntity)
        
        let deadTime = 5.0
        
        socket.emitWithAck(eventName, with: [json]).timingOut(after: deadTime) { (_callback) in
//            print(_callback)
            let entity = AnyDecoder<ResEntity>.decode(entity: _callback[0])
            response(entity)
        }
    }
}


















