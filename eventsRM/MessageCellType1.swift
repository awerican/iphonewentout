//
//  MessageCellType1.swift
//  WentOut
//
//  Created by Fure on 05.09.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

class MessageCellType1: UITableViewCell {

    @IBOutlet weak var messageText: UILabel!
    @IBOutlet weak var messageTime: UILabel!
    @IBOutlet weak var messageBackground: UIView!
    
    func confiCell(msg: MessageEntity) {
        messageText.text = msg.text ?? ""
        
        messageBackground.makeOval(cornerRadius: 15.0)
        
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        
        if let time = msg.time {
            let timeString = dateFormatter.string(from: time)
            messageTime.text = timeString
        } else {
            let timeString = dateFormatter.string(from: Date())
            messageTime.text = timeString
        }
    }
}
