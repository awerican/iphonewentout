//
//  PostMemoryViewController.swift
//  WentOut
//
//  Created by Fure on 20.05.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import UIKit
import AVKit
import CropViewController

class MemorySendEntity: Uploadable {
    var data: Data
    var mimeType: String
    
    init(videoURL: URL) {
        let asset = AVAsset(url: videoURL)
        let imgGenerator = AVAssetImageGenerator(asset: asset)
        imgGenerator.appliesPreferredTrackTransform = true
        
        self.mimeType = "video/mp4"
        self.data = try! Data.init(contentsOf: videoURL)
    }
    
    init(image: UIImage, compressionQuality: CGFloat = 0.75) {
        self.mimeType = "image/jpeg"
        self.data = UIImageJPEGRepresentation(image, compressionQuality)!
    }
}

class PostMemoryViewController: UIViewController {
    
    var media: UploadableMedia?
    
    var event: EventPanel!
    
    let memoryImageRation = CGSize.init(width: 1.1, height: 2)
    
    lazy var usernameDownloader: Downloader<UserMini, UserIDParameter> = {
        return Downloader<UserMini, UserIDParameter>.init(resource: URL.UserProfile.username, param: UserIDParameter.init(userID: LocalAuth.userID!))
    }()
    
    @IBOutlet weak var eventImage: UIImageView!
    
    @IBOutlet weak var eventTime: UILabel!
    @IBOutlet weak var eventTitle: UILabel!
    @IBOutlet weak var eventAddress: UILabel!
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var usernameLabel: UILabel!
    
    @IBAction func doneButton(_ sender: UIBarButtonItem) {
        let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        activityIndicator.color = .gray
        self.navigationItem.titleView = activityIndicator
        activityIndicator.startAnimating()
        sender.isEnabled = false
        
        uploadImage()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let media = self.media {
            self.imageView.image = media.thumbnail
        }
        
        usernameDownloader.getFirst {
            if let data = self.usernameDownloader.data {
                self.usernameLabel.text = data.username
            }
        }
        
        Webservice.loadImage(url: event!.eventPhoto) { (image) in
            self.eventImage.image = image
        }
        eventTitle.text = event!.title
        eventAddress.text = event!.location.location
        eventTime.text = Date.goodTime(inSec: Int(event!.time.timeIntervalSince1970))
        
        imageView.clipsToBounds = true
        imageView.makeOval(cornerRadius: imageView.frame.width / 12)
        eventImage.makeOval(cornerRadius: eventImage.frame.height / 7)
    }
}

extension PostMemoryViewController {
    private func uploadImage() {
        Webservice.uploadMemory(payload: self.media!) { (_proccess, _res) in
            if let res = _res {
                let param = InsertMemoryParameters(userID: LocalAuth.userID!, eventID: self.event.eventId, filename: res.filename, mimetype: res.mimeType)
                webservice.load(resource: ResList.insertMemory(param), completion: { (_res, _error) in
                    if let res = _res {
                        if let data = res.data {
                            let status = data.status
                            if status == .succeed {
                                self.dismiss(animated: true, completion: {
                                    self.navigationController?.dismiss(animated: true, completion: nil)
                                })
                            }
                            if status == .failed {
                                self.dismiss(animated: true, completion: nil)
                            }
                        }
                    }
                })
            } else {
                print("HOLE SHIT")
            }
        }
    }
}
