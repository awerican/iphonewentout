//
//  ReportViewControllerReasonCell.swift
//  WentOut
//
//  Created by Fure on 19.10.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

class ReportViewControllerReasonCell: UICollectionViewCell {

    static var currentSize: CGSize {
        get {
            return CGSize.init(width: UIScreen.width, height: 37.0)
        }
    }
    
    @IBOutlet weak var reason: UILabel!

    func configCell(reason: String) {
        self.reason.text = reason
    }
}
