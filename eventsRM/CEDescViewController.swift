//
//  CEDescViewController.swift
//  WentOut
//
//  Created by Fure on 26.07.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import UIKit

class CEDescViewController: UIViewController {
    
    let textViewTag: Int = 7396
    static let defaultTextViewText: String = "Add description"
    
    weak var delegate: PublishEventDelegate?
    
    lazy var doneButton: UIBarButtonItem = {
        let button = UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(doneButtonAction))
        button.isEnabled = false
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationBar()
        setup(textView: view.viewWithTag(textViewTag) as? UITextView)
    }
}

extension CEDescViewController {
    @objc private func doneButtonAction() {
        if let rootVC = self.navigationController?.viewControllers.first as? PublishEventDelegate {
            if let tv = view.viewWithTag(textViewTag) as? UITextView {
                rootVC.add(eventDesc: tv.text)
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
}

extension CEDescViewController {
    private func setupNavigationBar() {
        navigationItem.rightBarButtonItems = [doneButton]
    }
    
    private func setup(textView: UITextView?) {
        guard let tv = textView else {
            return
        }
        
        tv.delegate = self
        
        setDefaults(to: tv)
        
        tv.becomeFirstResponder()
    }
    
    private func setDefaults(to textView: UITextView) {
        textView.text = CEDescViewController.defaultTextViewText
        textView.textColor = .lightGray
        let newPosition = textView.beginningOfDocument
        textView.selectedTextRange = textView.textRange(from: newPosition, to: newPosition)
    }
    
    private func setNormal(to textView: UITextView) {
        textView.textColor = UIColor.darkGray
    }
}

extension CEDescViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText = textView.text
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        
        if text == "\n" {
            return false
        }
        
        if newText.count > 200 {
            return false
        }
        
        if currentText == CEDescViewController.defaultTextViewText && newText.count == 0 {
            print("Im here 0")
            return false
        }
        if currentText == CEDescViewController.defaultTextViewText && text.count > 0 {
            print("Im here 1")
            textView.text = text
            doneButton.isEnabled = true
            setNormal(to: textView)
            return false
        }
        if currentText == CEDescViewController.defaultTextViewText && text.count == 0 {
            print("Im here 2")
            doneButton.isEnabled = false
            setDefaults(to: textView)
            return true
        }
        if newText.count == 0 {
            print("Im here 3")
            doneButton.isEnabled = false
            setDefaults(to: textView)
            return false
        }
        print("Im here 4")
        doneButton.isEnabled = true
        setNormal(to: textView)
        return true
    }
}






















