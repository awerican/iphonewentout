//
//  UserProfileHeader2CollectionViewCell.swift
//  WentOut
//
//  Created by Fure on 03.07.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

class UserProfileEvent2CollectionView: UICollectionViewCell {
    
    @IBOutlet weak var eventTitle: UILabel!
    
    @IBOutlet weak var eventTime: UILabel!
    @IBOutlet weak var eventImage: UIImageView!
    
    @IBOutlet weak var eventLocation: UILabel!
    @IBOutlet weak var openButtonBackview: UIView!
    
    @IBOutlet weak var openButton: UIButton!
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.contentView.backgroundColor = UIColor(hex: "E4E4E4")
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
        self.contentView.backgroundColor = UIColor.white
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        self.contentView.backgroundColor = UIColor.white
    }
    
    var currentSize: CGSize {
        get {
            return CGSize.init(width: UIScreen.width, height: 90.0)
        }
    }
    
    override func draw(_ rect: CGRect) {
        eventImage.makeOval(cornerRadius: eventImage.frame.height / 7)
        openButtonBackview.makeOval(cornerRadius: openButtonBackview.frame.height / 5)
        openButton.makeOval(cornerRadius: openButton.frame.height / 7)
    }
    
    func configCell(model: EventPanel) {
        eventTitle.text = model.title
        eventTime.text = model.time.eventPanel1
        eventLocation.text = model.location.location
        Webservice.loadImage(url: model.eventPhoto) { (_image) in
            if let image = _image {
                self.eventImage.image = image
            }
        }
    }
}
