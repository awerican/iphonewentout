//
//  RecoverPageTwo.swift
//  WentOut
//
//  Created by Fure on 29.07.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import UIKit

class RecoverPageTwo: UIViewController {
    
    @IBOutlet weak var newPassTextField: UITextField!
    
    @IBOutlet weak var newPassRepTextField: UITextField!
    
    @IBOutlet weak var tokenTextField: UITextField!
    
    @IBOutlet weak var doneButton: UIButton!
    
    var email: String!
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    @IBAction func sendAction(_ sender: UIButton) {
        guard let password = passwordIsValid() else {
            print("password is invalid")
            return
        }
        
        guard let _token = tokenTextField.text, let token = Int(_token) else {
            print("token is invalid")
            return
        }
        
        let param = RecoverTwoParameters.init(email: email, token: token.description, newPassword: password.rsaEncrypt())
        
        webservice.load(resource: ResList.recoverTwo(param)) { (_result, _error) in
            if let data = _result?.data {
                switch data.status {
                case .succeed:
                    self.navigationController?.popToRootViewController(animated: true)
                case .failed:
                    print("email is not exist or some other reason")
                }
            } else {
                print("ERROR: could not init")
            }
            if let error = _error {
                print("REQ ERROR: ", error)
            }
        }
    }
}

extension RecoverPageTwo {
    private func setupView() {
        doneButton.makeOval(cornerRadius: doneButton.frame.height / 5)
        navigationItem.title = "Recovery"
    }
    
    private func passwordIsValid() -> String? {
        if let passOne = newPassTextField.text, let passTwo = newPassRepTextField.text {
            if passOne.count > 5 {
                if passOne == passTwo {
                    return passOne
                }
            }
        }
        return nil
    }
}



















