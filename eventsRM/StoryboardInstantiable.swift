//
//  StoryboardInstantiable.swift
//  WentOut
//
//  Created by Fure on 04.09.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import Foundation
import UIKit

//protocol StoryboardInstantiable {
//
//    static var storyboardName: String { get }
//    static var storyboardBundle: Bundle? { get }
//    static var storyboardIdentifier: String? { get }
//}
//
//extension StoryboardInstantiable {
//    
//    static var storyboardBundle: Bundle? { return nil }
//    static var storyboardIdentifier: String? { return nil }
//
//    static func makeFromStoryboard() -> Self {
//        let storyboard = UIStoryboard(name: storyboardName, bundle: storyboardBundle)
//
//        if let storyboardIdentifier = storyboardIdentifier {
//            return storyboard.instantiateViewController(withIdentifier: storyboardIdentifier) as! Self
//        } else {
//            return storyboard.instantiateInitialViewController() as! Self
//        }
//    }
//}
