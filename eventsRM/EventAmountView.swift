//
//  EventAmountButton.swift
//  WentOut
//
//  Created by Fure on 30.06.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import Foundation
import UIKit

class EventAmountView: UIView, Nibable {
    @IBOutlet var contentView: UIView!
    @IBOutlet var visualEffectView: UIVisualEffectView!
    @IBOutlet private var amountLabel: UILabel!
    
    var bundleName = "EventAmountView"
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupViewFromNib()
        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupViewFromNib()
        self.setupViews()
    }
    
    func setEventAmount(_ quantity: Int) {
        self.amountLabel.text = quantity.description + " events"
        if self.isHidden {
            self.isHidden = false
        }
    }
    
    private func setupViews() {
        self.visualEffectView.makeOval(cornerRadius: self.visualEffectView.frame.height / 4)
        self.visualEffectView.layer.borderColor = UIColor.white.cgColor
        self.visualEffectView.layer.borderWidth = 1.0
        self.isHidden = true
    }
}
