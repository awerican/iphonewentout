//
//  Downloader.swift
//  WentOut
//
//  Created by Fure on 09.06.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation

protocol Downloadable: class {
    func getData<T: Decodable & Equatable>() -> T?
    var nextHref: URL? { get set }
    func getNext(section: Int, insert: @escaping ([IndexPath])->())
    func getFirst(reload: @escaping ()->())
    var url: URL { get }
}

class Downloader<E: Decodable & Equatable, P: Decodable & Encodable>: Downloadable {
    
    func getData<T>() -> T? where T : Decodable, T : Equatable {
        return self.data as? T
    }
    
    private var param: P
    private var resource: URL
    private var next_href: URL?
    private var _firstDownloadIsFinished = false
    
    var url: URL {
        get {
            return resource
        }
    }
    
    var firstDownloadIsFinished: Bool {
        get {
            return _firstDownloadIsFinished
        }
    }
    
    var data: E? {
        didSet {
            if oldValue == nil {
                self.firstData = self.data
            }
        }
    }
    
    var nextHref: URL? {
        get {
            return next_href
        }
        set {
            next_href = newValue
        }
    }
    
    var firstData: E?
    
    init(resource: URL, param: P) {
        self.resource = resource
        self.param = param
    }
    
    func changeParam(newParam: P) {
        self.param = newParam
    }
    
    func getParam() -> P {
        return self.param
    }
    
    func clone() -> Downloader<E, P> {
        let downloader = Downloader<E, P>.init(resource: resource, param: param)
        downloader.data = data
        downloader.next_href = next_href
        return downloader
    }
    
    func getFirst(reload: @escaping ()->()) {
        download(nil) { (result, _next_url) in
            self._firstDownloadIsFinished = true
            if let res = result {
                // if result is array
                if let resArray = res as? [Decodable] {
                    // current data that is array is not null
                    if let currentData = self.data as? [Decodable] {
                        if currentData.count < resArray.count {
                            self.data = res
                            if let next_url = _next_url {
                                self.next_href = URL.init(string: next_url)
                            } else {
                                self.next_href = nil
                            }
                            reload()
                        } else {
                            if res != self.firstData! {
                                self.firstData = res
                                self.data = res
                                if let next_url = _next_url {
                                    self.next_href = URL.init(string: next_url)
                                } else {
                                    self.next_href = nil
                                }
                                reload()
                            }
                        }
                    } else {
                        self.data = res
                        if let next_url = _next_url {
                            self.next_href = URL.init(string: next_url)
                        } else {
                            self.next_href = nil
                        }
                        reload()
                    }
                } else {
                    if let currentData = self.data {
                        if !self.isEqual(value1: currentData, value2: res) {
                            self.data = res
                            reload()
                        }
                    } else {
                        self.data = res
                        reload()
                    }
                }
            } else {
                reload()
            }
        }
    }
    
    func isEqual<T: Equatable>(value1: T, value2: T) -> Bool {
        if value1 == value2 {
            return true
        } else {
            return false
        }
    }
    
    func isEqual(left: NSObjectProtocol, right: NSObjectProtocol) -> Bool {
        return left.isEqual(right)
    }
    
    func getNext(section: Int, insert: @escaping ([IndexPath])->()) {
        
        guard let href = next_href else {
            return
        }
        
        download(href) { (result, _next_url) in
            if let arrayResult = result as? Array<Any> {
                // on this place it crashes
                
                if var _data = self.data as? Array<Any> {
                    var paths: [IndexPath] = []
                    for (i, _) in arrayResult.enumerated() {
                        paths.append(IndexPath.init(row: _data.count + i, section: section))
                    }
                    _data.append(contentsOf: arrayResult)
                    self.data = _data as? E
                    if let next_url = _next_url {
                        self.next_href = URL.init(string: next_url)
                    } else {
                        self.next_href = nil
                    }
                    insert(paths)
                } else {
                    print("I DONT KNOW WHAT TO DO")
                }
                
//                var _data = self.data as! Array<Any>
//                var paths: [IndexPath] = []
//                for (i, _) in arrayResult.enumerated() {
//                    paths.append(IndexPath.init(row: _data.count + i, section: section))
//                }
//                _data.append(contentsOf: arrayResult)
//                self.data = _data as? E
//                if let next_url = _next_url {
//                    self.next_href = URL.init(string: next_url)
//                } else {
//                    self.next_href = nil
//                }
//                insert(paths)
            } else {
                print("I DONT KNOW WHAT TO DO 2")
            }
        }
    }
    
    private func download(_ next: URL?, callback: @escaping (E?, String?)->()) {
        
        let _url: URL = (next != nil) ? next! : self.resource
        
        let res = Resource<ResponseEntity<E>, P>.init(url: _url, params: param, parseJSON: { (json) -> ResponseEntity<E>? in
            guard let result = try? JSONDecoder().decode(ResponseEntity<E>.self, from: json as! Data) else {
                return nil
            }
            return result
        })
        
        webservice.load(resource: res) { (_result, _error) in
            if let res = _result {
                callback(res.data, res.next_url)
            } else {
                callback(nil, nil)
            }
        }
    }
}
