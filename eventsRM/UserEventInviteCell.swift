//
//  UserEventInviteCell.swift
//  WentOut
//
//  Created by Fure on 09.09.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

class UserEventInviteCell: UICollectionViewCell {

    @IBOutlet weak var eventTitle: UILabel!
    
    @IBOutlet weak var eventTime: UILabel!
    @IBOutlet weak var eventImage: UIImageView!
    @IBOutlet weak var eventLocation: UILabel!
    
    @IBOutlet weak var checkBox: UIButton!
    
    static var currentSize: CGSize {
        get {
            return CGSize.init(width: UIScreen.width, height: 90.0)
        }
    }
    
    override func draw(_ rect: CGRect) {
        eventImage.makeOval(cornerRadius: eventImage.frame.height / 7)
    }
    
    func configCell(model: EventPanel, isChecked: Bool) {
        eventTitle.text = model.title
        eventTime.text = model.time.eventPanel1
        eventLocation.text = model.location.location
        Webservice.loadImage(url: model.eventPhoto) { (_image) in
            if let image = _image {
                self.eventImage.image = image
            }
        }
        if isChecked {
            checkBox.setImage(#imageLiteral(resourceName: "CheckBoxState1"), for: .normal)
        } else {
            checkBox.setImage(#imageLiteral(resourceName: "CheckBoxState0"), for: .normal)
        }
    }
}
