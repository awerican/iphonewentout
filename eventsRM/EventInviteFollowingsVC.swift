//
//  EventInviteFollowingsVC.swift
//  WentOut
//
//  Created by Fure on 12.08.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import UIKit

class EventInviteFollowingsVC: UICollectionViewController {
    
    typealias userID = String
    
    lazy var followingsDownloader: Downloader<[UserMini], UserIDParameter> = {
        return Downloader<[UserMini], UserIDParameter>.init(resource: URL.Event.followingsList, param: UserIDParameter(userID: LocalAuth.userID!))
    }()
    
    var eventID: String!
    
    var selectedItems: [IndexPath: userID] = [:] {
        didSet {
            if selectedItems.count > 0 {
                addInviteButtonIfNeeded()
            } else {
                removeInviteButtonIfNeeded()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNetworking()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension EventInviteFollowingsVC {
    private func setupNetworking() {
        followingsDownloader.getFirst {
            self.collectionView?.reloadSections([0])
        }
    }
    
    private func setupViews() {
        setupCollectionView()
    }
    
    private func setupCollectionView() {
        collectionView!.contentInset.top = 9.0
        collectionView!.contentInset.bottom = UITabBar.height + 10.0
        for reuseID in self.reuseIds {
            collectionView!.register(UINib.init(nibName: reuseID, bundle: nil), forCellWithReuseIdentifier: reuseID)
        }
    }
    
    private func addInviteButtonIfNeeded() {
        func _addItem() {
            let inviteButton = UIBarButtonItem.init(title: "Invite", style: .done, target: self, action: #selector(inviteFriendsAction(_:)))
            navigationItem.rightBarButtonItems = [inviteButton]
        }
        
        if let items = navigationItem.rightBarButtonItems {
            if items.count == 0 {
                _addItem()
            }
        } else {
            _addItem()
        }
    }
    
    private func removeInviteButtonIfNeeded() {
        if navigationItem.rightBarButtonItems?.count > 0 {
            navigationItem.rightBarButtonItems = []
        }
    }
    
    @objc private func inviteFriendsAction(_ sender: UIBarButtonItem) {
        sender.isEnabled = false
        
        let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        activityIndicator.color = .gray
        navigationItem.titleView = activityIndicator
        activityIndicator.startAnimating()
        
        collectionView?.isUserInteractionEnabled = false
        
        let guestIDs = selectedItems.values.sorted()
        
        let param = EventInviteParameters(userID: LocalAuth.userID!, eventGuests: guestIDs, eventID: eventID)
        
        print("chigga param =", param)
        
        webservice.load(resource: ResList.invite(param)) { (_result, _error) in
            if let result = _result {
                if let status = result.data?.status {
                    switch status {
                    case .succeed:
                        print("chigga success")
                        self.dismiss(animated: true, completion: nil)
                    case .failed:
                        print("chigga failed")
                        self.dismiss(animated: true, completion: nil)
                    }
                } else {
                    print("chigga no status")
                    self.dismiss(animated: true, completion: nil)
                }
            } else {
                print("chigga no result")
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
}

extension EventInviteFollowingsVC {
    var reuseIds: [String] {
        get {
            return ["EventInviteFollowingsCell"]
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return followingsDownloader.data?.count ?? 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let _ = selectedItems[indexPath] {
            selectedItems.removeValue(forKey: indexPath)
        } else {
            selectedItems[indexPath] = followingsDownloader.data![indexPath.row].id
        }
        collectionView.reloadItems(at: [indexPath])
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIds[0], for: indexPath) as! EventInviteFollowingsCell
        
        let model = followingsDownloader.data!
        var isCheckBoxed = false
        if let _ = selectedItems[indexPath] {
            isCheckBoxed = true
        }
        cell.configCell(model: model[indexPath.row], isCheckBoxed: isCheckBoxed)
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let data = self.followingsDownloader.data else {
            return
        }
        if indexPath.row == data.count - 1 {
            self.followingsDownloader.getNext(section: 0) { (paths) in
                collectionView.insertItems(at: paths)
            }
        }
    }
}

extension EventInviteFollowingsVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return EventInviteFollowingsCell.currentSize
    }
}




























