//
//  BarControllerProtocols.swift
//  eventsRM
//
//  Created by Fure on 01.11.17.
//  Copyright © 2017 uzuner. All rights reserved.
//

import Foundation
import UIKit

protocol StatusBarMenuProtocol {
    func setupStatusBarView()
}

extension StatusBarMenuProtocol where Self: UIViewController {
    func setupStatusBarView() {
        let statusBarView = UIView(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 20))
        statusBarView.backgroundColor = UIColor.white
        self.view.addSubview(statusBarView)
    }
}
