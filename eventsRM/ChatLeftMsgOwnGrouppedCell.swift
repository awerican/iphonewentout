//
//  ChatLeftMsgOwnGrouppedCell.swift
//  WentOut
//
//  Created by Fure on 16.10.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import UIKit

class ChatLeftMsgOwnGrouppedCell: ChatMessageBaseCell {

    @IBOutlet var messageOutlet: UILabel!
    @IBOutlet var messageBackgroundOutlet: UIView!
    @IBOutlet var messageTimeOutlet: UILabel!
    @IBOutlet var timeLeadingConstrOutlet: NSLayoutConstraint!
    @IBOutlet var timeTopConstrOutlet: NSLayoutConstraint!

    override var message: UILabel! {
        return messageOutlet
    }
    override var messageBackground: UIView! {
        return messageBackgroundOutlet
    }
    override var time: UILabel! {
        return messageTimeOutlet
    }
    override var timeLeadingConstr: NSLayoutConstraint? {
        return timeLeadingConstrOutlet
    }
    override var timeTopConstr: NSLayoutConstraint? {
        return timeTopConstrOutlet
    }
}
