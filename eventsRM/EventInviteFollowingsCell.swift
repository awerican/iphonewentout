//
//  EventInviteFollowingsCell.swift
//  WentOut
//
//  Created by Fure on 12.08.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

class EventInviteFollowingsCell: UICollectionViewCell {

    @IBOutlet weak var userAvatar: UIImageView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var checkBox: UIButton!
    
    static var currentSize: CGSize {
        get {
            return CGSize.init(width: UIScreen.width, height: 65.0)
        }
    }
    
    func configCell(model: UserMini, isCheckBoxed: Bool) {
        Webservice.loadImage(url: model.avatar) { (_image) in
            self.userAvatar.image = _image
        }
        username.text = model.username
        userAvatar.makeCircle()
        if isCheckBoxed {
            checkBox.setImage(#imageLiteral(resourceName: "CheckBoxState1"), for: .normal)
        } else {
            checkBox.setImage(#imageLiteral(resourceName: "CheckBoxState0"), for: .normal)
        }
    }
}






