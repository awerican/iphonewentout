//
//  UINavBar.swift
//  eventsRM
//
//  Created by Fure on 21.08.17.
//  Copyright © 2017 uzuner. All rights reserved.
//

import UIKit
import VisualEffectView

extension UINavigationBar {

    var topBarButtonTag: Int {
        get {
            return 14020
        }
    }
    
    static private let navigationHeight: CGFloat = 35
    
    static var totalHeight: CGFloat {
        get {
            return navigationHeight + 20.0
        }
    }
}
