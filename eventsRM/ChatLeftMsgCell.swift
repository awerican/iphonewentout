//
//  TableViewCell.swift
//  WentOut
//
//  Created by Fure on 03.10.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import UIKit

class ChatLeftMsgCell: ChatMessageBaseCell {
    
    @IBOutlet var avatarOutlet: UIImageView!
    @IBOutlet var usernameOutlet: UILabel!
    @IBOutlet var messageOutlet: UILabel!
    @IBOutlet var messageBackgroundOutlet: UIView!
    @IBOutlet var timeOutlet: UILabel!
    @IBOutlet var timeLeadingConstrOutlet: NSLayoutConstraint!
    @IBOutlet var timeTopConstrOutlet: NSLayoutConstraint!
    
    override var username: UILabel? {
        return self.usernameOutlet
    }
    override var avatar: UIImageView! {
        return self.avatarOutlet
    }
    override var message: UILabel! {
        return self.messageOutlet
    }
    override var messageBackground: UIView! {
        return self.messageBackgroundOutlet
    }
    override var time: UILabel! {
        return self.timeOutlet
    }
    override var timeLeadingConstr: NSLayoutConstraint? {
        return self.timeLeadingConstrOutlet
    }
    override var timeTopConstr: NSLayoutConstraint? {
        return self.timeTopConstrOutlet
    }
}

//protocol ChatLeftMsgDelegate { }
//
//protocol ChatLeftMsgInteractor: MessageInteractor { }
//
//class ChatLeftMsgCell: UITableViewCell {
//    typealias CellDelegate = ChatLeftMsgDelegate
//    typealias CellData = MessageWrapper
//    typealias CellInteractor = ChatLeftMsgInteractor
//
//    @IBOutlet var username: UILabel!
//    @IBOutlet var avatar: UIImageView!
//    @IBOutlet var message: UILabel!
//    @IBOutlet var messageBackground: UIView!
//    @IBOutlet var messageTime: UILabel!
//
//    @IBOutlet var messageTimeLeadingConstr: NSLayoutConstraint!
//    @IBOutlet var messageTimeTopConstr: NSLayoutConstraint!
//
//    override func draw(_ rect: CGRect) {
//        self.avatar.makeCircle()
//        self.messageBackground.makeOval(cornerRadius: 15.0)
//    }
//}
//
//extension ChatLeftMsgCell: ChatLeftMsgInteractor { }
//
//extension ChatLeftMsgCell: ConfigurableCell {
//    func configure(with data: ChatLeftMsgCell.CellData, delegate: ChatLeftMsgCell.CellDelegate?) {
//        self.avatar.image = nil
//        Webservice.loadImage(url: data.message.senderAvatarURL) { (_image) in
//            if let image = _image {
//                self.avatar.image = image
//            }
//        }
//        self.username.text = data.message.senderUsername
//        self.message.text = data.message.msg
//
//        let dateFormatter: DateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "HH:mm"
//        let timeString = dateFormatter.string(from: data.message.time)
//        self.messageTime.text = timeString
//
//        if data.avatarIsHidden {
//            self.avatar.isHidden = true
//        } else {
//            self.avatar.isHidden = false
//        }
//
//        self.layoutIfNeeded()
//
//        if self.message.calculateMaxLines() > 1 {
//            self.messageTimeLeadingConstr.constant = -self.messageTime.frame.width
//            self.messageTimeTopConstr.constant = 0.0
//        } else {
//            self.messageTimeTopConstr.constant = -15.0
//            self.messageTimeLeadingConstr.constant = 11.0
//        }
//    }
//}

































