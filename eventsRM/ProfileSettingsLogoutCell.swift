//
//  ProfileSettingsLogoutCell.swift
//  WentOut
//
//  Created by Fure on 29.07.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

class ProfileSettingsLogoutCell: UICollectionViewCell {

    static var currentSize: CGSize {
        get {
            return CGSize.init(width: UIScreen.width, height: 49.0)
        }
    }
    
}
