//
//  SearchControllerSecondCell.swift
//  eventsRM
//
//  Created by Fure on 30.10.17.
//  Copyright © 2017 uzuner. All rights reserved.
//

import UIKit

class SearchControllerSecondCell: UICollectionViewCell, UISearchBarDelegate {

    @IBOutlet var searchBar: UISearchBar! {
        didSet {
            searchBar.delegate = self
        }
    }
    
    static var currentSize: CGSize {
        get {
            return CGSize.init(width: UIScreen.width, height: 56.0)
        }
    }
    
//    var dataModel: NumberOfMainSection!
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
//        dataModel?.showSearchNavigationBar = false
        self.searchBar.showsCancelButton = true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count > 0 {
//            dataModel?.searchBarTextFieldHasText = true
//            if searchText.count < 3 {
//                dataModel?.resultModel = ["one"]
//            } else {
//                dataModel?.resultModel = ["one", "two", "three"]
//            }
        } else {
//            dataModel?.searchBarTextFieldHasText = false
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = false
        searchBar.text = nil
        searchBar.resignFirstResponder()
//        dataModel?.searchBarTextFieldHasText = false
//        dataModel?.showSearchNavigationBar = true
    }
    
}
