//
//  LoginPageController.swift
//  WentOut
//
//  Created by Fure on 08.05.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import UIKit

class LoginPageController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
//    @IBOutlet weak var signupButton: UIButton!
    
    let segueID = "segueToMainApp"
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupNavBar()
        print("view did load bitch")
    }
    
//    @IBAction func termsOfUserAction(_ sender: UIButton) {
//        showSafariVC(for: URL.Agreement.termsOfUser)
//    }
//
//    @IBAction func privacyPolicyAction(_ sender: UIButton) {
//        showSafariVC(for: URL.Agreement.privacyPolicy)
//    }
    
    private func setupNavBar() {
        navigationItem.title = "Welcome"
    }
    
//    private func showSafariVC(for url: URL) {
//        let safariVC = SFSafariViewController(url: url)
//        safariVC.delegate = self
//        present(safariVC, animated: true, completion: nil)
//    }
    
    @IBAction func loginAction(_ sender: UIButton) {
        print("print login action")
        guard let email = validatedEmail() else {
            showMessage(text: "Email is incorrect", type: .error)
            print("email is incorrect")
            return
        }
        
        guard let password = validatedPass() else {
            showMessage(text: "Password is incorrect", type: .error)
            return
        }
        
        let param = LoginParameters.init(email: email, password: password.rsaEncrypt())
        
        showLoadingIndicator()
        
        webservice.load(resource: ResList.login(param)) { (_result, _error) in
            if let error = _error {
                print("REQ ERROR =", error)
                return
            }
            
            self.removeLoadingIndicator()
            
            if let result = _result?.data {
                switch result.status {
                case .success:
                    if let key = result.aesKey, let token = result.token, let userID = result.userID {
                        LocalAuth.saveLoginInfo(aesKey: key, token: token, userID: userID)
                        // open an app
                        self.performSegue(withIdentifier: self.segueID, sender: nil)
                    } else {
                        self.showMessage(text: "Sorry, something went wrong!", type: .error)
                    }
                case .incorrectEmail:
                    self.showMessage(text: "Email is incorrect", type: .error)
                case .incorrectPass:
                    self.showMessage(text: "Password is incorrect", type: .error)
                }
            } else {
                self.showMessage(text: "Error!", type: .error)
            }
        }
    }
}

extension LoginPageController {
    
    private func validatedPass() -> String? {
        if let pass = passTextField.text {
            if pass.count > 0 {
                return pass
            }
        }
        return nil
    }
    
    private func validatedEmail() -> String? {
        if let email = emailTextField.text {
            if EmailChecker.isValidEmail(email) {
                return email
            }
        }
        return nil
    }
    
    private func setupView() {
        view.backgroundColor = UIColor.init(hex: "F9F9F9")
        
        loginButton.makeOval(cornerRadius: loginButton.frame.height / 5)
//        signupButton.makeOval(cornerRadius: signupButton.frame.height / 5)
        
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(tapViewAction))
        self.view.addGestureRecognizer(tapGesture)
        self.view.isUserInteractionEnabled = true
    }
    
    @objc func tapViewAction() {
        self.emailTextField.resignFirstResponder()
        self.passTextField.resignFirstResponder()
    }
}












