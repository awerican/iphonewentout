//
//  StoryPageViewController.swift
//  WentOut
//
//  Created by Fure on 06.08.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import UIKit

class StoryPageViewController: UIPageViewController, UIPageViewControllerDelegate {
    
    var viewControllerList: [UIViewController] = []
    
    
//    var storyDownloader: Downloader<[StoryEntity], UserIDParameter>!
    
    var storyDownloader: Downloadable!
    
    var beginIndex: Int!
    
    var isAnimating: Bool = false
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        view.frame = UIScreen.main.bounds
        navigationController?.setNavigationBarHidden(true, animated: false)
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupVC()
        setupNetworking()
        print("STORY OPEN")
    }
}

private extension StoryPageViewController {
    
    private func setupVC() {
        delegate = self
        dataSource = self
        view.backgroundColor = UIColor.white
    }
    
    private func setupNetworking() {
        let sb = UIStoryboard.init(name: "Second", bundle: nil)

        for _ in self.storyDownloader.getData()! as [StoryEntity] {
            let vc = sb.instantiateViewController(withIdentifier: "StoryViewController")
            self.viewControllerList.append(vc)
        }
        
//        for _ in self.storyDownloader.data! {
//            let vc = sb.instantiateViewController(withIdentifier: "StoryViewController")
//            self.viewControllerList.append(vc)
//        }
        
        let firstViewController = self.viewControllerList[beginIndex]
        self.setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
    }
}

extension StoryPageViewController {
    func nextEvent(vc: UIViewController) {
        if isAnimating == true {
            return
        }
        guard let index = viewControllerList.index(of: vc) else {
            return
        }
        if index == viewControllerList.count - 1 {
            if storyDownloader.nextHref == nil {
                self.dismiss(animated: true, completion: nil)
            } else {
                storyDownloader.getNext(section: 0) { (indexPaths) in
                    print("chigga indexPaths =", indexPaths)
                    for _ in indexPaths {
                        let sb = UIStoryboard.init(name: "Second", bundle: nil)
                        let vc = sb.instantiateViewController(withIdentifier: "StoryViewController")
                        self.viewControllerList.append(vc)
                    }
                    self.isAnimating = true
                    self.view.isUserInteractionEnabled = false
                    self.setViewControllers([self.viewControllerList[index + 1]], direction: .forward, animated: true, completion: { (finish) in
                        if finish {
                            self.isAnimating = false
                            self.view.isUserInteractionEnabled = true
                        }
                    })
                }
            }
        } else {
            isAnimating = true
            view.isUserInteractionEnabled = false
            setViewControllers([viewControllerList[index + 1]], direction: .forward, animated: true) { (finish) in
                if finish {
                    self.isAnimating = false
                    self.view.isUserInteractionEnabled = true
                }
            }
        }
    }
    func prevEvent(vc: UIViewController) {
        if isAnimating == true {
            return
        }
        guard let index = viewControllerList.index(of: vc) else {
            return
        }
        if index > 0 {
            isAnimating = true
            view.isUserInteractionEnabled = false
            setViewControllers([viewControllerList[index - 1]], direction: .reverse, animated: true) { (finish) in
                if finish {
                    self.isAnimating = false
                    self.view.isUserInteractionEnabled = true
                }
            }
        }
    }
}

extension StoryPageViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let vcIndex = viewControllerList.index(of: viewController) else {
            return nil
        }
        let previousIndex = vcIndex + 1
        guard previousIndex >= 0 else {
            return nil
        }
        guard viewControllerList.count > previousIndex else {
            return nil
        }
        return viewControllerList[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let vcIndex = viewControllerList.index(of: viewController) else {
            return nil
        }
        let nextIndex = vcIndex - 1
        guard nextIndex >= 0 else {
            return nil
        }
        guard viewControllerList.count != nextIndex else {
            return nil
        }
        guard viewControllerList.count > nextIndex else {
            return nil
        }
        return viewControllerList[nextIndex]
    }
}
