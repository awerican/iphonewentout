//
//  BarControllerMap+DataSource.swift
//  eventsRM
//
//  Created by Fure on 10.03.17.
//  Copyright © 2017 uzuner. All rights reserved.
//

import UIKit

extension WorldViewController {
    @objc func longPressGestureHandler(_ gesture: UILongPressGestureRecognizer) {
        if gesture.state == .began {
            gestureBegin()
        } else if gesture.state == .changed {
            gestureChanged(gesture)
        } else if gesture.state == .ended {
            gestureEnded(gesture)
        } else if gesture.state == .cancelled {
            gestureEnded(gesture)
        } else if gesture.state == .failed {
            gestureEnded(gesture)
        }
        else if gesture.state == .recognized { }
        else if gesture.state == .possible { }
    }
    
    func gestureBegin() {
        self.worldTableVC = self.storyboard?.instantiateViewController(withIdentifier: "WorldTableViewController") as? WorldTableViewController
//        self.worldTableVC!.parentVC = self
        self.worldTableVC!.configVC(parentVC: self, imageCache: self.imageCache, state: .topEvents)
//        self.worldTableVC!.modalPresentationStyle = .overCurrentContext
//        self.worldTableVC!.state = .topEvents
        guard let child = self.worldTableVC else {
            return
        }
        self.addChildViewController(self.worldTableVC!)
        child.view.frame.origin.y = UIScreen.main.bounds.height - UITabBar.height - WorldTableView.headerHeight - WorldTableViewController.tableViewTopConstraintsConstant
        self.view.addSubview(child.view)
        child.didMove(toParentViewController: self)
        (self.navigationController?.tabBarController as! CustomBarController).customTabBar.isUserInteractionEnabled = false
    }
    
    private func gestureChanged(_ gesture: UILongPressGestureRecognizer) {
        guard let child = self.worldTableVC else {
            return
        }
        
        let maxY = UIScreen.main.bounds.height - UITabBar.height - WorldTableView.headerHeight
        
        let location = gesture.location(in: self.view)
        
        if location.y >  maxY {
            return
        }
        
        child.view.frame.origin.y = location.y - WorldTableViewController.tableViewTopConstraintsConstant

//        let diff = self.bottomView.frame.minY - child.view.frame.origin.y - tableViewTopConstraint
//        let newOriginY = (UIScreen.main.bounds.height - UITabBar.height) + UITabBar.height/(self.bottomView.frame.minY - tableViewTopConstraint)*diff
//        (self.navigationController?.tabBarController as! CustomBarController).customTabBar.frame.origin.y = newOriginY
    }
    
    private func gestureEnded(_ gesture: UILongPressGestureRecognizer) {
        guard let child = self.worldTableVC else {
            return
        }
        
        (self.navigationController?.tabBarController as! CustomBarController).moveTabBarDownFromTheSreen()
        UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseIn, animations: {
            [unowned self] in
            child.view.frame.origin.y = 0.0
            (self.navigationController?.tabBarController as! CustomBarController).view.layoutIfNeeded()
        }) { (finish) in
            if finish {
                (self.navigationController?.tabBarController as! CustomBarController).customTabBar.isUserInteractionEnabled = true
            }
        }
    }
}
