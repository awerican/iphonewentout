//
//  SearchPageUserTitleCollectionViewCell.swift
//  WentOut
//
//  Created by Fure on 21.07.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

class SearchPageUserTitleCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    static var currentSize: CGSize {
        get {
            return CGSize.init(width: UIScreen.width, height: 26.0)
        }
    }
    
    func configCell(title: String) {
        titleLabel.text = title
    }
}
