//
//  SearchTypeView.swift
//  WentOut
//
//  Created by Fure on 11.07.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import UIKit

protocol SearchTypeViewDelegate: class {
    func eventButtonWasPressed()
    func userButtonWasPressed()
}

class SearchTypeView: UIView {
    
    private var eventButton: UIButton
    private var userButton: UIButton
    private var underlineView: UIView
    private var bottomLineView: UIView
    private var blurView: UIVisualEffectView
    
    lazy var underlineLeadingConstr: NSLayoutConstraint = {
        return underlineView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0.0)
    }()
    
    weak var delegate: SearchTypeViewDelegate?
    
    override init(frame: CGRect) {
        eventButton = UIButton()
        userButton = UIButton()
        underlineView = UIView()
        bottomLineView = UIView()
        blurView = UIVisualEffectView()
        super.init(frame: frame)
        setupConstraints()
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        setupEventButton()
        setupUserButton()
        setupBlurView()
        setupUnderlineView()
        setupBottomLineView()
    }
    
    private func setupBlurView() {
        self.blurView.effect = UIBlurEffect.init(style: .extraLight)
    }
    
    private func setupBottomLineView() {
        bottomLineView.backgroundColor = .lightGray
    }
    
    private func setupUnderlineView() {
        underlineView.backgroundColor = .black
    }
    
    private func setupEventButton() {
        eventButton.setTitle("Events", for: .normal)
        eventButton.titleLabel?.font = UIFont.systemFont(ofSize: 14.0, weight: .medium)
        eventButton.setTitleColor(.black, for: .normal)
        eventButton.addTarget(self, action: #selector(eventButtonAction), for: .touchUpInside)
    }
    
    private func setupUserButton() {
        userButton.setTitle("Users", for: .normal)
        userButton.titleLabel?.font = UIFont.systemFont(ofSize: 14.0, weight: .medium)
        userButton.setTitleColor(.gray, for: .normal)
        userButton.addTarget(self, action: #selector(userButtonAction), for: .touchUpInside)
    }
    
    func setToDefault() {
        eventButton.setTitleColor(.black, for: .normal)
        userButton.setTitleColor(.gray, for: .normal)
        underlineLeadingConstr.constant = 0
        UIView.animate(withDuration: 0.2) {
            self.layoutIfNeeded()
        }
    }
    
    @objc func eventButtonAction() {
        eventButton.setTitleColor(.black, for: .normal)
        userButton.setTitleColor(.gray, for: .normal)
        underlineLeadingConstr.constant = 0
        self.delegate?.eventButtonWasPressed()
        UIView.animate(withDuration: 0.2) {
            self.layoutIfNeeded()
        }
    }
    
    @objc func userButtonAction() {
        eventButton.setTitleColor(.gray, for: .normal)
        userButton.setTitleColor(.black, for: .normal)
        underlineLeadingConstr.constant = frame.width / 2
        self.delegate?.userButtonWasPressed()
        UIView.animate(withDuration: 0.2) {
            self.layoutIfNeeded()
        }
    }
    
    private func setupConstraints() {
        eventButton.translatesAutoresizingMaskIntoConstraints = false
        userButton.translatesAutoresizingMaskIntoConstraints = false
        underlineView.translatesAutoresizingMaskIntoConstraints = false
        bottomLineView.translatesAutoresizingMaskIntoConstraints = false
        blurView.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(blurView)
        addSubview(eventButton)
        addSubview(userButton)
        addSubview(bottomLineView)
        addSubview(underlineView)
        
        NSLayoutConstraint.activate([
            blurView.leadingAnchor.constraint(equalTo: leadingAnchor),
            blurView.topAnchor.constraint(equalTo: topAnchor),
            blurView.trailingAnchor.constraint(equalTo: trailingAnchor),
            blurView.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            eventButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0.0),
            eventButton.topAnchor.constraint(equalTo: topAnchor, constant: 0.0),
            eventButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0.0),
            
            userButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0.0),
            userButton.topAnchor.constraint(equalTo: topAnchor, constant: 0.0),
            userButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0.0),
            
            userButton.leadingAnchor.constraint(equalTo: eventButton.trailingAnchor, constant: 0.0),
            
            userButton.widthAnchor.constraint(equalTo: eventButton.widthAnchor, constant: 0.0),
            userButton.heightAnchor.constraint(equalTo: eventButton.heightAnchor, constant: 0.0),
            
            underlineView.widthAnchor.constraint(equalTo: userButton.widthAnchor),
            underlineView.heightAnchor.constraint(equalToConstant: 1.0),
            underlineView.bottomAnchor.constraint(equalTo: bottomAnchor),
            underlineLeadingConstr,
            
            bottomLineView.widthAnchor.constraint(equalTo: widthAnchor),
            bottomLineView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0.0),
            bottomLineView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0.0),
            bottomLineView.bottomAnchor.constraint(equalTo: bottomAnchor),
            bottomLineView.heightAnchor.constraint(equalToConstant: 0.5)
            ])
    }
}
