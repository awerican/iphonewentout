//
//  ProfileSettingsLevel1VC.swift
//  WentOut
//
//  Created by Fure on 19.09.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit
import CropViewController

class ProfileSettingsLevel1VC: UICollectionViewController {

    var imagePicker = UIImagePickerController()
    
    lazy var headerDownloader: Downloader<UserProfileHeader, UserProfileHeaderParameters> = {
        return Downloader<UserProfileHeader, UserProfileHeaderParameters>.init(resource: URL.UserProfile.header, param: UserProfileHeaderParameters.init(visiterID: LocalAuth.userID!, profileOwnerID: LocalAuth.userID!))
    }()
    
    func _updatePhoto(param: ChangeUserAvatarParameters) -> Downloader<SucceedFailedEntity, ChangeUserAvatarParameters> {
        return Downloader<SucceedFailedEntity, ChangeUserAvatarParameters>.init(resource: URL.UserProfile.Setting.changeUserAvatar, param: param)
    }
    
    func _updateUsername(param: UpdateUsernameParameters) -> Downloader<ChangeUpdateStatus, UpdateUsernameParameters> {
        return Downloader<ChangeUpdateStatus, UpdateUsernameParameters>.init(resource: URL.UserProfile.Setting.updateUsername, param: param)
    }
    
    var userAvatar: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        setupCollectionView()
        setupImagePicker()
        setupNetwork()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        
        // Register cell classes
        

        // Do any additional setup after loading the view.
    }
}

extension ProfileSettingsLevel1VC: ProfileSettingsLevel2BioVCProtocol {
    func newBio(bio: String) {
        headerDownloader.data!.description = bio
        collectionView?.reloadItems(at: [IndexPath.init(row: 2, section: 0)])
    }
}

extension ProfileSettingsLevel1VC {
    
    private func setupView() {
        view.backgroundColor = UIColor.init(hex: "FAFAFA")
    }
    
    private func setupImagePicker() {
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        imagePicker.mediaTypes = ["public.image"]
    }
    
    private func setupNetwork() {
        addLoadingIndicator()
        
        headerDownloader.getFirst {
            self.removeLoadingIndicator()
            self.collectionView?.reloadData()
        }
    }
    
    private func setupCollectionView() {
        collectionView?.contentInset.top = 10.0
        collectionView?.backgroundColor = .clear
        for reuseID in reuseIDs {
            collectionView!.register(UINib.init(nibName: reuseID, bundle: nil), forCellWithReuseIdentifier: reuseID)
        }
    }
}

extension ProfileSettingsLevel1VC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imagePicker.dismiss(animated: true) {
                self.presentCropViewController(image: pickedImage)
            }
        } else {
            print("something went wrong")
        }
    }
}

extension ProfileSettingsLevel1VC: CropViewControllerDelegate {
    private func presentCropViewController(image: UIImage) {
        let cropViewController = CropViewController(image: image)
        cropViewController.customAspectRatio = CGSize.init(width: 1.0, height: 1.0)
        cropViewController.aspectRatioLockEnabled = true
        cropViewController.aspectRatioPickerButtonHidden = true
        cropViewController.resetButtonHidden = true
        cropViewController.delegate = self
        present(cropViewController, animated: true, completion: nil)
    }
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        cropViewController.dismiss(animated: true) {
            self.uploadAvatarToServer(image: image)
            self.userAvatar = image
            self.collectionView?.reloadItems(at: [IndexPath.init(row: 0, section: 0)])
        }
    }
    
    private func addLoadingIndicator() {
        let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        activityIndicator.color = .gray
        self.navigationItem.titleView = activityIndicator
        activityIndicator.startAnimating()
    }
    
    override func removeLoadingIndicator() {
        self.navigationItem.titleView = nil
        self.navigationItem.title = "Edit My Profile"
    }
    
    func uploadAvatarToServer(image: UIImage) {
//        var chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
//        chosenImage = chosenImage.cropToSquare()
        
        let photoForUpload = UploadableMedia.init(image: image, compressionQuality: 0.25)
        
        addLoadingIndicator()
        
        Webservice.uploadAvatar(avatar: photoForUpload) { (process, _url) in
            guard let url = _url else {
                return
            }
            let params = ChangeUserAvatarParameters(userID: LocalAuth.userID!, photo: url)
            
            let updatePhoto = self._updatePhoto(param: params)
            
            updatePhoto.getFirst {
                if let data = updatePhoto.data {
                    switch data.status {
                    case .failed:
                        self.removeLoadingIndicator()
                        print("ERROR")
                    case .succeed:
                        self.removeLoadingIndicator()
                    }
                } else {
                    self.removeLoadingIndicator()
                    print("ERROR")
                }
            }
        }
    }
    
    func cropViewController(_ cropViewController: CropViewController, didFinishCancelled cancelled: Bool) {
        cropViewController.dismiss(animated: true, completion: nil)
    }
}

extension ProfileSettingsLevel1VC: ProfileSettingsUsernameProtocol {
    func usernameDoneButtonWasPressed(username: String) {
        guard let cell = collectionView?.cellForItem(at: IndexPath.init(row: 1, section: 0)) as? ProfileSettingsUsernameCell else {
            return
        }
        cell.loadingStarted()
        
        let params = UpdateUsernameParameters(userID: LocalAuth.userID!, userNickname: username)
        
        let updateUsername = self._updateUsername(param: params)
        
        updateUsername.getFirst {
            if let data = updateUsername.data?.status {
                switch data {
                case .updated:
                    self.removeLoadingIndicator()
                    cell.loadingFinished()
                    cell.receiveResponse(.successfullyUpdated)
                    print("success")
                case .isExisted:
                    self.removeLoadingIndicator()
                    cell.loadingFinished()
                    cell.receiveResponse(.isAlreadyTaken)
                    print("is existed")
                }
            } else {
                self.removeLoadingIndicator()
                cell.loadingFinished()
                cell.receiveResponse(.smthWentWrong)
                print("error")
            }
        }
        
//        webservice.load(resource: ResList.updateUsername(UpdateUsernameParameters(userID: LocalAuth.userID!, userNickname: username))) { (_result, _error) in
//            cell.loadingFinished()
//            guard let data = _result?.data else {
//                cell.receiveResponse(.smthWentWrong)
//                return
//            }
//            switch data {
//            case .isExisted:
//                cell.receiveResponse(.isAlreadyTaken)
//            case .updated:
//                cell.receiveResponse(.successfullyUpdated)
//            }
//        }
    }
}

extension ProfileSettingsLevel1VC: UICollectionViewDelegateFlowLayout {
    
    var reuseIDs: [String] {
        get {
            return ["ProfileSettingsArrowCell", "ProfileSettingsAvatarCell", "ProfileSettingsUsernameCell", "ProfileSettingsBioCell"]
        }
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        if let _ = headerDownloader.data {
            return 1
        }
        return 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIDs[1], for: indexPath) as! ProfileSettingsAvatarCell
            cell.configCell(avatar_url: self.headerDownloader.data!.photo, avatar_image: self.userAvatar)
            cell.delegate = self
            return cell
        }
        if indexPath.row == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIDs[2], for: indexPath) as! ProfileSettingsUsernameCell
            cell.configCell(username: headerDownloader.data!.nickname)
            cell.delegate = self
            return cell
        }
        if indexPath.row == 2 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIDs[3], for: indexPath) as! ProfileSettingsBioCell
            cell.configCell(bio: headerDownloader.data!.description)
            return cell
        }
        if indexPath.row == 3 {
            
        }
        if indexPath.row == 4 {
            
        }
        return UICollectionViewCell()
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 2 {
            let storyboard = UIStoryboard(name: "Second", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "ProfileSettingsLevel2BioVC") as! ProfileSettingsLevel2BioVC
            controller.delegate = self
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.row == 0 {
            return ProfileSettingsAvatarCell.currentSize
        }
        if indexPath.row == 1 {
            return ProfileSettingsUsernameCell.currentSize
        }
        if indexPath.row == 2 {
            let bundle = Bundle.main.loadNibNamed("ProfileSettingsBioCell", owner: self, options: nil)?[0] as! ProfileSettingsBioCell
            bundle.configCell(bio: headerDownloader.data!.description)
            return bundle.currentSize
        }
        return CGSize.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
}

extension ProfileSettingsLevel1VC: ProfileSettingsAvatarDelegate {
    func changePhoto() {
        print("change photo")
        present(imagePicker, animated: true, completion: nil)
    }
}












