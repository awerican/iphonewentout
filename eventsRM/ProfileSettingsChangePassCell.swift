//
//  ProfileSettingsChangePassCell.swift
//  WentOut
//
//  Created by Fure on 26.09.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

protocol ProfileSettingsChangePassDelegate: class {
    func shouldAddDoneButton(shouldAdd: Bool)
}

class ProfileSettingsChangePassCell: UICollectionViewCell {
    
    @IBOutlet weak var oldPassword: UITextField!
    
    @IBOutlet weak var newPassword: UITextField!
    
    @IBOutlet weak var repPassword: UITextField!
    
    static var currentSize: CGSize {
        get {
            return CGSize.init(width: UIScreen.width, height: 209.0)
        }
    }
    
    weak var delegate: ProfileSettingsChangePassDelegate?
    
    func configCell() {
        oldPassword.delegate = self
        newPassword.delegate = self
        repPassword.delegate = self
    }
    
    func addDoneButton(shouldAdd: Bool) {
        self.delegate?.shouldAddDoneButton(shouldAdd: shouldAdd)
    }
}

extension ProfileSettingsChangePassCell: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var shouldAddDoneItem = false
        if textField == oldPassword {
            if let text = textField.text as NSString? {
                let newText = text.replacingCharacters(in: range, with: string)
                if newText.count > 0 {
                    if newPassword.text?.count > 0 {
                        if repPassword.text?.count > 0 {
                            shouldAddDoneItem = true
                        }
                    }
                }
            }
        }
        if textField == newPassword {
            if let text = textField.text as NSString? {
                let newText = text.replacingCharacters(in: range, with: string)
                if newText.count > 0 {
                    if oldPassword.text?.count > 0 {
                        if repPassword.text?.count > 0 {
                            shouldAddDoneItem = true
                        }
                    }
                }
            }
        }
        if textField == repPassword {
            if let text = textField.text as NSString? {
                let newText = text.replacingCharacters(in: range, with: string)
                if newText.count > 0 {
                    if oldPassword.text?.count > 0 {
                        if newPassword.text?.count > 0 {
                            shouldAddDoneItem = true
                        }
                    }
                }
            }
        }
        addDoneButton(shouldAdd: shouldAddDoneItem)
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.oldPassword {
            oldPassword.resignFirstResponder()
            newPassword.becomeFirstResponder()
        }
        if textField == self.newPassword {
            repPassword.becomeFirstResponder()
        }
        return true
    }

    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if let old = self.oldPassword.text?.count, let new = self.newPassword.text?.count, let rep = self.repPassword.text?.count {
            if old > 0 && new > 0 && rep > 0 {
                print("YOU ARE RIGHT")
            }
        }
        return true
    }
}





















