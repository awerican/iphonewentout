//
//  UserProfileActionsCollectionViewCell.swift
//  WentOut
//
//  Created by Fure on 03.07.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

protocol UserProfileActionsDelegate: class {
    func followAction(status: UserFollowStatus, callback: @escaping (Bool) -> ())
    func invite()
}


class UserProfileActionsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var followButton: UIButton!
    
    @IBOutlet weak var inviteButton: UIButton!
    
    let followTag = 251
    
    let unfollowTag = 319
    
    weak var delegate: UserProfileActionsDelegate?
    
    @IBAction func followAction(_ sender: UIButton) {
        
        if sender.tag == followTag {
            self.delegate?.followAction(status: .follow, callback: { (success) in
                if success {
                    self.followButton.layer.borderWidth = 1.0
                    self.followButton.layer.borderColor = UIColor(hex: "E0E0E0").cgColor
                    self.followButton.backgroundColor = .white
                    self.followButton.tag = self.unfollowTag
                    self.followButton.setTitleColor(.black, for: .normal)
                    self.followButton.setTitle("Unfollow",for: .normal)
                }
            })
        }
        
        if sender.tag == unfollowTag {
            self.delegate?.followAction(status: .unfollow, callback: { (success) in
                if success {
                    self.followButton.layer.borderWidth = 1.0
                    self.followButton.layer.borderColor = UIColor(hex: "1783FF").cgColor
                    self.followButton.backgroundColor = UIColor.init(hex: "1783FF")
                    self.followButton.tag = self.followTag
                    self.followButton.setTitleColor(.white, for: .normal)
                    self.followButton.setTitle("Follow",for: .normal)
                }
            })
        }
    }
    
    @IBAction func inviteAction(_ sender: UIButton) {
        self.delegate?.invite()
    }
    
    override func draw(_ rect: CGRect) {
        followButton.makeOval(cornerRadius: followButton.frame.height / 4)
        inviteButton.makeOval(cornerRadius: inviteButton.frame.height / 4)
        
        followButton.layer.borderWidth = 1.0
        followButton.layer.borderColor = UIColor(hex: "E0E0E0").cgColor
        inviteButton.layer.borderWidth = 1.0
        inviteButton.layer.borderColor = UIColor(hex: "E0E0E0").cgColor
    }
    
    func configCell(statusFollow: Bool) {
        if statusFollow {
            self.followButton.layer.borderWidth = 1.0
            self.followButton.layer.borderColor = UIColor(hex: "E0E0E0").cgColor
            self.followButton.backgroundColor = .white
            self.followButton.setTitleColor(.black, for: .normal)
            self.followButton.tag = unfollowTag
            self.followButton.setTitle("Unfollow",for: .normal)
        } else {
            self.followButton.layer.borderWidth = 1.0
            self.followButton.layer.borderColor = UIColor(hex: "1783FF").cgColor
            self.followButton.backgroundColor = UIColor.init(hex: "1783FF")
            self.followButton.setTitleColor(.white, for: .normal)
            self.followButton.tag = followTag
            self.followButton.setTitle("Follow",for: .normal)
        }
    }
}



















