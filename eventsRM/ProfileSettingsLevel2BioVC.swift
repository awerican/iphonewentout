//
//  ProfileSettingsLevel2BioVC.swift
//  WentOut
//
//  Created by Fure on 22.09.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import UIKit

protocol ProfileSettingsLevel2BioVCProtocol: class {
    func newBio(bio: String)
}

class ProfileSettingsLevel2BioVC: UIViewController {
    
    let textViewTag: Int = 7396
    static let defaultTextViewText: String = "Write a bio"
    
    weak var delegate: ProfileSettingsLevel2BioVCProtocol?
    
    lazy var doneButton: UIBarButtonItem = {
        let button = UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(doneButtonAction))
        button.isEnabled = false
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationBar()
        setup(textView: view.viewWithTag(textViewTag) as? UITextView)
    }
    
    private func addLoadingIndicator() {
        let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        activityIndicator.color = .gray
        self.navigationItem.titleView = activityIndicator
        activityIndicator.startAnimating()
    }
    
    override func removeLoadingIndicator() {
        self.navigationItem.titleView = nil
        self.navigationItem.title = ""
    }
}

extension ProfileSettingsLevel2BioVC {
    @objc private func doneButtonAction() {
        // replace this method
        
        guard let textView = view.viewWithTag(textViewTag) as? UITextView else {
            return
        }
        
        addLoadingIndicator()
        
        var bio = textView.text!
        if bio == ProfileSettingsLevel2BioVC.defaultTextViewText {
            bio = ""
        }
        
        webservice.load(resource: ResList.updateDescription(UpdateDescParameters(userID: LocalAuth.userID!, description: bio))) { (_result, _error) in
            guard let status = _result?.data?.status else {
                self.removeLoadingIndicator()
                return
            }
            switch status {
            case .succeed:
                self.navigationController?.popViewController(animated: true)
                self.delegate?.newBio(bio: bio)
                self.removeLoadingIndicator()
            case .failed:
                self.removeLoadingIndicator()
                print("failed")
            }
        }
        
        if let rootVC = self.navigationController?.viewControllers.first as? PublishEventDelegate {
            if let tv = view.viewWithTag(textViewTag) as? UITextView {
                rootVC.add(eventDesc: tv.text)
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
}

extension ProfileSettingsLevel2BioVC {
    private func setupNavigationBar() {
        navigationItem.rightBarButtonItems = [doneButton]
    }
    
    private func setup(textView: UITextView?) {
        guard let tv = textView else {
            return
        }
        
        tv.delegate = self
        
        setDefaults(to: tv)
        
        tv.becomeFirstResponder()
    }
    
    private func setDefaults(to textView: UITextView) {
        textView.text = ProfileSettingsLevel2BioVC.defaultTextViewText
        textView.textColor = .lightGray
        let newPosition = textView.beginningOfDocument
        textView.selectedTextRange = textView.textRange(from: newPosition, to: newPosition)
    }
    
    private func setNormal(to textView: UITextView) {
        textView.textColor = UIColor.darkGray
    }
}

extension ProfileSettingsLevel2BioVC: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText = textView.text
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        
        if text == "\n" {
            return false
        }
        
        if newText.count > 150 {
            return false
        }
        
        if currentText == ProfileSettingsLevel2BioVC.defaultTextViewText && newText.count == 0 {
            print("Im here 0")
            return false
        }
        if currentText == ProfileSettingsLevel2BioVC.defaultTextViewText && text.count > 0 {
            print("Im here 1")
            textView.text = text
            doneButton.isEnabled = true
            setNormal(to: textView)
            return false
        }
        if currentText == ProfileSettingsLevel2BioVC.defaultTextViewText && text.count == 0 {
            print("Im here 2")
//            doneButton.isEnabled = false
            setDefaults(to: textView)
            return true
        }
        if newText.count == 0 {
            print("Im here 3")
//            doneButton.isEnabled = false
            setDefaults(to: textView)
            return false
        }
        print("Im here 4")
        doneButton.isEnabled = true
        setNormal(to: textView)
        return true
    }
}










