//
//  CustomBarController.swift
//  eventsRM
//
//  Created by Fure on 06.12.16.
//  Copyright © 2016 uzuner. All rights reserved.
//

import UIKit
import Foundation

// comment for commit

class CustomBarController: UITabBarController {
    
    struct Constants {
        struct SeguesID {
            static let moreInfoVC = "segueToMoreInfoVC"
        }
    }
    
    let tab2 = #imageLiteral(resourceName: "tab_icon_2").setColor(.gray)
//    let tab3 = #imageLiteral(resourceName: "barCreate").setColor(.black)
    let tab3 = #imageLiteral(resourceName: "tab_icon_3-2").setColor(.black)
    let tab4 = #imageLiteral(resourceName: "tab_icon_4").setColor(.gray)
    let tab5 = #imageLiteral(resourceName: "tab_icon_5").setColor(.gray)
    
    lazy var tabImage1 = UIImageView(image: #imageLiteral(resourceName: "tab_icon_1_tapped"))
    lazy var tabImage2 = UIImageView(image: tab2)
    lazy var tabImage3 = UIImageView(image: tab3)
    lazy var tabImage4 = UIImageView(image: tab4)
    lazy var tabImage5 = UIImageView(image: tab5)
    
    lazy var customTabBarHeightC = NSLayoutConstraint.init(item: self.customTabBar, attribute: .height, relatedBy: .equal, toItem: self.bottomLayoutGuide, attribute: .height, multiplier: 1.0, constant: UITabBar.height)
    
    lazy var customTabBarWidthC = NSLayoutConstraint.init(item: self.customTabBar, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1.0, constant: UIScreen.main.bounds.width)
    
    lazy var customTabBarBottomC = NSLayoutConstraint.init(item: self.customTabBar, attribute: .bottom, relatedBy: .equal, toItem: self.bottomLayoutGuide, attribute: .bottom, multiplier: 1.0, constant: 0.0)
    
    lazy var customTabBarCenterXC = NSLayoutConstraint.init(item: self.customTabBar, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1.0, constant: 0.0)
    
//    lazy var customTabBarTopC = NSLayoutConstraint.init(item: self.customTabBar, attribute: .top, relatedBy: .equal, toItem: self.bottomLayoutGuide, attribute: .bottom, multiplier: 1.0, constant: 0.0)
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.navigationController!.setNavigationBarHidden(true, animated: false)
//        self.navigationController?.navigationBar.isHidden = true
    }
    
    var customTabBar: UITabBar = UITabBar()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCustomTabBar()
    }
}

extension CustomBarController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let id = segue.identifier else {
            return
        }
        switch id {
        case CustomBarController.Constants.SeguesID.moreInfoVC:
//            if let destVC = segue.destination as? MIMainProfileSettingViewController {
                print("the old version used to open blur settings menu")
//                destVC.configViewController(state: .userSetting)
//            }
        default:
            break
        }
    }
}

extension CustomBarController {
    func moveTabBarDownFromTheSreen() {
        self.customTabBarBottomC.constant = self.customTabBarHeightC.constant + self.bottomLayoutGuide.length
    }
    
    func moveTabBarBackToTheSreen() {
        self.customTabBarBottomC.constant = 0.0
    }
    
    private func setupCustomTabBar() {
        self.tabBar.isHidden = true
        
        self.customTabBar.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.customTabBar)
        
        NSLayoutConstraint.activate([self.customTabBarHeightC])
        NSLayoutConstraint.activate([self.customTabBarWidthC])
        NSLayoutConstraint.activate([self.customTabBarBottomC])
        NSLayoutConstraint.activate([self.customTabBarCenterXC])
        
        tabImage1.contentMode = .scaleAspectFit
        tabImage2.contentMode = .scaleAspectFit
        tabImage3.contentMode = .scaleAspectFit
        tabImage4.contentMode = .scaleAspectFit
        tabImage5.contentMode = .scaleAspectFit
        
        tabImage1.clipsToBounds = true
        tabImage2.clipsToBounds = true
        tabImage3.clipsToBounds = true
        tabImage4.clipsToBounds = true
        tabImage5.clipsToBounds = true
        
        tabImage1.translatesAutoresizingMaskIntoConstraints = false
        tabImage2.translatesAutoresizingMaskIntoConstraints = false
        tabImage3.translatesAutoresizingMaskIntoConstraints = false
        tabImage4.translatesAutoresizingMaskIntoConstraints = false
        tabImage5.translatesAutoresizingMaskIntoConstraints = false
        
        self.customTabBar.addSubview(tabImage1)
        self.customTabBar.addSubview(tabImage2)
        self.customTabBar.addSubview(tabImage3)
        self.customTabBar.addSubview(tabImage4)
        self.customTabBar.addSubview(tabImage5)
        
        let topAnchorConstant: CGFloat = 2.0
        let bottomAnchorConstant: CGFloat = 2.0
        
        tabImage1.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width / 5)
            .isActive = true
        tabImage1.heightAnchor.constraint(equalToConstant: UITabBar.height - topAnchorConstant - bottomAnchorConstant)
            .isActive = true
        tabImage1.leadingAnchor.constraint(equalTo: self.customTabBar.leadingAnchor, constant: 0.0)
            .isActive = true
        tabImage1.topAnchor.constraint(equalTo: self.customTabBar.topAnchor, constant: topAnchorConstant)
            .isActive = true
        
        tabImage2.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width / 5)
            .isActive = true
        tabImage2.heightAnchor.constraint(equalToConstant: UITabBar.height - topAnchorConstant - bottomAnchorConstant)
            .isActive = true
        tabImage2.leadingAnchor.constraint(equalTo: tabImage1.trailingAnchor, constant: 0.0)
            .isActive = true
        tabImage2.topAnchor.constraint(equalTo: self.customTabBar.topAnchor, constant: topAnchorConstant)
            .isActive = true
        
        tabImage3.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width / 5)
            .isActive = true
        tabImage3.heightAnchor.constraint(equalToConstant: UITabBar.height - topAnchorConstant - bottomAnchorConstant)
            .isActive = true
        tabImage3.leadingAnchor.constraint(equalTo: tabImage2.trailingAnchor, constant: 0.0)
            .isActive = true
        tabImage3.topAnchor.constraint(equalTo: self.customTabBar.topAnchor, constant: topAnchorConstant)
            .isActive = true
        
        tabImage4.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width / 5)
            .isActive = true
        tabImage4.heightAnchor.constraint(equalToConstant: UITabBar.height - topAnchorConstant - bottomAnchorConstant)
            .isActive = true
        tabImage4.leadingAnchor.constraint(equalTo: tabImage3.trailingAnchor, constant: 0.0)
            .isActive = true
        tabImage4.topAnchor.constraint(equalTo: self.customTabBar.topAnchor, constant: topAnchorConstant)
            .isActive = true
        
        tabImage5.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width / 5)
            .isActive = true
        tabImage5.heightAnchor.constraint(equalToConstant: UITabBar.height - topAnchorConstant - bottomAnchorConstant)
            .isActive = true
        tabImage5.leadingAnchor.constraint(equalTo: tabImage4.trailingAnchor, constant: 0.0)
            .isActive = true
        tabImage5.topAnchor.constraint(equalTo: self.customTabBar.topAnchor, constant: topAnchorConstant)
            .isActive = true
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(gestureHandler(gesture:)))
        
        self.customTabBar.addGestureRecognizer(tapGesture)
    }
    
    private func highlightTabBarIcon(at index: Int) {
        
        if index == 0 {
            
            let tab2 = #imageLiteral(resourceName: "tab_icon_2").setColor(.gray)
            let tab3 = #imageLiteral(resourceName: "tab_icon_3-2").setColor(.black)
            let tab4 = #imageLiteral(resourceName: "tab_icon_4").setColor(.gray)
            let tab5 = #imageLiteral(resourceName: "tab_icon_5").setColor(.gray)

            tabImage1.image = #imageLiteral(resourceName: "tab_icon_1_tapped")
            tabImage2.image = tab2
            tabImage3.image = tab3
            tabImage4.image = tab4
            tabImage5.image = tab5
        }
        
        if index == 1 {
            
            let tab1 = #imageLiteral(resourceName: "tab_icon_1").setColor(.gray)
            let tab3 = #imageLiteral(resourceName: "tab_icon_3-2").setColor(.black)
            let tab4 = #imageLiteral(resourceName: "tab_icon_4").setColor(.gray)
            let tab5 = #imageLiteral(resourceName: "tab_icon_5").setColor(.gray)
            
            tabImage1.image = tab1
            tabImage2.image = #imageLiteral(resourceName: "tab_icon_2_tapped")
            tabImage3.image = tab3
            tabImage4.image = tab4
            tabImage5.image = tab5
        }
        if index == 3 {
            
            let tab1 = #imageLiteral(resourceName: "tab_icon_1").setColor(.gray)
            let tab2 = #imageLiteral(resourceName: "tab_icon_2").setColor(.gray)
            let tab3 = #imageLiteral(resourceName: "tab_icon_3-2").setColor(.black)
            let tab5 = #imageLiteral(resourceName: "tab_icon_5").setColor(.gray)
            
            tabImage1.image = tab1
            tabImage2.image = tab2
            tabImage3.image = tab3
            tabImage4.image = #imageLiteral(resourceName: "tab_icon_4_tapped")
            tabImage5.image = tab5
        }
        if index == 4 {
            
            let tab1 = #imageLiteral(resourceName: "tab_icon_1").setColor(.gray)
            let tab2 = #imageLiteral(resourceName: "tab_icon_2").setColor(.gray)
            let tab3 = #imageLiteral(resourceName: "tab_icon_3-2").setColor(.black)
            let tab4 = #imageLiteral(resourceName: "tab_icon_4").setColor(.gray)
            
            tabImage1.image = tab1
            tabImage2.image = tab2
            tabImage3.image = tab3
            tabImage4.image = tab4
            tabImage5.image = #imageLiteral(resourceName: "tab_icon_5_tapped")
        }
    }
    
    @objc func gestureHandler(gesture: UIGestureRecognizer) {
        if gesture is UITapGestureRecognizer {
            guard let view = gesture.view else {
                return
            }
            let location = gesture.location(in: view)
            if location.x > 0 && location.x < view.frame.width / 5 {
                if (self.selectedIndex == 0) {
                    if let navBar = self.selectedViewController as? UINavigationController {
                        if navBar.viewControllers.count > 1 {
                            navBar.popViewController(animated: true)
                        } else {
                            if let mapVC = navBar.viewControllers.first as? WorldViewController {
                                mapVC.mapView.setZoomLevel(0, animated: true)
                            }
                        }
                    }
                } else {
                    self.selectedIndex = 0
                    highlightTabBarIcon(at: 0)
                }
            } else if location.x > view.frame.width / 5 && location.x < view.frame.width / 5 * 2 {
                if (self.selectedIndex == 1) {
                    if let navBar = self.selectedViewController as? UINavigationController {
                        if navBar.viewControllers.count > 1 {
                            navBar.popViewController(animated: true)
                        } else {
                            if let activityPage = navBar.childViewControllers.first as? ActivityPageViewController {
                                if let _ = activityPage.viewControllers?.first as? MyFeedbackViewController {
                                    activityPage.setViewControllers([activityPage.viewControllerList[0]], direction: .reverse, animated: true, completion: nil)
                                } else {
                                    (activityPage.viewControllers?.first as? DefaultStateVCDelegate)?.scrollToTop()
                                }
                            }
                        }
                    }
                } else {
                    self.selectedIndex = 1
                    highlightTabBarIcon(at: 1)
                }
            } else if location.x > view.frame.width / 5 * 2 && location.x < view.frame.width / 5 * 3 {
                let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: "CreateEventNavigationVC")
                self.present(controller, animated: true, completion: nil)
            } else if location.x > view.frame.width / 5 * 3 && location.x < view.frame.width / 5 * 4 {
                if (self.selectedIndex == 3) {
                    if let navBar = self.selectedViewController as? UINavigationController {
                        if navBar.viewControllers.count > 1 {
                            navBar.popViewController(animated: true)
                        } else {
                            if let searchPage = navBar.childViewControllers.first as? SearchPageViewController {
                                (searchPage as DefaultStateVCDelegate).scrollToTop()
                            }
                        }
                    }
                } else {
                    self.selectedIndex = 3
                    highlightTabBarIcon(at: 3)
                }
            } else if location.x > view.frame.width / 5 * 4 && location.x < view.frame.width / 5 * 5 {
                if (self.selectedIndex == 4) {
                    if let navBar = self.selectedViewController as? UINavigationController {
                        if navBar.viewControllers.count > 1 {
                            navBar.popViewController(animated: true)
                        } else {
                            if let userPage = navBar.childViewControllers.first as? UserProfileViewController {
                                (userPage as DefaultStateVCDelegate).scrollToTop()
                            }
                        }
                    }
                } else {
                    self.selectedIndex = 4
                    highlightTabBarIcon(at: 4)
                }
            }
        }
    }
}
