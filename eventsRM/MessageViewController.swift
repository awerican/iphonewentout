//
//  MessageViewController.swift
//  WentOut
//
//  Created by Fure on 25.08.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import UIKit
import ReverseExtension
import SocketIO

class MessageViewController: UIViewController {
    
    var tableView: UITableView!
    
    var data: [MessageEntity] = []
    
    fileprivate var heightDictionary: [Int : CGFloat] = [:]
    
    var eventID: String! {
        didSet {
            if let eventID = self.eventID {
                MessageManager.roomJoiner.eventID = eventID
            }
        }
    }
    
    var eventTitle: String!
    
    let tbHeightDiff: Int = 0
    
    lazy var tableViewVerticalAnchor: NSLayoutConstraint = {
        return tableView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -CGFloat(tbHeightDiff))
    }()
    
    lazy var roomJoiner: RoomJoiner = {
        return RoomJoiner.init(userID: LocalAuth.userID!, eventID: eventID)
    }()
    
    var lastOldMessage: MessageEntity?
    
    var viewWasLoadedTimes = 0
    
    let tvBottomInset: CGFloat = -14.0
    let tvTopInset: CGFloat = 80.0
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.view.frame.size.height = UIScreen.main.bounds.height
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        socketDisconnect()
        commentBar.notificationOff()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    lazy var commentBar: CommentBarManager = {
        let bar = CommentBarManager()
        bar.delegate = self
        return bar
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // add send message button
        // add MessageViewController to the main app!

        setupView()
        setupNetwork()
    }
}

extension MessageViewController: MessageCellType2Delegate {
    func avatarWasTapped(at indexPath: IndexPath) {
//        self.navigationController?.popViewController(animated: true)
//        let userID = data[indexPath.row].id
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let controller = storyboard.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
//        controller.profileOwnerID = userID
//        self.navigationController?.pushViewController(controller, animated: true)
    }
    func usernameWasTapped(at indexPath: IndexPath) {
//        self.navigationController?.popViewController(animated: true)
    }
}

extension MessageViewController: MessageManagerDelegate {
    func incomingMessage(message: MessageEntity) {
        print("incoming messages")
        // in case of double connection
        if message.senderID == LocalAuth.userID! {
            print("i receive the message, text =", message.text ?? "NO TEXT")
            return
        }
        for msg in data {
            if msg.id == message.id {
                print("i have return message")
                return
            }
        }
        insert(newData: [message])
    }
    
    func incomingRecentMessages(messages: [MessageEntity]) {
        print("incomingRecentMessages")
        if messages.count == 0 {
            return
        }
        
        if data.count >= messages.count {
            var theSame = true
            
            for (index, element) in messages.enumerated() {
                if data[index] != element {
                    theSame = false
                }
            }
            
            if !theSame {
                data = []
                tableView.reloadData()
                insert(newData: messages)
            }
            
            return
        }
        
        insert(newData: messages)
    }
    
    func incomingOlderMessages(messages: [MessageEntity]) {
        print("incomingOlderMessages")
        if messages.count > 0 {
            insert(oldData: messages)
        }
    }
}

extension MessageViewController {
    private func setupNetwork() {
        MessageManager.sharedInstance.delegate = self
        connect()
    }
    
    private func connect() {
        MessageManager.sharedInstance.connect(joiner: roomJoiner) { (success) in
            if success {
                print("chigga chat connection success")
            } else {
                print("chigga chat connection failed")
            }
        }
    }
    
    func socketDisconnect() {
        MessageManager.sharedInstance.disconnect()
    }
    
    private func setupView() {
        setupTableView()
        setupNavBar()
        setupCommentBar()
    }
    
    private func setupNavBar() {
        self.navigationItem.title = eventTitle
    }
    
    private func setupCommentBar() {
        commentBar.install(onTop: self.view, gestureView: self.tableView)
        commentBar.notificationOn()
    }
    
    private func setupTableView() {
        tableView = UITableView()
        tableView.autoresizingModeOn()
        tableView.separatorStyle = .none
//        tableView.backgroundColor = UIColor.init(hex: "FAFAFA")
        
        view.addSubview(self.tableView)

        tableView.dataSource = self
        tableView.allowsSelection = false
        tableView.separatorStyle = .none
        tableView.re.delegate = self
        tableView.re.scrollViewDidReachTop = { scrollView in
            self.scrollViewDidReachTop(scrollView: scrollView)
        }
        for reuseID in reuseIDs {
            tableView.register(UINib.init(nibName: reuseID, bundle: nil), forCellReuseIdentifier: reuseID)
        }
        
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0.0),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0.0),
            tableView.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.height + CGFloat(tbHeightDiff)),
            tableViewVerticalAnchor
        ])
    }
    
//    private func setupTableView() {
//        tableView.dataSource = self
//        tableView.allowsSelection = false
//        tableView.separatorStyle = .none
//        tableView.re.delegate = self
//        tableView.re.scrollViewDidReachTop = { scrollView in
//            self.scrollViewDidReachTop(scrollView: scrollView)
//        }
//        for reuseID in reuseIDs {
//            tableView.register(UINib.init(nibName: reuseID, bundle: nil), forCellReuseIdentifier: reuseID)
//        }
//    }
    
//    private func setupButton() {
//        let button = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: 80, height: 80))
//        button.backgroundColor = .black
//        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
//        view.addSubview(button)
//    }
    
    private func sendMessage(msg: String?, attachments: [MessageAttachment]) {
        // upload photos to server
        let currentDate = Date()
        print("local auth =", LocalAuth.userID!)
        let messageToSend = MessageSendEntity.init(senderID: LocalAuth.userID!, msg: msg, attch: attachments)
        let messageEntity = MessageEntity.init(messageSendEntity: messageToSend, date: currentDate)
        print("insert in sendMessage")
        insert(newData: [messageEntity])
        
        MessageManager.sharedInstance.sendMessage(message: messageToSend) { (response) in
            switch response {
            case .success(let msg):
                self.replaceMessage(by: currentDate, with: msg)
                // remove loading view
            case .fail:
                print("failed")
                // show error view
            }
        }
    }
    
    private func replaceMessage(by date: Date, with newMessage: MessageEntity) {
        var _index: Int?
        for (i, message) in data.enumerated() {
            if message.time == date {
                _index = i
            }
        }
        guard let index = _index else {
            return
        }
        data[index] = newMessage
    }
    
    private func tableViewIsAtBottom() -> Bool {
        if data.count == 0 {
            return true
        }
        if let cell = tableView.cellForRow(at: IndexPath.init(row: 0, section: 0)) {
            let cellBreakingPoint = CGPoint.init(x: 0, y: cell.frame.height * 1 / 3)
            let newPoint = cell.convert(cellBreakingPoint, to: view)
            if newPoint.y > view.frame.height {
                return false
            } else {
                return true
            }
        } else {
            return false
        }
    }
    
    private func insert(newData: [MessageEntity]) {

        
        var indexPaths: [IndexPath] = []
            
        for (index, _) in newData.enumerated() {
            let _indexPath = IndexPath.init(row: index, section: 0)
            indexPaths.append(_indexPath)
        }
        
        if self.tableViewIsAtBottom() {
            print("insert at bottom")
            data.insert(newData, at: 0)
            self.tableView.insertRows(at: indexPaths, with: .automatic)
        } else {
            print("insert at the middle")
            data.insert(newData, at: 0)
            let previousContentHeight = tableView.contentSize.height
            let previousContentOffset = tableView.contentOffset.y
            tableView.reloadData()
            let currenctContetnHeight = tableView.contentSize.height - previousContentHeight + previousContentOffset - 107
            tableView.contentOffset = CGPoint(x: 0, y: currenctContetnHeight)
        }
        tableView.contentInset.top = tvBottomInset
        tableView.contentInset.bottom = tvTopInset
    }
    
    private func insert(oldData: [MessageEntity]) {

        data.append(oldData)
        var indexPaths: [IndexPath] = []
        let initIndex = data.count - oldData.count
        
        for (index, _) in oldData.enumerated() {
            let _indexPath = IndexPath.init(row: initIndex + index, section: 0)
            indexPaths.append(_indexPath)
        }
        
        self.tableView.insertRows(at: indexPaths, with: .automatic)
        tableView.contentInset.top = tvBottomInset
        tableView.contentInset.bottom = tvTopInset
    }
}

extension MessageViewController: CommentBarManagerDelegate {
    func barHeightDidChange(heightDifference: CGFloat, _triggerMethod: String) {
        tableViewVerticalAnchor.constant -= heightDifference
    }
    
    func barHeightWillSet(to height: CGFloat) {
        // пох на этот метод
    }
    
    func show(imagePickerVC: UIImagePickerController) {
        print("attach button was tapped")
    }
    
    func sendButtonAction(text: String, medias: [CommentMedia]) -> Bool {
        sendMessage(msg: text, attachments: [])
        return false
//        if tableView.contentSize.height < UIScreen.main.bounds.height {
//            return true
//        } else {
//            return false
//        }
    }
}

extension MessageViewController: UITableViewDataSource {
    
    var reuseIDs: [String] {
        get {
            return ["MessageCellType1", "MessageCellType2"]
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        heightDictionary[indexPath.row] = cell.frame.size.height
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = heightDictionary[indexPath.row]
        return height ?? UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let msg = data[indexPath.row]
        if msg.senderID == LocalAuth.userID! {
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseIDs[0]) as! MessageCellType1
            cell.confiCell(msg: msg)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseIDs[1]) as! MessageCellType2
            cell.confiCell(msg: msg)
            cell.delegate = self
            return cell
        }
    }
}

extension MessageViewController: UITableViewDelegate {
    //ReverseExtension also supports handling UITableViewDelegate.
    func scrollViewDidScroll(_ scrollView: UIScrollView) { }
    
    private func scrollViewDidReachTop(scrollView: UIScrollView) {
        if self.data.count == 0 {
            return
        }
        let currentOldMessage = self.data[self.data.count - 1]
        if let _lastOldMessage = self.lastOldMessage {
            if _lastOldMessage != currentOldMessage {
                let time = currentOldMessage.time!
                MessageManager.sharedInstance.askForMessagesBefore(date: time)
            }
        } else {
            let latestMessageTime = currentOldMessage.time!
            MessageManager.sharedInstance.askForMessagesBefore(date: latestMessageTime)
        }
        self.lastOldMessage = currentOldMessage
    }
}
















