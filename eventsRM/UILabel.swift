//
//  UILabel.swift
//  WentOut
//
//  Created by Fure on 09.10.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    
    func calculateMaxLines() -> Int {
//        let maxSize = CGSize(width: frame.size.width, height: CGFloat(Float.infinity))
        let maxSize = CGSize(width: frame.size.width, height: frame.size.height)
        let charSize = font.lineHeight
        let text = (self.text ?? "") as NSString
        let textSize = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        let lines = Int(textSize.height/charSize)
        return lines
    }
    
}
