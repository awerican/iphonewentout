//
//  CLLocationCoordinate2D.swift
//  WentOut
//
//  Created by Fure on 26.08.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import Foundation
import MapKit

extension CLLocationCoordinate2D {
    static func isValid(latitude: Double, longitude: Double) -> Bool {
        return latitude > -90.0 && latitude < 90.0 &&
                longitude > -180.0 && longitude < 180.0
    }
}
