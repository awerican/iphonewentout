//
//  ExampleChatVC.swift
//  WentOut
//
//  Created by Fure on 07.11.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import Foundation
import UIKit
import SwiftSocket
import SocketIO

enum Mimetype: String {
    case jpeg = "image/jpeg"
    case mp4 = "video/mp4"
    
    var ext: String {
        get {
            switch self {
            case .jpeg:
                return "jpeg"
            case .mp4:
                return "mp4"
            }
        }
    }
}

class ExampleChatVC: UIViewController, TableDirectorController {
    
    var tableDirector: TableDirector!
    
    var tableView: UITableView!
    
    var msgManager: MessageManager!
    
    var sendingMsg: Int = 0
    
    var aesKey: AesKey!
    
    var eventID: String!
    
//    var commentTextView: UITextView!
//    var commentAttachButton: UIButton!
//    var commentSendButton: UIButton!
//    var commentBackground: UIView!
//    var commentSectionHeight: CGFloat = 60.0
    
    lazy var commentBar: CommentBarManager = {
        let bar = CommentBarManager()
        bar.delegate = self
        return bar
    }()
    
    lazy var tableViewVerticalAnchor: NSLayoutConstraint = {
        return tableView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 0.0)
    }()
    
    // судя по всему это никгда не вызывается
    deinit {
        commentBar.notificationOff()
    }
    
    var socket: SocketManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        setupCommentBar()
        
        print("hello i am here")
        
        connect { (finish) in
            if finish {
                let joiner = RoomJoiner.init(userID: LocalAuth.userID!, eventID: self.eventID)
                print("chigga =", joiner)
                self.setupMsgManager(joiner: joiner)
            }
        }

        setupTableDirector()
        view.backgroundColor = UIColor.white
        tableView.backgroundColor = UIColor.white
    }
    
    func connect(callback: ((Bool)->Void)?) {
        
        print("url =", URL.socketChat)
        
        socket = SocketManager(socketURL: URL.socketChat)

        socket.defaultSocket.on("connect") { (data, ack) in
            print("socket connected!!")
            callback?(true)
        }

        socket.defaultSocket.on("disconnect") { (data, ack) in
            print("socket disconnected!!")
            callback?(false)
        }

        socket.connect()
    }
}

extension ExampleChatVC {

    func updateCellAndReload(showLoadingIndicator: Bool, at indexPath: IndexPath) {
        var wrapper = self.tableDirector.getItem(by: indexPath) as! MessageWrapper
        wrapper.loadingIndicatorIsHidden = !showLoadingIndicator
        wrapper.errorIndicatorIsHidden = true
        self.tableDirector.sections[indexPath.section].replace(rowAt: indexPath.row, with: wrapper.getTableRow())
        self.tableView.reloadRows(at: [indexPath], with: .none)
    }
    
    func updateCellAndReload(showErrorIndicator: Bool, at indexPath: IndexPath) {
        var wrapper = self.tableDirector.getItem(by: indexPath) as! MessageWrapper
        wrapper.errorIndicatorIsHidden = !showErrorIndicator
        wrapper.loadingIndicatorIsHidden = true
        
        self.tableDirector.sections[indexPath.section].replace(rowAt: indexPath.row, with: wrapper.getTableRow())
        
        self.tableView.reloadRows(at: [indexPath], with: .none)
    }
    
    func updateCell(avatarIsHidden: Bool, at indexPath: IndexPath) {
        var wrapper = self.tableDirector.getItem(by: indexPath) as! MessageWrapper
        wrapper.avatarIsHidden = avatarIsHidden
        self.tableDirector.sections[indexPath.section].replace(rowAt: indexPath.row, with: wrapper.getTableRow())
        self.tableView.reloadRows(at: [indexPath], with: .none)
    }
}

extension ExampleChatVC {
    func setupMessageWrapperForAvatar(wrappers: inout [MessageWrapper]) {
        if wrappers.count < 2 {
            return
        }
        var grouppedSequenceBegin = false
        for i in 1...wrappers.count - 1 {
            let previousSenderID = wrappers[i - 1].message.senderID
            let currentSenderID = wrappers[i].message.senderID
            if previousSenderID == currentSenderID {
                let previousTime = wrappers[i - 1].message.time!
                let currentTime = wrappers[i].message.time!
                let timeDiff = Date.differenceInSeconds(beginDate: currentTime, endDate: previousTime)!
                if timeDiff < groupMsgTime {
                    grouppedSequenceBegin = true
                    wrappers[i - 1].avatarIsHidden = true
                    if grouppedSequenceBegin {
                        wrappers[i].isGroupped = true
                    }
                } else {
                    grouppedSequenceBegin = false
                }
            }
        }
    }
}

extension ExampleChatVC: TableDirectorDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        print(indexPath)
        if indexPath.section == 0 && indexPath.row == tableView.numberOfRows(inSection: 0) - 1 {
            let wrapper = (tableDirector.getItem(by: indexPath) as! MessageWrapper)
//            let seconds = wrapper.message.time.timeIntervalSince1970
//            let milisec = Int(seconds * 1000)
            // msgManager is undefined
//            msgManager.askForMessagesBefore(time: milisec)
            msgManager.askForMessagesBefore(date: wrapper.message.time)
        }
    }
    
    func tableView(_ tableView: UITableView, performAction action: Selector, forRowAt indexPath: IndexPath, withSender sender: Any?) {
        
        guard let _msg = sender as? MessageEntity else {
            return
        }
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let action1 = UIAlertAction(title: "Retry", style: .default) { (action: UIAlertAction) in
            let msg = MessageSendEntity.init(senderID: _msg.senderID, msg: _msg.text, attch: _msg.attch)
            self.performResendMessageUI(indexPath: indexPath, msg: msg)
        }
        let action2 = UIAlertAction(title: "Delete", style: .destructive) { (action: UIAlertAction) in
            self.tableDirector.delete(row: indexPath.row, section: indexPath.section)
        }
        
        let action3 = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)

        alertController.addAction(action1)
        alertController.addAction(action2)
        alertController.addAction(action3)
        
        self.present(alertController, animated: true, completion: nil)
    }
}

extension ExampleChatVC: MessageManagerDelegate {
    
    // within that quantity of SECONDS messages of the same user will be groupped,
    // otherwise user's name and avatar will be shown again
    var groupMsgTime: Int {
        get {
            return 60 * 5
        }
    }
    
    func incomingOlderMessages(messages: [MessageEntity]) {
        if messages.count < 1 {
            return
        }
        var rows: [Row] = []
        var wrappers: [MessageWrapper] = []
        for i in 0...messages.count - 1 {
            let wrapper = MessageWrapper.init(message: messages[i], indexPath: IndexPath.init(row: i, section: 0))
            wrappers.append(wrapper)
        }
        
        setupMessageWrapperForAvatar(wrappers: &wrappers)
        
        for wrapper in wrappers {
            rows.append(wrapper.getTableRow())
        }
        
        self.tableDirector.appendLast(rows: rows, atSection: 0, animation: .none)
    }
    
    // APPENDING MSG IN TABLEVIEW
    func incomingRecentMessages(messages: [MessageEntity]) {
        
        if messages.count < 1 {
            return
        }
        var rows: [Row] = []
        var wrappers: [MessageWrapper] = []
        for i in 0...messages.count - 1 {
            let wrapper = MessageWrapper.init(message: messages[i], indexPath: IndexPath.init(row: i, section: 0))
            wrappers.append(wrapper)
        }
        
        setupMessageWrapperForAvatar(wrappers: &wrappers)
        
        for wrapper in wrappers {
            rows.append(wrapper.getTableRow())
        }
        
        self.tableDirector.appendLast(rows: rows, atSection: 0, animation: .none)
    }

    // APPENDING MSG IN TABLEVIEW
//    @discardableResult
//    func incomingMessage(message: MessageEntity) -> IndexPath? {
    func incomingMessage(message: MessageEntity) {

        let isGroupped = self.hidePreviousMessageAvatarIfNeeded(message)
        
        let wrapper = MessageWrapper.init(message: message, indexPath: IndexPath.init(row: 0, section: 0), isGroupped: isGroupped)
        
        let meta: (oldHeight: CGFloat, offset: CGFloat) = (oldHeight: self.tableView.contentSize.height, offset: tableView.contentOffset.y)

        let firstIndexPaths = self.tableDirector.appendFirst(rows: [wrapper.getTableRow()], atSection: 0, animation: .top)
        
        if message.senderID == LocalAuth.userID! {
            tableView.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .bottom, animated: true)
        } else {
            if let _ = tableView.cellForRow(at: IndexPath.init(row: 0, section: 0)) {
                if tableView.numberOfRows(inSection: 0) > 0 {
                    let indexPath = IndexPath.init(row: 0, section: 0)
                    self.tableView.scrollToRow(at: indexPath, at: .top, animated: true)
                }
            } else {
                let newContentHeight = tableView.contentSize.height
                let diff = newContentHeight - meta.oldHeight
                let newContentOffset = meta.offset + diff
                view.layoutIfNeeded()
                tableView.setContentOffset(CGPoint.init(x: 0, y: newContentOffset), animated: false)
            }
        }
        
//        return firstIndexPaths.first
    }
    
    private func scrollToBottomIfNeeded(lastIndexPath: IndexPath) {
        let prevIndexPath = IndexPath.init(row: lastIndexPath.row - 1, section: 0)
        if let _ = tableView.cellForRow(at: prevIndexPath) {
            tableView.scrollToRow(at: lastIndexPath, at: .bottom, animated: true)
        }
    }
    
    private func scrollToBottom(lastIndexPath: IndexPath) {
        tableView.scrollToRow(at: lastIndexPath, at: .bottom, animated: true)
    }
    
    @discardableResult
    private func hidePreviousMessageAvatarIfNeeded(_ incomingMessage: MessageEntity) -> Bool {
        let sectionCount = self.tableDirector.sections.count
        guard sectionCount > 0 else { return false }
        let rowCount = self.tableDirector.sections[0].rows.count
        guard rowCount > 0 else { return false }
        
        let lastIndexPath = IndexPath.init(row: rowCount - 1, section: 0)
        
        let lastWrapper = self.tableDirector.sections[lastIndexPath.section].rows[lastIndexPath.row].getItem() as! MessageWrapper
        
        if lastWrapper.message.senderID == incomingMessage.senderID {
            let timeDiff = Date.differenceInSeconds(beginDate: incomingMessage.time, endDate: lastWrapper.message.time)!
            if timeDiff < groupMsgTime {
                self.updateCell(avatarIsHidden: true, at: lastIndexPath)
                return true
            } else {
                return false
            }
        } else {
            return false
        }
    }

    private func getWrappers() -> [MessageWrapper] {
        let sectionCount = self.tableDirector.sections.count
        guard sectionCount > 0 else { return [] }
        let rowCount = self.tableDirector.sections[0].rows.count
        guard rowCount > 0 else { return [] }
        var wrappers: [MessageWrapper] = []
        for row in self.tableDirector.sections[0].rows {
            wrappers.append(row.getItem() as! MessageWrapper)
        }
        return wrappers
    }
}

extension ExampleChatVC: CommentBarManagerDelegate {
    func barHeightDidChange(heightDifference: CGFloat, _triggerMethod: String) {
        tableViewVerticalAnchor.constant -= heightDifference
    }
    
    func barHeightWillSet(to height: CGFloat) {
        tableView.contentInset.bottom = UIApplication.shared.statusBarFrame.size.height
        tableView.contentInset.top = height + 2
    }
    
    func show(imagePickerVC: UIImagePickerController) {
        self.present(imagePickerVC, animated: true, completion: nil)
    }
    
    func sendButtonAction(text: String, medias: [CommentMedia]) -> Bool {
        let msgText = validateTextMessage(text: text)
        if medias.count == 0 {
            let msg = MessageSendEntity.init(senderID: LocalAuth.userID!, msg: msgText, attch: [])
            performSendMessageUI(msg: msg)
        } else {
            var attachments = [MessageAttachment]()
            
            for media in medias {
                let attch = MessageAttachment.init(image: media.image, mimetype: media.type.rawValue)
                attachments.append(attch)
            }
            
            let msg = MessageSendEntity.init(senderID: LocalAuth.userID!, msg: msgText, attch: attachments)
            performSendMessageUI(msg: msg)
        }
        if self.tableView.contentSize.height < UIScreen.main.bounds.height {
            return true
        } else {
            return false
        }
    }
    
    func performResendMessageUI(indexPath: IndexPath, msg: MessageSendEntity) {
        self.updateCellAndReload(showLoadingIndicator: true, at: indexPath)
        if msg.attch.count == 0 {
            self.msgManager.sendMessage(message: msg) { (response) in
                switch response {
                case .success:
                    self.updateCellAndReload(showLoadingIndicator: false, at: indexPath)
                    self.scrollToBottomIfNeeded(lastIndexPath: indexPath)
                case .fail:
                    self.updateCellAndReload(showErrorIndicator: true, at: indexPath)
                    self.scrollToBottomIfNeeded(lastIndexPath: indexPath)
                }
            }
        } else {
            uploadImagesToServer(attachments: msg.attch) { (_newAttch, _error) in
                
                if let newAttch = _newAttch {
                    var __msg = msg
                    
                    __msg.insertAttchs(newAttch)
                    self.msgManager.sendMessage(message: __msg) { (response) in
                        switch response {
                        case .success:
                            self.updateCellAndReload(showLoadingIndicator: false, at: indexPath)
                            self.scrollToBottomIfNeeded(lastIndexPath: indexPath)
                        case .fail:
                            self.updateCellAndReload(showErrorIndicator: true, at: indexPath)
                            self.scrollToBottomIfNeeded(lastIndexPath: indexPath)
                        }
                    }
                } else if let _ = _error {
                    self.updateCellAndReload(showErrorIndicator: true, at: indexPath)
                    self.scrollToBottomIfNeeded(lastIndexPath: indexPath)
                }
            }
        }
    }
    // replace method in proper extension and DELETE this comment
    private func performSendMessageUI(msg: MessageSendEntity) {
//        let indexPath = self.incomingMessage(message: msg.messageEntity)!
        // just so it compiles
        let indexPath = IndexPath.init(row: 0, section: 0)
        self.updateCellAndReload(showLoadingIndicator: true, at: indexPath)
        self.scrollToBottomIfNeeded(lastIndexPath: indexPath)
        
        if msg.attch.count == 0 {
            self.msgManager.sendMessage(message: msg) { (response) in
                switch response {
                case .success:
                    self.updateCellAndReload(showLoadingIndicator: false, at: indexPath)
                    self.scrollToBottomIfNeeded(lastIndexPath: indexPath)
                case .fail:
                    self.updateCellAndReload(showErrorIndicator: true, at: indexPath)
                    self.scrollToBottomIfNeeded(lastIndexPath: indexPath)
                }
            }
        } else {
            uploadImagesToServer(attachments: msg.attch) { (_newAttch, _error) in
                
                if let newAttch = _newAttch {
                    var __msg = msg
                    
                    __msg.insertAttchs(newAttch)
                    self.msgManager.sendMessage(message: __msg) { (response) in
                        switch response {
                        case .success:
                            self.updateCellAndReload(showLoadingIndicator: false, at: indexPath)
                            self.scrollToBottomIfNeeded(lastIndexPath: indexPath)
                        case .fail:
                            self.updateCellAndReload(showErrorIndicator: true, at: indexPath)
                            self.scrollToBottomIfNeeded(lastIndexPath: indexPath)
                        }
                    }
                } else if let _ = _error {
                    self.updateCellAndReload(showErrorIndicator: true, at: indexPath)
                    self.scrollToBottomIfNeeded(lastIndexPath: indexPath)
                }
            }
        }
    }
    
    private func uploadImagesToServer(attachments: [MessageAttachment], callback: @escaping (_ newAttach: [MessageAttachment]?, _ error: String?) -> Void) {
        
        func createURL(fileName: String, mimeType: String) -> String {
            let url = "http://wentout.me/media/original/" + fileName + "." + mimeType
            print(url)
            return url
        }
        
        var media: [(data: Data, mimetype: Mimetype)] = []
        
        for att in attachments {
            let image = att.image!
            let mimetype = Mimetype.jpeg
            let data = UIImageJPEGRepresentation(image, 0.75)!
            media.append((data: data, mimetype: mimetype))
        }
        
        var respondingAttachments: [MessageAttachment] = []
        
        Webservice.uploadChatImages(media: media) { (_p, _files, _err) in
            if let error = _err {
                callback(nil, error)
            }
            if let files = _files {
                for (_, filename) in files {
                    let mimetype = "jpeg"
                    let filename = filename
                    let url = createURL(fileName: filename, mimeType: mimetype)
                    let att = MessageAttachment.init(mimetype: mimetype, url: url)
                    respondingAttachments.append(att)
                }
                callback(respondingAttachments, nil)
            } else {
                print(_p ?? -1, media.description)
            }
        }
       
    }
}

extension ExampleChatVC {
    
    private func setupMsgManager(joiner: RoomJoiner) {
//        let _socket = (UIApplication.shared.delegate as! SocketProtocol).socket!
//        let _socket = socket!
//        self.msgManager = MessageManager.init(socket: _socket, joiner: joiner)
        self.msgManager.delegate = self
    }
    
    private func setupCommentBar() {
        commentBar.install(onTop: self.view, gestureView: self.tableView)
        commentBar.delegate = self
        commentBar.notificationOn()
    }
    
    private func setupTableView() {
        tableView = UITableView()
        tableView.autoresizingModeOn()
        tableView.separatorStyle = .none
        
        view.addSubview(self.tableView)

        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0.0),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0.0),
            tableView.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.height),
            tableViewVerticalAnchor
        ])
    }
    
    private func setupTableDirector() {
        self.tableDirector = TableDirector.init(tableView: tableView, delegate: self, controller: self, flipTableView: true)
    }
    
    private func validateTextMessage(text: String) -> String? {
        if text == "" {
            return nil
        } else {
            let validatedText = text.removingAllExtraNewLines
            if validatedText == "" {
                return nil
            } else {
                return validatedText
            }
        }
    }
}










