//
//  WorldVC_mapView.swift
//  eventsRM
//
//  Created by Fure on 29.06.17.
//  Copyright © 2017 uzuner. All rights reserved.
//

import UIKit
import MapKit
import Alamofire
import AlamofireImage
import Mapbox

extension WorldViewController: MGLMapViewDelegate, AnnotationSelectable {
    
    func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
        return false
    }
    
    func mapView(_ mapView: MGLMapView, didAdd annotationViews: [MGLAnnotationView]) {
        for annotationView in annotationViews {
            if let customAnnotationView = annotationView as? CustomAnnotationView {
                customAnnotationView.animateToAppear()
            }
        }
    }
    
    func willSelect(annotationView: CustomAnnotationView) {
        self.annotationTouchesBegan = true
    }
    
    func mapView(_ mapView: MGLMapView, didSelect annotation: MGLAnnotation) {
        mapView.deselectAnnotation(annotation, animated: false)
        self.view.isUserInteractionEnabled = false
        
        guard let eventId = (annotation as? CustomPointAnnotation)?.eventID else {
            return
        }
        guard let event = self.eventAndAnnotation[eventId] else {
            return
        }
        self.worldTableVC = self.storyboard?.instantiateViewController(withIdentifier: "WorldTableViewController") as? WorldTableViewController
        self.worldTableVC!.configVC(parentVC: self, imageCache: self.imageCache, state: .pickedEvent(eventID: event.id))

        guard let child = self.worldTableVC else {
            return
        }

        child.view.frame.origin.y = UIScreen.main.bounds.height - UITabBar.height - WorldTableView.headerHeight - WorldTableViewController.tableViewTopConstraintsConstant
        view.addSubview(child.view)
//        (self.navigationController?.tabBarController as! CustomBarController).customTabBar.isUserInteractionEnabled = false

        (self.tabBarController as! CustomBarController).customTabBar.isUserInteractionEnabled = false
        
//        (self.navigationController?.tabBarController as! CustomBarController).moveTabBarDownFromTheSreen()
        
        (self.tabBarController as! CustomBarController).moveTabBarDownFromTheSreen()
        
        UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseIn, animations: {
            [unowned self] in
            child.view.frame.origin.y = 0.0
//            (self.navigationController?.tabBarController as! CustomBarController).view.layoutIfNeeded()
            (self.tabBarController as! CustomBarController).view.layoutIfNeeded()
        }) { (finish) in
            if finish {
//                (self.navigationController?.tabBarController as! CustomBarController).customTabBar.isUserInteractionEnabled = true
                (self.tabBarController as! CustomBarController).customTabBar.isUserInteractionEnabled = true
                self.view.isUserInteractionEnabled = true
            }
        }
    }
    
    func askForEventsByRegion(_ mapView: MGLMapView) {
        let tlc = mapView.convert(CGPoint.init(x: mapView.frame.minX, y: mapView.frame.minY), toCoordinateFrom: mapView).geohash(precision: 8)
        let brc = mapView.convert(CGPoint.init(x: mapView.frame.maxX, y: mapView.frame.maxY), toCoordinateFrom: mapView).geohash(precision: 8)
        let zoom = Int.init(mapView.zoomLevel.rounded(.down))
        SocketMapManager.shared.askForEventsByRegion(zoom: zoom, tlc: tlc, brc: brc)
    }

    func mapViewRegionIsChanging(_ mapView: MGLMapView) {
        regionNeverChanged = false
        controlAnimationOfAnnotations(on: mapView)
    }
    
    func mapView(_ mapView: MGLMapView, regionDidChangeAnimated animated: Bool) {
        askForEventsByRegion(mapView)
    }

    func mapView(_ mapView: MGLMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {
        guard let annotation = annotation as? CustomPointAnnotation else {
            return nil
        }
        
        let reuseIdentifier = "\(annotation.coordinate.longitude)"
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier)
        
        if annotationView == nil {
            guard let event = self.eventAndAnnotation[annotation.eventID] else {
                return nil
            }
            annotationView = CustomAnnotationView(reuseIdentifier: reuseIdentifier, imageURL: event.imageURL, delegate: self)
            annotationView!.bounds = CGRect.init(origin: CGPoint.init(x: 0, y: 0), size: CustomAnnotationView.annotationsSize)
        }
        
        guard let event = self.eventAndAnnotation[annotation.eventID] else {
            return nil
        }
        
        Webservice.loadImage(url: event.imageURL) { (_image) in
            if let image = _image {
                (annotationView as! CustomAnnotationView).imageView.image = image
            }
        }

        if regionNeverChanged {
            (annotationView as! CustomAnnotationView).imageView.frame.size = CustomAnnotationView.imageViewSize
        } else {
            (annotationView as! CustomAnnotationView).isHidden = true
            (annotationView as! CustomAnnotationView).frame.origin = CGPoint.init(x: (annotationView as! CustomAnnotationView).frame.midX, y: (annotationView as! CustomAnnotationView).frame.midY)
            (annotationView as! CustomAnnotationView).frame.size = CGSize.zero
            (annotationView as! CustomAnnotationView).imageView.frame.size = CGSize.zero
            (annotationView as! CustomAnnotationView).imageView.frame.origin = CGPoint.zero
        }
        
        return annotationView
    }
}

extension WorldViewController: SocketMapManagerEvents {
    func eventAmountDidChangedResponse(data: [Any]) {
        guard let parsedData = (data[0] as? String)?.data(using: .utf8, allowLossyConversion: false) else {
            return
        }
        guard let eventAmount = try? JSONDecoder().decode(MapEventAmount.self, from: parsedData) else {
            return
        }
        self.eventAmountView.setEventAmount(eventAmount.quantity)
    }
    
    func regionDidChangedResponse(data: [Any]) {
        guard let parsedData = (data[0] as? String)?.data(using: .utf8, allowLossyConversion: false) else {
            return
        }
        guard let events = try? JSONDecoder().decode([MapEvent].self, from: parsedData) else {
            print("error")
            return
        }
        print("recieve events via socket quantity = \(events.count)")
        for event in events {
            if !self.events.contains(event) {
                self.events.insert(event)
                let annotation = CustomPointAnnotation(eventID: event.id)
                annotation.coordinate = CLLocationCoordinate2D.init(latitude: event.latitude, longitude: event.longitude)
                self.eventAndAnnotation[event.id] = event

                if CLLocationCoordinate2D.isValid(latitude: event.latitude, longitude: event.longitude) {
                    self.mapView.addAnnotation(annotation)
                } else {
                    print("Coordinates are not valid")
                    print("LATITUDE: ", event.latitude)
                    print("LONGITUDE: ", event.longitude)
                }
            }
        }
    }
}

extension WorldViewController {
    func controlAnimationOfAnnotations(on mapView: MGLMapView) {
        guard let annotations = mapView.visibleAnnotations else {
            return
        }
        
        var annotationMustBeSeen: [MGLAnnotation] = []
        var annotationMustBeHidden: [MGLAnnotation] = []
        
        let halfOfAnnotationWidth = (CustomAnnotationView.annotationsSize.width / 2)
        
        let tlc = mapView.convert(CGPoint.init(x: mapView.frame.minX - halfOfAnnotationWidth, y: mapView.frame.minY - halfOfAnnotationWidth), toCoordinateFrom: mapView)
        let brc = mapView.convert(CGPoint.init(x: mapView.frame.maxX + halfOfAnnotationWidth, y: mapView.frame.maxY + halfOfAnnotationWidth), toCoordinateFrom: mapView)
        
        let newTlc = CLLocationCoordinate2D.init(latitude: tlc.latitude, longitude: tlc.longitude + 180.0)
        let newBrc = CLLocationCoordinate2D.init(latitude: brc.latitude, longitude: brc.longitude + 180.0)
        
        for an in annotations {
            guard let annotation = an as? CustomPointAnnotation else {
                break
            }
            if let event = self.eventAndAnnotation[annotation.eventID] {
                if newBrc.longitude - newTlc.longitude > 0 {
                    if event.zoom <= self.currentZoom &&
                        event.longitude >= tlc.longitude &&
                        event.longitude <= brc.longitude &&
                        event.latitude <= tlc.latitude &&
                        event.latitude >= brc.latitude {
                        annotationMustBeSeen.append(an)
                    } else {
                        annotationMustBeHidden.append(an)
                    }
                } else {
                    if event.zoom <= self.currentZoom &&
                        (event.longitude >= tlc.longitude || event.longitude <= brc.longitude) &&
                        event.latitude <= tlc.latitude &&
                        event.latitude >= brc.latitude {
                        annotationMustBeSeen.append(an)
                    } else {
                        annotationMustBeHidden.append(an)
                    }
                }
            }
        }
        
        for an in annotationMustBeSeen {
            if let view = self.mapView.view(for: an) as? CustomAnnotationView {
                view.animateToAppear()
            }
        }
        
        for an in annotationMustBeHidden {
            if let view = self.mapView.view(for: an) as? CustomAnnotationView {
                view.animateToDisappear()
            }
        }
    }
}






























