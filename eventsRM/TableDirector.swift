//
//  TableDirector.swift
//  WentOut
//
//  Created by Fure on 10.09.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import UIKit
import ReverseExtension

@objc protocol TableDirectorDelegate: class {
    @objc optional func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath?
    @objc optional func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    @objc optional func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    @objc optional func scrollViewDidScroll(_ scrollView: UIScrollView)
    @objc optional func tableView(_ tableView: UITableView, performAction action: Selector, forRowAt indexPath: IndexPath, withSender sender: Any?)
}

protocol TableDirectorController: class { }

class TableDirector: NSObject {
    
    fileprivate(set) var sections = [TableSection]()

    private var cellRegisterer: TableCellRegisterer?
    
    private(set) weak var tableView: UITableView?
    
    private(set) weak var delegate: TableDirectorDelegate?
    
    private(set) weak var controller: TableDirectorController?
    
    private var flipTableView: Bool = false

    init(tableView: UITableView, delegate: TableDirectorDelegate?, controller: TableDirectorController?, flipTableView: Bool = false) {
        super.init()
        
        self.cellRegisterer = TableCellRegisterer(tableView: tableView)
        self.tableView = tableView
        self.delegate = delegate
        self.controller = controller
        self.tableView?.dataSource = self
        self.tableView?.delegate = self
        
        self.tableView?.estimatedRowHeight = 50.0
        
        if flipTableView {
//            tableView.re.delegate = self
//            tableView.re.scrollViewDidReachTop = { scrollView in
//                print("reached the TOP")
////                if self.data.count < 150 {
////                    self.addOlderData(quantity: 30)
////                }
//            }
//            tableView.re.scrollViewDidReachBottom = { scrollView in
//                print("reached the BOTTOM")
//            }
            self.flipTableView = true
            self.tableView?.transform = CGAffineTransform(rotationAngle: (CGFloat)(Double.pi))
            self.tableView?.transform = CGAffineTransform(scaleX: 1, y: -1)
        }
    }
}

extension TableDirector: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = sections[indexPath.section].rows[indexPath.row]
        cellRegisterer?.register(cellType: row.cellType, forCellReuseIdentifier: row.reuseIdentifier)
        let cell = tableView.dequeueReusableCell(withIdentifier: row.reuseIdentifier, for: indexPath)
        row.configure(cell, delegate: controller)
        cell.selectionStyle = .none
        if self.flipTableView {
            cell.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
            cell.transform = CGAffineTransform(scaleX: 1, y: -1)
        }
        return cell
    }
}

extension TableDirector: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.delegate?.scrollViewDidScroll?(scrollView)
    }
}

extension TableDirector: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return self.delegate?.tableView?(tableView, willSelectRowAt: indexPath) ?? indexPath
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.tableView?(tableView, didSelectRowAt: indexPath)
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.delegate?.tableView?(tableView, willDisplay: cell, forRowAt: indexPath)
    }
    func tableView(_ tableView: UITableView, performAction action: Selector, forRowAt indexPath: IndexPath, withSender sender: Any?) {
        self.delegate?.tableView?(tableView, performAction: action, forRowAt: indexPath, withSender: sender)
    }
}

extension TableDirector {
    
    @discardableResult
    private func append(sections: [TableSection]) -> Self {
        self.sections.append(contentsOf: sections)
        return self
    }
    
    func getItem(by indexPath: IndexPath) -> Any? {
        guard indexPath.section < sections.count && indexPath.section >= 0 else {
            return nil
        }
        guard indexPath.row < sections[indexPath.section].numberOfRows && indexPath.row >= 0 else {
            return nil
        }

        let row = self.sections[indexPath.section].rows[indexPath.row]
        
        return row.getItem()
    }
    
    func changeItem(at indexPath: IndexPath, with item: Any) {
        let row = self.sections[indexPath.section].rows[indexPath.row]
        row.changeItem(newItem: item)
        row.reloadCell(animation: .automatic)
    }
    
    func getRowAt<T: ConfigurableCell>(indexPath: IndexPath) -> TableRow<T>? where T: UITableViewCell {
        guard indexPath.section < sections.count && indexPath.section >= 0 else {
            return nil
        }
        guard indexPath.row < sections[indexPath.section].numberOfRows && indexPath.row >= 0 else {
            return nil
        }
        
        return self.sections[indexPath.section].rows[indexPath.row] as? TableRow<T>
    }
    
    @discardableResult
    func updateRowAt(at indexPath: IndexPath, with row: Row, animation: UITableViewRowAnimation = .none) -> Bool {
        guard indexPath.section < sections.count && indexPath.section >= 0 else {
            return false
        }
        guard indexPath.row < sections[indexPath.section].numberOfRows && indexPath.row >= 0 else {
            return false
        }
        self.sections[indexPath.section].replace(rowAt: indexPath.row, with: row)
        self.tableView?.reloadRows(at: [indexPath], with: animation)
        return true
    }

    @discardableResult
    func insert(row: Row, at indexPath: IndexPath, animation: UITableViewRowAnimation = .top) {
        if self.sections.count - 1 < indexPath.section {
            self.sections.append(TableSection.init(rows: [row]))
            self.tableView!.beginUpdates()
            self.tableView!.insertRows(at: [indexPath], with: animation)
            self.tableView!.endUpdates()
        } else {
            self.sections[0].append(row: row)
            self.tableView!.beginUpdates()
            self.tableView!.insertRows(at: [indexPath], with: animation)
            self.tableView!.endUpdates()
        }
    }
    
    @discardableResult
    func insert(rows: [Row], atSection section: Int, atRow row: Int, animation: UITableViewRowAnimation = .top) -> Bool {
        guard section >= 0 && row >= 0 else {
            return false
        }
        if self.sections.indices.contains(section) {
            if self.sections[section].numberOfRows > 0 {
                let maxIndex = self.sections[section].numberOfRows - 1
                if maxIndex >= row - 1 {
                    self.sections[section].insert(rows: rows, at: row)
                    var indexPathsForUpdate = [IndexPath]()
                    for i in 0 ..< rows.count {
                        indexPathsForUpdate.append(IndexPath.init(row: row + i, section: section))
                    }
                    self.tableView?.insertRows(at: indexPathsForUpdate, with: animation)
                    return true
                } else {
                    return false
                }
            } else if self.sections[section].numberOfRows == 0 {
                self.sections[section].insert(rows: rows, at: 0)
                var indexPathsForUpdate = [IndexPath]()
                for i in 0 ..< rows.count {
                    indexPathsForUpdate.append(IndexPath.init(row: row + i, section: section))
                }
                self.tableView?.insertRows(at: indexPathsForUpdate, with: animation)
                return true
            } else {
                return false
            }
        } else {
            if section == 0 {
                self.sections.append(TableSection.init(rows: rows))
                
                var indexPathsForUpdate = [IndexPath]()
                for i in 0 ..< rows.count {
                    indexPathsForUpdate.append(IndexPath.init(row: row + i, section: section))
                }
                
                print(indexPathsForUpdate)
                
                print("sections n =", self.tableView?.numberOfSections)
                print("rows n in 0 s =", self.tableView?.numberOfRows(inSection: 0))
                print("indexPathsForUpdate =", indexPathsForUpdate)
                
                self.tableView?.insertRows(at: indexPathsForUpdate, with: animation)
                
                return true
            } else {
                return false
            }
        }
    }
    
    @discardableResult
    func appendFirst(rows: [Row], atSection section: Int, animation: UITableViewRowAnimation = .automatic) -> [IndexPath] {
        guard section >= 0 else {
            return []
        }
        if self.sections.count >= section + 1 {
            self.sections[section].insert(rows: rows, at: 0)
            
            var indexPathsForUpdate = [IndexPath]()
            for i in 0 ..< rows.count {
                indexPathsForUpdate.append(IndexPath.init(row: i, section: section))
            }

            self.tableView?.insertRows(at: indexPathsForUpdate, with: animation)
            
            return indexPathsForUpdate
        } else if self.sections.count == section {
            self.sections.insert(TableSection.init(rows: rows), at: 0)
            if section == 0 {

                var indexPathsForUpdate = [IndexPath]()
                for i in 0 ..< rows.count {
                    indexPathsForUpdate.append(IndexPath.init(row: i, section: section))
                }
                
                self.tableView?.insertSections([0], with: animation)
                
                return indexPathsForUpdate
//                return []
            } else {
                var indexPathsForUpdate = [IndexPath]()
                for i in 0 ..< rows.count {
                    indexPathsForUpdate.append(IndexPath.init(row: i, section: section))
                }

                self.tableView?.insertRows(at: indexPathsForUpdate, with: animation)
                
                return indexPathsForUpdate
            }
        }
        return []
    }
    
    @discardableResult
    func appendLast(rows: [Row], atSection section: Int, animation: UITableViewRowAnimation = .automatic) -> [IndexPath] {
        guard section >= 0 else {
            return []
        }
        if self.sections.count >= section + 1 {
            self.sections[section].append(rows: rows)
            
            var indexPathsForUpdate = [IndexPath]()
            for i in 0 ..< rows.count {
                indexPathsForUpdate.append(IndexPath.init(row: self.sections[section].numberOfRows - rows.count + i, section: section))
            }
            
            self.tableView?.insertRows(at: indexPathsForUpdate, with: animation)
            
            return indexPathsForUpdate
        } else if self.sections.count == 0 {
            self.sections.append(TableSection.init())
            self.sections[section].append(rows: rows)
            
            var indexPathsForUpdate = [IndexPath]()
            for i in 0 ..< rows.count {
                indexPathsForUpdate.append(IndexPath.init(row: i, section: section))
            }

            self.tableView?.beginUpdates()
            self.tableView?.insertSections([0], with: animation)
            self.tableView?.insertRows(at: indexPathsForUpdate, with: animation)
            self.tableView?.endUpdates()
            
            return indexPathsForUpdate
        }
        
        return []
    }
    
    @discardableResult
    func delete(sectionAt index: Int, animation: UITableViewRowAnimation = .automatic) -> Bool {
        guard index < sections.count && index >= 0 else {
            return false
        }
        sections.remove(at: index)
        self.tableView?.deleteSections([index], with: animation)
        return true
    }
    
    func deleteAll() {
        self.sections = []
        self.tableView?.reloadData()
    }
    
    @discardableResult
    func delete(row: Int, section: Int, animation: UITableViewRowAnimation = .top) -> Bool {
        guard section < sections.count && section >= 0 else {
            return false
        }
        guard row < sections[section].numberOfRows && row >= 0 else {
            return false
        }
        sections[section].remove(rowAt: row)
        let indexPath = IndexPath.init(row: row, section: section)
        self.tableView?.deleteRows(at: [indexPath], with: animation)
        return true
    }
}



