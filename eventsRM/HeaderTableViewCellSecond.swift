//
//  HeaderTableViewCellSecond.swift
//  WentOut
//
//  Created by Fure on 16.07.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import UIKit

class HeaderTableViewCellSecond: UITableViewHeaderFooterView {
    let defaultTrailingConstraintConstant: CGFloat = 12.0
    
    @IBOutlet var labelInfo: UILabel!
    @IBOutlet private var basisView: UIView!
    @IBOutlet var labelInfoTrailingConstraint: NSLayoutConstraint!
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        basisView.layer.addBorder(edge: .bottom, color: UIColor(red: 0.91, green: 0.91, blue: 0.91, alpha: 1.0), thickness: 1.0)
    }
    
//    func configView() {
////        self.labelInfoTrailingConstraint.constant = defaultTrailingConstraintConstant
//        self.labelInfo.text = "Top Events"
//        self.labelInfo.textAlignment = .center
//    }
    
    func configView(_ state: HeaderTableViewCellSecondStates) {
        switch state {
        case .topEvents:
            self.labelInfoTrailingConstraint.constant = defaultTrailingConstraintConstant
            self.labelInfo.text = "Top Events"
            self.labelInfo.textAlignment = .center
        case .eventNear:
            self.labelInfoTrailingConstraint.constant = defaultTrailingConstraintConstant
            self.labelInfo.text = "Nearest events"
            self.labelInfo.textAlignment = .left
        case .eventInfo:
            self.labelInfoTrailingConstraint.constant = defaultTrailingConstraintConstant
            self.labelInfo.text = "Chosen event"
            self.labelInfo.textAlignment = .left
        }
    }
    
    //    func startLoading() {
    //        self.addSubview(wait)
    //        self.labelInfo.text = ""
    //        self.labelTime.isHidden = true
    //        self.wait.startAnimating()
    //    }
    //
    //    func stopLoading() {
    //        self.wait.removeFromSuperview()
    //        self.wait.stopAnimating()
    //        self.labelTime.isHidden = false
    //    }
    
}
