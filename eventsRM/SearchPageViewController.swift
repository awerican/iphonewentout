//
//  SearchPageViewController.swift
//  WentOut
//
//  Created by Fure on 11.07.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import UIKit

protocol SearchPageScrollViewDelegate: class {
    func controllerDidScroll(_ scrollView: UIScrollView)
}

protocol SearchableVC: class {
    func searchTextDidChange(text: String)
}

class SearchPageViewController: UIPageViewController, UIPageViewControllerDelegate {
    
    lazy var viewControllerList: [UIViewController] = {
        let sb = UIStoryboard.init(name: "Second", bundle: nil)
        let vc1 = sb.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        let vc2 = sb.instantiateViewController(withIdentifier: "SearchEventViewController") as! SearchEventViewController
        let vc3 = sb.instantiateViewController(withIdentifier: "SearchUserViewController") as! SearchUserViewController

        vc1.scrollViewDelegate = self
        vc2.scrollViewDelegate = self
        vc3.scrollViewDelegate = self
        return [vc1, vc2, vc3]
    }()
    
    lazy var searchBar: UISearchBar = {
        let searchBar = UISearchBar()
        searchBar.placeholder = "Search..."
        searchBar.delegate = self
        return searchBar
    }()
    
    lazy var searchTypeManager: SearchTypeManager = {
        let height = SearchPageViewController.searchTypeViewHeight
        let manager = SearchTypeManager.init(basisView: view, y: self.navigationController!.navigationBar.frame.maxY, height: height)
        manager.delegate = self
        return manager
    }()
    
    static let searchTypeViewHeight: CGFloat = 44.0
    
    var currentIndex: Int = 0 {
        didSet {
            if oldValue != currentIndex {
                searchTypeManager.switchStyle(to: currentIndex)
            }
        }
    }

    var isSearching: Bool = false {
        didSet {
            searchBar.showsCancelButton = isSearching
        }
    }
//    {
//        didSet {
//            if isSearching == true && isSearching != oldValue {
//
//            } else if isSearching == true && isSearching == oldValue {
//                (viewControllers?.first as! SearchableVC).searchTextDidChange(text: self.searchBar.text!)
//            } else if isSearching == false && isSearching != oldValue {
//
//            }
//        }
//    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.view.frame.size.height = UIScreen.main.bounds.height
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupVC()
        setupNavigationController()
        
        for v in view.subviews {
            if let scrollView = v as? UIScrollView {
                scrollView.delegate = self
            }
        }
    }
}

extension SearchPageViewController: DefaultStateVCDelegate {
    func scrollToTop() {
        if let searchVC = viewControllerList.first as? SearchViewController {
            if searchVC.collectionView.numberOfItems(inSection: 0) > 0 {
                searchVC.collectionView.scrollToItem(at: IndexPath.init(row: 0, section: 0), at: .top, animated: true)
            }
        }
    }
}

extension SearchPageViewController: SearchTypeViewDelegate {
    func eventButtonWasPressed() {
        if let vc = viewControllers?.first {
            if !(vc is SearchEventViewController) {
                setViewControllers([viewControllerList[1]], direction: .reverse, animated: true, completion: nil)
            }
        }
    }
    
    func userButtonWasPressed() {
        if let vc = viewControllers?.first {
            if !(vc is SearchUserViewController) {
                setViewControllers([viewControllerList[2]], direction: .forward, animated: true, completion: nil)
            }
        }
    }
}

extension SearchPageViewController {
    private func setupNavigationController() {
        navigationItem.titleView = searchBar
    }
    
    private func setupVC() {
        self.delegate = self
        self.dataSource = self
        let firstViewController = viewControllerList[0]
        self.setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
        self.view.backgroundColor = UIColor.init(hex: "FAFAFA") // UIColor.white
    }
    
    private func setCurrentIndex(contentOffset: CGPoint) {
        if (contentOffset.x - UIScreen.width) <= -(UIScreen.main.bounds.width) {
            currentIndex = 0
        }
        if (contentOffset.x - UIScreen.width) >= UIScreen.main.bounds.width {
            currentIndex = 1
        }
    }
}

extension SearchPageViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if isSearching == true {
            return
        }
        setViewControllers([viewControllerList[1]], direction: .forward, animated: false, completion: nil)
        searchTypeManager.addView()
        isSearching = true
        print("11:: begin editing")
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.text = nil
        let firstViewController = viewControllerList[0]
        setViewControllers([firstViewController], direction: .forward, animated: false, completion: nil)
        searchTypeManager.setToDefault()
        searchTypeManager.removeView()
        isSearching = false
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if let vcs = viewControllers {
            for vc in vcs {
                (vc as? SearchableVC)?.searchTextDidChange(text: searchText)
            }
        }
    }
}

extension SearchPageViewController: SearchPageScrollViewDelegate {
    func controllerDidScroll(_ scrollView: UIScrollView) {
        // for future deletion
    }
}

extension SearchPageViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        setCurrentIndex(contentOffset: scrollView.contentOffset)
    }
}

extension SearchPageViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        if isSearching == false {
            return nil
        }
        guard let vcIndex = viewControllerList.index(of: viewController) else {
            return nil
        }
        let previousIndex = vcIndex + 1
        guard previousIndex >= 0 else {
            return nil
        }
        guard viewControllerList.count > previousIndex else {
            return nil
        }

        return viewControllerList[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        if isSearching == false {
            return nil
        }
        guard let vcIndex = viewControllerList.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = vcIndex - 1

        // usually it's >= 0
        guard nextIndex > 0 else {
            return nil
        }
        
        guard viewControllerList.count != nextIndex else {
            return nil
        }
        guard viewControllerList.count > nextIndex else {
            return nil
        }

        return viewControllerList[nextIndex]
    }
}
