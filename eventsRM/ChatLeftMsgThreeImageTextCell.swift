//
//  ChatLeftMsgThreeImageTextCell.swift
//  WentOut
//
//  Created by Fure on 15.10.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import UIKit

class ChatLeftMsgThreeImageTextCell: ChatMessageBaseCell {
    
    @IBOutlet var avatarOutlet: UIImageView?
    @IBOutlet var messageOutlet: UILabel!
    @IBOutlet var mediaCollectionOutlet: [UIImageView]?
    @IBOutlet var usernameOutlet: UILabel!
    @IBOutlet var timeOutlet: UILabel!
    @IBOutlet var messageBackgroundOutlet: UIView!
    
    @IBOutlet var timeLeadingConstrOutlet: NSLayoutConstraint!
    @IBOutlet var timeTopConstrOutlet: NSLayoutConstraint!

    override var avatar: UIImageView? {
        return self.avatarOutlet
    }
    override var message: UILabel! {
        return self.messageOutlet
    }
    override var username: UILabel! {
        return self.usernameOutlet
    }
    override var time: UILabel! {
        return self.timeOutlet
    }
    override var mediaCollection: [UIImageView]? {
        return self.mediaCollectionOutlet
    }
    override var messageBackground: UIView! {
        return self.messageBackgroundOutlet
    }
    override var timeLeadingConstr: NSLayoutConstraint! {
        return self.timeLeadingConstrOutlet
    }
    override var timeTopConstr: NSLayoutConstraint! {
        return self.timeTopConstrOutlet
    }
    
//    override func draw(_ rect: CGRect) {
//        self.avatar.makeCircle()
//        self.messageBackground.makeOval(cornerRadius: 15.0)
//        self.mediaOne.makeOval(cornerRadius: 10.0)
//        self.mediaTwo.makeOval(cornerRadius: 10.0)
//        self.mediaThree.makeOval(cornerRadius: 10.0)
//    }
}

//protocol ChatLeftMsgThreeImageTextDelegate { }
//
//protocol ChatLeftMsgThreeImageTextInteractor: MessageInteractor { }
//
//extension ChatLeftMsgThreeImageTextCell: ChatLeftMsgThreeImageTextInteractor { }
//
//extension ChatLeftMsgThreeImageTextCell: ConfigurableCell {
//    typealias CellDelegate = ChatLeftMsgThreeImageTextDelegate
//    typealias CellData = MessageWrapper
//    typealias CellInteractor = ChatLeftMsgThreeImageTextInteractor
//
//    func configure(with data: MessageWrapper, delegate: CellDelegate?) {
//        self.avatar.image = nil
//        self.username.text = data.message.senderUsername
//        self.message.text = data.message.msg
//
//        Webservice.loadImage(url: data.message.senderAvatarURL) { (_image) in
//            if let image = _image {
//                self.avatar.image = image
//            }
//        }
//
//        Webservice.loadImage(url: data.message.attch[0].url) { (_image) in
//            if let image = _image {
//                self.mediaOne.image = image
//            }
//        }
//
//        Webservice.loadImage(url: data.message.attch[1].url) { (_image) in
//            if let image = _image {
//                self.mediaTwo.image = image
//            }
//        }
//
//        Webservice.loadImage(url: data.message.attch[2].url) { (_image) in
//            if let image = _image {
//                self.mediaThree.image = image
//            }
//        }
//
//        let dateFormatter: DateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "HH:mm"
//        let timeString = dateFormatter.string(from: data.message.time)
//        self.time.text = timeString
//
//        if data.avatarIsHidden {
//            self.avatar.isHidden = true
//        } else {
//            self.avatar.isHidden = false
//        }
//
//        self.layoutIfNeeded()
//
//        if self.message.calculateMaxLines() > 1 {
//            self.messageTimeLeadingConstr.constant = -self.time.frame.width
//            self.messageTimeTopConstr.constant = 0.0 // 3.0
//        } else {
//            self.messageTimeTopConstr.constant = -15.0
//            self.messageTimeLeadingConstr.constant = 11.0
//        }
//    }
//}












