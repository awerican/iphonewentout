//
//  UserInviteViewController.swift
//  WentOut
//
//  Created by Fure on 09.09.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import UIKit

class UserInviteViewController: UIViewController {
    
    typealias SelectedItem = (indexPath: IndexPath, eventID: String)
    
    let eventDownloader = Downloader<[EventPanel], UserProfileEventParameters>.init(resource: URL.UserProfile.events, param: UserProfileEventParameters.init(visiterID: LocalAuth.userID!, profileOwnerID: LocalAuth.userID!))
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        let cv = UICollectionView.init(frame: self.view.bounds, collectionViewLayout: layout)
        return cv
    }()
    
    var inviteUserID: String!
    
    var selectedItem: SelectedItem? {
        didSet {
            if let _ = selectedItem {
                addInviteButtonIfNeeded()
            } else {
                removeInviteButtonIfNeeded()
            }
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupNetwork()
    }
    
    @IBAction func closeAction(_ sender: UIBarButtonItem) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
}

extension UserInviteViewController {
    private func setupNetwork() {
        eventDownloader.getFirst {
            self.collectionView.reloadData()
        }
    }
    
    private func setupViews() {
        setupCollectionView()
        setupNavBar()
    }
    
    private func setupNavBar() {
        navigationItem.title = "Choose an event"
    }
    
    private func addInviteButtonIfNeeded() {
        func _addItem() {
            let inviteButton = UIBarButtonItem.init(title: "Invite", style: .done, target: self, action: #selector(inviteFriendsAction(_:)))
            navigationItem.rightBarButtonItems = [inviteButton]
        }
        
        if let items = navigationItem.rightBarButtonItems {
            if items.count == 0 {
                _addItem()
            }
        } else {
            _addItem()
        }
    }
    
    private func removeInviteButtonIfNeeded() {
        if navigationItem.rightBarButtonItems?.count > 0 {
            navigationItem.rightBarButtonItems = []
        }
    }
    
    private func setupCollectionView() {
        view.addSubview(collectionView)
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.backgroundColor = UIColor.init(hex: "FAFAFA")
        
        for id in reuseIDs {
            collectionView.register(UINib.init(nibName: id, bundle: nil), forCellWithReuseIdentifier: id)
        }
    }
}

extension UserInviteViewController {
    @objc private func inviteFriendsAction(_ sender: UIBarButtonItem) {
        sender.isEnabled = false
        
        let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        activityIndicator.color = .gray
        navigationItem.titleView = activityIndicator
        activityIndicator.startAnimating()
        
        collectionView.isUserInteractionEnabled = false
        
        guard let indexPath = selectedItem?.indexPath else {
            return
        }
        
        guard let eventID = eventDownloader.data?[indexPath.row].eventId else {
            return
        }
        
        guard let inviteUserID = self.inviteUserID else {
            return
        }
        
        let param = EventInviteParameters(userID: LocalAuth.userID!, eventGuests: [inviteUserID], eventID: eventID)
        
        webservice.load(resource: ResList.invite(param)) { (_result, _error) in
            if let result = _result {
                if let status = result.data?.status {
                    switch status {
                    case .succeed:
                        print("chigga success")
                        self.dismiss(animated: true, completion: nil)
                    case .failed:
                        print("chigga failed")
                        self.dismiss(animated: true, completion: nil)
                    }
                } else {
                    print("chigga no status")
                    self.dismiss(animated: true, completion: nil)
                }
            } else {
                print("chigga no result")
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
}

extension UserInviteViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    var reuseIDs: [String] {
        get {
            return ["UserEventInviteCell"]
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.eventDownloader.data?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIDs[0], for: indexPath) as! UserEventInviteCell
        var isChecked = false
        if let item = selectedItem {
            if item.indexPath == indexPath {
                isChecked = true
            }
        }
        cell.configCell(model: eventDownloader.data![indexPath.row], isChecked: isChecked)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        if let item = selectedItem {
            if item.indexPath == indexPath {
                selectedItem = nil
                collectionView.reloadItems(at: [indexPath])
            } else {
                let prevIndexPath = item.indexPath
                selectedItem = (indexPath: indexPath, eventID: eventDownloader.data![indexPath.row].eventId)
                collectionView.reloadItems(at: [indexPath, prevIndexPath])
            }
        } else {
            selectedItem = (indexPath: indexPath, eventID: eventDownloader.data![indexPath.row].eventId)
            collectionView.reloadItems(at: [indexPath])
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let data = self.eventDownloader.data else {
            return
        }
        if indexPath.row == data.count - 1 {
            self.eventDownloader.getNext(section: 0) { (paths) in
                self.collectionView.insertItems(at: paths)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return UserEventInviteCell.currentSize
    }
}




















