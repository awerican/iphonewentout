//
//  ImageCache.swift
//  WentOut
//
//  Created by Fure on 27.07.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

//import Foundation
//import UIKit
//
//public protocol ImageCacheFeedbackProtocol: Hashable {
//    var observingImageURLs: Set<URL> { get set }
//    func imageDidDownload(_ url: URL, _ image: UIImage)
//}
//
//public final class ImageCache<T: ImageCacheFeedbackProtocol>: NSObject {
//    private var imageCache = NSCache<NSURL, UIImage>()
//
//    private var subscribers: Set<T> = []
//
//    func subscribe(delegate: ImageCacheFeedbackProtocol) {
//        if !self.subscribers.contains(delegate as! AnyHashable) {
//            self.subscribers.insert(delegate as! AnyHashable)
//        }
//    }
//
//    func isDownloaded(_ url: URL?) -> Bool {
//        guard let URL = url as NSURL? else {
//            return false
//        }
//        if self.imageCache.object(forKey: URL) == nil {
//            return false
//        }
//        return true
//    }
//
//    func getImage(_ url: URL?) -> UIImage? {
//        guard let URL = url as NSURL? else {
//            return nil
//        }
//        return self.imageCache.object(forKey: URL)
//    }
//
//    func saveImage(by url: URL?, completion: @escaping (_ image: UIImage?) -> Void = {_ in}) {
//        guard let url = url else {
//            completion(nil)
//            return
//        }
//        if self.imageCache.object(forKey: url as NSURL) == nil {
//            Webservice.loadImage(by: url) { (_image) in
//                if let image = _image {
//                    self.imageCache.setObject(image, forKey: url as NSURL)
//                    self.tellImageWasDownloaded(url, image)
//                    completion(image)
//                } else {
//                    completion(nil)
//                }
//            }
//        } else {
//            if let image = imageCache.object(forKey: url as NSURL) {
//                completion(image)
//            } else {
//                completion(nil)
//            }
//        }
//    }
//
//    private func tellImageWasDownloaded(_ url: URL, _ image: UIImage) {
//        for sub in self.subscribers {
//            if sub.observingImageURLs.contains(url) {
//                sub.imageDidDownload(url, image)
//            }
//        }
//    }
//}
//
//
//
//
//
//
//
