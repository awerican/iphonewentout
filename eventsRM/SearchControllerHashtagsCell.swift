//
//  SearchControllerHashtagsCell.swift
//  eventsRM
//
//  Created by Fure on 30.10.17.
//  Copyright © 2017 uzuner. All rights reserved.
//

import UIKit

class SearchControllerHashtagsCell: UICollectionViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    private struct Constants {
        static let reuseNameNibName = "SearchControllerHashtagsCellSep"
    }
    
    var correctSize: CGSize {
        get {
            let cell = Bundle.main.loadNibNamed(Constants.reuseNameNibName, owner: self, options: nil)?.first as! SearchControllerHashtagsCellSep
            cell.configCell(text: "someText")
            return CGSize.init(width: UIScreen.main.bounds.width, height: 5 + 2 * cell.labelBackView.frame.height + 5)
        }
    }
    
    var model: [String] = ["#yoyoyo", "#cars", "#realniggagigup", "#truesound", "#fuckersgoon", "#everythingwontBeAllRight", "#nonofucku"] {
        didSet {
            self.collectionView.reloadData()
        }
    }
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        layout.sectionInset.left = 20 - 5
        layout.sectionInset.right = 20
        var collectionView = UICollectionView.init(frame: CGRect.init(x: 0.0, y: 0.0, width: self.frame.width, height: self.frame.height), collectionViewLayout: layout)
        collectionView.backgroundColor = UIColor.white
        collectionView.showsHorizontalScrollIndicator = false
        return collectionView
    }()
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        setupCollectionView()
    }
    
    private func setupCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib.init(nibName: Constants.reuseNameNibName, bundle: nil), forCellWithReuseIdentifier: Constants.reuseNameNibName)
        addSubview(collectionView)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return model.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.reuseNameNibName, for: indexPath) as! SearchControllerHashtagsCellSep
        cell.configCell(text: model[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cell = Bundle.main.loadNibNamed(Constants.reuseNameNibName, owner: self, options: nil)?.first as! SearchControllerHashtagsCellSep
        cell.configCell(text: model[indexPath.row])
        return cell.currentCellSize
    }
}























