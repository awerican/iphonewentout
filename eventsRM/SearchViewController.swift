//
//  SearchViewController.swift
//  eventsRM
//
//  Created by Fure on 29.10.17.
//  Copyright © 2017 uzuner. All rights reserved.
//

import Foundation
import UIKit

class SearchViewController: UIViewController {
    
    let eventsDownloader = Downloader<[EventCategoryEntity], EmptyParameters>.init(resource: URL.Search.events, param: EmptyParameters())
    
    let usersDownloader = Downloader<[UserMini], EmptyParameters>.init(resource: URL.Search.users, param: EmptyParameters())
    
    weak var scrollViewDelegate: SearchPageScrollViewDelegate?
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height), collectionViewLayout: layout)
        return collectionView
    }()
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupCollectionView()
        setupNetwork()
    }
}

extension SearchViewController {
    private func setupNetwork() {
        usersDownloader.getFirst {
            self.collectionView.reloadSections([0])
        }
        eventsDownloader.getFirst {
            self.collectionView.reloadSections([1, 2])
        }
    }
    
    private func setupView() {
        view.backgroundColor = UIColor.init(hex: "FAFAFA")
        view.addSubview(collectionView)
    }
    
    private func setupCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
//        collectionView.backgroundColor = UIColor.white
        collectionView.backgroundColor = UIColor.init(hex: "F9F9F9")
        collectionView.contentInset.top = 8.0
        collectionView.contentInset.bottom = UITabBar.height + 3.0
        for reuseID in reuseIDs {
            collectionView.register(UINib.init(nibName: reuseID, bundle: nil), forCellWithReuseIdentifier: reuseID)
        }
    }
}

extension SearchViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.scrollViewDelegate?.controllerDidScroll(scrollView)
    }
}

extension SearchViewController: UICollectionViewDataSource {
    var reuseIDs: [String] {
        get {
            return ["SearchControllerUserCell", "SearchControllerEventWrapperCell", "SearchPageSeperatorCollectionViewCell", "SearchPageUserTitleCollectionViewCell"]
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            if let usersCount = usersDownloader.data?.count {
                return usersCount + 1
            }
            return 0
        }
        if section == 1 {
            if let _ = eventsDownloader.data {
                return 1
            }
            return 0
        }
        if section == 2 {
            return eventsDownloader.data?.count ?? 0
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIDs[3], for: indexPath) as! SearchPageUserTitleCollectionViewCell
                return cell
            }
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIDs[0], for: indexPath) as! SearchControllerUserCell
            let model = usersDownloader.data![indexPath.row - 1]
            if indexPath.row == usersDownloader.data!.count - 1 + 1 {
//            if indexPath.row == usersDownloader.data!.count - 1 {
                cell.configCell(model: model, underlined: false)
            } else {
                cell.configCell(model: model, underlined: true)
            }
            return cell
        }
        if indexPath.section == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIDs[2], for: indexPath) as! SearchPageSeperatorCollectionViewCell
            return cell
        }
        if indexPath.section == 2 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIDs[1], for: indexPath) as! SearchControllerEventWrapperCell
            cell.model = eventsDownloader.data![indexPath.row]
            cell.delegate = self
            return cell
        }
        return UICollectionViewCell()
    }
}

extension SearchViewController: UICollectionViewDelegate, SearchControllerEventWrapperCellDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
            controller.profileOwnerID = self.usersDownloader.data?[indexPath.row - 1].id
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    func didSelectCell(at indexPath: IndexPath, pageType: SearchControllerPageType) {
        if pageType == .event {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "EventProfileViewController") as! EventProfileViewController
            controller.eventID = self.eventsDownloader.data?[indexPath.section].events[indexPath.row].eventId
            self.navigationController?.pushViewController(controller, animated: true)
        }
        if pageType == .user {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
            controller.profileOwnerID = self.eventsDownloader.data?[indexPath.section].events[indexPath.row].userId
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
//    func didSelectCell(at indexPath: IndexPath) {
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let controller = storyboard.instantiateViewController(withIdentifier: "EventProfileViewController") as! EventProfileViewController
//        controller.eventID = self.eventsDownloader.data?[indexPath.section].events[indexPath.row].eventId
//        self.navigationController?.pushViewController(controller, animated: true)
//    }
}

extension SearchViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                return SearchPageUserTitleCollectionViewCell.currentSize
            }
            return SearchControllerUserCell.currentSize
        }
        if indexPath.section == 1{
            return SearchPageSeperatorCollectionViewCell.currentSize
        }
        if indexPath.section == 2 {
            return SearchControllerEventWrapperCell.currentSize
        }
        return CGSize.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//        return UIEdgeInsets.zero
        return UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 15.0)
    }
}

































