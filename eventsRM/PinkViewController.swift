//
//  PinkViewController.swift
//  WentOut
//
//  Created by Fure on 08.09.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import Foundation
import UIKit

class PinkViewController: UIViewController {
    
    var tableDirector: TableDirector!
    var tableView: UITableView!
    
    var headerModel: IntTableViewModel = IntTableViewModel.init(leftNumber: 0, rightNumber: 0)
    var bodyModel: [StringTableViewModel] = [StringTableViewModel.init(title: "1", body: "11"),
                                             StringTableViewModel.init(title: "2", body: "22"),
                                             StringTableViewModel.init(title: "3", body: "33"),
                                             StringTableViewModel.init(title: "4", body: "44")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initTableView()
        initTableDirector()
        downloadData()
    }
}

extension PinkViewController {
    private func initTableView() {
        self.tableView = UITableView(frame: self.view.frame)
        self.view.addSubview(self.tableView)
    }
    
    private func initTableDirector() {
        self.tableDirector = TableDirector(tableView: self.tableView, delegate: self, controller: self)
    }
    
    private func downloadData() {
        
        let headerRow = TableRow<IntTableViewCell>.init(item: self.headerModel)
        var bodyRows = [TableRow<StringTableViewCell>]()
        for model in self.bodyModel {
            bodyRows.append(TableRow<StringTableViewCell>.init(item: model))
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [unowned self] in
            self.tableDirector.appendLast(rows: [headerRow] + bodyRows, atSection: 0, animation: .bottom)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) { [unowned self] in
            self.headerModel.leftNumber = 1000
            self.headerModel.rightNumber = 1200
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 4) { [unowned self] in
            self.tableDirector.updateRowAt(at: IndexPath.init(row: 0, section: 0), with: TableRow<IntTableViewCell>.init(item: self.headerModel), animation: .none)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 6) {
            let firstRow: TableRow<IntTableViewCell>? = self.tableDirector.getRowAt(indexPath: IndexPath.init(row: 0, section: 0))
            let secondRow: TableRow<StringTableViewCell>? = self.tableDirector.getRowAt(indexPath: IndexPath.init(row: 1, section: 0))
            
            firstRow?.interactor?.beginNewLife()
            secondRow?.interactor?.changeBackgroundColor()
        }
    }
}

extension PinkViewController: TableDirectorController { }

extension PinkViewController: IntTableViewCellDelegate {
    func leftButtonAction(at indexPath: IndexPath) {
        print("indexPath =", indexPath)
    }
    func rightButtonAction(at indexPath: IndexPath) {
        print("indexPath =", indexPath)
    }
}

extension PinkViewController: TableDirectorDelegate {
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        print("willSelectRowAt", indexPath)
        return indexPath
    }
}

























