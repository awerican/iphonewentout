//
//  AddMemoryViewController.swift
//  WentOut
//
//  Created by Fure on 20.05.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import CropViewController

//protocol AddMemoryDelegate: class {
//    func closeVC()
//    func keepVC()
//}

protocol AddMemoryViewControllerDelegate: class {
    func addMemoryViewController(image: UIImage)
}

class AddMemoryViewController: UIViewController, AVCapturePhotoCaptureDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    let memoryPhotoSize = CGSize.init(width: 1.1, height: 2)
    let eventPhotoSize = CGSize.init(width: 1.0, height: 1.0)
    
    var captureSession = AVCaptureSession()
    var sessionOutput = AVCapturePhotoOutput()
    var sessionOutputSetting = AVCapturePhotoSettings(format: [AVVideoCodecKey:AVVideoCodecJPEG])
    var previewLayer = AVCaptureVideoPreviewLayer()
    
    weak var delegate: AddMemoryViewControllerDelegate?
    
    private var permissionGranted = false
    
    var event: EventPanel!
    
//    weak var addMemoryDelegate: AddMemoryDelegate?
    
    var imagePicker = UIImagePickerController()
    
//    let memoryImageRation = CGSize.init(width: 1.1, height: 2)
    var memoryImageRation = CGSize.init(width: 1.1, height: 2)
    
    lazy var swipeDown: UISwipeGestureRecognizer = {
        let gesture = UISwipeGestureRecognizer.init(target: self, action: #selector(closeVC))
        gesture.direction = .down
        return gesture
    }()
    
    lazy var captureButton: UIButton = {
        let width: CGFloat = 60.0
        let height: CGFloat = width
        let hwidth: CGFloat = width / 2
        let bMargin: CGFloat = 30.0
        
        let button = UIButton()
        button.backgroundColor = UIColor.white
        button.frame = CGRect.init(x: UIScreen.width / 2 - hwidth, y: UIScreen.height - height - bMargin, width: width, height: height)
        button.makeCircle()
        
        button.addTarget(self, action: #selector(captureAction), for: .touchUpInside)
        
        return button
    }()
    
    lazy var swapCameraButton: UIButton = {
        let width: CGFloat = 33.0
        let height: CGFloat = width
        let margin: CGFloat = 30.0
        
        let button = UIButton()
        button.backgroundColor = UIColor.clear
        button.frame = CGRect.init(x: captureButton.frame.maxX + margin, y: captureButton.frame.midY - (height / 2), width: width, height: height)

        let image = #imageLiteral(resourceName: "addMemoryReverse").setColor(.white)

        button.setImage(image, for: .normal)
        
        button.addTarget(self, action: #selector(swapCameraAction), for: .touchUpInside)
        
        return button
    }()
    
    lazy var openLibrary: UIButton = {
        let width: CGFloat = 33.0
        let height: CGFloat = width
        let margin: CGFloat = 30.0
        
        let button = UIButton()
        button.backgroundColor = UIColor.clear
        button.frame = CGRect.init(x: captureButton.frame.minX - margin - width, y: captureButton.frame.midY - (height / 2), width: width, height: height)
        
        let image = #imageLiteral(resourceName: "addMemoryGallery").setColor(.white)
        
        button.setImage(image, for: .normal)
        
        button.addTarget(self, action: #selector(openLibraryAction), for: .touchUpInside)
        
        return button
    }()
    
    lazy var captureButtonBackground: UIVisualEffectView = {
        let margin: CGFloat = 14.0
        let width: CGFloat = captureButton.frame.width + margin + margin
        let height: CGFloat = width
        let view = UIVisualEffectView.init(frame: CGRect.init(x: captureButton.frame.minX - margin, y: captureButton.frame.minY - margin, width: width, height: height))
        view.effect = UIBlurEffect.init(style: UIBlurEffectStyle.extraLight)
        view.alpha = 0.80
        view.makeCircle()
        return view
    }()
    
    lazy var closeButton: UIButton = {
        let tMargin: CGFloat = 8.0
        let lMargin: CGFloat = 2.0
        let width: CGFloat = 55.0
        let height: CGFloat = width
        let button = UIButton()
        button.frame = CGRect.init(x: UIScreen.width - lMargin - width, y: tMargin, width: width, height: height)
        button.setTitle("×", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(closeVC), for: .touchUpInside)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 28.0)
        return button
    }()
    
    lazy var cameraView: UIView = {
        let view = UIView.init(frame: self.view.frame)
        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(swipeDown)
        return view
    }()
    
    private let sessionQueue = DispatchQueue(label: "session queue")
    
//    override func viewWillLayoutSubviews() {
//        super.viewWillLayoutSubviews()
//        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
//        statusBar.isHidden = true
//    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        previewLayer.frame = UIScreen.main.bounds
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.black
        
        setupImagePicker(imagePicker: imagePicker)
        
        self.view.addSubview(cameraView)
        
        checkPermission()
        sessionQueue.async { [unowned self] in
//            self.configureSession()
            self.setupCameraLayer(callback: {
                DispatchQueue.main.async {
                    self.cameraView.layer.addSublayer(self.previewLayer)
                    
                    self.view.addSubview(self.captureButton)
                    // self.view.addSubview(captureButtonBackground)
                    self.view.addSubview(self.closeButton)
                    self.view.addSubview(self.swapCameraButton)
                    self.view.addSubview(self.openLibrary)
                }
            })
            self.captureSession.startRunning()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        captureSession.startRunning()
    }
    
    private func presentCropViewController(image: UIImage) {
        let cropViewController = CropViewController(image: image)
        cropViewController.customAspectRatio = memoryImageRation
        cropViewController.aspectRatioLockEnabled = true
        cropViewController.aspectRatioPickerButtonHidden = true
        cropViewController.resetButtonHidden = true
        cropViewController.delegate = self
        present(cropViewController, animated: true, completion: nil)
    }
    
    func capture() {
        let photoSettings = AVCapturePhotoSettings()
        photoSettings.isAutoStillImageStabilizationEnabled = true
//        photoSettings.isHighResolutionPhotoEnabled = true
        
        addInputIfNeeded(position: .back)
        
        guard let input = captureSession.inputs.first as? AVCaptureDeviceInput else {
            print("NO INPUT")
            return
        }
        
        let position = input.device.position
        photoSettings.flashMode = position == .front || position == .unspecified ? .off : .auto
        sessionOutput.capturePhoto(with: photoSettings, delegate: self)
    }
    
    @objc private func swapCameraAction() {
        swapCamera()
    }
    
    private func addInputIfNeeded(position: AVCaptureDevice.Position) {
        guard captureSession.inputs.count == 0 else {
            return
        }
        
        captureSession.beginConfiguration()
        
        defer { captureSession.commitConfiguration() }
        
        // Create new capture device
//        var newDevice: AVCaptureDevice?
    
        guard let newDevice = captureDevice(with: position) else {
            print("NO DEVICE")
            return
        }
        
        // Create new capture input
        var deviceInput: AVCaptureDeviceInput!
        do {
            deviceInput = try AVCaptureDeviceInput(device: newDevice)
        } catch let error {
            print(error.localizedDescription)
            return
        }
        
        captureSession.addInput(deviceInput)
    }
    
    // MARK: AVSession configuration
    private func checkPermission() {
        switch AVCaptureDevice.authorizationStatus(for: AVMediaType.video) {
        case .authorized:
            permissionGranted = true
        case .notDetermined:
            requestPermission()
        case .denied:
            setupLimitAccessViews()
            permissionGranted = false
        case .restricted:
            setupLimitAccessViews()
            permissionGranted = false
        }
    }
    
    private func setupLimitAccessViews() {
        let buttonFrame = addButtonSettings()
        addDescription(buttonFrame)
        view.addSubview(closeButton)
    }
    
    private func addDescription(_ buttonFrame: CGRect) {
        let width = UIScreen.width / 4 * 3
        let size = CGSize.init(width: width, height: 100.0)
        
        let y1 = buttonFrame.midY + (UIScreen.height - buttonFrame.midY) / 2
        let y = y1 - size.height / 2
        
        let label = UILabel.init(frame: CGRect.init(x: (UIScreen.width - width) / 2, y: y, width: size.width, height: size.height))
        
        label.backgroundColor = .clear
        label.text = "Enable access so you can start taking photos."
        label.textAlignment = .center
        label.textColor = UIColor(hex: "999999")
        label.numberOfLines = 0
        let font = label.font
        let newFont = font?.withSize(14.0)
        label.font = newFont
        
        label.isUserInteractionEnabled = false
        
        view.addSubview(label)
    }
    
    private func addButtonSettings() -> CGRect {
        let size = CGSize.init(width: UIScreen.width, height: 100.0)
        let button = UIButton.init(frame: CGRect.init(x: 0, y: UIScreen.height / 4 * 3 - size.height / 2, width: size.width, height: size.height))
        button.backgroundColor = .clear
        button.setTitle("Enable Camera Access", for: .normal)
        button.setTitleColor(UIColor(hex: "3897f0"), for: .normal)
        let font = button.titleLabel!.font
        let newFont = font?.withSize(15.0)
        button.titleLabel?.font = newFont
        view.addSubview(button)
        
        button.addTarget(self, action: #selector(openUserSettingsAction), for: .touchUpInside)
        
        return button.frame
    }
    
    @objc private func openUserSettingsAction() {
        guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
            return
        }
        
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                print("Settings opened: \(success)") // Prints true
            })
        }
    }
    
    private func requestPermission() {
        sessionQueue.suspend()
        AVCaptureDevice.requestAccess(for: AVMediaType.video) { [unowned self] granted in
            self.permissionGranted = granted
            self.sessionQueue.resume()
        }
    }
    
    func swapCamera() {
        
        guard let input = captureSession.inputs.first as? AVCaptureDeviceInput else {
            print("NO INPUT WHEN SWAP CAMERA")
            return
        }
        
        // Begin new session configuration and defer commit
        captureSession.beginConfiguration()
        
        defer { captureSession.commitConfiguration() }
        
        // Create new capture device
        var newDevice: AVCaptureDevice?
        
        if input.device.position == .back {
            newDevice = captureDevice(with: .front)
        } else {
            newDevice = captureDevice(with: .back)
        }
        
        // Create new capture input
        var deviceInput: AVCaptureDeviceInput!
        do {
            deviceInput = try AVCaptureDeviceInput(device: newDevice!)
        } catch let error {
            print(error.localizedDescription)
            return
        }
        
        // Swap capture device inputs
        
        captureSession.removeInput(input)
        captureSession.addInput(deviceInput)
    }
    
    fileprivate func captureDevice(with position: AVCaptureDevice.Position) -> AVCaptureDevice? {
        
        let devices = AVCaptureDevice.DiscoverySession(deviceTypes: [ .builtInWideAngleCamera, .builtInMicrophone, .builtInDualCamera, .builtInTelephotoCamera ], mediaType: AVMediaType.video, position: .unspecified).devices
        
        for device in devices {
            if device.position == position {
                return device
            }
        }
        
        return nil
    }
    
    private func setupCameraLayer(callback: @escaping ()->()) {
        print("111 in setupCameraLayer()")
        
        guard let captureDevice = AVCaptureDevice.default(for: .video) else {
            print("111 no captureDevice")
            return
        }
        
        AVCaptureDevice.default(for: .video)
        
        do {
            let input = try AVCaptureDeviceInput(device: captureDevice)
            
            if (captureSession.canAddInput(input)) {
                captureSession.addInput(input)
                
                if (captureSession.canAddOutput(sessionOutput)) {
                    captureSession.addOutput(sessionOutput)
                    previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
                    previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
                    previewLayer.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
                    
                    callback()
                    
//                    self.cameraView.layer.addSublayer(previewLayer)
//
//                    self.view.addSubview(captureButton)
//                    // self.view.addSubview(captureButtonBackground)
//                    self.view.addSubview(closeButton)
//                    self.view.addSubview(swapCameraButton)
//                    self.view.addSubview(openLibrary)
                } else {
                    print("111 CANT ADD OUTPUR")
                }
            } else {
                print("111 CANT ADD INPUT")
            }
        } catch {
            print("111 ERROR", error)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavBar()
    }
}

extension AddMemoryViewController: CropViewControllerDelegate {
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        self.dismiss(animated: true) {
            if self.memoryImageRation == self.eventPhotoSize {
                self.navigationController!.dismiss(animated: true, completion: {
                    print("gimme an image")
                    self.delegate?.addMemoryViewController(image: image)
                })
//                print("vc =", createPageVC?.description ?? "null")
//                print("close me and send an image back")
            } else if self.memoryImageRation == self.memoryPhotoSize {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: "PostMemoryViewController") as! PostMemoryViewController
                controller.media = UploadableMedia(image: image)
                controller.event = self.event
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }
    
    func cropViewController(_ cropViewController: CropViewController, didFinishCancelled cancelled: Bool) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension AddMemoryViewController {
//    private func setupViews() {
//
//
//        print("111 setupCameraLayer")
//        setupCameraLayer()
//    }
//
    private func setupImagePicker(imagePicker: UIImagePickerController) {
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        imagePicker.mediaTypes = ["public.image"]
//        imagePicker.mediaTypes = ["public.image", "public.movie"]
    }
    
    @objc private func captureAction() {
        self.capture()
    }
    
    @objc private func openLibraryAction() {
        self.present(self.imagePicker, animated: true, completion: nil)
    }
    
    @objc private func closeVC() {
        self.dismiss(animated: true) {
//            let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
//            statusBar.isHidden = false
        }
    }
    
    private func setupNavBar() {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
}

extension AddMemoryViewController {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            dismiss(animated: true) {
                self.presentCropViewController(image: pickedImage)
            }
        } else {
            print("something went wrong")
        }
    }
}

extension AddMemoryViewController {
    
    @available(iOS 11.0, *)
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        print("Image captured.")
        
        guard let imageData = photo.fileDataRepresentation() else {
            print("Error while generating image from photo capture data.");
            return
        }
        
        guard let image = UIImage(data: imageData) else {
            print("Unable to generate UIImage from image data.")
            return
        }
        
        presentCropViewController(image: image)
        
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let controller = storyboard.instantiateViewController(withIdentifier: "CapturedImageViewController") as! CapturedImageViewController
//        controller.media = PickedLibraryMedia(image: image)
//        controller.eventID = self.eventID
//        self.navigationController?.pushViewController(controller, animated: true)
    }
}
