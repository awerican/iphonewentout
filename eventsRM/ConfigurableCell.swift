//
//  ConfigurableCell.swift
//  WentOut
//
//  Created by Fure on 10.09.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import UIKit

protocol ConfigurableCell {
    associatedtype CellData
    associatedtype CellDelegate
    associatedtype CellInteractor
    
    static var reuseIdentifier: String { get }
    
    func configure(with _: CellData, delegate: CellDelegate?)
}

extension ConfigurableCell where Self: UITableViewCell {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}




















