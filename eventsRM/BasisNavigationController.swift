//
//  WorldNavigationViewController.swift
//  eventsRM
//
//  Created by Fure on 20.07.17.
//  Copyright © 2017 uzuner. All rights reserved.
//

import UIKit

class BottomNavigationController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBar.delegate = self
    }
}

extension BottomNavigationController: UINavigationBarDelegate {
    func navigationBar(_ navigationBar: UINavigationBar, shouldPush item: UINavigationItem) -> Bool // called to push. return NO not to.
    {
        print("shouldPush")
        return true
    }
    
    func navigationBar(_ navigationBar: UINavigationBar, didPush item: UINavigationItem) // called at end of animation of push or immediately if not animated
    {
        print("didPush")
    }
    
    func navigationBar(_ navigationBar: UINavigationBar, shouldPop item: UINavigationItem) -> Bool // same as push methods
    {
        print("shouldPop")
        return true
    }
    
    func navigationBar(_ navigationBar: UINavigationBar, didPop item: UINavigationItem) {
        print("didPop")
    }
}
