//
//  MessageManager.swift
//  WentOut
//
//  Created by Fure on 22.01.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import SocketIO

enum ManagerMessageResponse {
    case success(MessageEntity)
    case fail
}

struct MessageTimeLimit {
    var timeLimit: Int
}

extension MessageTimeLimit: Encodable {
    private enum CodingKeys: String, CodingKey {
        case timeLimit = "timeLimit"
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(timeLimit, forKey: .timeLimit)
    }
}

extension MessageTimeLimit: Decodable {
    private enum EventApiResponseCodingKeys: String, CodingKey {
        case timeLimit = "timeLimit"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: EventApiResponseCodingKeys.self)
        self.timeLimit = try container.decode(Int.self, forKey: .timeLimit)
    }
}

protocol MessageManagerDelegate: class {
//    @discardableResult
//    func incomingMessage(message: MessageEntity) -> IndexPath?
    func incomingMessage(message: MessageEntity)
    func incomingRecentMessages(messages: [MessageEntity])
    func incomingOlderMessages(messages: [MessageEntity])
}

struct ConnectionResponse {
    var aesKey: AesKey
    var joinerResponse: RoomJoinerRes
}

class MessageManager: NSObject {
    
    private struct SocketEventName {
        static let connection = "connection"
        static let disconnect = "disconnect"
        static let msgRequest = "msgReq"
        static let msgResponse = "msgRes"
        static let getTokenRes = "getTokenRes"
        static let getRecentMsg = "getRecentMsg"
        static let joinRoomReq = "joinRoomReq"
        static let getNewMessages = "getNewMsg"
    }
    
    static var sharedInstance: MessageManager = MessageManager()
    
    private var manager: SocketManager
    
    private var socket: SocketIOClient
    
    static var roomJoiner = RoomJoiner.init(userID: LocalAuth.userID!, eventID: "a69009f0-afc0-11e9-8cb2-5f584c1cc222")
    
    private var aesKey: AesKey?
    
    weak var delegate: MessageManagerDelegate?
    
    override init() {
        let config : [String: Any] = ["compress": true,
                                      "forcePolling": true,
                                      "forceNew": true]
        
//        self.manager = SocketManager(socketURL: URL.socketChat)
        self.manager = SocketManager(socketURL: URL.socketChat, config: config)
        self.manager.forceNew = true
        self.socket = self.manager.defaultSocket
        
        super.init()
        
        self.socket.on("connect") { (data, ack) in
            self.setup(joiner: MessageManager.roomJoiner, callback: { (_res) in
                if let _ = _res {
                    print("CHAT: connection success")
                } else {
                    print("CHAT: connection failed")
                }
            })
        }
        
        self.socket.on("disconnect") { (data, ack) in
            print("socket disconnected!!")
//            self.reinit()
        }
    }
    
    func connect(joiner: RoomJoiner, success: @escaping((Bool)->())) {
        socket.connect()
    }
    
//    private func reinit() {
//        let delegate = self.delegate
//        MessageManager.sharedInstance = MessageManager()
//        MessageManager.sharedInstance.delegate = delegate
//        self.aesKey = nil
//    }
    
    func disconnect() {
        socket.disconnect()
//        reinit()
        let delegate = self.delegate
        MessageManager.sharedInstance = MessageManager()
        MessageManager.sharedInstance.delegate = delegate
        self.aesKey = nil
    }
    
    func sendMessage(message: MessageSendEntity, callback: @escaping (ManagerMessageResponse)->()) {
        
        let socket = self.socket
        let eventName = SocketEventName.msgRequest
        let encMessage = message.encrypt(aesKey: self.aesKey!)
        
        IOEmitter<MessageSendEntity, MessageResponse>.emit(socket: socket, eventName: eventName, reqEntity: encMessage) { (_res) in
            guard let res = _res else {
                callback(.fail)
                return
            }
            if res.status {
                let msg = res.msg!
                let decryptedMsg = msg.decrypt(key: self.aesKey!)
                callback(.success(decryptedMsg))
            } else {
                callback(.fail)
            }
        }
    }
    
//    func askForMessagesBefore(time: Int) {
    func askForMessagesBefore(date: Date) {
        let seconds = date.timeIntervalSince1970
        let milisec = Int(seconds * 1000)
//        let time = milisec
        
        let socket = self.socket
        let eventName = SocketEventName.getNewMessages
        let timeLimit = MessageTimeLimit.init(timeLimit: milisec)
        IOEmitter<MessageTimeLimit, [MessageEntity]>.emit(socket: socket, eventName: eventName, reqEntity: timeLimit) { (_msgs) in
            guard let msgs = _msgs else {
                return
            }
            
            var decMsgs = [MessageEntity]()
            
            for msg in msgs {
                let decryptedMSG = msg.decrypt(key: self.aesKey!)
                decMsgs.append(decryptedMSG)
            }
            self.delegate?.incomingOlderMessages(messages: decMsgs)
        }
    }
}

extension MessageManager {
    
    private func setup(joiner: RoomJoiner, callback: ((ConnectionResponse?)->())? = nil) {
        let socket = self.socket
        
        IOOnner<AesKey>.on(socket: socket, eventName: SocketEventName.getTokenRes) { (_key) in
            guard let key = _key else {
                return
            }
            // in case of server send it more than one time by mistake
            if self.aesKey != nil {
                print("ERROR: socket event appeared twice")
                return
            }
            
            self.aesKey = key
        
            let encRoomJoiner = joiner.encrypt(aesKey: key)
            
            IOEmitter<RoomJoiner, RoomJoinerRes>.emit(socket: socket, eventName: SocketEventName.joinRoomReq, reqEntity: encRoomJoiner, response: { (_res) in
                guard let joinerRes = _res else {
                    callback?(nil)
                    return
                }
                
                if joinerRes.status == false {
                    callback?(nil)
                    return
                }
                
                let connectionRes = ConnectionResponse.init(aesKey: key, joinerResponse: joinerRes)
                
                callback?(connectionRes)
                
                self.__onGetRecentMsg()
                self.__onMsgResponse()
            })
        }
    }
    
    private func __onMsgResponse() {
        let socket = self.socket
        let eventName = SocketEventName.msgResponse
        
        IOOnner<MessageEntity>.on(socket: socket, eventName: eventName) { (_msg) in
            guard let msg = _msg else {
                return
            }
            
            let decMsg = msg.decrypt(key: self.aesKey!)
            
            self.delegate?.incomingMessage(message: decMsg)
        }
    }
    
    private func __onGetRecentMsg() {
        
        let socket = self.socket
        let eventName = SocketEventName.getRecentMsg
        
        IOOnner<[MessageEntity]>.on(socket: socket, eventName: eventName) { (_msgs) in
            guard let msgs = _msgs else {
                return
            }
            
            var decMsgs = [MessageEntity]()
            
            for msg in msgs {
                decMsgs.append(msg.decrypt(key: self.aesKey!))
            }
            
            self.delegate?.incomingRecentMessages(messages: decMsgs)
        }
    }
}




























