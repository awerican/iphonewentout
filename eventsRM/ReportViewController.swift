//
//  ReportViewController.swift
//  WentOut
//
//  Created by Fure on 19.10.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import UIKit

class ReportViewController: UICollectionViewController {
    
    let data: [String] = ["Spam", "Harassment", "Adult content", "Child pornography", "Drug advocacy", "Selling weapons", "Violence", "Encouraging bullying", "Encouraging suicide", "Animal abuse", "Misleading information", "Fraud", "Extremism"]
    
//    var memoryID: String!
    var downloader: Downloadable!
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        view.frame.size.height = UIScreen.main.bounds.height
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCV()
        setupNavBar()
    }
}

extension ReportViewController {
    private func setupNavBar() {
        navigationItem.title = "Report"
        let cancelButton = UIBarButtonItem.init(title: "Cancel", style: .plain, target: self, action: #selector(closeVC))
        navigationItem.leftBarButtonItems = [cancelButton]
    }
    
    @objc private func closeVC() {
        navigationController?.dismiss(animated: true, completion: nil)
    }
    
    private func setupCV() {
        for id in reuseIDs {
            collectionView?.register(UINib.init(nibName: id, bundle: nil), forCellWithReuseIdentifier: id)
        }
    }
}

extension ReportViewController: UICollectionViewDelegateFlowLayout {
    var reuseIDs: [String] {
        get {
            return ["ReportViewControllerTitleCell", "ReportViewControllerReasonCell"]
        }
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        if section == 1 {
            return data.count
        }
        return 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            let layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .vertical
            let controller = ReportSecViewController.init(collectionViewLayout: layout)
            controller.reportReason = data[indexPath.row]
//            controller.memoryID = memoryID
            controller.downloader = self.downloader
            navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIDs[0], for: indexPath) as! ReportViewControllerTitleCell
            cell.configCell(title: "Choose a reason for reporting:")
            return cell
        }
        if indexPath.section == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIDs[1], for: indexPath) as! ReportViewControllerReasonCell
            cell.configCell(reason: data[indexPath.row])
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 0 {
            return ReportViewControllerTitleCell.currentSize
        }
        if indexPath.section == 1 {
            return ReportViewControllerReasonCell.currentSize
        }
        return CGSize.zero
    }
}

















