//
//  FeedbackCollectionVCCell.swift
//  eventsRM
//
//  Created by Fure on 07.10.17.
//  Copyright © 2017 uzuner. All rights reserved.
//

import UIKit

class FeedbackCollectionVCCellFirst: UICollectionViewCell {
    
    @IBOutlet var bodyView: UIView!
    @IBOutlet var avatarImageView: UIImageView!
    @IBOutlet var eventImageView: UIImageView!
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 8.0
        self.layer.shadowOpacity = 0.3
        self.layer.masksToBounds = false
        let bounds = CGRect.init(x: self.bodyView.frame.origin.x + 2, y: self.bodyView.frame.origin.y + 2, width: self.bodyView.frame.width, height: self.bodyView.frame.height)
        self.layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: 15).cgPath
        
        self.bodyView.makeOval(cornerRadius: 15)
        self.avatarImageView.makeCircle()
        self.eventImageView.makeOval(cornerRadius: eventImageView.frame.height / 4)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
