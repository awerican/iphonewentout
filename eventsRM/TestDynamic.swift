//
//  TestDynamic.swift
//  WentOut
//
//  Created by Fure on 03.10.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import Foundation
import UIKit

class Dynamic<T> {
    typealias Listener = (T) -> Void
    
    var listener: Listener?
    
    func bind(listener: Listener?) {
        self.listener = listener
    }
    
    func bindAndFire(listener: Listener?) {
        self.listener = listener
        self.listener?(value)
    }
    
    var value: T {
        didSet {
            listener?(value)
        }
    }
    
    init(_ value: T) {
        self.value = value
    }
}

struct Article {
    let title: String
    let body: String
}

class ArticleViewViewModelFromArticle {
    let article: Article
    let title: Dynamic<String>
    let body: Dynamic<String>
    
    init(_ article: Article) {
        self.article = article
        
        self.title = Dynamic(article.title)
        self.body = Dynamic(article.body)
    }
}




































