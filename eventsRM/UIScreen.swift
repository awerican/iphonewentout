//
//  UIScreen.swift
//  WentOut
//
//  Created by Fure on 19.04.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import UIKit

extension UIScreen {
    static var width: CGFloat {
        return self.main.bounds.width
    }
    
    static var height: CGFloat {
        return self.main.bounds.height
    }
}
