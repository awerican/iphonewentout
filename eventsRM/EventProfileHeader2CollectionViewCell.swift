//
//  EventProfileHeader2CollectionViewCell.swift
//  WentOut
//
//  Created by Fure on 08.11.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

class EventProfileHeader2CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var basisView: UIView!
    
    @IBOutlet weak var guestsStackView: UIStackView!
    
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var guests: UILabel!
    @IBOutlet weak var views: UILabel!
    @IBOutlet weak var memoryCount: UILabel!
    //    @IBOutlet weak var eventType: UILabel!
    @IBOutlet weak var eventTitle: UILabel!
    
    @IBOutlet weak var hostedByLabel: UILabel!
    @IBOutlet weak var takingPlaceLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var friendsLabel: UILabel!
    
    @IBOutlet weak var memoryTitleLabel: UILabel!
    
    @IBOutlet weak var desc: UILabel!
    
    var location: String?
    
    var currentSize: CGSize {
        get {
            self.frame.size.width = UIScreen.main.bounds.width
            self.layoutIfNeeded()
            return CGSize.init(width: UIScreen.main.bounds.width, height: self.basisView.frame.maxY)
        }
    }
    
    weak var delegate: EventProfileHeaderDelegate?
    
    override func draw(_ rect: CGRect) {
        self.avatar.makeOval(cornerRadius: avatar.frame.height / 5)
    }
    
    func config(model: EventProfileHeader) {
        Webservice.loadImage(url: model.avatar) { (_image) in
            self.avatar.image = _image
        }
        
        guests.text = model.guests.description
        views.text = model.views.description
        memoryCount.text = model.memoryCount.description
        //        eventType.text = getEventType(model.eventType)
        eventTitle.text = model.title
        
        hostedByLabel.attributedText = hostedBy(username: model.creatorUsername)
        takingPlaceLabel?.attributedText = takingPlace(in: model.location?.location)
        timeLabel.attributedText = takingTime(at: model.timeStart)
        friendsLabel?.attributedText = getFriends(friends: model.friends)
        location = model.location?.location
        
        desc.text = model.description
        
        guestsStackView.isUserInteractionEnabled = true
        let guestsTapGuesture = UITapGestureRecognizer.init(target: self, action: #selector(guestsWasTappedAction))
        guestsStackView.addGestureRecognizer(guestsTapGuesture)
        
        hostedByLabel.isUserInteractionEnabled = true
        let hostTapGesture = UITapGestureRecognizer.init(target: self, action: #selector(hostWasTappedAction))
        hostedByLabel.addGestureRecognizer(hostTapGesture)
        
        takingPlaceLabel?.isUserInteractionEnabled = true
        let takingPlaceGesture = UITapGestureRecognizer.init(target: self, action: #selector(placeWasTappedAction))
        takingPlaceLabel?.addGestureRecognizer(takingPlaceGesture)
        
        friendsLabel?.isUserInteractionEnabled = true
        let friendsGesture = UITapGestureRecognizer.init(target: self, action: #selector(friendsWasTappedAction))
        friendsLabel?.addGestureRecognizer(friendsGesture)
        
        memoryTitleLabel.isUserInteractionEnabled = true
        let memoryTitleGesture = UITapGestureRecognizer.init(target: self, action: #selector(memoryWasTapped))
        memoryTitleLabel.addGestureRecognizer(memoryTitleGesture)
        
        memoryCount.isUserInteractionEnabled = true
        let memoryCountGesture = UITapGestureRecognizer.init(target: self, action: #selector(memoryWasTapped))
        memoryCount.addGestureRecognizer(memoryCountGesture)
        
        if model.statusGo == false && model.eventType == .priv {
            friendsLabel?.removeFromSuperview()
            takingPlaceLabel?.removeFromSuperview()
        }
    }
    
    private func getEventType(_ type: EventType) -> String {
        switch type {
        case .priv: return "🚷 Private event"
        case .pub: return "Public event"
        }
    }
    
    func config(desc: String) {
        self.desc.text = desc
    }
}

extension EventProfileHeader2CollectionViewCell {
    @objc private func memoryWasTapped() {
        delegate?.memoryWasTapped()
    }
    @objc private func hostWasTappedAction() {
        delegate?.hostWasTapped()
    }
    @objc private func placeWasTappedAction() {
        if let _ = self.location {
            delegate?.placeWasTapped()
        }
    }
    @objc private func friendsWasTappedAction() {
        delegate?.friendsWasTapped()
    }
    @objc private func guestsWasTappedAction() {
        delegate?.guestsWasTapped()
    }
}

extension EventProfileHeader2CollectionViewCell {
    private func getFriends(friends: [UserMini]) -> NSMutableAttributedString {
        
        let helveticaNeue = [
            NSAttributedStringKey.foregroundColor: UIColor.black,
            NSAttributedStringKey.font: UIFont.init(name: "Helvetica Neue", size: 14.0)!]
        
        let helveticaBoldBlue = [
            NSAttributedStringKey.foregroundColor: UIColor.init(hex: "0096FF"),
            NSAttributedStringKey.font: UIFont.init(name: "HelveticaNeue-Bold", size: 14.0)!]
        
        let helveticaBoldBlack = [
            NSAttributedStringKey.foregroundColor: UIColor.black,
            NSAttributedStringKey.font: UIFont.init(name: "HelveticaNeue-Bold", size: 14.0)!]
        
        let textCombination = NSMutableAttributedString()
        
        if friends.count < 1 {
            let _one = NSMutableAttributedString(string: "no friends here", attributes: helveticaNeue)
            
            textCombination.append(_one)
            
            return textCombination
            
        } else if friends.count == 1 {
            let _one = NSMutableAttributedString(string: "@" + friends[0].username, attributes: helveticaBoldBlue)
            
            let _two = NSMutableAttributedString(string: " is in here", attributes: helveticaNeue)
            
            textCombination.append(_one)
            textCombination.append(_two)
            
            return textCombination
            
        } else if friends.count == 2 {
            
            let _one = NSMutableAttributedString(string: "@" + friends[0].username, attributes: helveticaBoldBlue)
            
            let _two = NSMutableAttributedString(string: ", ", attributes: helveticaNeue)
            
            let _three = NSMutableAttributedString(string: "@" + friends[1].username, attributes: helveticaBoldBlue)
            
            let _four = NSMutableAttributedString(string: " are in here", attributes: helveticaNeue)
            
            textCombination.append(_one)
            textCombination.append(_two)
            textCombination.append(_three)
            textCombination.append(_four)
            
            return textCombination
        } else {
            let _one = NSMutableAttributedString(string: friends[0].username, attributes: helveticaBoldBlue)
            
            let _two = NSMutableAttributedString(string: ", ", attributes: helveticaNeue)
            
            let _three = NSMutableAttributedString(string: "@" + friends[1].username, attributes: helveticaBoldBlue)
            
            let _four = NSMutableAttributedString(string: " and others ", attributes: helveticaBoldBlack)
            
            let _five = NSMutableAttributedString(string: "are in here", attributes: helveticaNeue)
            
            textCombination.append(_one)
            textCombination.append(_two)
            textCombination.append(_three)
            textCombination.append(_four)
            textCombination.append(_five)
            
            return textCombination
        }
    }
    
    private func takingTime(at time: Int) -> NSMutableAttributedString {
        let helveticaNeue = [
            NSAttributedStringKey.foregroundColor: UIColor.black,
            NSAttributedStringKey.font: UIFont.init(name: "Helvetica Neue", size: 14.0)!]
        
        let _one = NSMutableAttributedString(string: "at ", attributes: helveticaNeue)
        
        let _time = Date.init(timeIntervalSince1970: Double(time)).eventPageTime
        
        let _two = NSMutableAttributedString(string: _time, attributes: helveticaNeue)
        
        let textCombination = NSMutableAttributedString()
        textCombination.append(_one)
        textCombination.append(_two)
        
        return textCombination
    }
    
    private func takingPlace(in place: String?) -> NSMutableAttributedString {
        let helveticaNeue = [
            NSAttributedStringKey.foregroundColor: UIColor.black,
            NSAttributedStringKey.font: UIFont.init(name: "Helvetica Neue", size: 14.0)!]
        let helveticaBold = [
            NSAttributedStringKey.foregroundColor: UIColor.init(hex: "0096FF"),
            NSAttributedStringKey.font: UIFont.init(name: "HelveticaNeue-Bold", size: 14.0)!]
        
        guard let place = place else {
            let _one = NSMutableAttributedString(string: "you can't see location", attributes: helveticaNeue)
            
            let textCombination = NSMutableAttributedString()
            textCombination.append(_one)
            
            return textCombination
        }
        
//        let _one = NSMutableAttributedString(string: "taking place in ", attributes: helveticaNeue)
        let _two = NSMutableAttributedString(string: place, attributes: helveticaBold)
        
        let textCombination = NSMutableAttributedString()
//        textCombination.append(_one)
        textCombination.append(_two)
        
        return textCombination
    }
    
    private func hostedBy(username: String) -> NSMutableAttributedString {
        let helveticaNeue = [
            NSAttributedStringKey.foregroundColor: UIColor.black,
            NSAttributedStringKey.font: UIFont.init(name: "Helvetica Neue", size: 14.0)!]
        
        let helveticaBold = [
            NSAttributedStringKey.foregroundColor: UIColor.init(hex: "0096FF"),
            NSAttributedStringKey.font: UIFont.init(name: "HelveticaNeue-Bold", size: 14.0)!]
        
        let _one = NSMutableAttributedString(string: "hosted by ", attributes: helveticaNeue)
        let _two = NSMutableAttributedString(string: "@" + username, attributes: helveticaBold)
        
        let textCombination = NSMutableAttributedString()
        textCombination.append(_one)
        textCombination.append(_two)
        
        return textCombination
    }
}

