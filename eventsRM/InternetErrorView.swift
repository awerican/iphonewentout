//
//  InternetErrorView.swift
//  WentOut
//
//  Created by Fure on 22.08.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import Foundation
import UIKit

class InternetErrorView: UIView, Nibable {
    var bundleName: String = "InternetErrorView"
    
    @IBOutlet var contentView: UIView!
    @IBOutlet var title: UILabel!
    
    static var height: CGFloat {
        get {
            return 35.0
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        setupViewFromNib()
        self.backgroundColor = .clear
        self.title.textColor = .pink
    }
}
