//
//  TableSection.swift
//  WentOut
//
//  Created by Fure on 10.09.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

//import Foundation
import UIKit

protocol RowConfigurable {
    func configure<ActionDelegate>(_ cell: UITableViewCell, delegate: ActionDelegate?)
}

protocol RowHashable {
    
    var hashValue: Int { get }
}

protocol Row: RowConfigurable, RowHashable {
    
    var reuseIdentifier: String { get }
    var cellType: AnyClass { get }

    func changeItem(newItem: Any)
    
    func getItem() -> Any
    
    func getInteractor() -> Any?
    
    func reloadCell(animation: UITableViewRowAnimation)
}

class TableSection {
    
    private(set) var rows = [Row]()
    
    var headerTitle: String?
    var footerTitle: String?
    var indexTitle: String?
    
    var headerView: UIView?
    var footerView: UIView?
    
    var headerHeight: CGFloat? = nil
    var footerHeight: CGFloat? = nil
    
    var numberOfRows: Int {
        return rows.count
    }
    
    var isEmpty: Bool {
        return rows.isEmpty
    }
    
    init(rows: [Row]? = nil) {
        
        if let initialRows = rows {
            self.rows.append(contentsOf: initialRows)
        }
    }
    
    convenience init(headerTitle: String?, footerTitle: String?, rows: [Row]? = nil) {
        self.init(rows: rows)
        
        self.headerTitle = headerTitle
        self.footerTitle = footerTitle
    }
    
    convenience init(headerView: UIView?, footerView: UIView?, rows: [Row]? = nil) {
        self.init(rows: rows)
        
        self.headerView = headerView
        self.footerView = footerView
    }
}

extension TableSection {

    func clear() {
        rows.removeAll()
    }
    
    func append(row: Row) {
        append(rows: [row])
    }
    
    func append(rows: [Row]) {
        self.rows.append(contentsOf: rows)
    }
    
    func insert(row: Row, at index: Int) {
        rows.insert(row, at: index)
    }
    
    func insert(rows: [Row], at index: Int) {
        self.rows.insert(contentsOf: rows, at: index)
    }
    
    func replace(rowAt index: Int, with row: Row) {
        rows[index] = row
    }
    
    func swap(from: Int, to: Int) {
        rows.swapAt(from, to)
    }
    
    func delete(rowAt index: Int) {
        rows.remove(at: index)
    }
    
    func remove(rowAt index: Int) {
        rows.remove(at: index)
    }
}
