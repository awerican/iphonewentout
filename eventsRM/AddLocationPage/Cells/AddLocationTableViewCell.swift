//
//  AddLocationTableViewCell.swift
//  CreateEventPagev2
//
//  Created by Fure on 04.05.2018.
//  Copyright © 2018 NotifyMe. All rights reserved.
//

import UIKit

class AddLocationTableViewCell: UITableViewCell {
    
    @IBOutlet private var addressTitle: UILabel!
    
    func configCell(address: String) {
        self.addressTitle.text = address
    }
}
