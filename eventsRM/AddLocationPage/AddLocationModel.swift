//
//  AddLocationModel.swift
//  CreateEventPagev2
//
//  Created by Fure on 03.05.2018.
//  Copyright © 2018 NotifyMe. All rights reserved.
//

import Foundation
import UIKit
import Mapbox
import Alamofire
import CoreLocation
import MapKit

func fetchCityAndCountry(from location: CLLocation, completion: @escaping (_ city: String?, _ country:  String?, _ error: Error?) -> ()) {
    CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
        completion(placemarks?.first?.locality,
                   placemarks?.first?.country,
                   error)
    }
}

protocol AddressSetting {
    func currentLocationWasSetted(_ address: String)
}

//class AddressApi: NSObject, CLLocationManagerDelegate {
//
//    private var locationManager: CLLocationManager
//
//    private var currentLocation: CLLocationCoordinate2D? {
//        didSet {
//            guard let location = self.currentLocation else {
//                return
//            }
//
//            getAddressString(location: location) { (address) in
//                self.delegate?.currentLocationWasSetted(address)
//            }
//        }
//    }
//
//    func getAddress() {
//        if CLLocationManager.locationServicesEnabled() {
//            locationManager.delegate = self
//            locationManager.desiredAccuracy = kCLLocationAccuracyBest
//            locationManager.requestAlwaysAuthorization()
//            locationManager.startUpdatingLocation()
//        } else {
//            self.locationManager.requestWhenInUseAuthorization()
//        }
//    }
//
//    var delegate: AddressSetting?
//
//    @discardableResult
//    init(delegate: AddressSetting) {
//        self.delegate = delegate
//        self.locationManager = CLLocationManager()
//    }
//
//    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        if let location = manager.location?.coordinate {
//            self.currentLocation = location
//            self.locationManager.stopUpdatingLocation()
//        }
//    }
//}

private let googleApiKey = "AIzaSyDEcemtfrglQmJTsZQgfMU6xzAsvaIKeAI"

func getCityCountryString(location: CLLocationCoordinate2D, completion: @escaping (_ city: String, _ country: String) -> () = { _,_  in }) {
    let url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + location.latitude.description + "," + location.longitude.description + "&key=" + googleApiKey + "&language=en"
    Alamofire.request(url).responseJSON { (response) in
        if let json = response.result.value as? [String: Any] {
            if let results = json["results"] as? [[String: Any]] {
                if results.count > 0 {
                    if let address_components = results[3]["address_components"] as? [Any] {
                        var city: String!
                        var country: String!
                        if let cityObject = address_components[2] as? [String: Any] {
                            if let _city = cityObject["long_name"] as? String {
                                city = _city
                            }
                        }
                        if let countryObject = address_components[4] as? [String: Any] {
                            if let _country = countryObject["long_name"] as? String {
                                country = _country
                            }
                        }
                        completion(city, country)
                    }
                }
            }
        }
    }
}

typealias CountryCity = (country: String, city: String)

func getCountryCityNominatim(location: CLLocationCoordinate2D, completion: @escaping (CountryCity) -> () = { _ in }) {
    let latitude = location.latitude.round(to: 9).description
    let longitude = location.longitude.round(to: 9).description
    let url = "https://nominatim.openstreetmap.org/reverse?lat=" + latitude + "&lon=" + longitude + "&format=json"
    print("nominatim url =", url)
    Alamofire.request(url).responseJSON { (response) in
        if let json = response.result.value as? [String: Any] {
            if let results = json["address"] as? [String: Any] {
                let country = results["country"] as? String ?? "unknown"
                let city = results["state"] as? String ?? "unknown"
//                let street = results["pedestrian"] as? String ?? "unknown"
//                let houseNumber = results["house_number"] as? String ?? "unknown"
                completion((country: country, city: city))
            }
        }
    }
}

func getAddressGoogle(location: CLLocationCoordinate2D, completion: @escaping (String) -> () = { _ in }) {
    let url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + location.latitude.round(to: 9).description + "," + location.longitude.round(to: 9).description + "&key=" + googleApiKey
    print("google url =", url)
    Alamofire.request(url).responseJSON { (response) in
        if let json = response.result.value as? [String: Any] {
            if let results = json["results"] as? [[String: Any]] {
                if results.count > 0 {
                    if let formatted_address = results[0]["formatted_address"] as? String {
                        completion(formatted_address)
                    }
                }
            }
        }
    }
}

struct GoogleAddress {
    var address: String
    var lat: Double
    var lng: Double
}

func getAddressGoogle(strings: [String], completion: @escaping ([GoogleAddress])->() = { _ in }) {
    var finalString = ""
    for str in strings {
        finalString.append(str.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!)
        finalString.append("+")
    }
    finalString = finalString.dropLast().description
    
    let strrr = "https://maps.googleapis.com/maps/api/geocode/json?address=" + finalString + "&key=" + googleApiKey
    
    if let url = URL(string: strrr) {
        print("final url =", url)
        Alamofire.request(url).responseJSON { (response) in
            if let json = response.result.value as? [String: Any] {
                if let results = json["results"] as? [[String: Any]] {
                    if results.count > 0 {
                        var addresses: [GoogleAddress] = []
                        for result in results {
                            guard let formatted_address = result["formatted_address"] as? String else {
                                return
                            }
                            guard let geometry = result["geometry"] as? [String: Any] else {
                                return
                            }
                            guard let location = geometry["location"] as? [String: Any] else {
                                return
                            }
                            guard let lat = location["lat"] as? Double, let lng = location["lng"] as? Double else {
                                return
                            }
                            let address = GoogleAddress.init(address: formatted_address, lat: lat, lng: lng)
                            addresses.append(address)
                        }
                        completion(addresses)
                    }
                }
            }
        }
    }
}

//func getAddressGoogle(strings: [String], completion: @escaping (String, Double, Double)->() = { _, _, _ in }) {
//    var finalString = ""
//    for str in strings {
//        finalString.append(str.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!)
//        finalString.append("+")
//    }
//    finalString = finalString.dropLast().description
//
//    let strrr = "https://maps.googleapis.com/maps/api/geocode/json?address=" + finalString + "&key=" + googleApiKey
//
//    if let url = URL(string: strrr) {
//        print("final url =", url)
//        Alamofire.request(url).responseJSON { (response) in
//            if let json = response.result.value as? [String: Any] {
//                if let results = json["results"] as? [[String: Any]] {
//                    if results.count > 0 {
//                        if let formatted_address = results[0]["formatted_address"] as? String {
//                            if let geometry = results[0]["geometry"] as? [String: Any] {
//                                if let location = geometry["location"] as? [String: Any] {
//                                    if let lat = location["lat"] as? Double, let lng = location["lng"] as? Double {
//                                        completion(formatted_address, lat, lng)
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
//    }
//}

class AddLocationModel {
    static func getEventLocation(location: CLLocationCoordinate2D, completion: @escaping (EventLocation)->()) {
        let latitude = location.latitude.round(to: 9)
        let longitude = location.longitude.round(to: 9)
        
        getAddressGoogle(location: location) { (_address) in
            getCountryCityNominatim(location: location, completion: { (_countryCity) in
                completion(EventLocation.init(address: _address, country: _countryCity.country, city: _countryCity.city, longitude: longitude, latitude: latitude))
            })
        }
    }
    
    static func getEventLocation(string: String, completion: @escaping (EventLocation?)->()) {
        let s = string
        var arr = [String]()
        s.enumerateSubstrings(in: s.startIndex..<s.endIndex, options: String.EnumerationOptions.byWords) { (str, range, range2, inoutBool) in
            if let str = str {
                arr.append(str)
            }
        }
        guard arr.count > 0 else {
            completion(nil)
            return
        }
        
        getAddressGoogle(strings: arr) { (addresses) in
            
            guard addresses.count > 0 else {
                completion(nil)
                return
            }
            
            getCountryCityNominatim(location: CLLocationCoordinate2D.init(latitude: addresses[0].lat, longitude: addresses[0].lng), completion: { (_countryCity) in
                
                let eventLocation = EventLocation.init(address: addresses[0].address, country: _countryCity.country, city: _countryCity.city, longitude: addresses[0].lng, latitude: addresses[0].lat)
                
                completion(eventLocation)
            })
        }
    }
}

























