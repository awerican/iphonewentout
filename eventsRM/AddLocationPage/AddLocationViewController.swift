//
//  AddLocationViewController.swift
//  CreateEventPagev2
//
//  Created by Fure on 03.05.2018.
//  Copyright © 2018 NotifyMe. All rights reserved.
//

import Foundation
import UIKit
import Mapbox
import CoreLocation

class AddLocationViewController: UIViewController {
    
    @IBOutlet private var textViewAddress: UITextView!
    @IBOutlet private var textViewHeightConstr: NSLayoutConstraint!
    @IBOutlet private var blurViewBottomConstr: NSLayoutConstraint!
    @IBOutlet private var bottomBlurView: UIVisualEffectView!
    @IBOutlet weak var clearTextImageView: UIImageView!
    @IBOutlet weak var clearTextButton: UIButton!
    
    private let mapMarkerIconTag: Int = 2454325
    private var model: AddLocationModel?
    private let textViewMaxHeight: CGFloat = 100.0
    private let tableViewTag: Int = 2314732
    private var mapViewSetCenterAnimationFinished: Bool = false
    
    weak var delegate: PublishEventDelegate?
    
    var locationManager: CLLocationManager!
    
    private lazy var mapView: MGLMapView = {
        let mapView = MGLMapView.init(frame: UIScreen.main.bounds)
        mapView.delegate = self
        return mapView
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView.init(frame: UIScreen.main.bounds)
        tableView.tag = tableViewTag
        return tableView
    }()
    
    private lazy var mapMarkerIcon: UIImageView = {
        let image = UIImageView.init(image: #imageLiteral(resourceName: "map-marker-icon"))
        image.tag = mapMarkerIconTag
        return image
    }()
    
    var eventLocation: EventLocation?
    
    var userLocation: CLLocationCoordinate2D? {
        didSet {
            if let location = userLocation {
                mapView.setCenter(location, zoomLevel: 16, animated: true)
            }
        }
    }
    
    var typedEventLocation: EventLocation? {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    func _getAddressByString(param: AppVersionParam) -> Downloader<SucceedFailedEntity, AppVersionParam> {
        return Downloader<SucceedFailedEntity, AppVersionParam>.init(resource: URL.App.checkVersion, param: param)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initModel()
        setupObserver()
        setupMapView(mapView)
        addMarkerIcon()
        setupTextView(textViewAddress)
        setupTableView()
        setupGestures()
        setupNavigationBar()
        retrieveUserLocation()
    }
    
    @objc func doneButtonAction() {
        if let rootVC = self.navigationController?.viewControllers.first as? CEViewController {
            rootVC.addLocation(location: eventLocation)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func clearTextAction(_ sender: UIButton) {
        print("clear text plz")
        textViewAddress.text = String()
        textViewBecomeEmpty()
//        textViewAddress.becomeFirstResponder()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

extension AddLocationViewController: CLLocationManagerDelegate {
    private func retrieveUserLocation() {
        if (CLLocationManager.locationServicesEnabled()) {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last! as CLLocation
        
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        
        userLocation = center
        
        print("user location =", userLocation ?? "error")
        
        locationManager.stopUpdatingLocation()
    }
}

extension AddLocationViewController: MGLMapViewDelegate {
    
    func mapViewDidFinishLoadingMap(_ mapView: MGLMapView) {
        let location = mapView.convert(mapView.center, toCoordinateFrom: self.mapView)
        setLocationToLabel(location)
    }
    
    func mapViewRegionIsChanging(_ mapView: MGLMapView) {
        self.mapViewSetCenterAnimationFinished = true
        if textViewAddress.isFirstResponder == true {
            textViewAddress.resignFirstResponder()
            addMarkerIcon()
        }
    }
    
    func mapView(_ mapView: MGLMapView, regionDidChangeAnimated animated: Bool) {
        guard self.mapViewSetCenterAnimationFinished == true else {
            return
        }
        let location = mapView.convert(mapView.center, toCoordinateFrom: self.mapView)
        setLocationToLabel(location)
    }
    
    private func setLocationToLabel(_ location: CLLocationCoordinate2D) {
        AddLocationModel.getEventLocation(location: location) { (eventLocation) in
            self.textViewAddress.text = eventLocation.location
            self.adjustTextViewHeight()
            self.eventLocation = eventLocation
        }
    }
}

extension AddLocationViewController: UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate {
    var tableViewReuseIds: [String] {
        get {
            return ["AddLocationTableViewCell"]
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = typedEventLocation {
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: tableViewReuseIds[0]) as! AddLocationTableViewCell
        cell.configCell(address: typedEventLocation!.location)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        self.dismiss(animated: true, completion: nil)
        
        if let rootVC = self.navigationController?.viewControllers.first as? CEViewController {
            rootVC.addLocation(location: typedEventLocation!)
            self.navigationController?.popViewController(animated: true)
        }
    }
}

extension AddLocationViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        adjustTextViewHeight()
        print("textView.text =", textView.text)
        AddLocationModel.getEventLocation(string: textView.text) { (eventLocation) in
            self.typedEventLocation = eventLocation
        }
        if textView.text.count == 0 {
            self.textViewBecomeEmpty()
        } else {
            self.textViewBecomeNotEmpty()
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        addTableView()
        clearTextImageView.alpha = 1
        clearTextButton.isEnabled = true
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}

extension AddLocationViewController {
    
    private func setupNavigationBar() {
        let done = UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(doneButtonAction))
//        let done = UIBarButtonItem.getSettingsBarButton(target: self, selector: #selector(doneButtonAction))
        
        navigationItem.rightBarButtonItems = [done]
        navigationItem.title = "Address"
    }
    
    private func setupMapView(_ mapView: MGLMapView) {
        mapView.isRotateEnabled = false
        mapView.isPitchEnabled = false
        self.view.insertSubview(mapView, at: 0)
//        guard let coordinates = self.delegate.eventAddress.coordinates else {
//            return
//        }
//        mapView.setCenter(coordinates, zoomLevel: 17, animated: false)
    }
    private func addMarkerIcon() {
        let width: CGFloat = 40.0
        let height: CGFloat = 40.0
        let x = UIScreen.main.bounds.width / 2 - (width / 2)
        let y = UIScreen.main.bounds.height / 2 - (height / 2)
        mapMarkerIcon.frame = CGRect.init(x: x, y: y, width: width, height: height)
        self.view.addSubview(mapMarkerIcon)
    }
    private func removeMarkerIcon() {
        self.mapMarkerIcon.removeFromSuperview()
    }
    private func setupObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardNotification(notification:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    private func setupTextView(_ textView: UITextView) {
        textView.delegate = self
//        textView.text = self.delegate.eventAddress.address
        adjustTextViewHeight()
    }
    private func initModel() {
//        self.model = AddLocationModel.init(tableView: self.tableView, delegate: self.delegate)
    }
    private func adjustTextViewHeight() {
        let contentSize = self.textViewAddress.sizeThatFits(self.textViewAddress.bounds.size)
        if contentSize.height < textViewMaxHeight {
            self.textViewHeightConstr.constant = contentSize.height
            self.textViewAddress.setContentOffset(CGPoint.zero, animated: false)
            self.view.layoutIfNeeded()
            self.bottomViewHeightWasChanged(to: self.bottomBlurView.frame.height)
        }
    }
    private func textViewBecomeEmpty() {
        removeTableView()
        clearTextImageView.alpha = 0
        clearTextButton.isEnabled = false
    }
    private func textViewBecomeNotEmpty() {
        addTableView()
        clearTextImageView.alpha = 1
        clearTextButton.isEnabled = true
    }
    private func setupGestures() {
        if let gestureRecognizers = self.tableView.gestureRecognizers {
            for gesture in gestureRecognizers {
                if let panGesture = gesture as? UIPanGestureRecognizer {
                    panGesture.addTarget(self, action: #selector(gestureRecognizer(_:)))
                }
            }
        }
    }
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 60.0
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.contentInset.top = UIApplication.shared.statusBarFrame.height
        tableView.contentInset.bottom = self.bottomBlurView.frame.height
        tableView.separatorStyle = .none
        tableView.backgroundView = UIView.blurView(bounds: self.tableView.frame, effect: .dark)
        tableView.backgroundColor = UIColor.clear
        tableView.allowsSelection = true
        tableView.showsVerticalScrollIndicator = false
        for reuseId in self.tableViewReuseIds {
            self.tableView.register(UINib.init(nibName: reuseId, bundle: nil), forCellReuseIdentifier: reuseId)
        }
    }
    private func removeTableView() {
        if let tb = self.view.viewWithTag(self.tableViewTag) {
            tb.removeFromSuperview()
        }
    }

    private func addTableView() {
        if self.view.viewWithTag(self.tableViewTag) == nil {
            self.view.insertSubview(self.tableView, at: 1)
        }
    }
    private func bottomViewHeightWasChanged(to newHeight: CGFloat) {
        self.tableView.contentInset.bottom = newHeight
    }
    @objc private func gestureRecognizer(_ gesture: UIGestureRecognizer) {
        if gesture is UIPanGestureRecognizer {
            if self.textViewAddress.isFirstResponder {
                self.textViewAddress.resignFirstResponder()
            }
        }
    }
    @objc private func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let endFrameY = endFrame?.origin.y ?? 0
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if endFrameY >= UIScreen.main.bounds.size.height {
                self.blurViewBottomConstr?.constant = 0.0
            } else {
                self.blurViewBottomConstr?.constant = endFrame?.size.height ?? 0.0
                self.removeMarkerIcon()
            }
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
    }
}
