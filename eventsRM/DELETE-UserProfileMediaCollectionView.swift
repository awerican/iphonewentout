//
//  UserProfileMediaCollectionView.swift
//  WentOut
//
//  Created by Fure on 28.07.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import UIKit

class UserProfileMediaCollectionView: UICollectionViewCell {

    @IBOutlet var basisView: UIView!
    @IBOutlet var mediaImage: UIImageView!
    
    static var currentSize: CGSize {
        get {
            return CGSize.init(width: UIScreen.main.bounds.width, height: 480.0) // 520.0
        }
    }
    
    func configCell(image: String) {
        self.mediaImage.image = nil
        self.mediaImage.moa.url = image
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 8.0
        self.layer.shadowOpacity = 0.3
        self.layer.masksToBounds = false
        let bounds = CGRect.init(x: self.basisView.frame.origin.x + 0, y: self.basisView.frame.origin.y + 2, width: self.basisView.frame.width, height: self.basisView.frame.height)
        self.layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: 15).cgPath
        
        basisView.makeOval(cornerRadius: 15)
    }
}
