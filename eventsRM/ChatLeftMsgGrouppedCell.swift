//
//  ChatLeftMsgGrouppedCell.swift
//  WentOut
//
//  Created by Fure on 09.10.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import Foundation
import UIKit

class ChatLeftMsgGrouppedCell: ChatMessageBaseCell {
    
    @IBOutlet var avatarOutlet: UIImageView!
    @IBOutlet var messageOutlet: UILabel!
    @IBOutlet var messageBackgroundOutlet: UIView!
    @IBOutlet var messageTimeOutlet: UILabel!
    @IBOutlet var timeLeadingConstrOutlet: NSLayoutConstraint!
    @IBOutlet var timeTopConstrOutlet: NSLayoutConstraint!
    
    override var avatar: UIImageView! {
        return avatarOutlet
    }
    override var message: UILabel! {
        return messageOutlet
    }
    override var messageBackground: UIView! {
        return messageBackgroundOutlet
    }
    override var time: UILabel! {
        return messageTimeOutlet
    }
    override var timeLeadingConstr: NSLayoutConstraint? {
        return timeLeadingConstrOutlet
    }
    override var timeTopConstr: NSLayoutConstraint? {
        return timeTopConstrOutlet
    }
}
