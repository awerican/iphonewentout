//
//  ActivityPageViewController.swift
//  WentOut
//
//  Created by Fure on 22.05.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import UIKit

class ActivityViewController: UICollectionViewController {

    let numberOfSections: Int = 3
    
//    let sizeOfMemoryList: CGSize = CGSize.init(width: UIScreen.width, height: 127.0 + 24)
    let sizeOfMemoryList: CGSize = CGSize.init(width: UIScreen.width, height: UIScreen.width / 2 + 24)
    
    let newsMemory = Downloader<[StoryEntity], UserIDParameter>.init(resource: URL.News.memory, param: UserIDParameter.init(userID: LocalAuth.userID!))
    
    let newsActivity = Downloader<[FeedbackEntity], SelectNews>.init(resource: URL.News.feedback, param: SelectNews.init(userID: LocalAuth.userID!))
    
    lazy var feedbackCat4Bundle: FeedbackCellCat4 = {
        return Bundle.main.loadNibNamed(reuseIds[1], owner: self, options: nil)?[0] as! FeedbackCellCat4
    }()
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.view.frame.size.height = UIScreen.main.bounds.height
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNetworking()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
}

extension ActivityViewController: DefaultStateVCDelegate {
    func scrollToTop() {
        if collectionView?.numberOfItems(inSection: 0) > 0 {
            collectionView?.scrollToItem(at: IndexPath.init(row: 0, section: 0), at: .top, animated: true)
        }
        
    }
}

extension ActivityViewController: ActivityPageMemoryListDelegate {
    func addMemory() {
        let storyboard = UIStoryboard(name: "Second", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ChooseEventViewController") as! ChooseEventViewController
        controller.delegate = self
        self.present(controller, animated: true, completion: nil)
    }
    func openStory(eventID: String, index: Int) {
        let storyboard = UIStoryboard.init(name: "Second", bundle: nil)
        let navController = storyboard.instantiateViewController(withIdentifier: "StoryPageNavVC") as! UINavigationController
        let storyController = navController.viewControllers.first! as! StoryPageViewController
        storyController.storyDownloader = newsMemory.clone()
        storyController.beginIndex = index
        navController.modalPresentationStyle = .overCurrentContext
        self.navigationController!.tabBarController?.present(navController, animated: true, completion: nil)
    }
}

extension ActivityViewController: ChooseEventDelegate {
    func eventWasSelected(event: EventPanel) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "NavAddMemoryViewController") as! UINavigationController
        if let vc = controller.viewControllers.first as? AddMemoryViewController {
            vc.event = event
            self.present(controller, animated: true, completion: nil)
        }
    }
}

extension ActivityViewController: FeedbackCellCatDelegate {
    func leftImageWasTapped(at indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
        controller.profileOwnerID = self.newsActivity.data?[indexPath.row].creatorID
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func rightImageWasTapped(at indexPath: IndexPath, category: FeedbackCategory) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "EventProfileViewController") as! EventProfileViewController
        controller.eventID = self.newsActivity.data?[indexPath.row].nodeID
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

extension ActivityViewController {
    
    var reuseIds: [String] {
        get {
            return ["ActivityPageMemoryListCollectionViewCell", "FeedbackCellCat4", "ActivityPageSeparateCell", "CollectionViewErrorImageCell", "FeedbackCellCat5", "FeedbackCellCat3", "FeedbackCellCat6", "FeedbackCellCat10", "FeedbackCellCat11", "FeedbackCellCatServer", "FeedbackCellCatMoney", "FeedbackCellCat1"]
        }
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.numberOfSections
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else if section == 1 {
            return 1
        } else if section == 2 {
//            guard let _ = self.newsMemory.data else {
//                return 0
//            }
            if newsActivity.firstDownloadIsFinished {
                if let count = newsActivity.data?.count {
                    if count == 0 {
                        return 1
                    } else {
                        return count
                    }
                } else {
                    return 1
                }
            } else {
                return newsActivity.data?.count ?? 0
            }
        } else {
            return 0
        }
    }

    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.section == 2 {
            guard let feedback = self.newsActivity.data else {
                return
            }
            if indexPath.row == feedback.count - 1 {
                self.newsActivity.getNext(section: 2) { (paths) in
                    self.collectionView?.insertItems(at: paths)
                }
            }
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let model = self.newsActivity.data![indexPath.row]
        if model.type == .cat3 ||
            model.type == .cat4 ||
            model.type == .cat5 ||
            model.type == .cat6 ||
            model.type == .cat10 ||
            model.type == .cat11 ||
            model.type == .catMoney {
                rightImageWasTapped(at: indexPath, category: model.type)
        } else if model.type == .cat1 {
            leftImageWasTapped(at: indexPath)
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIds[0], for: indexPath) as! ActivityPageMemoryListCollectionViewCell
            cell.delegate = self.newsMemory
            cell.addMemoryDelegate = self
            cell.configCell(superviewHeight: sizeOfMemoryList.height)
            return cell
        } else if indexPath.section == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIds[2], for: indexPath) as! ActivityPageSeparateCell
            return cell
        } else if indexPath.section == 2 {
            if let data = self.newsActivity.data {
                if data.count == 0 {
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIds[3], for: indexPath) as! CollectionViewErrorImageCell
                    cell.configCell(image: #imageLiteral(resourceName: "errorNoActivities"))
                    return cell
                } else {
                    let model = self.newsActivity.data![indexPath.row]
                    if true == false {
                        return UICollectionViewCell()
                    }
                    else if model.type == .cat1 {
                        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIds[11], for: indexPath) as! FeedbackCellCat1
                        cell.configCell(model: model)
                        cell.delegate = self
                        return cell
                    }
                    else if model.type == .cat4 {
                        // WENT TO YOUR EVENT
                        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIds[1], for: indexPath) as! FeedbackCellCat4
                        cell.configCell(model: model)
                        cell.delegate = self
                        return cell
                    }
//                    else if model.type == .cat2 {
//                        // YOUR MEDIA WAS COMMENTTED
//                        return UICollectionViewCell()
//                    }
                    else if model.type == .cat3 {
                        // NEW MESSAGE IN CHAT
                        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIds[5], for: indexPath) as! FeedbackCellCat3
                        cell.configCell(model: model)
                        cell.delegate = self
                        return cell
                    }
                    else if model.type == .cat5 {
                        // USER CREATED AN EVENT
                        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIds[4], for: indexPath) as! FeedbackCellCat5
                        cell.configCell(model: model)
                        cell.delegate = self
                        return cell
                    }
                    else if model.type == .cat6 {
                        // ???
                        // USER INVITED U TO EVENT
                        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIds[6], for: indexPath) as! FeedbackCellCat6
                        cell.configCell(model: model)
                        cell.delegate = self
                        return cell
                    }
//                    else if model.type == .cat7 {
//                        // USER CHANGED an Event Title
//                        return UICollectionViewCell()
//                    }
//                    else if model.type == .cat8 {
//                        // CREATOR CHANGED time of the event to Time
//                        return UICollectionViewCell()
//                    }
//                    else if model.type == .cat9 {
//                        // CREATOR CHANGED place of the event to Address
//                        return UICollectionViewCell()
//                    }
                    else if model.type == .cat10 {
                        // USER WILL GO TO AN EVENT
                        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIds[7], for: indexPath) as! FeedbackCellCat10
                        cell.configCell(model: model)
                        cell.delegate = self
                        return cell
                    }
                    else if model.type == .cat11 {
                        // EVENT WILL START AT
                        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIds[8], for: indexPath) as! FeedbackCellCat11
                        cell.configCell(model: model)
                        cell.delegate = self
                        return cell
                    }
//                    else if model.type == .cat12 {
//                        // CREATOR MADE YOU AN AUTHOR
//                        return UICollectionViewCell()
//                    }
//                    else if model.type == .cat13 {
//                        // USER WANTS TO GO TO YOUR PRIV EVENT
//                        return UICollectionViewCell()
//                    }
//                    else if model.type == .cat14 {
//                        // YOU WAS ACCEPTED TO GO TO A PRIVATE EVENT
//                        return UICollectionViewCell()
//                    }
                    else if model.type == .catServer {
                        // ONE PHOTO CELL
                        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIds[9], for: indexPath) as! FeedbackCellCatServer
                        cell.configCell(model: model)
                        cell.delegate = self
                        return cell
                    }
                    else if model.type == .catMoney {
                        // TWO PHOTO CELL
                        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIds[10], for: indexPath) as! FeedbackCellCatMoney
                        cell.configCell(model: model)
                        cell.delegate = self
                        return cell
                    }
                    else {
                        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIds[1], for: indexPath) as! FeedbackCellCat4
                        cell.configCell(model: model)
                        cell.delegate = self
                        return cell
                    }
                }
            } else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIds[3], for: indexPath) as! CollectionViewErrorImageCell
                cell.configCell(image: #imageLiteral(resourceName: "errorNoActivities"))
                return cell
            }
        } else {
            return UICollectionViewCell()
        }
    }
}

extension ActivityViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 0 {
            return self.sizeOfMemoryList
        } else if indexPath.section == 1 {
            return CGSize.init(width: UIScreen.width, height: 1.5)
        } else if indexPath.section == 2 {

            guard let data = self.newsActivity.data else {
                if newsActivity.firstDownloadIsFinished {
                    return CollectionViewErrorImageCell.currentSize
                }
                return CGSize.zero
            }
            if data.count == 0 {
                return CollectionViewErrorImageCell.currentSize
            } else {
                let model = data[indexPath.row]
//                if model.type == FeedbackCategory.cat4 {
                    let bundle = feedbackCat4Bundle
                    bundle.configCell(model: self.newsActivity.data![indexPath.row])
                    return bundle.currentSize
//                } else {
//                    return CGSize.zero
//                }
            }
        } else {
            return CGSize.zero
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if section == 0 {
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        } else if section == 1 {
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        } else {
            return UIEdgeInsets.zero
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if section == 1 {
            return -1.0
        } else {
            return 0.0
        }
    }
}

extension ActivityViewController {
    
    private func setupNetworking() {
        downloadMemory()
        downloadActivities()
    }
    
    private func downloadMemory() {
        self.newsMemory.getFirst {
            self.collectionView?.reloadSections([0])
        }
    }
    
    private func downloadActivities() {
        self.newsActivity.getFirst {
            self.collectionView?.reloadSections([2])
        }
    }
    
    private func setupViews() {
        setupNavBar()
        setupCollectionView()
    }
    
    fileprivate func setupNavBar() {
        self.navigationItem.title = "Activity"
    }
    
    fileprivate func setupCollectionView() {
        self.collectionView!.contentInset.bottom = UITabBar.height + 6.0
        self.collectionView!.backgroundColor = UIColor.white
        for reuseID in self.reuseIds {
            self.collectionView!.register(UINib.init(nibName: reuseID, bundle: nil), forCellWithReuseIdentifier: reuseID)
        }
    }
}


















