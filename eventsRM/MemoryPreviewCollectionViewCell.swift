//
//  MemoryPreviewCollectionViewCell.swift
//  WentOut
//
//  Created by Fure on 18.04.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

class MemoryPreviewCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var usernameRank: UILabel!
    
    override func draw(_ rect: CGRect) {
        self.imageView.makeOval(cornerRadius: imageView.frame.width / 6)
    }

    func configCell(memory: StoryMemory, hostID: String) {
        self.username.text = memory.username
        if memory.userID == hostID {
            self.usernameRank.text = "host"
        } else {
            self.usernameRank.text = " "
        }
        Webservice.loadImage(url: memory.mediaPrev) { (_image) in
            self.imageView.image = _image
        }
    }
    
//    func configCell(memory: MemoryEntity) {
//        self.username.text = memory.username
//        if memory.userID == "1" {
//            self.usernameRank.text = "host"
//        } else {
//            self.usernameRank.text = " "
//        }
//        Webservice.loadImage(url: memory.mediaPrev) { (_image) in
//            self.imageView.image = _image
//        }
//    }
}
