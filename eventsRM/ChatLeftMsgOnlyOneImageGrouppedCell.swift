//
//  ChatLeftMsgOnlyOneImageGrouppedCell.swift
//  WentOut
//
//  Created by Fure on 10.10.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import UIKit

class ChatLeftMsgOnlyOneImageGrouppedCell: ChatMessageBaseCell {
    @IBOutlet var avatarOutlet: UIImageView!
    @IBOutlet var mediaCollectionOutlet: [UIImageView]!
    @IBOutlet var timeOutlet: UILabel!
    @IBOutlet var messageBackgroundOutlet: UIView!
    @IBOutlet var timeBackgroundOutlet: UIView!

    override var avatar: UIImageView! {
        return self.avatarOutlet
    }
    override var mediaCollection: [UIImageView]? {
        return self.mediaCollectionOutlet
    }
    override var time: UILabel! {
        return self.timeOutlet
    }
    override var messageBackground: UIView! {
        return self.messageBackgroundOutlet
    }
    override var timeBackground: UIView? {
        return self.timeBackgroundOutlet
    }
}
