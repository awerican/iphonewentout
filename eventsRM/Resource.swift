//
//  Resource.swift
//  WentOut
//
//  Created by Fure on 15.03.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation

struct Resource<A: Decodable, T: Encodable> {
    let url: URL
    let params: T?
    let parse: (Data) -> A?
}

extension Resource {
    init(url: URL, params: T? = nil, parseJSON: @escaping (Any) -> A?) {
        self.url = url
        self.params = params
        self.parse = { data in
            return try? JSONDecoder().decode(A.self, from: data)
        }
    }
}

func getNoParam<Res: Decodable>(url: URL) -> (()->Resource<ResponseEntity<Res>, EmptyParameters>) {
    func f()->Resource<ResponseEntity<Res>, EmptyParameters> {
        return Resource<ResponseEntity<Res>, EmptyParameters>.init(url: url, parseJSON: { (json) -> ResponseEntity<Res>? in
            guard let result = try? JSONDecoder().decode(ResponseEntity<Res>.self, from: json as! Data) else {
                return nil
            }
            return result
        })
    }
    return f
}

func getWithParam<Res: Decodable, Req: Encodable>(url: URL) -> ((Req)->Resource<ResponseEntity<Res>, Req>) {
    func f(params: Req)->Resource<ResponseEntity<Res>, Req> {
        return Resource<ResponseEntity<Res>, Req>.init(url: url, params: params, parseJSON: { (json) -> ResponseEntity<Res>? in
            guard let result = try? JSONDecoder().decode(ResponseEntity<Res>.self, from: json as! Data) else {
                return nil
            }
            return result
        })
    }
    return f
}
