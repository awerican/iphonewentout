//
//  ProfileSettingsChangePassVC.swift
//  WentOut
//
//  Created by Fure on 26.09.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

class ProfileSettingsChangePassVC: UICollectionViewController {
    
    func _changePass(oldPass: String, newPass: String) -> Downloader<SucceedFailedEntity, UserSettingsChangePsw> {
        return Downloader<SucceedFailedEntity, UserSettingsChangePsw>.init(resource: URL.UserProfile.Setting.changePassword, param: UserSettingsChangePsw.init(userID: LocalAuth.userID!, oldPassword: oldPass, newPassword: newPass))
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupCV()
        setupNavBar()
    }
}

extension ProfileSettingsChangePassVC {
    private func setupNavBar() {
        navigationItem.title = "Change a Password"
    }
    
    private func setupView() {
        view.backgroundColor = UIColor.init(hex: "FAFAFA")
    }

    private func setupCV() {
        collectionView?.backgroundColor = .clear
        for id in reuseIDs {
            collectionView?.register(UINib.init(nibName: id, bundle: nil), forCellWithReuseIdentifier: id)
        }
    }
}

extension ProfileSettingsChangePassVC: ProfileSettingsChangePassDelegate {
    func shouldAddDoneButton(shouldAdd: Bool) {
        if shouldAdd {
            let done = UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(doneButtonAction))
            navigationItem.rightBarButtonItems = [done]
        } else {
            navigationItem.rightBarButtonItems = []
        }
    }
    
    @objc private func doneButtonAction() {
        guard let cell = collectionView?.cellForItem(at: IndexPath.init(row: 0, section: 0)) as? ProfileSettingsChangePassCell else {
            return
        }
        
        guard let oldPass = cell.oldPassword.text, let newPass = cell.newPassword.text, let repPass = cell.repPassword.text else {
            return
        }
        
        guard newPass == repPass else {
            print("passwords are not the same")
            return
        }
        
        guard newPass.count >= 6 else {
            print("too short")
            return
        }
        
        guard newPass.isAlphanumeric else {
            print("invalid symbols")
            return
        }
        
        guard newPass.count < 32 else {
            print("too long")
            return
        }
        
        let resource = _changePass(oldPass: oldPass, newPass: newPass)
        resource.getFirst {
            if let status = resource.data?.status {
                switch status {
                case .failed:
                    print("hello")
                case .succeed:
                    self.navigationController?.popToRootViewController(animated: true)
                }
            } else {
                print("error")
            }
        }

        // upload this new pass
        print("oldPass =", oldPass)
        print("newPass =", newPass)
        print("repPass =", repPass)
    }
}

extension ProfileSettingsChangePassVC: UICollectionViewDelegateFlowLayout {
    
    var reuseIDs: [String] {
        get {
            return ["ProfileSettingsChangePassCell"]
        }
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIDs[0], for: indexPath) as! ProfileSettingsChangePassCell
        cell.configCell()
        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return ProfileSettingsChangePassCell.currentSize
    }
}


















