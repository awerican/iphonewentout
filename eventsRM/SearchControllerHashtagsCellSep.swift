//
//  SearchControllerHashtagsCellSep.swift
//  eventsRM
//
//  Created by Fure on 01.11.17.
//  Copyright © 2017 uzuner. All rights reserved.
//

import Foundation
import UIKit

class SearchControllerHashtagsCellSep: UICollectionViewCell {
    
    @IBOutlet var labelBackView: UIView!
    @IBOutlet var labelHashtag: UILabel!
    
    var currentCellSize: CGSize {
        get {
            return CGSize.init(width: labelBackView.frame.width + 5, height: labelBackView.frame.height + 5)
        }
    }
    
    func configCell(text: String) {
        self.labelHashtag.text = text
        self.layoutIfNeeded()
    }
}
