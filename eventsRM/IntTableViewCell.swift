//
//  IntTableViewCell.swift
//  WentOut
//
//  Created by Fure on 11.09.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import Foundation
import UIKit

struct IntTableViewModel {
    var leftNumber: Int
    var rightNumber: Int
}

@objc protocol IntTableViewCellDelegate: class {
    @objc optional func leftButtonAction(at indexPath: IndexPath)
    @objc optional func rightButtonAction(at indexPath: IndexPath)
}

@objc protocol IntTableViewCellInteractor {
    @objc func beginNewLife()
}

class IntTableViewCell: UITableViewCell, ConfigurableCell {
    typealias CellInteractor = IntTableViewCellInteractor
    typealias CellData = IntTableViewModel
    typealias CellDelegate = IntTableViewCellDelegate
    
    @IBOutlet var leftLabel: UILabel!
    @IBOutlet var rightLabel: UILabel!
    
    private(set) weak var delegate: CellDelegate?
    
    func configure(with model: IntTableViewModel, delegate: IntTableViewCellDelegate? = nil) {
        self.leftLabel.text = model.leftNumber.description
        self.rightLabel.text = model.rightNumber.description
        self.delegate = delegate
    }
    
    @IBAction func leftButtonAction(_ sender: UIButton) {
        if let superView = self.superview as? UITableView {
            if let indexPath = superView.indexPath(for: self) {
                self.delegate?.leftButtonAction?(at: indexPath)
            }
        }
    }
    
    @IBAction func rightButtonAction(_ sender: UIButton) {
        if let superView = self.superview as? UITableView {
            if let indexPath = superView.indexPath(for: self) {
                self.delegate?.rightButtonAction?(at: indexPath)
            }
        }
    }
}

extension IntTableViewCell: IntTableViewCellInteractor {
    func beginNewLife() {
        if let superView = self.superview as? UITableView {
            if let indexPath = superView.indexPath(for: self) {
                print("indexPath =", indexPath)
            }
        }
        self.leftLabel.backgroundColor = .white
        self.leftLabel.text = 50.description
        self.rightLabel.backgroundColor = .white
    }
}




























