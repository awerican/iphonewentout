//
//  NotificationCategory.swift
//  WentOut
//
//  Created by Fure on 18.09.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import UserNotifications

typealias catId = NotificationCategoryId
typealias action = NotificationAction

public enum NotificationCategoryId: String {
    case cat1 = "CAT1"
    case cat2 = "CAT2"
    case cat3 = "CAT3"
    case cat4 = "CAT4"
    case cat5 = "CAT5"
    case cat6 = "CAT6"
    case cat7 = "CAT7"
    case cat8 = "CAT8"
    case cat9 = "CAT9"
    case cat10 = "CAT10"
    case cat11 = "CAT11"
    case cat12 = "CAT12"
    case cat13 = "CAT13"
    case cat14 = "CAT14"
    case cat0g = "CAT0G"
    case cat1g = "CAT1G"
}

public enum NotificationActionId: String {
    case openUserPage = "openUserPage"
    case followBack = "followBack"
    case openMediaPage = "openMediaPage"
    case openChatPage = "openChatPage"
    case openEventPage = "openEventPage"
    case showOnMap = "showOnMap"
    case go = "go"
    case accept = "accept"
}

public struct NotificationAction {
    public static private(set) var openUserPage: UNNotificationAction = {
        return UNNotificationAction(identifier: NotificationActionId.openUserPage.rawValue, title: "Open user page", options: [.foreground])
    }()
    public static private(set) var openMediaPage: UNNotificationAction = {
        return UNNotificationAction(identifier: NotificationActionId.openMediaPage.rawValue, title: "Open media page", options: [.foreground])
    }()
    public static private(set) var openChatPage: UNNotificationAction = {
        return UNNotificationAction(identifier: NotificationActionId.openChatPage.rawValue, title: "Open chat page", options: [.foreground])
    }()
    public static private(set) var openEventPage: UNNotificationAction = {
        return UNNotificationAction(identifier: NotificationActionId.openEventPage.rawValue, title: "Open event page", options: [.foreground])
    }()
    public static private(set) var showOnMap: UNNotificationAction = {
        return UNNotificationAction(identifier: NotificationActionId.showOnMap.rawValue, title: "Show on map", options: [.foreground])
    }()
    public static private(set) var followBack: UNNotificationAction = {
        return UNNotificationAction(identifier: NotificationActionId.followBack.rawValue, title: "Follow back", options: [])
    }()
    public static private(set) var go: UNNotificationAction = {
        return UNNotificationAction(identifier: NotificationActionId.go.rawValue, title: "Go", options: [])
    }()
    public static private(set) var accept: UNNotificationAction = {
        return UNNotificationAction(identifier: NotificationActionId.accept.rawValue, title: "Accept", options: [])
    }()
    private init() {}
}

public struct NotificationCategory {
    public static private(set) var cat1: UNNotificationCategory = {
        return UNNotificationCategory(identifier: catId.cat1.rawValue, actions: [action.openUserPage, action.followBack], intentIdentifiers: [], options: [])
    }()
    public static private(set) var cat2: UNNotificationCategory = {
        return UNNotificationCategory(identifier: catId.cat2.rawValue, actions: [action.openMediaPage], intentIdentifiers: [], options: [])
    }()
    public static private(set) var cat3: UNNotificationCategory = {
        return UNNotificationCategory(identifier: catId.cat3.rawValue, actions: [action.openChatPage], intentIdentifiers: [], options: [])
    }()
    public static private(set) var cat4: UNNotificationCategory = {
        return UNNotificationCategory(identifier: catId.cat4.rawValue, actions: [action.openUserPage], intentIdentifiers: [], options: [])
    }()
    public static private(set) var cat5: UNNotificationCategory = {
        return UNNotificationCategory(identifier: catId.cat5.rawValue, actions: [action.openEventPage, action.go], intentIdentifiers: [], options: [])
    }()
    public static private(set) var cat6: UNNotificationCategory = {
        return UNNotificationCategory(identifier: catId.cat6.rawValue, actions: [action.openEventPage, action.go], intentIdentifiers: [], options: [])
    }()
    public static private(set) var cat7: UNNotificationCategory = {
        return UNNotificationCategory(identifier: catId.cat7.rawValue, actions: [action.openEventPage], intentIdentifiers: [], options: [])
    }()
    public static private(set) var cat8: UNNotificationCategory = {
        return UNNotificationCategory(identifier: catId.cat8.rawValue, actions: [action.openEventPage], intentIdentifiers: [], options: [])
    }()
    public static private(set) var cat9: UNNotificationCategory = {
        return UNNotificationCategory(identifier: catId.cat9.rawValue, actions: [action.openEventPage, action.showOnMap], intentIdentifiers: [], options: [])
    }()
    public static private(set) var cat10: UNNotificationCategory = {
        return UNNotificationCategory(identifier: catId.cat10.rawValue, actions: [action.openEventPage, action.go], intentIdentifiers: [], options: [])
    }()
    public static private(set) var cat11: UNNotificationCategory = {
        return UNNotificationCategory(identifier: catId.cat11.rawValue, actions: [action.openEventPage], intentIdentifiers: [], options: [])
    }()
    public static private(set) var cat12: UNNotificationCategory = {
        return UNNotificationCategory(identifier: catId.cat12.rawValue, actions: [action.openEventPage], intentIdentifiers: [], options: [])
    }()
    public static private(set) var cat13: UNNotificationCategory = {
        return UNNotificationCategory(identifier: catId.cat13.rawValue, actions: [action.openUserPage, action.accept], intentIdentifiers: [], options: [])
    }()
    public static private(set) var cat14: UNNotificationCategory = {
        return UNNotificationCategory(identifier: catId.cat14.rawValue, actions: [action.openEventPage], intentIdentifiers: [], options: [])
    }()
    public static private(set) var cat0g: UNNotificationCategory = {
        return UNNotificationCategory(identifier: catId.cat0g.rawValue, actions: [], intentIdentifiers: [], options: [])
    }()
    public static private(set) var cat1g: UNNotificationCategory = {
        return UNNotificationCategory(identifier: catId.cat1g.rawValue, actions: [action.openEventPage], intentIdentifiers: [], options: [])
    }()
}
