//
//  TableRow.swift
//  WentOut
//
//  Created by Fure on 10.09.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import Foundation
import UIKit

class TableRow<CellType: ConfigurableCell>: Row where CellType: UITableViewCell {

    var item: CellType.CellData
    
    var interactor: CellType.CellInteractor? {
        get {
            return self.cell as? CellType.CellInteractor
        }
    }
    
    private(set) var cell: UITableViewCell?

    private(set) var editingActions: [UITableViewRowAction]?
    
    open var hashValue: Int {
        return ObjectIdentifier(self).hashValue
    }
    
    open var reuseIdentifier: String {
        return CellType.reuseIdentifier
    }
    
    var cellType: AnyClass {
        return CellType.self
    }
    
    func getItem() -> Any {
        return item as Any
    }
    
    func getInteractor() -> Any? {
        return self.interactor
    }
    
    init(item: CellType.CellData) {
        self.item = item
    }
    
    func changeItem(newItem: Any) {
        self.item = newItem as! CellType.CellData
        reloadCell(animation: .none)
    }
    
    func reloadCell(animation: UITableViewRowAnimation) {
        if let cell = self.cell {
            if let tableView = cell.superview as? UITableView {
                if let indexPath = tableView.indexPath(for: cell) {
                    tableView.reloadRows(at: [indexPath], with: animation)
                }
            }
        }
    }
    
    func configure<ActionDelegate>(_ cell: UITableViewCell, delegate: ActionDelegate?) {
        self.cell = cell
        (cell as? CellType)?.configure(with: item, delegate: delegate as? CellType.CellDelegate)
    }
}
