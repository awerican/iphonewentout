//
//  SearchControllerUserCell.swift
//  WentOut
//
//  Created by Fure on 08.07.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

class SearchControllerUserCell: UICollectionViewCell {

    @IBOutlet weak var userAvatar: UIImageView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var buttonBackground: UIView!
    @IBOutlet weak var bottomLine: UIView!
    
    static var currentSize: CGSize {
        get {
//            return CGSize.init(width: UIScreen.width, height: 73.0)
            return CGSize.init(width: UIScreen.width, height: 65.0)
        }
    }

    func configCell(model: UserMini, underlined: Bool) {
        Webservice.loadImage(url: model.avatar) { (_image) in
            self.userAvatar.image = _image
        }
        username.text = model.username
        if !underlined {
            self.bottomLine?.removeFromSuperview()
        }
        userAvatar.makeCircle()
        buttonBackground.makeOval(cornerRadius: buttonBackground.frame.height / 4)
    }
}
