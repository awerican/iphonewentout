//
//  TimeInterval.swift
//  WentOut
//
//  Created by Fure on 29.07.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation

extension TimeInterval {
    
    static var oneYearPeriod: TimeInterval {
        get {
            return Double(31536000)
        }
    }
}
