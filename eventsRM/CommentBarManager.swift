//
//  CommentTypeBarVC.swift
//  WentOut
//
//  Created by Fure on 22.01.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import UIKit
import MobileCoreServices
import AVFoundation

protocol CommentBarManagerDelegate {
    func show(imagePickerVC: UIImagePickerController)
    func barHeightWillSet(to height: CGFloat)
    func barHeightDidChange(heightDifference: CGFloat, _triggerMethod: String)
    func sendButtonAction(text: String, medias: [CommentMedia]) -> Bool
}

func generateThumbnail(path: URL) -> UIImage? {
    do {
        let asset = AVURLAsset(url: path, options: nil)
        let imgGenerator = AVAssetImageGenerator(asset: asset)
        imgGenerator.appliesPreferredTrackTransform = true
        let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(0, 1), actualTime: nil)
        let thumbnail = UIImage(cgImage: cgImage)
        return thumbnail
    } catch let error {
        print("*** Error generating thumbnail: \(error.localizedDescription)")
        return nil
    }
}

class CommentBarManager: NSObject {
    
    private var topBorderView: UIView = UIView()
    private var background: UIView = UIView()
    private var attachButton: UIButton = UIButton()
    private var textView: UITextView = UITextView()
    private var sendButton: UIButton = UIButton()
    
    private static let font: UIFont = UIFont.systemFont(ofSize: 17.0, weight: UIFont.Weight.regular)
    
    private var basisView: UIView!
    private var gestureView: UIView!
    
    private var imagePicker = UIImagePickerController()
    
    private var collectionView: UICollectionView!
    
    private let maxMediaQuantity: Int = 6
    
    private let collectionViewCellID = "CommentBarManagerCell"
    
    private static let aMargin: CGFloat = 5.0
    
    lazy private var commentMedia: CommentMediaObserver = {
        let observer = CommentMediaObserver()
        observer.delegate = self
        return observer
    }()
    
    static let collectionViewHeight: CGFloat = 88
    
    private var messageCanBeSend: Bool = false {
        didSet {
            if oldValue != messageCanBeSend {
                if messageCanBeSend {
                    sendButton.isEnabled = true
                } else {
                    if commentMedia.quantity == 0 && textView.text.count == 0 {
                        sendButton.isEnabled = false
                    } else {
                        messageCanBeSend = true
                    }
                }
            }
        }
    }
    
    private lazy var collectionViewHeightAnchor: NSLayoutConstraint = {
        let height = CommentBarManager.collectionViewHeight
        return collectionView.heightAnchor.constraint(equalToConstant: height)
    }()
    
//    private let zeroPoint: CGFloat = 0.0
    
    private var zeroPoint: CGFloat {
        get {
//            if #available(iOS 11.0, *) {
//                let window = UIApplication.shared.keyWindow
//                let safeBottomConstant = -window!.safeAreaInsets.bottom
//                print("zeroPoint =", safeBottomConstant)
//                return safeBottomConstant
//            } else {
                print("zeroPoint =", 0.0)
                return 0.0
//            }
        }
    }
    
//    private lazy var heightBottomAnchor: NSLayoutConstraint = {
//        var safeBottomConstant: CGFloat = 0.0
//
//        if #available(iOS 11.0, *) {
//            let window = UIApplication.shared.keyWindow
//            safeBottomConstant = window!.safeAreaInsets.bottom
//        }
//
//        return background.heightAnchor.constraint(equalToConstant: 80.0)
//
////        return background.heightAnchor.constraint(equalTo: textView.heightAnchor, constant: safeBottomConstant)
//    }()
    
    private lazy var backgroundBottomAnchor: NSLayoutConstraint = {
        
//        var safeBottomConstant: CGFloat = 0.0
//
//        if #available(iOS 11.0, *) {
//            let window = UIApplication.shared.keyWindow
//            safeBottomConstant = -window!.safeAreaInsets.bottom
//        }
        
        return background.bottomAnchor.constraint(equalTo: basisView.bottomAnchor, constant: 0.0)
    }()
    
    private lazy var textViewHeightAnchor: NSLayoutConstraint = {
        return textView.heightAnchor.constraint(equalToConstant: CommentBarManager.getTextViewHeight(basedOn: CommentBarManager.font))
    }()
    
    private lazy var tapGesture: UITapGestureRecognizer = {
        return UITapGestureRecognizer.init(target: self, action: #selector(tapGestureAction(_:)))
    }()
    
    var delegate: CommentBarManagerDelegate?
    
    func install(onTop view: UIView, gestureView: UIView) {
        self.basisView = view
        self.gestureView = gestureView
        
        setupViews(basisView: view)
        setupImagePicker()
        setupCollectionView()
    }
    
    func notificationOn() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func notificationOff() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
}

extension CommentBarManager: CommentMediaObserverDelegate {
    func mediaQuantityDidChange(newQuantity: Int, oldQuantity: Int) {
        // hide or show cv
        if oldQuantity == 0 && newQuantity > 0 {
            addCollectionView()
            messageCanBeSend = true
        } else if newQuantity == 0, oldQuantity > 0 {
            removeCollectionView()
            messageCanBeSend = false
        }
        // attachButton active/inactive
        if newQuantity >= maxMediaQuantity {
            attachButton.isEnabled = false
        } else {
            attachButton.isEnabled = true
        }
        // reload in proper manner
        collectionView.reloadData()
        if newQuantity > 1 {
            if newQuantity > oldQuantity {
                collectionView.scrollToItem(at: IndexPath.init(row: newQuantity - 1, section: 0), at: UICollectionViewScrollPosition.centeredHorizontally, animated: false)
            }
        }
    }
}

extension CommentBarManager: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let mediaType = info[UIImagePickerControllerMediaType] as? NSString {
            if mediaType == kUTTypeImage {
                if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                    let media = CommentMedia.init(type: .image, image: pickedImage)
                    self.commentMedia.add(media: media)
                }
            } else if mediaType == kUTTypeMovie {
                if let videoURL = info[UIImagePickerControllerMediaURL] as? URL {
                    if let thumbnail = generateThumbnail(path: videoURL) {
                        let media = CommentMedia.init(type: .video, image: thumbnail, url: videoURL)
                        self.commentMedia.add(media: media)
                    }
                }
            }
        }
        self.imagePicker.dismiss(animated: true, completion: nil)
    }
}

extension CommentBarManager: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        self.addGesture()
        if textView.text.count == 0 {
            textView.setContentOffset(CGPoint.zero, animated: false)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.removeGesture()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        
        let textAttributes = [NSAttributedStringKey.font : textView.font!]
        var textWidth: CGFloat = UIEdgeInsetsInsetRect(textView.frame, textView.textContainerInset).width
        textWidth -= 2.0 * textView.textContainer.lineFragmentPadding
        let boundingRect: CGRect = newText.boundingRect(with: CGSize(width: textWidth, height: 0), options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: textAttributes, context: nil)
        let numberOfLines = Int(floor(boundingRect.height / textView.font!.lineHeight))
        
        if numberOfLines <= 5 {
            setTextView(newText: newText)
            return false
        } else {
            textView.setContentOffset(CGPoint.init(x: 0, y: textView.contentSize.height - textView.frame.size.height), animated: false)
            return true
        }
    }
}

extension CommentBarManager {
    @objc private func tapGestureAction(_ gesture: UITapGestureRecognizer) {
        self.textView.resignFirstResponder()
    }
    
    @objc private func attachButtonAction() {
        self.delegate?.show(imagePickerVC: self.imagePicker)
    }
}

extension CommentBarManager {
    @objc private func deleteMediaButtonAction(_ button: UIButton) {
        let cell = button.superview!.superview as! CommentBarManagerCell
        let cv = cell.superview as! UICollectionView
        let indexPath = cv.indexPath(for: cell)!
        // deletion media data and reloading collectionView
        commentMedia.remove(at: indexPath.row)
    }
    @objc private func sendButtonAction(_ button: UIButton) {
        let resignFirstResponder = delegate?.sendButtonAction(text: textView.text, medias: commentMedia.medias)
        if resignFirstResponder == true {
            textView.resignFirstResponder()
        }
        setTextView(newText: "")
        commentMedia.removeAll()
    }
}

extension CommentBarManager: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    // implement item deletion
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return commentMedia.quantity
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: collectionViewCellID, for: indexPath) as! CommentBarManagerCell
        let media = commentMedia.getItem(at: indexPath.row)
        cell.configCell(type: media.type, image: media.image)
        cell.button.addTarget(self, action: #selector(deleteMediaButtonAction(_:)), for: .touchUpInside)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = collectionViewHeightAnchor.constant - 2 - 2
        let width = height
        return CGSize.init(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 0.0, left: 2.0, bottom: 0.0, right: 2.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2.0
    }
}

extension CommentBarManager {
    private func setTextView(newText: String) {
        textView.text = newText
        let newHeight = textView.sizeThatFits(CGSize.init(width: textView.frame.width, height: CGFloat.greatestFiniteMagnitude)).height
        let oldHeight = textViewHeightAnchor.constant
        textViewHeightAnchor.constant = newHeight
        delegate?.barHeightDidChange(heightDifference: newHeight - oldHeight, _triggerMethod: "shouldChangeTextIn")
        textView.setContentOffset(CGPoint.zero, animated: false)
        
        if newText.count == 0 {
            messageCanBeSend = false
        } else {
            messageCanBeSend = true
        }
    }
    
    private func addGesture() {
        self.gestureView.addGestureRecognizer(self.tapGesture)
    }
    
    private func removeGesture() {
        self.gestureView.removeGestureRecognizer(self.tapGesture)
    }
    
    private func setupImagePicker() {
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.mediaTypes = [kUTTypeImage as NSString, kUTTypeMovie as NSString] as [String]
    }
    
    private static func getTextViewHeight(basedOn font: UIFont) -> CGFloat {
        let textView = UITextView.init(frame: CGRect.zero)
        textView.font = font
        textView.text = "TEST"
        let size = textView.sizeThatFits(CGSize.init(width: UIScreen.main.bounds.width, height: CGFloat.greatestFiniteMagnitude))
        return size.height
    }
    
    static func getCommentBarHeight() -> CGFloat {
        let aMargin = CommentBarManager.aMargin
        let tvHeight = CommentBarManager.getTextViewHeight(basedOn: CommentBarManager.font)
        return aMargin + tvHeight + aMargin
    }
    
    private func setupCollectionView() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        collectionView = UICollectionView.init(frame: CGRect.zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .white
        collectionView.autoresizingModeOn()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib.init(nibName: collectionViewCellID, bundle: nil), forCellWithReuseIdentifier: collectionViewCellID)
    }
    
    private func addCollectionView() {
        basisView.addSubview(collectionView)
        delegate?.barHeightDidChange(heightDifference: collectionViewHeightAnchor.constant, _triggerMethod: "addCollectionView")
        NSLayoutConstraint.activate([
            collectionView.leadingAnchor.constraint(equalTo: basisView.leadingAnchor, constant: 0.0),
            collectionView.trailingAnchor.constraint(equalTo: basisView.trailingAnchor, constant: 0.0),
            collectionView.bottomAnchor.constraint(equalTo: background.topAnchor, constant: 0.0),
            collectionViewHeightAnchor
        ])
    }
    
    private func removeCollectionView() {
        delegate?.barHeightDidChange(heightDifference: -collectionViewHeightAnchor.constant, _triggerMethod: "removeCollectionView")
        self.collectionView.removeFromSuperview()
    }
    
    private func setupViews(basisView: UIView) {
        
        let textViewColor = UIColor.init(hex: "f2f3f5")
//        let buttonColor = UIColor.init(hex: "99a2ad")
//        let inactiveAttachButtonColor = UIColor.init(hex: "dcdcdc")
//        let activeSendButtonColor = UIColor.init(hex: "84b1ff")
        let textColor = UIColor.init(hex: "000000")
        
//        background.backgroundColor = .white
        background.backgroundColor = UIColor.init(hex: "FAFAFA")
        basisView.addSubview(background)
        background.autoresizingModeOn()
        
        topBorderView.backgroundColor = UIColor.init(hex: "FAFAFA")
        basisView.addSubview(topBorderView)
        topBorderView.autoresizingModeOn()
        
        attachButton.backgroundColor = .clear
        basisView.addSubview(attachButton)
        attachButton.autoresizingModeOn()
        attachButton.imageEdgeInsets = UIEdgeInsets.init(top: 5, left: 5, bottom: 5, right: 5)
//        let attachButtonImage = #imageLiteral(resourceName: "attachButton").setColor(buttonColor)
//        let inactiveAttachButtonImage = #imageLiteral(resourceName: "attachButton").setColor(inactiveAttachButtonColor)
//        attachButton.setImage(attachButtonImage, for: .normal)
//        attachButton.setImage(inactiveAttachButtonImage, for: .disabled)
        attachButton.addTarget(self, action: #selector(attachButtonAction), for: UIControlEvents.touchUpInside)
        
        let textViewBackground = UIView()
        textViewBackground.autoresizingModeOn()
        textViewBackground.backgroundColor = textViewColor
        textViewBackground.makeOval(cornerRadius: textViewHeightAnchor.constant / 2)
        basisView.addSubview(textViewBackground)
        
        textView.font = CommentBarManager.font
        textView.delegate = self
        textView.backgroundColor = .clear
        textView.textColor = textColor
        basisView.addSubview(textView)
        textView.autoresizingModeOn()
        
        sendButton.backgroundColor = .clear
        let inactiveSendButtonImage = #imageLiteral(resourceName: "sendButton-inact")
        let activeSendButtonImage = #imageLiteral(resourceName: "sendButton-act")
        sendButton.setImage(activeSendButtonImage, for: .normal)
        sendButton.setImage(inactiveSendButtonImage, for: .disabled)
        sendButton.isEnabled = messageCanBeSend
        sendButton.imageEdgeInsets = UIEdgeInsets.init(top: 3, left: 3, bottom: 3, right: 3)
        basisView.addSubview(sendButton)
        sendButton.autoresizingModeOn()
        sendButton.addTarget(self, action: #selector(sendButtonAction(_:)), for: .touchUpInside)
        
        let textViewHeight = CommentBarManager.getTextViewHeight(basedOn: CommentBarManager.font)
        
        let futureTotalHeight = CommentBarManager.aMargin + textViewHeight + CommentBarManager.aMargin
        delegate?.barHeightWillSet(to: futureTotalHeight)
        
//        textView.backgroundColor = .red
//        background.backgroundColor = .yellow
        
        var safeBottomConstant: CGFloat = 0.0
        
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            safeBottomConstant = window?.safeAreaInsets.bottom ?? 0.0
        }
        
        print("safeBottom =", safeBottomConstant)
        
        NSLayoutConstraint.activate([
            background.leadingAnchor.constraint(equalTo: basisView.leadingAnchor, constant: 0.0),
            background.trailingAnchor.constraint(equalTo: basisView.trailingAnchor, constant: 0.0),
            background.topAnchor.constraint(equalTo: textView.topAnchor, constant: -CommentBarManager.aMargin),
            backgroundBottomAnchor,
//            heightBottomAnchor,
            
            topBorderView.leadingAnchor.constraint(equalTo: basisView.leadingAnchor, constant: 0.0),
            topBorderView.trailingAnchor.constraint(equalTo: basisView.trailingAnchor, constant: 0.0),
            topBorderView.topAnchor.constraint(equalTo: background.topAnchor, constant: 0.0),
            topBorderView.heightAnchor.constraint(equalToConstant: 1.0),
            
            attachButton.heightAnchor.constraint(equalToConstant: textViewHeight),
//            attachButton.widthAnchor.constraint(equalToConstant: textViewHeight),
            attachButton.widthAnchor.constraint(equalToConstant: 10.0),
            attachButton.bottomAnchor.constraint(equalTo: background.bottomAnchor, constant: -CommentBarManager.aMargin),
            attachButton.leadingAnchor.constraint(equalTo: background.leadingAnchor, constant: CommentBarManager.aMargin),
            
            textViewHeightAnchor,
            textView.leadingAnchor.constraint(equalTo: attachButton.trailingAnchor, constant: CommentBarManager.aMargin),
            textView.trailingAnchor.constraint(equalTo: sendButton.leadingAnchor, constant: -10.0),
            textView.bottomAnchor.constraint(equalTo: background.bottomAnchor, constant: -CommentBarManager.aMargin - safeBottomConstant),
            
            textViewBackground.leadingAnchor.constraint(equalTo: textView.leadingAnchor, constant: -7.0),
            textViewBackground.trailingAnchor.constraint(equalTo: textView.trailingAnchor, constant: 7.0),
            textViewBackground.topAnchor.constraint(equalTo: textView.topAnchor, constant: 0.0),
            textViewBackground.bottomAnchor.constraint(equalTo: textView.bottomAnchor, constant: 0.0),
            
            sendButton.heightAnchor.constraint(equalToConstant: textViewHeight),
            sendButton.widthAnchor.constraint(equalToConstant: textViewHeight),
//            sendButton.trailingAnchor.constraint(equalTo: background.trailingAnchor, constant: -CommentBarManager.aMargin),
            sendButton.trailingAnchor.constraint(equalTo: background.trailingAnchor, constant: -CommentBarManager.aMargin),
            sendButton.bottomAnchor.constraint(equalTo: background.bottomAnchor, constant: -CommentBarManager.aMargin - safeBottomConstant)
        ])
    }
}

extension CommentBarManager {
    @objc private func keyboardWillShow(_ notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve: UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            
            print("backgroundBottomAnchor KwillShow after =", backgroundBottomAnchor.constant)
            
            if let frame = endFrame {
                if backgroundBottomAnchor.constant == zeroPoint {
//                    backgroundBottomAnchor.constant = -frame.height
                    
                    var iPhoneXBottom: CGFloat = 0.0
                    
                    if #available(iOS 11.0, *) {
                        let window = UIApplication.shared.keyWindow
                        iPhoneXBottom = window!.safeAreaInsets.bottom
                    }
                    
                    print("iphoneXBottom =", iPhoneXBottom)
                    
                    backgroundBottomAnchor.constant = -frame.height + iPhoneXBottom

                    delegate?.barHeightDidChange(heightDifference: frame.height - iPhoneXBottom, _triggerMethod: "keyboardWillShow")
//                    delegate?.barHeightDidChange(heightDifference: frame.height, _triggerMethod: "keyboardWillShow")
                }
            }
            
            print("backgroundBottomAnchor KwillHide bef =", backgroundBottomAnchor.constant)
            
            UIView.animate(withDuration: duration, delay: TimeInterval(0), options: animationCurve, animations: {
                self.basisView.layoutIfNeeded()
            }, completion: nil)
        }
    }
    
    @objc private func keyboardWillHide(_ notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve: UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            
            print("backgroundBottomAnchor KwillHide bef =", backgroundBottomAnchor.constant)
            
            if let frame = endFrame {
                if backgroundBottomAnchor.constant != zeroPoint {
                    
                    var iPhoneXBottom: CGFloat = 0.0

                    if #available(iOS 11.0, *) {
                        let window = UIApplication.shared.keyWindow
                        iPhoneXBottom = window!.safeAreaInsets.bottom
                    }
                    
                    
                    delegate?.barHeightDidChange(heightDifference: -frame.height + iPhoneXBottom, _triggerMethod: "keyboardWillHide")
                }
            }
            self.backgroundBottomAnchor.constant = zeroPoint
            
            print("backgroundBottomAnchor KwillHide aft =", backgroundBottomAnchor.constant)
            
            UIView.animate(withDuration: duration, delay: TimeInterval(0), options: animationCurve, animations: {
                self.basisView.layoutIfNeeded()
            }, completion: nil)
        }
    }
}





























