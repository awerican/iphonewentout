//
//  ChatLeftMsgOwnThreeImageTextGrouppedCell.swift
//  WentOut
//
//  Created by Fure on 16.10.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import UIKit

class ChatLeftMsgOwnThreeImageTextGrouppedCell: ChatMessageBaseCell {

    @IBOutlet var messageOutlet: UILabel!
    @IBOutlet var mediaCollectionOutlet: [UIImageView]!
    @IBOutlet var timeOutlet: UILabel!
    @IBOutlet var messageBackgroundOutlet: UIView!
    
    @IBOutlet var timeLeadingConstrOutlet: NSLayoutConstraint!
    @IBOutlet var timeTopConstrOutlet: NSLayoutConstraint!
    
    override var message: UILabel? {
        return self.messageOutlet
    }
    override var mediaCollection: [UIImageView]? {
        return self.mediaCollectionOutlet
    }
    override var time: UILabel! {
        return self.timeOutlet
    }
    override var messageBackground: UIView! {
        return self.messageBackgroundOutlet
    }
    override var timeLeadingConstr: NSLayoutConstraint! {
        return self.timeLeadingConstrOutlet
    }
    override var timeTopConstr: NSLayoutConstraint! {
        return self.timeTopConstrOutlet
    }
}
