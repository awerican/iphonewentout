//
//  Entities.swift
//  eventsRM
//
//  Created by Fure on 14.11.17.
//  Copyright © 2017 uzuner. All rights reserved.
//

import Foundation
import UIKit

struct ResponseEntity<T: Decodable> {
    var data: T?
    var verified: Bool
    var next_url: String?
}

extension ResponseEntity: Decodable {
    private enum EventApiResponseCodingKeys: String, CodingKey {
        case data     = "data"
        case verified = "verifyStatus"
        case next_url = "next_href"
    }
    
    init(from decoder: Decoder) throws {
        do {
            let container = try decoder.container(keyedBy: EventApiResponseCodingKeys.self)
            self.data     = try? container.decode(T.self, forKey: .data)
            self.verified = try container.decode(Bool.self, forKey: .verified)
            self.next_url = try? container.decode(String.self, forKey: .next_url)
        } catch {
            throw error
        }
    }
}

struct MapEvent: Equatable, Hashable {
    var hashValue: Int {
        get {
            return id.hashValue
        }
    }
    
    var id: String
    var latitude: Double
    var longitude: Double
    var imageURL: String
    var index: Double
    var zoom: Int
    
    var image: UIImage?
    
    static func ==(lhs: MapEvent, rhs: MapEvent) -> Bool {
        return lhs.id == rhs.id
    }
}

extension MapEvent: Decodable {
    private enum EventApiResponseCodingKeys: String, CodingKey {
        case id = "id"
        case latitude = "latitude"
        case longitude = "longitude"
        case imageURL = "image_url"
        case index = "index"
        case zoom = "zoom"
    }
    
    init(from decoder: Decoder) throws {
        do {
            let container = try decoder.container(keyedBy: EventApiResponseCodingKeys.self)
            self.id = try container.decode(String.self, forKey: .id)
            self.latitude = try container.decode(Double.self, forKey: .latitude)
            self.longitude = try container.decode(Double.self, forKey: .longitude)
            self.imageURL = try container.decode(String.self, forKey: .imageURL)
            self.index = try container.decode(Double.self, forKey: .index)
            self.zoom = try container.decode(Int.self, forKey: .zoom)
        } catch {
            throw error
        }
    }
}

struct MapEventMini: Equatable, Hashable {
    var hashValue: Int {
        get {
            return id.hashValue
        }
    }
    
    var id: String
    var title: String
    var timeStart: Date
    
    static func ==(lhs: MapEventMini, rhs: MapEventMini) -> Bool {
        return lhs.id == rhs.id
    }
}

struct MapEventAmount: Decodable {
    var quantity: Int
}

extension MapEventAmount {
    private enum EventApiResponseCodingKeys: String, CodingKey {
        case quantity = "quantity"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: EventApiResponseCodingKeys.self)
        self.quantity = try container.decode(Int.self, forKey: .quantity)
    }
}

enum RegistrationFirstStatus: String, Decodable {
    case isExisted = "isExisted"
}

struct RegistrationFirstEntity {
    var userID: String?
}

extension RegistrationFirstEntity: Decodable {
    private enum Keys: String, CodingKey {
        case userID = "userID"
    }
    
    init(from decoder: Decoder) throws {
        do {
            let container = try decoder.container(keyedBy: Keys.self)
            self.userID = try container.decode(String.self, forKey: .userID)
        } catch {
            throw error
        }
    }
}

struct EventPanel: Equatable, Hashable {
    var hashValue: Int {
        get {
            return eventId.hashValue
        }
    }
    
    static var sample: EventPanel {
        get {
            return EventPanel.init(eventID: "", eventPhoto: "https://amtrakdowneaster.com/sites/default/files/2017-10/Concert_2_0.jpg", title: "MegaParty", time: Date(), location: EventLocation.init(address: "Moscow, Korolev 15", country: "Russia", city: "Moscow", longitude: 23.123432, latitude: 76.456124), guests: 23, userID: "id", username: "makefure", userPhoto: "https://images-na.ssl-images-amazon.com/images/I/71zjy0nn2wL.png")
        }
    }
    
    var eventId: String
    var eventPhoto: String
    var title: String
    var time: Date
    var location: EventLocation
    var guests: Int
    var userId: String
    var userNickname: String
    var userPhoto: String
    
    static func ==(lhs: EventPanel, rhs: EventPanel) -> Bool {
        return lhs.eventId == rhs.eventId &&
        lhs.eventPhoto == rhs.eventPhoto &&
        lhs.title == rhs.title &&
        lhs.time == rhs.time &&
        lhs.location == rhs.location &&
        lhs.guests == rhs.guests &&
        lhs.userId == rhs.userId &&
        lhs.userNickname == rhs.userNickname &&
        lhs.userPhoto == rhs.userPhoto
    }
    
    init(eventID: String, eventPhoto: String, title: String, time: Date, location: EventLocation, guests: Int, userID: String, username: String, userPhoto: String) {
        self.eventId = eventID
        self.eventPhoto = eventPhoto
        self.title = title
        self.time = time
        self.location = location
        self.guests = guests
        self.userId = userID
        self.userNickname = username
        self.userPhoto = userPhoto
    }
}

extension EventPanel: Decodable {
    private enum EventApiResponseCodingKeys: String, CodingKey {
        case eventId      = "e_id"
        case eventPhoto   = "e_photo"
        case title        = "title"
        case time         = "time"
        case location     = "location"
        case guests       = "guests"
        case userId       = "u_id"
        case userNickname = "nickname"
        case userPhoto    = "u_photo"
    }
    
    init(from decoder: Decoder) throws {
        do {
            let container = try decoder.container(keyedBy: EventApiResponseCodingKeys.self)
            self.eventId = try container.decode(String.self, forKey: .eventId)
            self.eventPhoto = try container.decode(String.self, forKey: .eventPhoto)
            self.title = try container.decode(String.self, forKey: .title)
            let timespan = try container.decode(Double.self, forKey: .time)
            self.time = Date.init(timeIntervalSince1970: timespan)
            self.location = try container.decode(EventLocation.self, forKey: .location)
            self.guests = try container.decode(Int.self, forKey: .guests)
            self.userId = try container.decode(String.self, forKey: .userId)
            self.userNickname = try container.decode(String.self, forKey: .userNickname)
            self.userPhoto = try container.decode(String.self, forKey: .userPhoto)
        } catch {
            throw error
        }
    }
}

struct EEventName: Equatable {
    var eventName: String
    
    static func ==(lhs: EEventName, rhs: EEventName) -> Bool {
        return lhs.eventName == rhs.eventName
    }
}

extension EEventName: Decodable {
    private enum Keys: String, CodingKey {
        case eventName = "eventName"
    }
    init(from decoder: Decoder) throws {
        do {
            let container = try decoder.container(keyedBy: Keys.self)
            self.eventName = try container.decode(String.self, forKey: .eventName)
        } catch {
            throw error
        }
    }
}

struct EventTitle: Equatable {
    var eventID: String
    var eventTitle: String
    
    static func ==(lhs: EventTitle, rhs: EventTitle) -> Bool {
        return lhs.eventID == rhs.eventID &&
                lhs.eventTitle == lhs.eventTitle
    }
}

extension EventTitle: Decodable {
    private enum Keys: String, CodingKey {
        case eventID     = "id"
        case eventTitle  = "title"
    }
    
    init(from decoder: Decoder) throws {
        do {
            let container = try decoder.container(keyedBy: Keys.self)
            
            self.eventID = try container.decode(String.self, forKey: .eventID)
            self.eventTitle = try container.decode(String.self, forKey: .eventTitle)
        } catch {
            throw error
        }
    }
}

struct EventMemoryMini: Equatable {
    var eventID: String
    var mediaID: String
    var photo: String
    var eventName: String
    
    static func ==(lhs: EventMemoryMini, rhs: EventMemoryMini) -> Bool {
        return lhs.mediaID == rhs.mediaID && lhs.eventID == rhs.eventID
    }
}

extension EventMemoryMini: Decodable {
    private enum Keys: String, CodingKey {
        case eventID = "e_id"
        case mediaID = "media_id"
        case photo = "media_photo"
        case eventName = "event_name"
    }
    
    init(from decoder: Decoder) throws {
        do {
            let container = try decoder.container(keyedBy: Keys.self)
            
            self.eventID = try container.decode(String.self, forKey: .eventID)
            self.mediaID = try container.decode(String.self, forKey: .mediaID)
            self.photo = try container.decode(String.self, forKey: .photo)
            self.eventName = try container.decode(String.self, forKey: .eventName)
        } catch {
            throw error
        }
    }
}

struct NewsMemory: Equatable {
    var eventID: String
    var eventAvatar: String
    var eventTitle: String
    
    static func ==(lhs: NewsMemory, rhs: NewsMemory) -> Bool {
        return lhs.eventID == rhs.eventID
    }
}

extension NewsMemory: Decodable {
    private enum Keys: String, CodingKey {
        case eventID     = "eventID"
        case eventAvatar = "eventPhoto"
        case eventTitle  = "eventTitle"
    }
    
    init(from decoder: Decoder) throws {
        do {
            let container = try decoder.container(keyedBy: Keys.self)
            
            self.eventID = try container.decode(String.self, forKey: .eventID)
            self.eventAvatar = try container.decode(String.self, forKey: .eventAvatar)
            self.eventTitle = try container.decode(String.self, forKey: .eventTitle)
        } catch {
            throw error
        }
    }
}

struct StoryEntity: Hashable, Equatable {
    var eventID: String
    var eventAvatar: String
    var eventTitle: String
    var totalMemories: Int
    var memories: StoryMemoryWrapper
    
    var hashValue: Int {
        get {
            return eventID.hashValue
        }
    }
    
    static func ==(lhs: StoryEntity, rhs: StoryEntity) -> Bool {
        return lhs.eventID == rhs.eventID &&
            lhs.eventAvatar == rhs.eventAvatar &&
            lhs.eventTitle == rhs.eventTitle
    }
}

extension StoryEntity: Decodable {
    private enum CodingKeys: String, CodingKey {
        case eventID        = "eventID"
        case eventAvatar    = "eventAvatar"
        case eventTitle     = "eventTitle"
        case totalMemories  = "totalMemories"
        case memories       = "memories"
    }
    
    init(from decoder: Decoder) throws {
//        do {
            let container       = try! decoder.container(keyedBy: CodingKeys.self)
            
            self.eventID = try! container.decode(String.self, forKey: .eventID)
            self.eventAvatar = try! container.decode(String.self, forKey: .eventAvatar)
            self.eventTitle = try! container.decode(String.self, forKey: .eventTitle)
            self.totalMemories = try! container.decode(Int.self, forKey: .totalMemories)
            self.memories = try! container.decode(StoryMemoryWrapper.self, forKey: .memories)
//        } catch {
//            throw error
//        }
    }
}

struct StoryMemoryWrapper: Equatable { //, Hashable {
//    var hashValue: Int {
//        get {
//            return self.data.toHexString().hashValue
//        }
//    }
    
    var data: [StoryMemory]
    var nextHref: String?
    var verifyStatus: Bool
    
    static func ==(lhs: StoryMemoryWrapper, rhs: StoryMemoryWrapper) -> Bool {
        return lhs.nextHref == rhs.nextHref &&
            lhs.data.count == rhs.data.count
    }
}

extension StoryMemoryWrapper: Decodable {
    private enum CodingKeys: String, CodingKey {
        case data         = "data"
        case nextHref     = "next_href"
        case verifyStatus = "verifyStatus"
    }
    
    init(from decoder: Decoder) throws {
//        do {
            let container       = try! decoder.container(keyedBy: CodingKeys.self)
            
            self.nextHref = try? container.decode(String.self, forKey: .nextHref)
            self.verifyStatus = try! container.decode(Bool.self, forKey: .verifyStatus)
            self.data = try! container.decode([StoryMemory].self, forKey: .data)
//        } catch {
//            throw error
//        }
    }
}

struct StoryMemory: Hashable, Equatable {
    var id: String
    var userID: String
    var username: String
    var media: String
    var mediaPrev: String
    var time: Int
    var mimeType: String
    
    static func ==(lhs: StoryMemory, rhs: StoryMemory) -> Bool {
        return lhs.id == rhs.id &&
            lhs.username == rhs.username
    }
}

extension StoryMemory: Decodable {
    private enum CodingKeys: String, CodingKey {
        case id         = "id"
        case userID     = "userID"
        case username   = "username"
        case media      = "media_original"
        case mediaPrev  = "media_prev"
        case time       = "media_time"
        case mimeType   = "mimetype"
    }
    
    init(from decoder: Decoder) throws {
//        do {
            let container       = try! decoder.container(keyedBy: CodingKeys.self)
            
            self.id = try! container.decode(String.self, forKey: .id)
            self.userID = try! container.decode(String.self, forKey: .userID)
            self.username = try! container.decode(String.self, forKey: .username)
            self.media = try! container.decode(String.self, forKey: .media)
            self.mediaPrev = try! container.decode(String.self, forKey: .mediaPrev)
            let _time = try! container.decode(Double.self, forKey: .time)
            self.time = Int(_time)
//            self.time = try container.decode(Int.self, forKey: .time)
            self.mimeType = try! container.decode(String.self, forKey: .mimeType)
//        } catch {
//            throw error
//        }
    }
}

struct MemoryEntity: Hashable, Equatable {
    var id: String
    var eventID: String
    var eventAvatar: String
    var eventTitle: String
    var userID: String
    var username: String
    var mediaOriginal: String
    var mediaPrev: String
    var time: Int
    
    static func ==(lhs: MemoryEntity, rhs: MemoryEntity) -> Bool {
        return lhs.id == rhs.id
    }
    
    static var sample: MemoryEntity {
        get {
            return MemoryEntity.init(id: "", eventID: "", eventAvatar: "", eventTitle: "Title #1", userID: "", username: "awerican", mediaOriginal: "", mediaPrev: "", time: 123454456)
        }
    }
    
    init(id: String, eventID: String, eventAvatar: String, eventTitle: String, userID: String, username: String, mediaOriginal: String, mediaPrev: String, time: Int) {
        self.id = id
        self.eventID = eventID
        self.eventAvatar = eventAvatar
        self.eventTitle = eventTitle
        self.userID = userID
        self.username = username
        self.mediaOriginal = mediaOriginal
        self.mediaPrev = mediaPrev
        self.time = time
    }
}

extension MemoryEntity: Decodable {
    private enum EventApiResponseCodingKeys: String, CodingKey {
        case mediaID   = "id"
        case userID    = "userID"
        case username  = "username"
        case mediaOrig = "media_original"
        case mediaPrev = "media_prev"
        case eventID   = "eventID"
    }
    
    init(from decoder: Decoder) throws {
        do {
            let container       = try decoder.container(keyedBy: EventApiResponseCodingKeys.self)
            
            self.id = try container.decode(String.self, forKey: .mediaID)
            self.userID = try container.decode(String.self, forKey: .userID)
            self.username = try container.decode(String.self, forKey: .username)
            self.mediaOriginal = try container.decode(String.self, forKey: .mediaOrig)
            self.mediaPrev = try container.decode(String.self, forKey: .mediaPrev)
            self.eventID = try container.decode(String.self, forKey: .eventID)
            
            // missing properties from server
            self.eventAvatar = "https://pbs.twimg.com/profile_images/618812444492832768/UNPR-hqw_400x400.jpg"
            self.eventTitle = "Event Title"
            self.time = 1555830893
        } catch {
            throw error
        }
    }
}

struct EventLocation: Hashable, Equatable, Encodable {
    var location: String
    var country: String
    var city: String
    var longitude: Double
    var latitude: Double
    
    static func ==(lhs: EventLocation, rhs: EventLocation) -> Bool {
        return lhs.location == rhs.location &&
                lhs.country == rhs.country &&
                lhs.city == rhs.city &&
                lhs.longitude == rhs.longitude &&
                lhs.latitude == rhs.latitude
    }
    
    init(address: String, country: String, city: String, longitude: Double, latitude: Double) {
        self.location = address
        self.country = country
        self.city = city
        self.longitude = longitude
        self.latitude = latitude
    }
}

extension EventLocation: Decodable {
    private enum Keys: String, CodingKey {
        case city       = "city"
        case country    = "country"
        case address    = "location"
        case latitude   = "latitude"
        case longitude  = "longitude"
    }
    
    init(from decoder: Decoder) throws {
        do {
            let container       = try decoder.container(keyedBy: Keys.self)
            
            self.location = try container.decode(String.self, forKey: .address)
            self.country = try container.decode(String.self, forKey: .country)
            self.city = try container.decode(String.self, forKey: .city)
            self.latitude =  try container.decode(Double.self, forKey: .latitude)
            self.longitude =  try container.decode(Double.self, forKey: .longitude)
        } catch {
            throw error
        }
    }
}

struct UserMini: Equatable {
    var id: String
    var username: String
    var avatar: String
    
    static func ==(lhs: UserMini, rhs: UserMini) -> Bool {
        return lhs.id == rhs.id &&
            lhs.username == rhs.username &&
            lhs.avatar == rhs.avatar
    }
    
    static var sample: UserMini {
        get {
            return UserMini(id: "", username: "awerican", avatar: "https://pixel.nymag.com/imgs/daily/vulture/2018/06/28/drake/28-drake-2.w700.h700.jpg")
        }
    }
    
//    static func ==(lhs: EventLocation, rhs: EventLocation) -> Bool {
//        return lhs.id == rhs.id &&
//            lhs.username == rhs.username &&
//            lhs.avatar == rhs.avatar
//    }
}

extension UserMini: Decodable {
    private enum Keys: String, CodingKey {
        case id = "userID"
        case username = "userNickname"
        case avatar = "userAvatar"
    }
    init(from decoder: Decoder) throws {
        do {
            let container = try decoder.container(keyedBy: Keys.self)
            
            self.id = try container.decode(String.self, forKey: .id)
            self.username = try container.decode(String.self, forKey: .username)
            self.avatar = try container.decode(String.self, forKey: .avatar)
        } catch {
            throw error
        }
    }
}

struct EventProfileHeader: Equatable {
    var eventID: String
    var title: String
    var statusGo: Bool
    var creatorID: String
    var creatorUsername: String
    var guests: Int
    var timeStart: Int
    var views: Int
    var avatar: String
    var eventType: EventType
    var description: String
    var location: EventLocation?
    var memoryCount: Int
    var friends: [UserMini]
    
    static func ==(lhs: EventProfileHeader, rhs: EventProfileHeader) -> Bool {
        return lhs.eventID == rhs.eventID &&
                lhs.title == rhs.title &&
                lhs.statusGo == rhs.statusGo &&
                lhs.creatorID == rhs.creatorID &&
                lhs.creatorUsername == rhs.creatorUsername &&
                lhs.guests == rhs.guests &&
                lhs.timeStart == rhs.timeStart &&
                lhs.views == rhs.views &&
                lhs.avatar == rhs.avatar &&
                lhs.eventType == rhs.eventType &&
                lhs.description == rhs.description &&
                lhs.location == rhs.location &&
                lhs.memoryCount == rhs.memoryCount
    }
}

extension EventProfileHeader: Decodable {
    private enum Keys: String, CodingKey {
        case eventID         = "event_id"
        case title           = "title"
        case description     = "description"
        case statusGO        = "status_go"
        case creatorID       = "creator_id"
        case creatorUsername = "creator_nickname"
        case guests          = "guests"
        case timeStart       = "time_start"
        case views           = "views"
        case eventAvatar     = "photo"
        case eventType       = "event_type"
        case friends         = "friends"
        case location        = "location"
        case memoryCount     = "memory_count"
    }
    
    init(from decoder: Decoder) throws {
        do {
            let container = try decoder.container(keyedBy: Keys.self)
            
            self.eventID            = try container.decode(String.self, forKey: .eventID)
            self.title              = try container.decode(String.self, forKey: .title)
            self.description        = try container.decode(String.self, forKey: .description)
            self.statusGo           = try container.decode(Bool.self, forKey: .statusGO)
            self.creatorID          = try container.decode(String.self, forKey: .creatorID)
            self.creatorUsername    = try container.decode(String.self, forKey: .creatorUsername)
            self.guests             = try container.decode(Int.self, forKey: .guests)
            self.timeStart          = try container.decode(Int.self, forKey: .timeStart)
            self.views              = try container.decode(Int.self, forKey: .views)
            self.avatar             = try container.decode(String.self, forKey: .eventAvatar)
            self.eventType          = try container.decode(EventType.self, forKey: .eventType)
            self.friends            = try container.decode([UserMini].self, forKey: .friends)
            self.location           = try? container.decode(EventLocation.self, forKey: .location)
            self.memoryCount        = try container.decode(Int.self, forKey: .memoryCount)
        } catch {
            throw error
        }
    }
}

struct EventCategoryEntity: Hashable, Equatable {

    var name: String
    var events: [EventPanel]
    
    var hashValue: Int {
        get {
            return name.hashValue
        }
    }
    
    static func ==(lhs: EventCategoryEntity, rhs: EventCategoryEntity) -> Bool {
        if lhs.name != rhs.name {
            return false
        }
        if lhs.events.count != rhs.events.count {
            return false
        } else {
            for (_index, _event) in lhs.events.enumerated() {
                if (_event != rhs.events[_index]) {
                    return false
                }
            }
            return true
        }
    }
}

extension EventCategoryEntity: Decodable {
    private enum CodingKeys: String, CodingKey {
        case name = "categoryName"
        case events = "events"
    }
    
    init(from decoder: Decoder) throws {
        do {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            self.name = try container.decode(String.self, forKey: .name)
            self.events = try container.decode([EventPanel].self, forKey: .events)
        } catch {
            throw error
        }
    }
}

struct UserProfileHeader: Hashable, Equatable {
    var userID: String
    var nickname: String
    var guests: Int
    var events: Int
    var followers: Int
    var following: Int
    var description: String
    var photo: String
    var isFollowed: Bool

    static func ==(lhs: UserProfileHeader, rhs: UserProfileHeader) -> Bool {
        return lhs.userID == rhs.userID &&
                lhs.nickname == rhs.nickname &&
                lhs.guests == rhs.guests &&
                lhs.events == rhs.events &&
                lhs.followers == rhs.followers &&
                lhs.following == rhs.following &&
                lhs.description == rhs.description &&
                lhs.photo == rhs.photo &&
                lhs.isFollowed == rhs.isFollowed
    }
}

extension UserProfileHeader: Decodable {
    private enum EventApiResponseCodingKeys: String, CodingKey {
        case userID         = "user_id"
        case nickname       = "nickname"
        case guests         = "guests"
        case events         = "events"
        case followers      = "followers"
        case following      = "following"
        case description    = "description"
        case photo          = "photo"
        case isFollowed     = "status"
    }
    
    init(from decoder: Decoder) throws {
        do {
            let container       = try decoder.container(keyedBy: EventApiResponseCodingKeys.self)
            
            self.userID         = try container.decode(String.self, forKey: .userID)
            self.nickname       = try container.decode(String.self, forKey: .nickname)
            self.guests         = try container.decode(Int.self, forKey: .guests)
            self.events         = try container.decode(Int.self, forKey: .events)
            self.followers      = try container.decode(Int.self, forKey: .followers)
            self.following      = try container.decode(Int.self, forKey: .following)
            self.description    = try container.decode(String.self, forKey: .description)
            self.photo          = try container.decode(String.self, forKey: .photo)
            self.isFollowed     = try container.decode(Bool.self, forKey: .isFollowed)
        } catch {
            throw error
        }
    }
}

struct UserEditProfileSetting: Hashable, Equatable {
    var photo: String
    var nickname: String
    var description: String
    var email: String
    
    static func ==(lhs: UserEditProfileSetting, rhs: UserEditProfileSetting) -> Bool {
        if lhs.photo == rhs.photo &&
            lhs.nickname == rhs.nickname &&
            lhs.email == rhs.email &&
            lhs.description == rhs.description {
            return true
        } else {
            return false
        }
    }
    
    mutating func changeDescription(to newDescription: String) {
        self.description = newDescription
    }
}

extension UserEditProfileSetting: Decodable {
    private enum EventApiResponseCodingKeys: String, CodingKey {
        case photo = "photo"
        case nickname = "nickname"
        case description = "description"
        case email = "email"
    }
    
    init(from decoder: Decoder) throws {
        do {
            let container = try decoder.container(keyedBy: EventApiResponseCodingKeys.self)
            
            self.photo = try container.decode(String.self, forKey: .photo)
            self.nickname = try container.decode(String.self, forKey: .nickname)
            self.description = try container.decode(String.self, forKey: .description)
            self.email = try container.decode(String.self, forKey: .email)
        } catch {
            throw error
        }
    }
}

enum LoginStatusEntity: String, Decodable {
    case success = "success"
    case incorrectPass = "incorrectPass"
    case incorrectEmail = "incorrectEmail"
}

struct LoginEntity {
    var aesKey: String?
    var token: String?
    var userID: String?
    var status: LoginStatusEntity
}

extension LoginEntity: Decodable {
    private enum Keys: String, CodingKey {
        case aesKey = "aesKey"
        case token = "token"
        case userID = "userID"
        case status = "status"
    }
    
    init(from decoder: Decoder) throws {
        do {
            let container = try decoder.container(keyedBy: Keys.self)
            
            if let _aesKey = try? container.decode(String.self, forKey: .aesKey) {
                self.aesKey = _aesKey.rsaDecrypt()
            }
            
            self.token = try? container.decode(String.self, forKey: .token)
            self.userID = try? container.decode(String.self, forKey: .userID)
            self.status = try container.decode(LoginStatusEntity.self, forKey: .status)
        } catch {
            throw error
        }
    }
}

struct EventHashtag: Equatable {
    var name: String
    
    static func ==(lhs: EventHashtag, rhs: EventHashtag) -> Bool {
        return lhs.name == rhs.name
    }
    
    static func convert(hashtags: [EventHashtag]) -> [String] {
        var array: [String] = []
        for item in hashtags {
            array.append(item.name)
        }
        return array
    }
}

extension EventHashtag: Decodable {
    private enum Keys: String, CodingKey {
        case name = "hashtag_name"
    }
    
    init(from decoder: Decoder) throws {
        do {
            let container = try decoder.container(keyedBy: Keys.self)
            self.name = try container.decode(String.self, forKey: .name)
        } catch {
            throw error
        }
    }
}

struct MediaPanel: Hashable, Equatable {
    var mediaID: String
    var mediaPhoto: String
    var mediaTime: Date
    var mediaCreatorNickname: String
    var mediaCreatorID: String
    var eventID: String
    var eventAvatar: String
    var eventCreatorNickname: String
    var eventTitle: String
    var guestQuantity: Int
    var friendsQuantity: Int
    
    
    static func ==(lhs: MediaPanel, rhs: MediaPanel) -> Bool {
        return lhs.mediaID == rhs.mediaID
    }
}

extension MediaPanel: Decodable {
    private enum EventApiResponseCodingKeys: String, CodingKey {
        case mediaID                = "media_id"
        case mediaPhoto             = "media_photo"
        case mediaTime              = "media_time"
        case mediaCreatorNickname   = "media_creator_nickname"
        case mediaCreatorID         = "media_creator_id"
        case eventID                = "e_id"
        case eventAvatar            = "event_photo"
        case eventCreatorNickname   = "creator_nickname"
        case eventTitle             = "event_name"
        case guestQuantity          = "guests_quantity"
        case friendsQuantity        = "friends_quantity"
    }
    
    init(from decoder: Decoder) throws {
        do {
            let container               = try decoder.container(keyedBy: EventApiResponseCodingKeys.self)
            
            self.mediaID                = try container.decode(String.self, forKey: .mediaID)
            self.mediaPhoto             = try container.decode(String.self, forKey: .mediaPhoto)
            let _mediaTime              = try container.decode(Int.self, forKey: .mediaTime)
            self.mediaTime              = Date.init(timeIntervalSince1970: TimeInterval(_mediaTime))
            self.mediaCreatorNickname   = try container.decode(String.self, forKey: .mediaCreatorNickname)
            self.mediaCreatorID         = try container.decode(String.self, forKey: .mediaCreatorID)
            self.eventID                = try container.decode(String.self, forKey: .eventID)
            self.eventAvatar            = try container.decode(String.self, forKey: .eventAvatar)
            self.eventCreatorNickname   = try container.decode(String.self, forKey: .eventCreatorNickname)
            self.eventTitle             = try container.decode(String.self, forKey: .eventTitle)
            self.friendsQuantity        = try container.decode(Int.self, forKey: .friendsQuantity)
            self.guestQuantity     = try container.decode(Int.self, forKey: .guestQuantity)
        } catch {
            throw error
        }
    }
}

enum ChangeUpdateEntity: String, Decodable {
    case updated = "success"
    case isExisted = "isExisted"
}

struct ChangeUpdateStatus: Decodable, Equatable {
    var status: ChangeUpdateEntity
    
    static func ==(lhs: ChangeUpdateStatus, rhs: ChangeUpdateStatus) -> Bool {
        return lhs.status == rhs.status
    }
}

enum SucceedFailedStatus: String, Decodable {
    case succeed = "success"
    case failed = "failed"
}

struct SucceedFailedEntity: Decodable, Equatable {
    var status: SucceedFailedStatus
    
    static func ==(lhs: SucceedFailedEntity, rhs: SucceedFailedEntity) -> Bool {
        return lhs.status == rhs.status
    }
}

enum RegSecEntityStatus: String, Decodable {
    case success = "success"
    case failed = "failed"
    case isExisted = "isExisted"
}

struct RegSecEntity: Decodable {
    var status: RegSecEntityStatus
}

extension RegSecEntity {
    private enum Key: String, CodingKey {
        case status = "status"
    }
    
    init(from decoder: Decoder) throws {
        do {
            let container = try decoder.container(keyedBy: Key.self)
            self.status = try container.decode(RegSecEntityStatus.self, forKey: .status)
        } catch {
            throw error
        }
    }
}

struct ChangePassStatus: Decodable, Equatable {
    var status: ChangePasswordEntity
    
    static func ==(lhs: ChangePassStatus, rhs: ChangePassStatus) -> Bool {
        return lhs.status == rhs.status
    }
}

extension ChangePassStatus {
    private enum Key: String, CodingKey {
        case status = "status"
    }
    
    init(from decoder: Decoder) throws {
        do {
            let container = try decoder.container(keyedBy: Key.self)
            self.status = try container.decode(ChangePasswordEntity.self, forKey: .status)
        } catch {
            throw error
        }
    }
}

enum ChangePasswordEntity: String, Decodable {
    case incorrectPassword
    case succeed
    case failed
}

enum AuthVerifierEntity: String, Decodable {
    case invalid = "invalid"
    case success = "success"
}

enum FeedbackCategory: String, Decodable {
    case cat1 = "CAT1" // started following u
    case cat2 = "CAT2" //
    case cat3 = "CAT3"
    case cat4 = "CAT4" // went to event
    case cat5 = "CAT5"
    case cat6 = "CAT6"
    case cat7 = "CAT7"
    case cat8 = "CAT8"
    case cat9 = "CAT9"
    case cat10 = "CAT10"
    case cat11 = "CAT11"
    case cat12 = "CAT12"
    case cat13 = "CAT13"
    case cat14 = "CAT14"
    case catServer = "CAT0G"
    case catMoney = "CAT1G"
}

struct FeedbackEntity: Equatable {
    var id: String
    var body: String
    var time: Int
    var type: FeedbackCategory
    var nodeID: String?
    var quantity: Int
    var photo: String
    var creatorID: String
    var creatorPhoto: String
    var creatorUsername: String
    
    static func ==(lhs: FeedbackEntity, rhs: FeedbackEntity) -> Bool {
        return lhs.id == rhs.id
    }
}

extension FeedbackEntity: Decodable {
    private enum Key: String, CodingKey {
        case id = "id"
        case body = "body"
        case time = "time"
        case type = "type"
        case nodeID = "node_id"
        case quantity = "quantity"
        case photo = "photo"
        case creatorID = "creator_id"
        case creatorPhoto = "creator_photo"
        case creatorUsername = "creator_nickname"
    }
    
    init(from decoder: Decoder) throws {
        do {
            let container = try decoder.container(keyedBy: Key.self)
            
            self.id = try container.decode(String.self, forKey: .id)
            self.body = try container.decode(String.self, forKey: .body)
            self.time = try container.decode(Int.self, forKey: .time)
            self.type = try container.decode(FeedbackCategory.self, forKey: .type)
            self.nodeID = try? container.decode(String.self, forKey: .nodeID)
            self.quantity = try container.decode(Int.self, forKey: .quantity)
            self.photo = try container.decode(String.self, forKey: .photo)
            self.creatorID = try container.decode(String.self, forKey: .creatorID)
            self.creatorPhoto = try container.decode(String.self, forKey: .creatorPhoto)
            self.creatorUsername = try container.decode(String.self, forKey: .creatorUsername)
        } catch {
            print("COULD NOT PARSE")
            throw error
        }
    }
}

























