//
//  UserProfileViewController.swift
//  WentOut
//
//  Created by Fure on 25.07.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import Foundation
import UIKit

enum UserProfileState {
    case `default`
    case userPage(String)
}

typealias MediaURL = String

protocol UserProfileMemoryDelegate: class {
    func openStory(eventID: String, index: Int)
}

class UserProfileViewController: UICollectionViewController {
    
    lazy var headerDownloader: Downloader<UserProfileHeader, UserProfileHeaderParameters> = {
        if let ownerID = profileOwnerID {
            return Downloader<UserProfileHeader, UserProfileHeaderParameters>.init(resource: URL.UserProfile.header, param: UserProfileHeaderParameters.init(visiterID: LocalAuth.userID!, profileOwnerID: ownerID))
        } else {
            return Downloader<UserProfileHeader, UserProfileHeaderParameters>.init(resource: URL.UserProfile.header, param: UserProfileHeaderParameters.init(visiterID: LocalAuth.userID!, profileOwnerID: LocalAuth.userID!))
        }
    }()
    
    lazy var eventsDownloader: Downloader<[EventPanel], UserProfileEventParameters> = {
        if let ownerID = profileOwnerID {
            return Downloader<[EventPanel], UserProfileEventParameters>.init(resource: URL.UserProfile.events, param: UserProfileEventParameters.init(visiterID: LocalAuth.userID!, profileOwnerID: ownerID))
        } else {
            return Downloader<[EventPanel], UserProfileEventParameters>.init(resource: URL.UserProfile.events, param: UserProfileEventParameters.init(visiterID: LocalAuth.userID!, profileOwnerID: LocalAuth.userID!))
        }
    }()

    lazy var memoryDownloader: Downloader<[StoryEntity], UserProfileEventParameters> = {
        if let ownerID = profileOwnerID {
            return Downloader<[StoryEntity], UserProfileEventParameters>.init(resource: URL.UserProfile.memory, param: UserProfileEventParameters.init(visiterID: LocalAuth.userID!, profileOwnerID: ownerID))
        } else {
            return Downloader<[StoryEntity], UserProfileEventParameters>.init(resource: URL.UserProfile.memory, param: UserProfileEventParameters.init(visiterID: LocalAuth.userID!, profileOwnerID: LocalAuth.userID!))
        }
    }()
    
    func _report(userID: String) -> Downloader<SucceedFailedEntity, BlockUserParameters> {
        return Downloader<SucceedFailedEntity, BlockUserParameters>.init(resource: URL.Block.user, param: BlockUserParameters.init(userID: LocalAuth.userID!, profileOwnerID: userID))
    }
    
    let imageViewTag: Int = 445682
    
    lazy var noDataImageView: UIImageView? = {
        if let maxY = navigationController?.navigationBar.frame.maxY {
            let imageView = UIImageView.init(frame: CGRect.init(x: 0, y: maxY, width: UIScreen.width, height: UIScreen.width))
            imageView.tag = imageViewTag
            imageView.image = #imageLiteral(resourceName: "errorNoData")
            return imageView
        }
        return nil
    }()
    
//    let sizeOfMemoryList: CGSize = CGSize.init(width: UIScreen.width, height: 127.0 + 24)
    let sizeOfMemoryList: CGSize = CGSize.init(width: UIScreen.width, height: UIScreen.width / 2 + 24)
    
    var userProfileParams: (visiterID: String, profileOwnerID: String) {
        get {
            if let profileOwnerID = self.profileOwnerID {
                return (visiterID: self.visitorID, profileOwnerID: profileOwnerID)
            } else {
                return (visiterID: self.visitorID, profileOwnerID: self.visitorID)
            }
        }
    }
    
    lazy var visitorID = LocalAuth.userID!
    
    lazy var eventBundle: UserProfileEvent3CollectionView = {
        return Bundle.main.loadNibNamed(reuseIds[2], owner: self, options: nil)?[0] as! UserProfileEvent3CollectionView
    }()
    
    // this value setted after vc was initted
    var profileOwnerID: String?
    
    let minimumLineSpacingForSectionAt: CGFloat = 0.0 // 10.0
    
    var downloadedAllEventModel = false
    var eventModelIsDownloading = false

    var state: UserProfileState = .default
    
    let timeLimit: Int = Int(Date().timeIntervalSince1970)
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNetworking()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.view.frame.size.height = UIScreen.main.bounds.height
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
}

// Visuals
extension UserProfileViewController {
    private func setupViews() {
        setupCollectionView()
        setupViewsDependsOnState()
    }
    
    private func addNavItemsIsNeeded() {
        let settings = UIBarButtonItem.getSettingsBarButton(target: self, selector: #selector(openUserSettings))
        navigationItem.rightBarButtonItems = [settings]
    }
    
    private func setupCollectionView() {
        self.collectionView!.contentInset.bottom = UITabBar.height + 10.0
        for reuseID in self.reuseIds {
            self.collectionView!.register(UINib.init(nibName: reuseID, bundle: nil), forCellWithReuseIdentifier: reuseID)
        }
    }
    
    private func setupViewsDependsOnState() {
        switch self.state {
        case .default:
            print()
        case .userPage(_):
            print()
        }
    }
}

extension UserProfileViewController: UserProfileMemoryDelegate {
    func openStory(eventID: String, index: Int) {
        let storyboard = UIStoryboard.init(name: "Second", bundle: nil)
        let navController = storyboard.instantiateViewController(withIdentifier: "StoryPageNavVC") as! UINavigationController
        let storyController = navController.viewControllers.first! as! StoryPageViewController
        storyController.storyDownloader = memoryDownloader.clone()
        storyController.beginIndex = index
        navController.modalPresentationStyle = .overCurrentContext
        self.navigationController!.tabBarController?.present(navController, animated: true, completion: nil)
    }
}

// Networking Layer
extension UserProfileViewController {
    func setupNetworking() {
        downloadHeader()
        downloadMemory()
        downloadEvents()
    }
    
    private func downloadHeader() {
        if headerDownloader.data == nil {
            let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            activityIndicator.color = .gray
            self.navigationItem.titleView = activityIndicator
            activityIndicator.startAnimating()
        }
        
        headerDownloader.getFirst {
            if self.headerDownloader.data == nil {
                if self.view.viewWithTag(self.imageViewTag) == nil {
                    if let iv = self.noDataImageView {
                        self.view.addSubview(iv)
                    }
                }
            }
            self.navigationItem.titleView = nil
            self.navigationItem.title = self.headerDownloader.data?.nickname ?? ""
            self.collectionView?.reloadSections([0, 1, 2])
            self.addNavItemsIsNeeded()
        }
    }
    
    private func downloadMemory() {
        memoryDownloader.getFirst {
            self.collectionView?.reloadSections([1])
        }
    }
    
    private func downloadEvents() {
        eventsDownloader.getFirst {
            self.collectionView?.reloadSections([2])
        }
    }
}

extension UserProfileViewController {
    var reuseIds: [String] {
        get {
            return ["UserProfileHeaderCollectionViewCell", "UserProfileEventCollectionView", "UserProfileEvent3CollectionView", "UserProfileActionsCollectionViewCell", "UserPageMemoryListCollectionViewCell", "CollectionViewErrorImageCell"]
        }
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 3
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("comment")
        switch section {
        case 0:
            if let _ = self.headerDownloader.data {
                return 2
            }
            return 0
        case 1:
            guard let _ = self.headerDownloader.data else {
                return 0
            }
            return 1
        case 2:
            guard let _ = self.headerDownloader.data else {
                return 0
            }
            if eventsDownloader.firstDownloadIsFinished {
                if let count = self.eventsDownloader.data?.count {
                    if count == 0 {
                        return 1
                    } else {
                        return count
                    }
                } else {
                    return 1
                }
            } else {
                return self.eventsDownloader.data?.count ?? 0
            }
        default:
            return 0
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let data = eventsDownloader.data else {
            return
        }
        guard data.count > 0 else {
            return
        }
        if indexPath.section == 2 {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "EventProfileViewController") as! EventProfileViewController
            controller.eventID = self.eventsDownloader.data![indexPath.row].eventId
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.section == 2 {
            guard let events = eventsDownloader.data else {
                return
            }
            if indexPath.row == events.count - 1 {
                eventsDownloader.getNext(section: 2) { (paths) in
                    self.collectionView?.insertItems(at: paths)
                }
            }
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.section {
        case 0:
            if indexPath.row == 0 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIds[0], for: indexPath) as! UserProfileHeaderCollectionViewCell
                
                cell.delegate = self
                
                var userProfileHeaderState: UserProfileHeaderState!
                
                if let profileOwner = self.profileOwnerID {
                    if profileOwner == LocalAuth.userID! {
                        userProfileHeaderState = .owner
                    } else {
                        userProfileHeaderState = .otherUser(isFollowed: self.headerDownloader.data?.isFollowed ?? false)
                    }
                } else {
                    userProfileHeaderState = .owner
                }
                guard let model = self.headerDownloader.data else {
                    return cell
                }
                cell.configCell(username: model.nickname, followers: model.followers, following: model.following, events: model.events, guests: model.guests, userDescription: model.description, userAvatar: model.photo, state: userProfileHeaderState)
                return cell
            } else if indexPath.row == 1 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIds[3], for: indexPath) as! UserProfileActionsCollectionViewCell
                cell.configCell(statusFollow: self.headerDownloader.data!.isFollowed)
                cell.delegate = self
                return cell
            } else {
                return UICollectionViewCell()
            }
        case 1:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIds[4], for: indexPath) as! UserPageMemoryListCollectionViewCell
            cell.delegate = self.memoryDownloader
            cell.userProfileDelegate = self
            cell.configCell(superviewHeight: sizeOfMemoryList.height)
            return cell
        case 2:
            if let data = self.eventsDownloader.data {
                if data.count == 0 {
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIds[5], for: indexPath) as! CollectionViewErrorImageCell
                    if let ownerID = profileOwnerID {
                        if ownerID == LocalAuth.userID! {
                            cell.configCell(image: UIImage.init(named: "errorNoEventsYou")!)
                        } else {
                            cell.configCell(image: UIImage.init(named: "errorNoEventsUser")!)
                        }
                    } else {
                        cell.configCell(image: UIImage.init(named: "errorNoEventsYou")!)
                    }
                    return cell
                } else {
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIds[2], for: indexPath) as! UserProfileEvent3CollectionView
                    cell.configCell(model: data[indexPath.row])
                    return cell
                }
            } else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIds[5], for: indexPath) as! CollectionViewErrorImageCell
                if let ownerID = profileOwnerID {
                    if ownerID == LocalAuth.userID! {
                        cell.configCell(image: UIImage.init(named: "errorNoEventsYou")!)
                    } else {
                        cell.configCell(image: UIImage.init(named: "errorNoEventsUser")!)
                    }
                } else {
                    cell.configCell(image: UIImage.init(named: "errorNoEventsYou")!)
                }
                return cell
            }
        default:
            return UICollectionViewCell()
        }
    }
}

extension UserProfileViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return self.minimumLineSpacingForSectionAt
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if section == 1 {
            return UIEdgeInsets.init(top: 0, left: 0, bottom: 7, right: 0)
        }
        return UIEdgeInsets.zero
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch indexPath.section {
        case 0:
            if indexPath.row == 0 {
                let bundle = Bundle.main.loadNibNamed(reuseIds[0], owner: self, options: nil)?[0] as! UserProfileHeaderCollectionViewCell
                guard let model = self.headerDownloader.data else {
                    return bundle.currentSize
                }
                bundle.bundleConfigCell(username: model.nickname, followers: model.followers, following: model.following, events: model.events, guests: model.guests, userDescription: model.description)
                return bundle.currentSize
            } else if indexPath.row == 1 {
                if let ownerID = profileOwnerID {
                    if ownerID == LocalAuth.userID! {
                        return CGSize.init(width: UIScreen.width, height: 0.0)
                    }
                    return CGSize.init(width: UIScreen.width, height: 50.0)
                }
                return CGSize.init(width: UIScreen.width, height: 0.0)
            } else {
                return CGSize.zero
            }
        case 1:
            if let memories = memoryDownloader.data {
                if memories.count > 0 {
                    return self.sizeOfMemoryList
                }
            }
            return CGSize.init(width: UIScreen.width, height: 5.0)
        case 2:
            let bundle = eventBundle
            guard let data = self.eventsDownloader.data else {
                if eventsDownloader.firstDownloadIsFinished {
                    return CollectionViewErrorImageCell.currentSize
                }
                return CGSize.zero
            }
            if data.count == 0 {
                return CollectionViewErrorImageCell.currentSize
            } else {
//                return bundle.currentSize
                return UserProfileEvent3CollectionView.currentSize
            }
        default:
            return CGSize.zero
        }
    }
}

extension UserProfileViewController: DefaultStateVCDelegate {
    func scrollToTop() {
        if collectionView?.numberOfItems(inSection: 0) > 0 {
            collectionView?.scrollToItem(at: IndexPath.init(row: 0, section: 0), at: .top, animated: true)
        }
    }
}

extension UserProfileViewController: UserProfileHeaderCollectionViewDelegate {
    func followersWasTapped() {
        let storyboard = UIStoryboard.init(name: "Second", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "UserListViewController") as! UserListViewController
        controller.modalPresentationStyle = .overCurrentContext
        
        let downloader = Downloader<[UserMini], UserListParameters>.init(resource: URL.UserProfile.followers, param: UserListParameters.init(userID: LocalAuth.userID!, profileOwnerID: profileOwnerID ?? LocalAuth.userID!))
        
        controller.setupVC(downloadable: downloader, title: "Followers")
        
//        controller.setupVC(url: URL.UserProfile.followers, profileOwnerID: profileOwnerID ?? LocalAuth.userID!, title: "Followers")
        self.navigationController!.pushViewController(controller, animated: true)
    }
    
    func followingsWasTapped() {
        let storyboard = UIStoryboard.init(name: "Second", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "UserListViewController") as! UserListViewController
        controller.modalPresentationStyle = .overCurrentContext
        
        let downloader = Downloader<[UserMini], UserListParameters>.init(resource: URL.UserProfile.followings, param: UserListParameters.init(userID: LocalAuth.userID!, profileOwnerID: profileOwnerID ?? LocalAuth.userID!))
        
        controller.setupVC(downloadable: downloader, title: "Followings")
        
//        controller.setupVC(url: URL.UserProfile.followings, profileOwnerID: profileOwnerID ?? LocalAuth.userID!, title: "Followings")
        self.navigationController!.pushViewController(controller, animated: true)
    }
    
    func eventsWasTapped() {
        // NEEDED BE TESTED
        print("events was tapped")
        if let eventCount = collectionView?.numberOfItems(inSection: 2) {
            if eventCount > 0 {
                let topEventIP = IndexPath.init(row: 0, section: 2)
                collectionView?.scrollToItem(at: topEventIP, at: .top, animated: true)
            }
        }
    }
}

extension UserProfileViewController: ProfileSettingsProtocol {
    func settingsWasClosed() {
        setupNetworking()
    }
}

extension UserProfileViewController {
    @objc func openUserSettings() {
        guard let owner = self.profileOwnerID else {
            setupSettingsButtonForOwner()
            return
        }
        if owner != LocalAuth.userID! {
            setupSettingsButtonForStranger()
        } else {
            setupSettingsButtonForOwner()
        }
    }
    
    private func setupSettingsButtonForOwner() {
        let storyboard = UIStoryboard.init(name: "Second", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ProfileSettingsNavController") as! UINavigationController
        controller.modalPresentationStyle = .overCurrentContext
        if let topVC = controller.viewControllers.first as? ProfileSettingsViewController {
            topVC.delegate = self
        }
        self.tabBarController?.present(controller, animated: true, completion: nil)
    }
    
    private func setupSettingsButtonForStranger() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let action1 = UIAlertAction(title: "Block", style: .default) { (action: UIAlertAction) in
            self.reportUser()
        }
        
        let action2 = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            print("cancel")
        }
        
        alertController.addAction(action1)
        alertController.addAction(action2)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func reportUser() {
        self.showLoadingIndicator()
        guard let owner = self.profileOwnerID else {
            return
        }
        guard owner != LocalAuth.userID! else {
            return
        }
        let req = _report(userID: owner)
        req.getFirst {
            if let data = req.data {
                self.removeLoadingIndicator()
                switch data.status {
                case .succeed:
                    self.navigationController?.popViewController(animated: true)
                case .failed:
                    self.showMessage(text: "Error!", type: .error)
                }
            }
        }
    }
    
    private func makeFollowRequest(status: UserFollowStatus, callback: @escaping (Bool)->()) {
        
        let param = FollowUserParameters.init(status: status, targetID: userProfileParams.profileOwnerID, newFollowerID: LocalAuth.userID!)
        
        webservice.load(resource: ResList.follow(param)) { (_result, _error) in
            if let result = _result {
                if let status = result.data?.status {
                    switch status {
                    case .succeed:
                        callback(true)
                    case .failed:
                        callback(false)
                    }
                } else {
                    callback(false)
                }
            } else {
                callback(false)
            }
        }
    }
    
    @objc func openUserReport() {
        print("report user")
    }
}

extension UserProfileViewController: UserProfileActionsDelegate {
    func followAction(status: UserFollowStatus, callback: @escaping (Bool) -> ()) {
        
        switch status {
        case .follow:
            self.makeFollowRequest(status: status) { (success) in
                if success {
                    self.headerDownloader.data?.isFollowed = true
                    callback(true)
                }
            }
        case .unfollow:
            let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            let action1 = UIAlertAction(title: "Unfollow", style: .default) { (action: UIAlertAction) in
                self.makeFollowRequest(status: status, callback: { (success) in
                    if success {
                        self.headerDownloader.data?.isFollowed = false
                        callback(true)
                    }
                })
            }
            
            let action2 = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                callback(false)
            }
            
            alertController.addAction(action1)
            alertController.addAction(action2)
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func invite() {
        guard let inviteUserID = profileOwnerID else {
            return
        }
        let storyboard = UIStoryboard(name: "Second", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "UserInviteNavController") as! UINavigationController
        if let vc = controller.viewControllers.first as? UserInviteViewController {
            vc.inviteUserID = inviteUserID
            self.present(controller, animated: true, completion: nil)
        }
    }
}

extension UserProfileViewController: UserProfileEventCollectionViewDelegate {
    func userAvatarWasTapped(indexPath: IndexPath) {
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let controller = storyboard.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
//        controller.profileOwnerID = self.eventsDownloader.data![indexPath.row].userId
//        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func usernameWasTapped(indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
        controller.profileOwnerID = self.eventsDownloader.data![indexPath.row].userId
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func eventAvatarWasTapped(indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "EventProfileViewController") as! EventProfileViewController
        controller.eventID = self.eventsDownloader.data![indexPath.row].eventId
        self.navigationController?.pushViewController(controller, animated: true)
    }
}













