//
//  AppDelegate.swift
//  eventsRM
//
//  Created by Студент on 27.10.16.
//  Copyright © 2016 uzuner. All rights reserved.
//

import UIKit
import Foundation
import SwiftSocket
import SocketIO
import Alamofire

import RSASwiftGenerator
import AES256CBC
import CryptoSwift
import SwiftyRSA

import CoreLocation
import CoreData

import UserNotifications
//import NotificationFramework

/* ENCRYPT
 let publicKey = try PublicKey(pemNamed: "public")
 let clear = try ClearMessage(string: "Clear Text", using: .utf8)
 let encrypted = try clear.encrypted(with: publicKey, padding: .PKCS1)
 
 // Then you can use:
 let data = encrypted.data
 let base64String = encrypted.base64String
 */

/** DECRYPT
 let privateKey = try PrivateKey(pemNamed: "private")
 let encrypted = try EncryptedMessage(base64Encoded: "AAA===")
 let clear = try encrypted.decrypted(with: privateKey, padding: .PKCS1)
 
 // Then you can use:
 let data = clear.data
 let base64String = clear.base64String
 let string = clear.string(using: .utf8)
 
 */

//protocol ApplicationWillResignActiveDelegate {
//    func applicationWillResignActive(_ application: UIApplication)
//}

protocol InternetObserverDelegate {
    func reachabilityStatusHasChanged(to status: Network.Status)
}

extension InternetObserverDelegate {
    var status: Network.Status? {
        get {
            return Network.reachability?.status
        }
    }
}

let webservice = Webservice()

protocol SocketProtocol {
    var socket: SocketManager! { get set }
}

func encodeURLToNotURL(urlString: String) -> String {
    return urlString.replacingOccurrences(of: "/", with: "%", options: .literal, range: nil)
}

func decodeNotURLToURL(notURLString: String) -> String {
    return notURLString.replacingOccurrences(of: "%", with: "/", options: .literal, range: nil)
}

func saveImageToLocalStorage(image: UIImage, imageURL: String, completion: ((Bool)->())?) {
    DispatchQueue.global().async {
        print(imageURL)
        let docDir = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        let fileURL = docDir.appendingPathComponent(imageURL)
        print(fileURL)
        let image = image
        let imageData = UIImageJPEGRepresentation(image, 4.0)!
        do {
            try imageData.write(to: fileURL)
            completion?(true)
        } catch {
            completion?(false)
        }
    }
}

func executionTime(block: ()->()) {
    let methodStart = Date()
    
    block()
    
    let methodFinish = Date()
    let executionTime = methodFinish.timeIntervalSince(methodStart)
    print("Execution time: \(executionTime)")
}

func getImageFromLocalStorage(imageURL: String, completion: ((UIImage?)->())?) {
    DispatchQueue.global().async {
        let docDirCheck = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        let fileURL = docDirCheck.appendingPathComponent(encodeURLToNotURL(urlString: imageURL))
        
        //    if FileManager.default.fileExists(atPath: fileURL.path) {
        if let data = try? Data.init(contentsOf: fileURL) {
            if let image = UIImage.init(data: data) {
                DispatchQueue.main.async {
                    completion?(image)
                }
                return
            }
        }
        DispatchQueue.main.async {
            completion?(nil)
        }
    }
}

struct AesKey {
    var key: String
    var data: [UInt8]
    
    init(key: String) {
        self.key = key
        self.data = [UInt8](Data.init(base64Encoded: key)!)
    }
}

extension AesKey: Decodable {
    private enum CodingKeys: String, CodingKey {
        case key = "key"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let incomingKey = try container.decode(String.self, forKey: .key)
        self.key = incomingKey.rsaDecrypt()
        self.data = [UInt8](Data.init(base64Encoded: self.key)!)
    }
}

var aesKey: AesKey?

var pushTokenID: String? {
    didSet {
        print("push token =", pushTokenID ?? "ERROR")
    }
}

var userLocation: CLLocationCoordinate2D?

let appVersion = 1.4

@UIApplicationMain
//class AppDelegate: UIResponder, UIApplicationDelegate, SocketProtocol {
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    static let appPageID = "rootAppPage"
    static let signUpPageID = "rootSignUpPage"
    
    var locationManager: CLLocationManager!
    
    static var applicationWasAlreadyLaunched = false
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "CoreData")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error {
                fatalError("Unresolved error, \((error as NSError).userInfo)")
            }
        })
        return container
    }()
    
    func _checkAppVersion(param: AppVersionParam) -> Downloader<SucceedFailedEntity, AppVersionParam> {
        return Downloader<SucceedFailedEntity, AppVersionParam>.init(resource: URL.App.checkVersion, param: param)
    }
    
    func _sendPushToken(param: PushTokenParam) -> Downloader<SucceedFailedEntity, PushTokenParam> {
        return Downloader<SucceedFailedEntity, PushTokenParam>.init(resource: URL.App.pushToken, param: param)
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        print("didFinishLaunchingWithOptions")
        initReachability()
        
        AppDelegate.applicationWasAlreadyLaunched = true
        
        self.requestPermissionsWithCompletionHandler { (finish) in
            DispatchQueue.main.async {
                if finish {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
        }
        
        if let _ = LocalAuth.getHeader() {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: AppDelegate.appPageID)
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = vc
        } else {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: AppDelegate.signUpPageID)
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = vc
        }
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        print("applicationWillResignActive")
        
        // close chat and disconnect socket if needed
        if let vc = currentVC as? MessageViewController {
            vc.navigationController?.popViewController(animated: true)
            vc.socketDisconnect()
        }
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        print("applicationDidEnterBackground")
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        print("applicationWillEnterForeground")
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        print("applicationDidBecomeActive")
        checkAppVersion()
//        retrieveUserLocation()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        print("applicationWillTerminate")
    }
}

//extension AppDelegate: CLLocationManagerDelegate {
//    
//    private func retrieveUserLocation() {
//        if (CLLocationManager.locationServicesEnabled()) {
//            locationManager = CLLocationManager()
//            locationManager.delegate = self
//            locationManager.desiredAccuracy = kCLLocationAccuracyBest
//            locationManager.requestAlwaysAuthorization()
//            locationManager.startUpdatingLocation()
//        }
//    }
//    
//    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        let location = locations.last! as CLLocation
//        
//        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
//        
//        userLocation = center
//        
//        print("user location =", userLocation ?? "error")
//        
//        locationManager.stopUpdatingLocation()
//    }
//}

extension AppDelegate {
    private func checkAppVersion() {
        let req = _checkAppVersion(param: AppVersionParam.init(version: appVersion))
        req.getFirst {
            if let data = req.data {
                switch data.status {
                case .failed:
                    print("122 version failed")
                    let storyboard = UIStoryboard(name: "Second", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "appVersionErrorVC")
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController = vc
                case .succeed:
                    print("122 version success")
                }
            }
        }
    }
}

extension AppDelegate {
    private func requestPermissionsWithCompletionHandler(completion: ((Bool) -> ())? = nil) {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { [weak self] (granted, error) in
            guard error == nil else {
                completion?(false)
                return
            }
            if granted {
                UNUserNotificationCenter.current().delegate = self
                self?.setNotificationCategories()
            }
            completion?(granted)
        }
    }
    
    private func setNotificationCategories() {
        typealias category = NotificationCategory
        UNUserNotificationCenter.current().setNotificationCategories([category.cat1, category.cat2, category.cat3, category.cat4, category.cat5, category.cat6, category.cat7, category.cat8, category.cat9, category.cat10, category.cat11, category.cat12, category.cat13, category.cat14, category.cat0g, category.cat0g, category.cat1g])
    }
    
    private func enableRemoteNotificationFeatures() { }
    
    private func forwardTokenToServer(token: String) {
        let req = _sendPushToken(param: PushTokenParam.init(userID: LocalAuth.userID!, pushToken: token))
        req.getFirst {
            if let data = req.data {
                switch data.status {
                case .succeed:
                    print("token was sent successfully")
                case .failed:
                    print("token sent was failed")
                }
            } else {
                print("token data did not parse")
            }
        }
    }
    
    private func disableRemoteNotificationFeatures() { }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        self.enableRemoteNotificationFeatures()
        let token = deviceToken.map { String(format: "%02hhx", $0) }.joined()
        self.forwardTokenToServer(token: token)
        pushTokenID = token
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Remote notification support is unavailable due to error: \(error.localizedDescription)")
        self.disableRemoteNotificationFeatures()
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        if application.applicationState == .active {
            print("REMOTE NOTIFICATION CAME IN APP")
            UIApplication.shared.applicationIconBadgeNumber = 0
            // If Current ViewController == Notification's Destination ViewController, Then do not show NotificationView, but do UI update of Current ViewController
            // Else Show NotificationView
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.badge, .alert, .sound])
    }
}

// Reachability
extension AppDelegate {
    private func initReachability() {
        do {
            Network.reachability = try Reachability(hostname: "www.google.com")
            do {
                try Network.reachability?.start()
                setupReachabilityObserver()
            } catch let error as Network.Error {
                print(error)
            } catch {
                print(error)
            }
        } catch {
            print(error)
        }
    }
    
    private func setupReachabilityObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(internetObserverEvent), name: .flagsChanged, object: Network.reachability)
    }
    
    @objc private func internetObserverEvent() {
        DispatchQueue.main.async { [weak self] in
            guard let status = Network.reachability?.status else { return }
            guard let currentVC = self?.currentVC as? InternetObserverDelegate else { return }
            
            currentVC.reachabilityStatusHasChanged(to: status)
        }
        
    }
    
    var currentVC: UIViewController? {
        get {
            guard let window = UIApplication.shared.delegate?.window else {
                return nil
            }
            guard let rootVC = window?.rootViewController else {
                return nil
            }
            if let navBar = rootVC as? UINavigationController {
                return navBar.visibleViewController
            }
            if let tabBarVC = rootVC as? UITabBarController {
                if let navBar = tabBarVC.selectedViewController as? UINavigationController {
                    return navBar.visibleViewController
                } else {
                    return tabBarVC.selectedViewController
                }
            }
            return rootVC
        }
    }
}

