//
//  CEDescriptionViewController.swift
//  WentOut
//
//  Created by Fure on 29.11.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import UIKit

class CEDescriptionViewController: UIViewController {
    
    @IBOutlet weak var textViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var defaultText: String?
    
    lazy var doneButton: UIBarButtonItem = {
        let button = UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(doneButtonAction))
        button.isEnabled = false
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        notificationOn()
    }
}

extension CEDescriptionViewController {
    @objc private func doneButtonAction() {
        if let rootVC = self.navigationController?.viewControllers.first as? PublishEventDelegate {
            rootVC.add(eventDesc: textView.text)
            self.navigationController?.popViewController(animated: true)
        }
    }
}

extension CEDescriptionViewController {
    private func setupViews() {
        navigationItem.rightBarButtonItems = [doneButton]
        navigationItem.title = "Description"
        textView.delegate = self
        textView.becomeFirstResponder()
        textView.font = UIFont.init(name: "Helvetica", size: 21.0)
    }
    
    private func notificationOn() {
        NotificationCenter.default.addObserver(self, selector: #selector(handle(keyboardShowNotification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    }
    
    @objc private func handle(keyboardShowNotification notification: Notification) {
        if let userInfo = notification.userInfo, let keyboardRectangle = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            setupTextView(keyboardHeight: keyboardRectangle.height)
        }
    }
    
    private func setupTextView(keyboardHeight: CGFloat) {
        let topMargin = abs(textView.convert(textView.frame, from: view).minY)
        textViewHeightConstraint.constant = UIScreen.height - topMargin - keyboardHeight
        view.layoutIfNeeded()
        textView.text = defaultText ?? ""
    }
}

extension CEDescriptionViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
//        let currentText = textView.text
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)

        if newText.count > 1000 {
            return false
        }

        doneButton.isEnabled = true
        
        return true
    }
}








