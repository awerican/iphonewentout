//
//  LoadingView.swift
//  WentOut
//
//  Created by Fure on 13.08.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import UIKit

class LoadingView: UIView, Nibable {
    var bundleName: String = "LoadingView"
    
    @IBOutlet var heightConstraint: NSLayoutConstraint!
    @IBOutlet var contentView: UIView!
    @IBOutlet private var activityIndicator: UIActivityIndicatorView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func configCell(_ height: CGFloat) {
        self.heightConstraint.constant = height
    }

    func setActivityIndicatorStyle(color: UIColor?) {
        self.activityIndicator.color = color
    }
    
    private func commonInit() {
        self.setupViewFromNib()
        self.activityIndicator.startAnimating()
        self.backgroundColor = .clear
    }
}
