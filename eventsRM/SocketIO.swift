//
//  SocketIO.swift
//  WentOut
//
//  Created by Fure on 27.06.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import Foundation
import SocketIO
import CoreLocation

typealias GeoHash = String

enum SocketConnectionStatus: Equatable {
    case alreadyConnected
    case success
    case failed([Any])
    case unknown
    case isConnecting
    
    static func ==(lhs: SocketConnectionStatus, rhs: SocketConnectionStatus) -> Bool {
        switch (lhs, rhs) {
        case (.alreadyConnected, .alreadyConnected):
            return true
        case (.success, .success):
            return true
        case (let .failed(a1), let .failed(a2)):
            return a1.count == a2.count
        case (.unknown, .unknown):
            return true
        default:
            return false
        }
    }
}

enum SocketManagerType {
    case map
    case news
    case chat
}

protocol SocketBasicProtocol {
    func establishConnection(callback: @escaping (SocketConnectionStatus)->())
    func closeConnection()
}

protocol SocketIOManagerProtocol: SocketBasicProtocol {
    var socket: SocketIOClient { get set }
    var type: SocketManagerType { get }
}

class SocketController {
    private static var socketManagers: [SocketManagerType: SocketIOManagerProtocol] = [:]
    
    var currentSocketConnections: Int {
        return SocketController.socketManagers.count
    }
    
    func push(manager: SocketIOManagerProtocol) {
        guard SocketController.socketManagers[manager.type] == nil else {
            return
        }
        SocketController.socketManagers[manager.type] = manager
    }
    
    func remove(manager: SocketIOManagerProtocol) {
        SocketController.socketManagers.removeValue(forKey: manager.type)
    }
}

class SocketIOManager: SocketIOManagerProtocol {
    private struct SocketEventNames {
        static let connection = "connectionRes"
        static let error = "error"
    }
    
    private var manager: SocketManager
    var socket: SocketIOClient
    var type: SocketManagerType
    var ip: URL
    
    init(ip: URL, type: SocketManagerType) {
        self.ip = ip
        self.manager = SocketManager(socketURL: ip)
        self.socket = manager.defaultSocket
        self.type = type
    }

    func establishConnection(callback: @escaping (SocketConnectionStatus)->()) {
        if socket.status == .notConnected || socket.status == .disconnected {
            socket.connect(timeoutAfter: 15) {
                callback(.failed(["timeoutAfter fired"] as [Any]))
            }
            socket.once(SocketEventNames.connection) { (data, ack) in
                self.subscribe()
                callback(.success)
            }
            socket.once(SocketEventNames.error) { (data, ack) in
//                switch self.type {
//                case .map:
//                    SocketMapManager.resetup(delegate:)
//                case .news:
//                    print()
//                }
                print(data)
                print(ack)
                callback(.failed(data))
            }
        } else if socket.status == .connected {
            callback(.alreadyConnected)
        } else {
            callback(.isConnecting)
        }
    }
    func closeConnection() {
        socket.disconnect()
        socket.removeAllHandlers()
    }
    
    func subscribe() { }
}



protocol SocketMapManagerPublicAPI: SocketBasicProtocol {
    var delegate: SocketMapManagerEvents? { get set }
    var socket: SocketIOClient { get }
    func askForEventsByRegion(zoom: Int, tlc: GeoHash, brc: GeoHash)
}

protocol SocketMapManagerEvents {
    func regionDidChangedResponse(data: [Any])
    func eventAmountDidChangedResponse(data: [Any])
}

protocol SocketChatManagerPublicAPI: SocketBasicProtocol {
    var delegate: SocketChatManagerEvents? { get set }
    func joinEventChat(eventID: String, callback: @escaping (SocketConnectionStatus)->())
    func closeConnection()
}

protocol SocketChatManagerEvents {
    func receivedNewMessage(data: [Any])
}

//class SocketChatManager: SocketIOManager, SocketChatManagerPublicAPI {
//
//    private struct SocketEventNames {
//        static let joinRoomRes = "joinRoomRes"
//        static let joinRoomReq = "joinRoomReq"
//        static let newMessageRes = "newMessageRes"
//        static let connectionRes = "connectionRes"
//    }
//
//    var delegate: SocketChatManagerEvents?
//
//    static var shared: SocketChatManagerPublicAPI = SocketChatManager()
//
//    func joinEventChat(eventID: String, callback: @escaping (SocketConnectionStatus) -> ()) {
//        let json = ["eventID": eventID]
//        print("askForEventsByRegion, socket status = ", socket.status)
//        if socket.status == .connected {
//            socket.emit(SocketEventNames.joinRoomReq, [json])
//        } else if socket.status != .connecting {
//            SocketMapManager.shared.establishConnection { [unowned self] (status) in
//                self.socket.emit(SocketEventNames.joinRoomReq, [json])
//            }
//        }
//    }
//
//    private init() {
//        super.init(ip: URL.socketChat, type: .chat)
//    }
//}

class SocketMapManager: SocketIOManager, SocketMapManagerPublicAPI {
    
    static var shared: SocketMapManagerPublicAPI = SocketMapManager()
    
    static func resetup(delegate: SocketMapManagerEvents) {
        let manager = SocketMapManager()
        manager.delegate = delegate
        SocketMapManager.shared = manager
    }
    
    private struct SocketEventNames {
        static let eventAmountRes = "eventAmountRes"
        static let regionEventReq = "regionEventReq"
        static let regionEventRes = "regionEventRes"
        static let connectionRes = "connectionRes"
    }
    
    var delegate: SocketMapManagerEvents?
    
    override func subscribe() {
        super.subscribe()
        socket.on(SocketEventNames.regionEventRes) { (data, ack) in
            self.delegate?.regionDidChangedResponse(data: data)
        }
        socket.on(SocketEventNames.eventAmountRes) { (data, ack) in
            self.delegate?.eventAmountDidChangedResponse(data: data)
        }
    }
    
    func askForEventsByRegion(zoom: Int, tlc: GeoHash, brc: GeoHash) {
        let json = ["zoom": zoom.description, "tlc": tlc, "brc": brc]
        print("askForEventsByRegion, socket status = ", socket.status)
        if socket.status == .connected {
            socket.emit(SocketEventNames.regionEventReq, with: [json])
        } else if socket.status != .connecting {
            SocketMapManager.shared.establishConnection { [unowned self] (status) in
                self.socket.emit(SocketEventNames.regionEventReq, with: [json])
            }
        }
    }
    
    private init() {
        print("map url =", URL.socketMapServerUrl)
        super.init(ip: URL.socketMapServerUrl, type: .map)
    }
}

