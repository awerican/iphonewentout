//
//  Int.swift
//  WentOut
//
//  Created by Fure on 15.02.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import Foundation

extension UInt64 {
    func withCommas() -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        return numberFormatter.string(from: NSNumber(value:self))!
    }
}
