//
//  UIFont.swift
//  WentOut
//
//  Created by Fure on 09.10.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    func sizeOfString(string: String) -> CGSize {
        return (string as NSString).size(withAttributes: [kCTFontAttributeName as NSAttributedStringKey:self])
    }
    func sizeOfStringConstrained(string: String, constrainedToWidth width: Double) -> CGSize {
        return NSString(string: string).boundingRect(with: CGSize(width: width, height: .greatestFiniteMagnitude), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [kCTFontAttributeName as NSAttributedStringKey: self], context: nil).size
    }
}
