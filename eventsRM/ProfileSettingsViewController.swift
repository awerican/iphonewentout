//
//  ProfileSettingsViewController.swift
//  WentOut
//
//  Created by Fure on 29.07.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import UIKit

protocol ProfileSettingsProtocol: class {
    func settingsWasClosed()
}

class ProfileSettingsViewController: UICollectionViewController {
    
    @IBAction func closeAction(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true) {
            self.delegate?.settingsWasClosed()
        }
//        self.dismiss(animated: true, completion: nil)
    }
    
    weak var delegate: ProfileSettingsProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
}

extension ProfileSettingsViewController {
    fileprivate func setupViews() {
        view.backgroundColor = UIColor.init(hex: "FAFAFA")
        setupNavBar()
        setupCollectionView()
    }
    
    private func setupNavBar() {
        navigationItem.title = "Settings"
    }
    
    fileprivate func setupCollectionView() {
//        self.collectionView!.contentInset.bottom = UITabBar.height + 6.0
        self.collectionView!.backgroundColor = .clear
        for reuseID in self.reuseIDs {
            self.collectionView!.register(UINib.init(nibName: reuseID, bundle: nil), forCellWithReuseIdentifier: reuseID)
        }
    }
    
    fileprivate func loggingOut() {
        guard let window = UIApplication.shared.keyWindow else {
            return
        }
        
        guard let rootViewController = window.rootViewController else {
            return
        }
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: AppDelegate.signUpPageID)
        vc.view.frame = rootViewController.view.frame
        vc.view.layoutIfNeeded()
        
        UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
            window.rootViewController = vc
        }, completion: { completed in
            LocalAuth.removeAll()
        })
    }
}

extension ProfileSettingsViewController {
    
    var reuseIDs: [String] {
        get {
            return ["ProfileSettingsLogoutCell", "ProfileSettingsArrowCell"]
        }
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return 2
        }
        if section == 1 {
            return 1
        }
        return 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let layout = UICollectionViewFlowLayout()
                layout.scrollDirection = .vertical
                let controller = ProfileSettingsLevel1VC.init(collectionViewLayout: layout)
                self.navigationController?.pushViewController(controller, animated: true)
            }
            if indexPath.row == 1 {
                let layout = UICollectionViewFlowLayout()
                layout.scrollDirection = .vertical
                let controller = ProfileSettingsSecurityListVC.init(collectionViewLayout: layout)
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
        if indexPath.section == 1 {
            loggingOut()
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIDs[1], for: indexPath) as! ProfileSettingsArrowCell
                cell.configCell(title: "Edit My Profile")
                return cell
            }
            if indexPath.row == 1 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIDs[1], for: indexPath) as! ProfileSettingsArrowCell
                cell.configCell(title: "Security")
                return cell
            }
            return UICollectionViewCell()
        }
        if indexPath.section == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIDs[0], for: indexPath) as! ProfileSettingsLogoutCell
            return cell
        }
        return UICollectionViewCell()
    }
}

extension ProfileSettingsViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 0 {
            return ProfileSettingsArrowCell.currentSize
        }
        if indexPath.section == 1 {
            return ProfileSettingsLogoutCell.currentSize
        }
        return CGSize.zero
    }
}









