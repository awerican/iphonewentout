//
//  ResourceList.swift
//  WentOut
//
//  Created by Fure on 15.03.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation

class ResList {
    private init() { }
}

extension ResList {
    
//    static var verifyAuth: (()->Resource<ResponseEntity<AuthVerifierEntity>, EmptyParameters>) {
//        get {
//            return getNoParam(url: URL.Auth.verify)
//        }
//    }
    
    static func _get<P: Encodable & Decodable, R: Equatable & Decodable>(origin: URL, next: URL?) -> ((P)->Resource<ResponseEntity<R>, P>) {
        if let next = next {
            return getWithParam(url: next)
        } else {
            return getWithParam(url: origin)
        }
    }
    
    static func newsFeedback(url: URL? = nil) -> ((SelectNews)->Resource<ResponseEntity<[FeedbackEntity]>, SelectNews>) {
        if let url = url {
            return getWithParam(url: url)
        } else {
            return getWithParam(url: URL.News.feedback)
        }
    }
    
    static func newsMemory(url: URL? = nil) -> ((SelectNews)->Resource<ResponseEntity<[NewsMemory]>, SelectNews>) {
        if let url = url {
            return getWithParam(url: url)
        } else {
            return getWithParam(url: URL.News.memory)
        }
    }
    
//    static var newsMemory: ((SelectNewsMemory)->Resource<ResponseEntity<[NewsMemory]>, SelectNewsMemory>) {
//        get {
//            return getWithParam(url: URL.News.memory)
//        }
//    }
    
    static var insertMemory: ((InsertMemoryParameters)->Resource<ResponseEntity<SucceedFailedEntity>, InsertMemoryParameters>) {
        get {
            return getWithParam(url: URL.Event.insertMemory)
        }
    }
    
    static var invite: ((EventInviteParameters)->Resource<ResponseEntity<SucceedFailedEntity>, EventInviteParameters>) {
        get {
            return getWithParam(url: URL.Event.invite)
        }
    }
    
    static var goEvent: ((EventGoParameters)->Resource<ResponseEntity<SucceedFailedEntity>, EventGoParameters>) {
        get {
            return getWithParam(url: URL.Event.go)
        }
    }
    
    static var follow: ((FollowUserParameters)->Resource<ResponseEntity<SucceedFailedEntity>, FollowUserParameters>) {
        get {
            return getWithParam(url: URL.UserProfile.follow)
        }
    }
    
    static var login: ((LoginParameters)->Resource<ResponseEntity<LoginEntity>, LoginParameters>) {
        get {
            return getWithParam(url: URL.Reg.login)
        }
    }
    
    static var regThree: ((RegistrationThreeParameters)->Resource<ResponseEntity<RegSecEntity>, RegistrationThreeParameters>) {
        get {
            return getWithParam(url: URL.Reg.three)
        }
    }
    
    static var regSec: ((RegistrationSecParameters)->Resource<ResponseEntity<RegSecEntity>, RegistrationSecParameters>) {
        get {
            return getWithParam(url: URL.Reg.sec)
        }
    }
    
    static var regFirst: ((RegistrationFirstParameters)->Resource<ResponseEntity<RegistrationFirstEntity>, RegistrationFirstParameters>) {
        get {
            return getWithParam(url: URL.Reg.first)
        }
    }
    
    static var recoverOne: ((RecoverOneParameters)->Resource<ResponseEntity<SucceedFailedEntity>, RecoverOneParameters>) {
        get {
            return getWithParam(url: URL.Recover.one)
        }
    }
    
    static var recoverTwo: ((RecoverTwoParameters)->Resource<ResponseEntity<SucceedFailedEntity>, RecoverTwoParameters>) {
        get {
            return getWithParam(url: URL.Recover.two)
        }
    }
    
    static var getTopEvents: (()->Resource<ResponseEntity<[EventPanel]>, EmptyParameters>) {
        get {
            return getNoParam(url: URL.Map.eventTopPanels)
        }
    }
    
    static var getEventProfile: ((EventPageParameters)->Resource<ResponseEntity<EventProfileHeader>, EventPageParameters>) {
        get {
            return getWithParam(url: URL.Event.header)
        }
    }
    
    static var getEventByID: ((EventByIDParameters)->Resource<ResponseEntity<EventPanel>, EventByIDParameters>) {
        get {
            return getWithParam(url: URL.Map.eventMedium)
        }
    }
    
    static var getNearestEventsOnMap: ((EventByIDParameters)->Resource<ResponseEntity<[EventPanel]>, EventByIDParameters>) {
        get {
            return getWithParam(url: URL.Map.nearestEvents)
        }
    }
    
    static var getUserProfileEvents: ((UserProfileEventParameters)->Resource<ResponseEntity<[EventPanel]>, UserProfileEventParameters>) {
        get {
            return getWithParam(url: URL.UserProfile.events)
        }
    }
    
    static var getUserHeader: ((UserProfileHeaderParameters)->Resource<ResponseEntity<UserProfileHeader>, UserProfileHeaderParameters>) {
        get {
            return getWithParam(url: URL.UserProfile.header)
        }
    }
    
    static var getSettings: ((UserIDParameter)->Resource<ResponseEntity<UserEditProfileSetting>, UserIDParameter>) {
        get {
            return getWithParam(url: URL.UserProfile.Setting.editProfile)
        }
    }
    
    static var getUserMedia: ((UserProfileMediaParameters)->Resource<ResponseEntity<[MediaPanel]>, UserProfileMediaParameters>) {
        get {
            return getWithParam(url: URL.UserProfile.memory)
        }
    }
    
    static var updateUsername: ((UpdateUsernameParameters)->Resource<ResponseEntity<ChangeUpdateEntity>, UpdateUsernameParameters>) {
        get {
            return getWithParam(url: URL.UserProfile.Setting.updateUsername)
        }
    }
    
    static var updateEmail: ((UpdateEmailParameters)->Resource<ResponseEntity<ChangeUpdateEntity>, UpdateEmailParameters>) {
        get {
            return getWithParam(url: URL.UserProfile.Setting.updateEmail)
        }
    }
    
    static var validateEmail: ((UpdateEmailTokenParameters)->Resource<ResponseEntity<ChangeUpdateEntity>, UpdateEmailTokenParameters>) {
        get {
            return getWithParam(url: URL.UserProfile.Setting.validateEmail)
        }
    }
    
    static var insertEvent: ((InsertEventParameters)->Resource<ResponseEntity<SucceedFailedEntity>, InsertEventParameters>) {
        get {
            return getWithParam(url: URL.Event.insertEvent)
        }
    }
    
    static var updateDescription: ((UpdateDescParameters)->Resource<ResponseEntity<SucceedFailedEntity>, UpdateDescParameters>) {
        get {
            return getWithParam(url: URL.UserProfile.Setting.updateDescription)
        }
    }
    
    static var changeUserAvatar: ((ChangeUserAvatarParameters)->Resource<ResponseEntity<SucceedFailedEntity>, ChangeUserAvatarParameters>) {
        get {
            return getWithParam(url: URL.UserProfile.Setting.changeUserAvatar)
        }
    }
    
    static var changePassword: ((UserSettingsChangePsw)->Resource<ResponseEntity<ChangePasswordEntity>, UserSettingsChangePsw>) {
        get {
            return getWithParam(url: URL.UserProfile.Setting.changePassword)
        }
    }
    
    static var getBlockedUsers: ((UserIDParameter)->Resource<ResponseEntity<[UserMini]>, UserIDParameter>) {
        get {
            return getWithParam(url: URL.UserProfile.Setting.blockedList)
        }
    }
}

