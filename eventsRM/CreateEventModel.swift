//
//  CreateEventModel.swift
//  CreateEventPagev2
//
//  Created by Fure on 27.04.2018.
//  Copyright © 2018 NotifyMe. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import AVFoundation

enum EventType: String, Decodable, Encodable {
    case pub = "public"
    case priv = "private"
    
    init(type: String) {
        if type == "private" {
            self = .priv
        } else {
            self = .pub
        }
    }
}

protocol Uploadable {
    var mimeType: String { get set }
    var data: Data { get set }
}

class UploadableMedia: Uploadable {
    var mimeType: String
    var data: Data
    var thumbnail: UIImage
    
    static let defaultImageName = "default_event.jpg"
    static var defaultImage: UIImage {
        get {
            return #imageLiteral(resourceName: "default_event_avatar")
        }
    }
    
    init(videoURL: URL) {
        let asset = AVAsset(url: videoURL)
        let imgGenerator = AVAssetImageGenerator(asset: asset)
        imgGenerator.appliesPreferredTrackTransform = true
        let cgImage = try! imgGenerator.copyCGImage(at: CMTimeMake(0, 1), actualTime: nil)
        
        self.thumbnail = UIImage(cgImage: cgImage)
        self.mimeType = "video/mp4"
        self.data = try! Data.init(contentsOf: videoURL)
    }
    
    init(image: UIImage, compressionQuality: CGFloat = 0.25) {
        self.mimeType = "image/jpeg"
        self.data = UIImageJPEGRepresentation(image, compressionQuality)!
        self.thumbnail = image
    }
}
