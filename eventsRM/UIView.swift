//
//  UIView.swift
//  eventsRM
//
//  Created by Fure on 24.07.17.
//  Copyright © 2017 uzuner. All rights reserved.
//

import UIKit

protocol Nibable: class {
    var bundleName: String { get set }
    var contentView: UIView! { get set }
    func setupViewFromNib()
}

extension Nibable where Self: UIView {
    func setupViewFromNib() {
        self.contentView = UINib.init(nibName: self.bundleName, bundle: nil).instantiate(withOwner: self, options: nil).first as! UIView
        self.contentView.frame = self.frame
        self.contentView.backgroundColor = .clear
        self.addSubview(self.contentView)
    }
}

extension UIView {
    
    static func blurView(bounds: CGRect, effect: UIBlurEffectStyle) -> UIVisualEffectView {
        
        let blurEffectView: UIVisualEffectView = {
            let blurEffect = UIBlurEffect(style: effect)
            let view = UIVisualEffectView(effect: blurEffect)
            view.frame = bounds
            return view
        }()

        return blurEffectView
    }
    
    class func fromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
    
    func autoresizingModeOn() {
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func autoresizingModeOff() {
        self.translatesAutoresizingMaskIntoConstraints = true
    }
    
    func makeCircle() {
        self.layer.masksToBounds = false
        self.layer.cornerRadius = self.frame.height / 2
        self.clipsToBounds = true
    }
    
    func makeOval(cornerRadius: CGFloat) {
        self.layer.masksToBounds = false
        self.layer.cornerRadius = cornerRadius
        self.clipsToBounds = true
    }
    
    func makeRound(roudingCorners: UIRectCorner, cornerRadius: CGSize){
        let maskPAth1 = UIBezierPath(roundedRect: self.bounds,
                                     byRoundingCorners: roudingCorners,
                                     cornerRadii: cornerRadius)
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = self.bounds
        maskLayer1.path = maskPAth1.cgPath
        self.layer.mask = maskLayer1
    }
    
    func dropShadow() {
        self.layer.masksToBounds = true
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.layer.shadowRadius = 1
        
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        
        self.layer.rasterizationScale = UIScreen.main.scale
    }
    
    func addShadow(shadowColor: CGColor = UIColor.black.cgColor,
                   shadowOffset: CGSize = CGSize(width: 0.0, height: 2.0),
                   shadowOpacity: Float = 0.5,
                   shadowRadius: CGFloat = 5) {
        layer.shadowColor = shadowColor
        layer.shadowOffset = shadowOffset
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = shadowRadius
    }
    
    func updateShadowPath(toBounds bounds: CGRect, withDuration duration: TimeInterval) {
        let oldPath = layer.shadowPath
        let newPath: CGPath = CGPath.init(rect: bounds, transform: nil)
        if duration > 0 {
            let theAnimation = CABasicAnimation(keyPath: "shadowPath")
            theAnimation.duration = duration
            theAnimation.fromValue = oldPath
            theAnimation.toValue = newPath
            theAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            layer.add(theAnimation, forKey: "shadowPath")
        }
    }
}
