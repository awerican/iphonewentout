//
//  ProfileSettingsArrowCell.swift
//  WentOut
//
//  Created by Fure on 19.09.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

class ProfileSettingsArrowCell: UICollectionViewCell {

    static var currentSize: CGSize {
        get {
            return CGSize.init(width: UIScreen.width, height: 49.0)
        }
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    
    func configCell(title: String) {
        titleLabel.text = title
    }
}








