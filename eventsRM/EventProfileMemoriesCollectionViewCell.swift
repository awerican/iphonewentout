//
//  EventProfileMemoriesCollectionViewCell.swift
//  WentOut
//
//  Created by Fure on 18.04.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

protocol EventProfileMemoriesDelegate {
    func addMemory()
}

class EventProfileMemoriesCollectionViewCell: UICollectionViewCell {
    
    static var currentSize: CGSize {
        return CGSize.init(width: UIScreen.main.bounds.width, height: 253.0)
    }
    
    static var noContentSize: CGSize {
        return CGSize.init(width: UIScreen.main.bounds.width, height: 135)
    }
    
    @IBOutlet weak var noMemoriesImage: UIImageView!
    @IBOutlet weak var noMemoriesLabel: UILabel!
    
    var memories: [MemoryEntity] = [] {
        didSet {
            self.collectionView.reloadData()
        }
    }
    
    var delegate: EventProfileMemoriesDelegate?
    
    @IBAction func addMemory(_ sender: UIButton) {
        self.delegate?.addMemory()
    }
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    func configCell(memories: [MemoryEntity]) {
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        for reuseID in self.reuseIDs {
            self.collectionView.register(UINib.init(nibName: reuseID, bundle: nil), forCellWithReuseIdentifier: reuseID)
        }
        if memories.count > 0 {
            self.memories = memories
            self.noMemoriesImage.removeFromSuperview()
            self.noMemoriesLabel.removeFromSuperview()
        }
    }
}

extension EventProfileMemoriesCollectionViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var reuseIDs: [String] {
        get {
            return ["MemoryPreviewCollectionViewCell"]
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return memories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIDs[0], for: indexPath) as! MemoryPreviewCollectionViewCell
        let memory = self.memories[indexPath.row]
//        cell.configCell(memory: memory, hostID: <#String#>)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: 128, height: self.collectionView.frame.height)
    }
    
    // space between items in row
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 0, left: 6, bottom: 0, right: 6)
    }
}























