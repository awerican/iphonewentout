//
//  AesWrapper.swift
//  WentOut
//
//  Created by Fure on 22.01.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import CryptoSwift

class AesWrapper {
    
    static func encrypt(_ message: String, aesKey: AesKey) -> String {
        
        let msg = message.bytes
        
        let randomIV: Array<UInt8> = AES.randomIV(16)
        
        let aes = try! AES(key: aesKey.data, blockMode: CBC(iv: randomIV))
        
        let result = try! aes.encrypt(msg)
        
        let ivBase64 = Data.init(bytes: randomIV).base64EncodedString()
        var encMsgBase64 = Data.init(bytes: result).base64EncodedString()
        
        encMsgBase64 += ivBase64
        
        return encMsgBase64
    }
    
    static func decrypt(_ message: String, aesKey: AesKey) -> String {
        
        let msg = message
        
        let _message = String(msg.prefix(msg.count-24))
        let iv = String(msg.suffix(24))
        
        let ivDataFormat = Data.init(base64Encoded: iv)!.bytes
        let messageDataFormat = Data.init(base64Encoded: _message)!.bytes
        
        let aes = try! AES(key: aesKey.data, blockMode: CBC(iv: ivDataFormat), padding: .pkcs7)
        let decrypted = try! aes.decrypt(messageDataFormat)
        
        return String(bytes: decrypted, encoding: .utf8)!
    }
}
