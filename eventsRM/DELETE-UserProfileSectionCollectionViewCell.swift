////
////  UserProfileSectionCollectionViewCell.swift
////  WentOut
////
////  Created by Fure on 27.07.2018.
////  Copyright © 2018 uzuner. All rights reserved.
////
//
//import UIKit
//
//protocol UserProfileSectionProtocol {
//    func mediaButtonWasPressed(_ button: UIButton)
//    func eventButtonWasPressed(_ button: UIButton)
//}
//
//class UserProfileSectionCollectionViewCell: UICollectionViewCell {
//
//    @IBOutlet var mediaButton: UIButton!
//    @IBOutlet var eventButton: UIButton!
//
//    lazy var underlineView: UIView = {
//        let size = CGSize.init(width: UIScreen.main.bounds.width / 2, height: 1.2)
//        let view = UIView.init(frame: CGRect.init(origin: CGPoint.zero, size: size))
//        view.backgroundColor = .black
//        return view
//    }()
//
//    let mediumFont = UIFont.systemFont(ofSize: 17.0, weight: .medium)
//    let regularFont = UIFont.systemFont(ofSize: 17.0, weight: .regular)
//    let lightFont = UIFont.systemFont(ofSize: 17.0, weight: .light)
//
//    var currentSize: CGSize {
//        get {
//            return CGSize.init(width: UIScreen.main.bounds.width, height: 45.0)
//        }
//    }
//
//    var delegate: UserProfileSectionProtocol?
//
//    @IBAction func mediaButtonAction(_ sender: UIButton) {
//        self.delegate?.mediaButtonWasPressed(sender)
//        if underlineView.frame.origin.x != 0.0 {
//            UIView.animate(withDuration: 0.3) { [weak self] in
//                self?.underlineView.frame.origin.x = 0.0
//            }
//        }
//    }
//
//    @IBAction func eventMediaAction(_ sender: UIButton) {
//        self.delegate?.eventButtonWasPressed(sender)
//        if underlineView.frame.origin.x != UIScreen.main.bounds.width / 2 {
//            UIView.animate(withDuration: 0.3) { [weak self] in
//                self?.underlineView.frame.origin.x = UIScreen.main.bounds.width / 2
//            }
//        }
//    }
//
//    func configCell(_ delegate: UserProfileSectionProtocol, state: UserProfileCVState) {
//        self.delegate = delegate
//        self.addSubview(underlineView)
//        self.layer.addBorder(edge: .bottom, color: .gray, thickness: 0.5)
//        switch state {
//        case .media:
//            underlineView.frame.origin = CGPoint.init(x: 0, y: self.frame.height - underlineView.frame.size.height)
//            self.mediaButton.titleLabel?.textColor = .black
//            self.eventButton.titleLabel?.textColor = .gray
//        case .events:
//            underlineView.frame.origin = CGPoint.init(x: self.frame.width / 2, y: self.frame.height - underlineView.frame.size.height)
//            self.mediaButton.titleLabel?.textColor = .gray
//            self.eventButton.titleLabel?.textColor = .black
//        }
//    }
//}
