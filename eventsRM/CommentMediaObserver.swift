//
//  CommentMediaObserver.swift
//  WentOut
//
//  Created by Fure on 11.02.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import UIKit

enum CommentMediaType: String {
    case image = "jpeg"
    case video = "mp4"
}

struct CommentMedia {
    let type: CommentMediaType
    let image: UIImage
    let url: URL?
    init(type: CommentMediaType, image: UIImage, url: URL? = nil) {
        self.type = type
        self.image = image
        self.url = url
    }
}

protocol CommentMediaObserverDelegate: class {
    func mediaQuantityDidChange(newQuantity: Int, oldQuantity: Int)
}

class CommentMediaObserver {
    private var _medias: [CommentMedia] = []
    
    var medias: [CommentMedia] {
        get {
            return _medias
        }
    }
    
    var delegate: CommentMediaObserverDelegate?
    
    var quantity: Int {
        get {
            return _medias.count
        }
    }
    
    func removeAll() {
        let oldQuantity = _medias.count
        _medias = []
        delegate?.mediaQuantityDidChange(newQuantity: 0, oldQuantity: oldQuantity)
    }
    
    func getItem(at index: Int) -> CommentMedia {
        return _medias[index]
    }
    
    func add(media: CommentMedia) {
        _medias.append(media)
        delegate?.mediaQuantityDidChange(newQuantity: self._medias.count, oldQuantity: self._medias.count - 1)
    }
    
    func remove(at index: Int) {
        _medias.remove(at: index)
        delegate?.mediaQuantityDidChange(newQuantity: self._medias.count, oldQuantity: self._medias.count + 1)
    }
}
