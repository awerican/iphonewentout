//
//  AssociatedTypeExample.swift
//  WentOut
//
//  Created by Fure on 29.08.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import Foundation

protocol Mappable {
    associatedtype MappingResult
    
    static func map(response: [String: String]) -> MappingResult
}

struct User: Mappable {
//    typealias MappingResult = User
    
    var username: String?
    
    static func map(response: [String: String]) -> User {
        return User()
    }
}

class DataProvidet {
    func fetch<T: Mappable>(completion: (_ result: T) -> Void) where T.MappingResult == T {
        let response = [String: String]()
        let result = T.map(response: response)
        completion(result)
    }
}



































