//
//  StringProtocol.swift
//  WentOut
//
//  Created by Fure on 14.02.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation

extension StringProtocol where Index == String.Index {
    var lines: [SubSequence] {
        return split(maxSplits: .max, omittingEmptySubsequences: true, whereSeparator: { $0 == "\n" })
    }
    var removingAllExtraNewLines: String {
        return lines.joined(separator: "\n")
    }
}
