//
//  Crypto.swift
//  WentOut
//
//  Created by Fure on 20.10.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import Foundation
import SwiftyRSA
//import CryptoSwift
//import RSASwiftGenerator
//import AES256CBC

extension String {
    func rsaEncrypt() -> String {
        let publicKey = try! PublicKey(pemNamed: "client.public")
        let clear = try! ClearMessage(string: self, using: .utf8)
        let encrypted = try! clear.encrypted(with: publicKey, padding: .OAEP)
        
        return encrypted.base64String
    }
    
    func rsaDecrypt() -> String {
        let privateKey = try! PrivateKey(pemNamed: "client.private")
        let encrypted = try! EncryptedMessage(base64Encoded: self)
        let clear = try! encrypted.decrypted(with: privateKey, padding: Padding.OAEP)
        
        return try! clear.string(encoding: .utf8)
    }
}

//func rsaDecrypt(string: String) -> String {
//    let privateKey = try! PrivateKey(pemNamed: "client.private")
//    let encrypted = try! EncryptedMessage(base64Encoded: string)
//    let clear = try! encrypted.decrypted(with: privateKey, padding: Padding.OAEP)
//
//    let string = try! clear.string(encoding: .utf8)
//    return string
//}
