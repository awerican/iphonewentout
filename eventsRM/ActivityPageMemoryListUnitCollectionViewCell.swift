//
//  ActivityPageMemoryListUnitCollectionViewCell.swift
//  WentOut
//
//  Created by Fure on 22.05.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

class ActivityPageMemoryListUnitCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var eventTitle: UILabel!
    @IBOutlet weak var imageViewFront: UIImageView!
    @IBOutlet weak var basisView: UIView!
    
//    var __image: UIImage?
    
    override func prepareForReuse() {
        imageViewFront.image = nil
//        self.imageViewFront.image = __image
    }
    
    func configCell(eventTitle: String, memory: StoryMemory) {
//        imageViewFront.image = self.__image ?? nil
        imageViewFront.image = nil
        
        Webservice.loadImage(url: memory.media) { (_image) in
            if let image = _image {
                self.imageViewFront.image = image
//                self.__image = image
            }
        }
        self.eventTitle.text = eventTitle
//        basisView.makeOval(cornerRadius: 11.0)
        basisView.makeOval(cornerRadius: basisView.frame.width / 10)
    }
    
//    func configCell(memory: EventMemoryMini) {
//        imageViewFront.image = self.__image ?? nil
//
//        Webservice.loadImage(url: memory.photo) { (_image) in
//            if let image = _image {
//                self.imageViewFront.image = image
//                self.__image = image
//            }
//        }
//        eventTitle.text = memory.eventName
//        basisView.makeOval(cornerRadius: 11.0)
//    }
}
