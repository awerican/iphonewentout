//
//  WorldBottomView.swift
//  WentOut
//
//  Created by Fure on 08.07.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import UIKit

struct WorldBottomModel {
    var eventId: String
    var eventName: String
    var eventTime: Date
}

enum WorldBottomState {
    case topEvents
    case eventInfo(WorldBottomModel)
    case loading
}

extension WorldBottomModel: Equatable {
    static func ==(lhs: WorldBottomModel, rhs: WorldBottomModel) -> Bool {
        if lhs.eventId == rhs.eventId {
            return true
        } else {
            return false
        }
    }
}

extension WorldBottomState: Equatable {
    static func ==(lhs: WorldBottomState, rhs: WorldBottomState) -> Bool {
        switch (lhs, rhs) {
        case (.topEvents, .topEvents):
            return true
        case (.eventInfo(let fModel), .eventInfo(let sModel)):
            if fModel == sModel {
                return true
            } else {
                return false
            }
        default:
            return false
        }
    }
}

class WorldBottomView: UIView {
    @IBOutlet var contentView: UIView!
    @IBOutlet var basisView: UIView!
    @IBOutlet var labelTitle: UILabel!
    @IBOutlet var labelTime: UILabel!
    @IBOutlet var labelInfoTrailingConstraint: NSLayoutConstraint!
    
    static var isLoading: Bool = false
    
    private lazy var activityIndicatorView: UIActivityIndicatorView = {
        let view = UIActivityIndicatorView(frame: CGRect.zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.color = UIColor.gray
        return view
    }()
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.basisView.makeRound(roudingCorners: [.topLeft, .topRight], cornerRadius: CGSize.init(width: 10, height: 10))
        self.basisView.layer.addBorder(edge: .bottom, color: UIColor(red: 0.91, green: 0.91, blue: 0.91, alpha: 1.0), thickness: 1.0)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
}

extension WorldBottomView {
    private var activityIndicatorSize: CGSize {
        get {
            return CGSize.init(width: 30, height: 30)
        }
    }
    
    private var defaultTrailingConstraintConstant: CGFloat {
        get {
            return 12.0
        }
    }
    private var bundleName: String {
        get {
            return "WorldBottomView"
        }
    }
}

extension WorldBottomView {
    private func commonInit() {
        Bundle.main.loadNibNamed(bundleName, owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.backgroundColor = UIColor.clear
        self.configCell(state: .topEvents)
    }
    
    private func configCell(state: WorldBottomState) {
//        switch state {
//        case .loading:
//            self.setupLoading()
//        case .eventInfo(let model):
//            if WorldBottomView.isLoading {
//                self.setupEventInto(model)
//            }
//        case .topEvents:
            self.setupTopEvents()
//        }
    }
    
    private func setupTopEvents() {
//        self.stopLoading()
        self.labelTitle.text = "Top Events"
        self.labelTitle.isHidden = false
        self.labelTitle.textAlignment = .center
        self.labelTime.isHidden = true
        self.labelInfoTrailingConstraint.constant = defaultTrailingConstraintConstant
    }
    
    private func setupEventInto(_ model: WorldBottomModel) {
//        self.stopLoading()
        self.labelTitle.text = model.eventName
        self.labelTime.text = model.eventTime.timeDifferenceOnMap
        self.labelTime.sizeToFit()
        self.labelTime.isHidden = false
        self.labelTitle.isHidden = false
        self.labelTitle.textAlignment = .left
        self.labelInfoTrailingConstraint.constant = defaultTrailingConstraintConstant + self.labelTime.frame.width + 12
    }
    
//    private func setupLoading() {
//        WorldBottomView.isLoading = true
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
//            if WorldBottomView.isLoading {
//                self.addSubview(self.activityIndicatorView)
//                self.activityIndicatorView.isHidden = false
//                self.labelTitle.text = ""
//                self.labelTime.isHidden = true
//                self.labelTitle.isHidden = true
//                self.activityIndicatorView.startAnimating()
//
//                self.activityIndicatorView.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0.0).isActive = true
//                self.activityIndicatorView.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0.0).isActive = true
//                self.activityIndicatorView.widthAnchor.constraint(equalToConstant: self.activityIndicatorSize.width).isActive = true
//                self.activityIndicatorView.heightAnchor.constraint(equalToConstant: self.activityIndicatorSize.height).isActive = true
//            }
//        }
//    }
    
//    private func stopLoading() {
//        WorldBottomView.isLoading = false
//        self.activityIndicatorView.removeFromSuperview()
//        self.activityIndicatorView.isHidden = true
//        self.activityIndicatorView.stopAnimating()
//    }
}
