//
//  WorldTableViewController.swift
//  WentOut
//
//  Created by Fure on 07.07.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import Foundation
import UIKit

enum WID: String {
    case cell = "TableViewCellSecond"
    case loadingCell = "TableViewCellSecondLoading"
    case headerView = "HeaderTableViewCell"
    case headerViewSec = "HeaderTableViewCellSecond"
    case errorCell = "TableViewErrorCell"
    
    static func reuseID(_ id: WID) -> String {
        return id.rawValue
    }
}

class WorldTableViewController: UIViewController {
    struct Constants {
        struct TableView {
            static let footerHeight: CGFloat = UIScreen.main.bounds.height * 2
        }
    }
    
    static var beginMoving: Bool = false
    
    @IBOutlet var tableView: WorldTableView!
    @IBOutlet var tableViewTopConstraints: NSLayoutConstraint!
    
    var heightForFooter: CGFloat {
        get {
            return 2 * UIScreen.main.bounds.height
        }
    }

    private var parentVC: WorldViewController?
    
//    var state: WorldTableViewState!
    
    var viewModel: WorldTableViewModel!
    
    lazy var tableViewHeaderGesture: UITapGestureRecognizer = {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(closeVC))
        return gesture
    }()
    
    func configVC(parentVC: WorldViewController, imageCache: NSCache<NSURL, UIImage>, state: WorldTableViewState) {
        self.parentVC = parentVC
//        self.state = state
        self.modalPresentationStyle = .overCurrentContext
        self.viewModel = WorldTableViewModel.init(state: state, imageCache: imageCache, delegate: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        setupView()
        self.viewModel.downloadData()
    }
}

extension WorldTableViewController {
    
    static var tableViewTopConstraintsConstant: CGFloat {
        get {
            return 45.0
        }
    }
    
    private func setupView() {
        let button = UIButton.init(frame: UIScreen.main.bounds)
        button.addTarget(self, action: #selector(closeVC), for: .touchUpInside)
        self.view.insertSubview(button, at: 0)
    }
    
    @objc func closeVC() {
        performDismissing()
    }
    
    private func setupTableView() {
        self.tableView.allowsSelection = true
        self.tableView.allowsMultipleSelection = false
        
        switch self.viewModel.tableViewState {
        case .topEvents:
            self.tableViewTopConstraints.constant = WorldTableViewController.tableViewTopConstraintsConstant
        case .pickedEvent(eventID: _):
            let bundleCell = Bundle.main.loadNibNamed(WID.reuseID(.cell), owner: self, options: nil)?.first as! TableViewCellSecond
            bundleCell.configNib()
            self.tableViewTopConstraints.constant = UIScreen.main.bounds.height - bundleCell.currentSize.height - WorldTableView.headerHeight - self.bottomLayoutGuide.length
        }
        
        self.tableView.bounces = true
        self.tableView.contentInset.top = 0.0
        self.tableView.contentInset.bottom = -heightForFooter
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.backgroundColor = .clear
        
        self.tableView.register(UINib.init(nibName: WID.reuseID(.cell), bundle: nil), forCellReuseIdentifier: WID.reuseID(WID.cell))
        self.tableView.register(UINib.init(nibName: WID.reuseID(.loadingCell), bundle: nil), forCellReuseIdentifier: WID.reuseID(WID.loadingCell))
        self.tableView.register(UINib.init(nibName: WID.reuseID(.errorCell), bundle: nil), forCellReuseIdentifier: WID.reuseID(.errorCell))
        self.tableView.register(UINib.init(nibName: WID.reuseID(.headerView), bundle: nil), forHeaderFooterViewReuseIdentifier: WID.reuseID(.headerView))
        self.tableView.register(UINib.init(nibName: WID.reuseID(.headerViewSec), bundle: nil), forHeaderFooterViewReuseIdentifier: WID.reuseID(.headerViewSec))
    }
}

extension WorldTableViewController: UIScrollViewDelegate {
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if scrollView.contentOffset.y < -70.0 {
            scrollView.contentInset.top = abs(scrollView.contentOffset.y)
            scrollView.bounces = false
            performDismissing()
        }
    }
    
    func performDismissing(completion: ((Bool)->())? = nil) {
//        (self.parentVC?.navigationController?.tabBarController as! CustomBarController).moveTabBarBackToTheSreen()
        (self.parentVC?.tabBarController as! CustomBarController).moveTabBarBackToTheSreen()
        self.willMove(toParentViewController: nil)
        self.removeFromParentViewController()
        UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseIn, animations: {
            [weak self] in
//            (self?.parentVC?.navigationController?.tabBarController as! CustomBarController).view.layoutIfNeeded()
            (self?.parentVC?.tabBarController as! CustomBarController).view.layoutIfNeeded()
            self?.view.frame.origin.y = UIScreen.main.bounds.height
        }) { (finish) in
            if finish {
                self.view.removeFromSuperview()
                self.dismiss(animated: false)
                completion?(true)
            }
        }
    }
}

extension WorldTableViewController: TableViewCellSecondDelegate {
    func usernameWasTapped(at indexPath: IndexPath) {
        print("username at", indexPath)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
        controller.profileOwnerID = viewModel.model[0].userId
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func avatarWasTapped(at indexPath: IndexPath) {
        print("avatar was tapped at", indexPath)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
        controller.profileOwnerID = viewModel.model[0].userId
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

extension WorldTableViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.numberOfRowsInSection(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellType = self.viewModel.cellForRow(at: indexPath)
        
        switch cellType {
        case .normal(model: let model):
            let cell = tableView.dequeueReusableCell(withIdentifier: WID.reuseID(.cell), for: indexPath) as! TableViewCellSecond
            cell.config(model: model)
            cell.delegate = self
            return cell
        case .isLoading:
            let cell = tableView.dequeueReusableCell(withIdentifier: WID.reuseID(.loadingCell), for: indexPath) as! TableViewCellSecondLoading
            return cell
        case .fail:
            let cell = tableView.dequeueReusableCell(withIdentifier: WID.reuseID(.errorCell), for: indexPath) as! TableViewErrorCell
            let bundleCell = Bundle.main.loadNibNamed(WID.reuseID(.cell), owner: self, options: nil)?.first as! TableViewCellSecond
            bundleCell.configNib()
            cell.config(bundleCell.currentSize.height)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch self.viewModel.tableViewState {
        case .topEvents:
            let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: WID.reuseID(.headerView)) as! HeaderTableViewCell
            view.configView(.topEvents, gesture: self.tableViewHeaderGesture)
            return view
        case .pickedEvent(eventID: _):
            switch section {
            case 0:
                let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: WID.reuseID(.headerView)) as! HeaderTableViewCell
                view.configView(.eventInfo, gesture: self.tableViewHeaderGesture)
                return view
            case 1:
                let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: WID.reuseID(.headerView)) as! HeaderTableViewCell
                view.configView(.eventNear)
                return view
            default:
                return nil
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        switch self.viewModel.tableViewState {
        case .pickedEvent(eventID: _):
            return section == 1 ? self.heightForFooter : 0.0
        case .topEvents:
            return section == 0 ? self.heightForFooter : 0.0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch self.viewModel.tableViewState {
        case .topEvents:
            return WorldTableView.headerHeight
        case .pickedEvent(eventID: _):
            if section == 0 {
                return WorldTableView.headerHeight
            } else if section == 1 {
                if self.viewModel.model.count > 1 {
                    return WorldTableView.headerHeight
                }
            }
        }
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch self.viewModel.downloadState {
        case .isLoading:
            return
        default:
            break
        }
        self.performDismissing { (finish) in
            if finish {
                let eventID = self.viewModel.model[indexPath.row].eventId
                self.parentVC?.performSegue(withIdentifier: WorldViewController.Constants.Segues.toEventPage, sender: eventID)
            }
        }
    }
}

extension WorldTableViewController: WorldTableViewModelDelegate {
    func tableViewBeginUpdates() {
        self.tableView.beginUpdates()
    }
    
    func tableViewShouldReloadData() {
        self.tableView.reloadData()
    }
    
    func tableViewShouldReloadRows(at indexPath: [IndexPath]) {
        self.tableView.reloadRows(at: indexPath, with: .none)
    }
    
    func tableViewShouldInsertRows(at indexPath: [IndexPath]) {
        switch self.viewModel.tableViewState {
        case .topEvents:
            self.tableView.insertRows(at: indexPath, with: .none)
        case .pickedEvent(eventID: _):
            self.tableView.insertRows(at: indexPath, with: .none)
            if self.viewModel.model.count > 1 {
                self.tableViewTopConstraints.constant = WorldTableViewController.tableViewTopConstraintsConstant
                UIView.animate(withDuration: 0.25) {
                    self.view.layoutIfNeeded()
                }
            } else if self.viewModel.model.count == 1 {
                let bundleCell = Bundle.main.loadNibNamed(WID.reuseID(.cell), owner: self, options: nil)?.first as! TableViewCellSecond
                let event = self.viewModel.model[0]
                
                bundleCell.configNib(username: event.userNickname, eventTitle: event.title, time: event.time.timeDifferenceOnMap ?? "", location: event.location.city, guests: event.guests.description)

                self.tableViewTopConstraints.constant = UIScreen.main.bounds.height - bundleCell.currentSize.height - WorldTableView.headerHeight - self.topLayoutGuide.length - self.bottomLayoutGuide.length
                UIView.animate(withDuration: 0.25) {
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
    
    func tableViewShouldDeleteRows(at indexPath: [IndexPath]) {
        self.tableView.deleteRows(at: indexPath, with: .none)
    }
    
    func tableViewEndUpdates() {
        self.tableView.endUpdates()
    }
}






































