//
//  ChooseEventViewController.swift
//  WentOut
//
//  Created by Fure on 20.06.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import UIKit

protocol ChooseEventDelegate: class {
    func eventWasSelected(event: EventPanel)
}

class ChooseEventViewController: UIViewController {
    
    let eventData = Downloader<[EventPanel], UserProfileEventParameters>.init(resource: URL.UserProfile.events, param: UserProfileEventParameters.init(visiterID: LocalAuth.userID!, profileOwnerID: LocalAuth.userID!))
    
    @IBOutlet weak var tableView: UITableView!
    
    var navViewController: UINavigationController!
    
    weak var delegate: ChooseEventDelegate?
    
    @IBAction func backAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupNetworking()
        setupTableView()
        setupNavVC()
    }
}

extension ChooseEventViewController {
    private func setupNetworking() {
        self.eventData.getFirst {
            self.tableView.reloadData()
        }
    }
    
    private func setupNavVC() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let navController = storyboard.instantiateViewController(withIdentifier: "NavAddMemoryViewController") as! UINavigationController
        self.navViewController = navController
    }
    
    private func setupView() {
        self.view.backgroundColor = UIColor.white
    }
    
    private func setupTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.contentInset.top = 40.0
        self.tableView.contentInset.bottom = 50.0
        
        for id in reuseIDs {
            self.tableView.register(UINib.init(nibName: id, bundle: nil), forCellReuseIdentifier: id)
        }
    }
}

extension ChooseEventViewController: UITableViewDelegate, UITableViewDataSource {
    
    var reuseIDs: [String] {
        return ["ChooseEventViewControllerCell"]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.eventData.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIDs[0]) as! ChooseEventViewControllerCell
        let title = self.eventData.data![indexPath.row].title
        cell.configCell(title: title)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dismiss(animated: true) {
            self.delegate?.eventWasSelected(event: self.eventData.data![indexPath.row])
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let data = self.eventData.data else {
            return
        }
        if indexPath.row == data.count - 1 {
            self.eventData.getNext(section: 0) { (paths) in
                self.tableView.insertRows(at: paths, with: .automatic)
            }
        }
    }
}




















