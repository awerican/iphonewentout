//
//  ChatLeftMsgMoreImageGrouppedCell.swift
//  WentOut
//
//  Created by Fure on 15.10.2018.
//  Copyright © 2018 uzuner. All rights reserved.
//

import UIKit

class ChatLeftMsgMoreImageGrouppedCell: ChatMessageBaseCell {

    @IBOutlet var avatarOutlet: UIImageView!
    @IBOutlet var mediaCollectionOutlet: [UIImageView]!
    @IBOutlet var timeOutlet: UILabel!
    @IBOutlet var messageBackgroundOutlet: UIView!
    @IBOutlet var timeBackgroundOutlet: UIView!
    
    @IBOutlet var imageHiddenQuantityBlurViewOutlet: UIVisualEffectView!
    @IBOutlet var imageHiddenQuantityOutlet: UILabel!
    
    override var avatar: UIImageView! {
        return self.avatarOutlet
    }
    override var time: UILabel! {
        return self.timeOutlet
    }
    override var messageBackground: UIView! {
        return self.messageBackgroundOutlet
    }
    override var mediaCollection: [UIImageView]? {
        return self.mediaCollectionOutlet
    }
    override var timeBackground: UIView? {
        return self.timeBackgroundOutlet
    }
    override var imageHiddenQuantity: UILabel? {
        return self.imageHiddenQuantityOutlet
    }
    override var imageHiddenQuantityBlurView: UIVisualEffectView? {
        return self.imageHiddenQuantityBlurViewOutlet
    }
}
























