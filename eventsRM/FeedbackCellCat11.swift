//
//  FeedbackCellCat11.swift
//  WentOut
//
//  Created by Fure on 16.09.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

class FeedbackCellCat11: UICollectionViewCell {

//    @IBOutlet weak var userAvatar: UIImageView!
    @IBOutlet weak var eventAvatar: UIImageView!
    @IBOutlet weak var feedbackBody: UILabel!
    
    @IBOutlet weak var basisView: UIView!
    
    var delegate: FeedbackCellCatDelegate?
    
    var currentSize: CGSize {
        get {
            self.frame.size.width = UIScreen.width
            self.layoutIfNeeded()
            return CGSize.init(width: UIScreen.main.bounds.width, height: self.basisView.frame.size.height)
        }
    }
    
    func configCell(model: FeedbackEntity) {
        
//        self.userAvatar.image = nil
//        Webservice.loadImage(url: model.creatorPhoto) { (_image) in
//            self.userAvatar.image = _image
//        }
        
        self.eventAvatar.image = nil
        Webservice.loadImage(url: model.photo) { (_image) in
            self.eventAvatar.image = _image
        }
        
        self.feedbackBody.attributedText = self.makeBody(username: model.creatorUsername, time: Date.goodTime(inSec: model.time) ?? "", body: model.body)
        
//        self.userAvatar.makeCircle()
        self.eventAvatar.makeOval(cornerRadius: self.eventAvatar.frame.height / 5)
        
//        let leftImageTapGesture = UITapGestureRecognizer.init(target: self, action: #selector(leftImageGestureAction))
//        self.userAvatar.isUserInteractionEnabled = true
//        self.userAvatar.addGestureRecognizer(leftImageTapGesture)
        
        let rightImageTapGesture = UITapGestureRecognizer.init(target: self, action: #selector(rightImageGestureAction))
        self.eventAvatar.isUserInteractionEnabled = true
        self.eventAvatar.addGestureRecognizer(rightImageTapGesture)
    }
    
    @objc func leftImageGestureAction() {
        if let ip = (self.superview as? UICollectionView)?.indexPath(for: self) {
            self.delegate?.leftImageWasTapped(at: ip)
        }
    }
    
    @objc func rightImageGestureAction() {
        if let ip = (self.superview as? UICollectionView)?.indexPath(for: self) {
            self.delegate?.rightImageWasTapped(at: ip, category: .cat4)
        }
    }
    
    private func makeBody(username: String, time: String, body: String) -> NSMutableAttributedString {
        let helveticaBold = [
            NSAttributedStringKey.foregroundColor: UIColor.black,
            NSAttributedStringKey.font: UIFont.init(name: "HelveticaNeue-Bold", size: 13.5)!]
        let helveticaLight = [
            NSAttributedStringKey.foregroundColor: UIColor.black,
            NSAttributedStringKey.font: UIFont.init(name: "HelveticaNeue-Light", size: 13.5)!]
        let helveticaHour = [
            NSAttributedStringKey.foregroundColor: UIColor.gray,
            NSAttributedStringKey.font: UIFont.init(name: "HelveticaNeue-Light", size: 12.0)!]
        
        let textPartOne = NSMutableAttributedString(string: "Event", attributes: helveticaBold)
        let textPartTwo = NSMutableAttributedString(string: " will start " + body, attributes: helveticaLight)
        let textPartThree = NSMutableAttributedString(string: " " + time, attributes: helveticaHour)
        
        let textCombination = NSMutableAttributedString()
        textCombination.append(textPartOne)
        textCombination.append(textPartTwo)
        textCombination.append(textPartThree)
        
        return textCombination
    }
}
