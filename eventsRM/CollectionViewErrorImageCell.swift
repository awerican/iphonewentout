//
//  CollectionViewErrorImageCell.swift
//  WentOut
//
//  Created by Fure on 05.09.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

class CollectionViewErrorImageCell: UICollectionViewCell {

    @IBOutlet weak var errorImageView: UIImageView!
    
    static var currentSize: CGSize {
        get {
            return CGSize.init(width: UIScreen.width, height: UIScreen.width)
        }
    }

    func configCell(image: UIImage) {
        errorImageView.image = image
    }
}
