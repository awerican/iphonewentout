//
//  EventProfileLocationCollectionViewCell.swift
//  WentOut
//
//  Created by Fure on 18.04.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

class EventProfileLocationCollectionViewCell: UICollectionViewCell {

    var currentSize: CGSize {
        return CGSize.init(width: UIScreen.main.bounds.width, height: self.addressLabel.frame.maxY + 12)
    }
    
    @IBOutlet weak var addressLabel: UILabel!
    
    func configCell(address: String) {
        self.addressLabel.text = address
    }
}
