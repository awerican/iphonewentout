//
//  UITextView.swift
//  eventsRM
//
//  Created by Fure on 29.11.2017.
//  Copyright © 2017 uzuner. All rights reserved.
//

import Foundation
import UIKit

protocol TextViewBackSpaceDeleting {
    func textViewWillDelete(_ textView: TextViewMore)
}

class TextViewMore: UITextView {
    var myDelegate: TextViewBackSpaceDeleting?
    
    override func deleteBackward() {
        myDelegate?.textViewWillDelete(self)
        super.deleteBackward()
    }
}


extension UITextView {
    
    func numberOfLines() -> Int {
        let layoutManager = self.layoutManager
        let numberOfGlyphs = layoutManager.numberOfGlyphs
        var lineRange: NSRange = NSMakeRange(0, 1)
        var index = 0
        var numberOfLines = 0
        
        while index < numberOfGlyphs {
            layoutManager.lineFragmentRect(forGlyphAt: index, effectiveRange: &lineRange)
            index = NSMaxRange(lineRange)
            numberOfLines += 1
        }
        return numberOfLines
    }
}
