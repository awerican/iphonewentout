//
//  UserDefaults.swift
//  WentOut
//
//  Created by Fure on 09.05.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import UIKit

typealias AuthHeader = [String: String]

final class AuthManager {
    
}

enum AuthVerificationState {
    case valid
    case invalid
    case unknown
}

final class LocalAuth {
    
    private init() { }
    
    private static let _aesKey = "aesKey"
    private static let _token = "token"
    private static let _userID = "userID"
    
//    static func authIsValid(callback: @escaping (_ state: AuthVerificationState)->()) {
//        guard let header = getHeader() else {
//            callback(.invalid)
//            return
//        }
//
//        webservice.load(auth: header, resource: ResList.verifyAuth()) { (_result, _error) in
//            guard let error = _error {
//                print("error =", error)
//                callback(.unknown)
//                return
//            }
//            guard let result = _result else {
//                print("unable to parse an object")
//                callback(.unknown)
//                return
//            }
//            switch result.data {
//            case .invalid:
//                callback(.invalid)
//            case .success:
//                callback(.valid)
//            }
//        }
//    }
    
    static var aesKey: String? {
        get {
            return UserDefaults.standard.string(forKey: _aesKey)
        }
    }
    
    static var token: String? {
        get {
            return UserDefaults.standard.string(forKey: _token)
        }
    }
    
    static var userID: String? {
        get {
            return UserDefaults.standard.string(forKey: _userID) ?? "for testing purposes"
        }
    }

    static func removeAll() {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
    }
    
    static func getHeader() -> AuthHeader? {
        
        guard let _aesKey = LocalAuth.aesKey, let _token = LocalAuth.token, let _userID = LocalAuth.userID else {
            return nil
        }
        
        let date = Int(Date().timeIntervalSince1970)
        let key = AesWrapper.encrypt(date.description, aesKey: AesKey.init(key: _aesKey))
        let token = _token
        let userID = _userID
        
        var header = [String: String]()

        header["date"] = date.description
        header["key"] = key
        header["token"] = token
        header["userid"] = userID
        
        return header
    }
    
    static func saveLoginInfo(aesKey: String, token: String, userID: String) {
        let standart = UserDefaults.standard
        standart.set(aesKey, forKey: _aesKey)
        standart.set(token, forKey: _token)
        standart.set(userID, forKey: _userID)
    }
}























