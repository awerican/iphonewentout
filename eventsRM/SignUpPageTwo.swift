//
//  SignUpPageTwo.swift
//  WentOut
//
//  Created by Fure on 08.05.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import Foundation
import UIKit

class SignUpPageTwo: UIViewController {
    
    var userID: String!
    var password: String!
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var nextButton: UIButton!
    
    let mainAppSegue = "mainAppSegue"
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        print("SignUpPageTwo loaded")
        print("userID =", userID)
    }
    
    @IBAction func sendAction(_ sender: UIButton) {
        
        guard let email = emailTextField.text else {
            showMessage(text: "Email is invalid", type: .error)
            return
        }
        
        if !EmailChecker.isValidEmail(email) {
            showMessage(text: "Email is invalid", type: .error)
            return
        }
        
        let param = RegistrationSecParameters.init(userID: userID, email: email)
        
        webservice.load(resource: ResList.regSec(param)) { (_result, _error) in
            if let data = _result?.data {
                switch data.status {
                case .success:
                    let param = LoginParameters.init(email: email, password: self.password.rsaEncrypt())
                    self.login(param: param)
//                    self.segue(email: email)
                case .isExisted:
                    self.showMessage(text: "Write another email", type: .error)
                //                    print("isExisted")
                case .failed:
                    self.showMessage(text: "Sorry, Error!", type: .error)
                    //                    print("failed")
                }
            } else {
                self.showMessage(text: "Sorry, Error!", type: .error)
                //                print("ERROR: could not init")
            }
            if let error = _error {
                print("REQ ERROR: ", error)
            }
        }
    }
}

extension SignUpPageTwo {
    private func setupViews() {
        setupMainView()
        
        nextButton.makeOval(cornerRadius: nextButton.frame.height / 5)
        
        navigationItem.title = "Sign Up"
    }
    
    private func setupMainView() {
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(tapViewAction))
        self.view.addGestureRecognizer(tapGesture)
        self.view.isUserInteractionEnabled = true
    }
    
    @objc func tapViewAction() {
        emailTextField.resignFirstResponder()
    }
    
//    private func segue(email: String) {
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let controller = storyboard.instantiateViewController(withIdentifier: "SignUpPageThree") as! SignUpPageThree
//        controller.userID = userID
//        controller.password = password
//        controller.email = email
//        self.navigationController?.pushViewController(controller, animated: true)
//    }
    
    private func login(param: LoginParameters) {
        webservice.load(resource: ResList.login(param)) { (_result, _error) in
            if let error = _error {
                print("REQ ERROR =", error)
                return
            }
            
            if let result = _result?.data {
                switch result.status {
                case .success:
                    if let key = result.aesKey, let token = result.token, let userID = result.userID {
                        LocalAuth.saveLoginInfo(aesKey: key, token: token, userID: userID)
                        // open an app
                        self.performSegue(withIdentifier: self.mainAppSegue, sender: nil)
                    } else {
                        print("something went wrong")
                    }
                case .incorrectEmail:
                    self.navigationController?.popToRootViewController(animated: true)
                    print("email incorrect")
                case .incorrectPass:
                    self.navigationController?.popToRootViewController(animated: true)
                    print("pass incorrect")
                }
            } else {
                self.navigationController?.popToRootViewController(animated: true)
                print("ERROR")
            }
        }
    }
}

