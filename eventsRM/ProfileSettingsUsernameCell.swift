//
//  ProfileSettingsUsernameCell.swift
//  WentOut
//
//  Created by Fure on 19.09.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

@objc enum MIEditProfileUsernameResponse: Int {
    case isAlreadyTaken
    case successfullyUpdated
    case smthWentWrong
}

enum MIEditProfileUsernameStatusImageStatus {
    case succeed, failed
    var color: UIColor {
        get {
            switch self {
            case .succeed:
                return UIColor.green.withAlphaComponent(0.75)
            case .failed:
                return UIColor.pink
            }
        }
    }
    var image: UIImage {
        get {
            switch self {
            case .succeed:
                return #imageLiteral(resourceName: "iconSuccess")
            case .failed:
                return #imageLiteral(resourceName: "iconFail")
            }
        }
    }
}

protocol ProfileSettingsUsernameProtocol {
    func usernameDoneButtonWasPressed(username: String)
}

class ProfileSettingsUsernameCell: UICollectionViewCell {

    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var statusImage: UIImageView!
    
    var oldUsername: String?
    
    var delegate: ProfileSettingsUsernameProtocol?
    
    var animator: UIViewPropertyAnimator?
    
    static var currentSize: CGSize {
        return CGSize.init(width: UIScreen.width, height: 11 + 19 + 11)
    }
    
    @IBOutlet weak var textField: UITextField!
    
    override func draw(_ rect: CGRect) {
        self.statusImage.alpha = 0.0
        self.activityIndicator.alpha = 0.0
    }
    
    func configCell(username: String) {
        textField.text = username
        textField.delegate = self
    }
    
    private func animate(for status: MIEditProfileUsernameStatusImageStatus) {
        if let animation = self.animator {
            if animation.isRunning {
                return
            }
        }
        self.statusImage.alpha = 1.0
        
        self.statusImage.image = status.image
        self.statusImage.setColor(status.color)
        
        self.animator = UIViewPropertyAnimator.init(duration: 0.5, curve: .easeIn) { [weak self] in
            self?.statusImage.alpha = 0.0
        }
        self.animator?.startAnimation(afterDelay: 0.5)
        self.animator?.addCompletion { (position) in
            self.animator = nil
        }
    }
}

extension ProfileSettingsUsernameCell {
    func loadingStarted() {
        self.activityIndicator.alpha = 1.0
        self.activityIndicator.startAnimating()
    }
    
    func loadingFinished() {
        self.activityIndicator.alpha = 0.0
        self.activityIndicator.startAnimating()
    }
    
    func receiveResponse(_ response: MIEditProfileUsernameResponse) {
        loadingFinished()
        switch response {
        case .isAlreadyTaken:
            print("is already taken")
            self.animate(for: .failed)
        case .smthWentWrong:
            print("something went wrong")
            self.animate(for: .failed)
//            self.textField.text = self.oldUsername
//            self.textField.resignFirstResponder()
        case .successfullyUpdated:
            print("success")
            self.animate(for: .succeed)
            self.textField.resignFirstResponder()
        }
    }
}

extension ProfileSettingsUsernameCell: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return UsernameChecker(oldUsername: self.oldUsername).firstValidation(textField.text ?? "", range: range, replacementString: string)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let newUsername = textField.text ?? ""
        
        let validation = UsernameChecker(oldUsername: self.oldUsername).secondValidation(newUsername)
        
        print("IM HERE")
        
        switch validation {
        case .usernameDoNotChanged:
            textField.resignFirstResponder()
            return true
        case .correct:
            self.delegate?.usernameDoneButtonWasPressed(username: newUsername)
            return false
        default:
            return false
        }
    }
}
