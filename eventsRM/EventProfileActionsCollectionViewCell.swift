//
//  EventProfileActionsCollectionViewCell.swift
//  WentOut
//
//  Created by Fure on 18.04.2019.
//  Copyright © 2019 uzuner. All rights reserved.
//

import UIKit

enum EventProfileActionsLeftAction {
    case go
    case leave
}

protocol EventProfileActionsDelegate: class {
    func leftAction(status: EventProfileActionsLeftAction, callback: @escaping (Bool)->())
    func invite()
    func addMemory()
}

class EventProfileActionsCollectionViewCell: UICollectionViewCell {
    
    static var currentSize: CGSize {
        return CGSize.init(width: UIScreen.main.bounds.width, height: 37 + 2 + 2 + 10)
    }
    
    @IBOutlet weak var buttonLeft: UIButton!
    
    @IBOutlet weak var buttonRight: UIButton!
    
    @IBOutlet weak var memoryButton: UIButton!
    
    let leaveTag = 346
    
    let goTag = 286
    
    weak var delegate: EventProfileActionsDelegate?
    
    @IBAction func plusMemoryAction(_ sender: UIButton) {
        delegate?.addMemory()
    }
    @IBAction func leftAction(_ sender: UIButton) {
        
        if sender.tag == goTag {
            self.delegate?.leftAction(status: .go, callback: { (success) in
                if success {
                    self.buttonLeft.layer.borderWidth = 1.0
                    self.buttonLeft.layer.borderColor = UIColor(hex: "E0E0E0").cgColor
                    self.buttonLeft.backgroundColor = .white
                    self.buttonLeft.tag = self.leaveTag
                    self.buttonLeft.setTitleColor(.black, for: .normal)
                    self.buttonLeft.setTitle("Leave",for: .normal)
                }
            })
        }
        
        if sender.tag == leaveTag {
            self.delegate?.leftAction(status: .leave, callback: { (success) in
                if success {
                    self.buttonLeft.layer.borderWidth = 1.0
                    self.buttonLeft.layer.borderColor = UIColor(hex: "1783FF").cgColor
                    self.buttonLeft.backgroundColor = UIColor.init(hex: "1783FF")
                    self.buttonLeft.tag = self.goTag
                    self.buttonLeft.setTitleColor(.white, for: .normal)
                    self.buttonLeft.setTitle("Go",for: .normal)
                }
            })
        }
    }
    
    @IBAction func rightAction(_ sender: UIButton) {
        delegate?.invite()
    }
    
    override func draw(_ rect: CGRect) {
        buttonLeft?.makeOval(cornerRadius: buttonLeft.frame.height / 4)
        buttonRight?.makeOval(cornerRadius: buttonRight.frame.height / 4)
        memoryButton?.makeOval(cornerRadius: memoryButton.frame.height / 4)
        buttonRight?.layer.borderWidth = 1.0
        buttonRight?.layer.borderColor = UIColor(hex: "E0E0E0").cgColor
        memoryButton?.layer.borderWidth = 1.0
        memoryButton?.layer.borderColor = UIColor(hex: "E0E0E0").cgColor
    }
    
    func configCell(model: EventProfileHeader) {
        if model.statusGo {
            self.buttonLeft.layer.borderWidth = 1.0
            self.buttonLeft.layer.borderColor = UIColor(hex: "E0E0E0").cgColor
            self.buttonLeft.backgroundColor = .white
            self.buttonLeft.setTitleColor(UIColor(hex: "424242"), for: .normal)
            self.buttonLeft.tag = leaveTag
            self.buttonLeft.setTitle("Leave",for: .normal)
        } else {
            self.buttonLeft.layer.borderWidth = 1.0
            self.buttonLeft.layer.borderColor = UIColor(hex: "1783FF").cgColor
            self.buttonLeft.backgroundColor = UIColor.init(hex: "1783FF")
            self.buttonLeft.setTitleColor(.white, for: .normal)
            self.buttonLeft.tag = goTag
            self.buttonLeft.setTitle("Go",for: .normal)
        }
        if model.eventType == .priv && model.statusGo == false {
            self.memoryButton?.removeFromSuperview()
            self.buttonRight?.removeFromSuperview()
        }
    }
    
//    func configCell(statusGo: Bool) {
//        if statusGo {
//            self.buttonLeft.layer.borderWidth = 1.0
//            self.buttonLeft.layer.borderColor = UIColor(hex: "E0E0E0").cgColor
//            self.buttonLeft.backgroundColor = .white
//            self.buttonLeft.setTitleColor(UIColor(hex: "424242"), for: .normal)
//            self.buttonLeft.tag = leaveTag
//            self.buttonLeft.setTitle("Leave",for: .normal)
//        } else {
//            self.buttonLeft.layer.borderWidth = 1.0
//            self.buttonLeft.layer.borderColor = UIColor(hex: "1783FF").cgColor
//            self.buttonLeft.backgroundColor = UIColor.init(hex: "1783FF")
//            self.buttonLeft.setTitleColor(.white, for: .normal)
//            self.buttonLeft.tag = goTag
//            self.buttonLeft.setTitle("Go",for: .normal)
//        }
//    }
}
